# HarpoonHpublicVendorconfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.id
type | Number | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.type
category | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.category
logo | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.logo
cover | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.cover
facebookPageId | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookPageId
facebookPageToken | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookPageToken
facebookEventEnabled | Number | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookEventEnabled
facebookEventUpdatedAt | Date | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookEventUpdatedAt
facebookEventCount | Number | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookEventCount
facebookFeedEnabled | Number | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookFeedEnabled
facebookFeedUpdatedAt | Date | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookFeedUpdatedAt
facebookFeedCount | Number | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.facebookFeedCount
stripePublishableKey | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.stripePublishableKey
stripeRefreshToken | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.stripeRefreshToken
stripeAccessToken | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.stripeAccessToken
feesEventTicket | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.feesEventTicket
stripeUserId | String | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.stripeUserId
vendorconfigId | Number | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.vendorconfigId
createdAt | Date | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.createdAt
updatedAt | Date | cannot | can | harpoonApi.HarpoonHpublicVendorconfig.updatedAt
