# DealGroupMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.DealGroup.id
product | Product | can | can | harpoonApi.DealGroup.product
