# CatalogCategoryProductMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
categoryId | Number | cannot | can | harpoonApi.CatalogCategoryProduct.categoryId
productId | Number | cannot | can | harpoonApi.CatalogCategoryProduct.productId
position | Number | cannot | can | harpoonApi.CatalogCategoryProduct.position
catalogProduct | object | cannot | can | Property catalogProduct of harpoonApi.CatalogCategoryProduct describes a relationship belongsTo harpoonApi.CatalogProduct
catalogCategory | object | cannot | can | Property catalogCategory of harpoonApi.CatalogCategoryProduct describes a relationship belongsTo harpoonApi.CatalogCategory
