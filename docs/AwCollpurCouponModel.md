# AwCollpurCouponModel
## Usage
```javascript
var vm = AwCollpurCouponModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.AwCollpurCoupon.id
dealId | Number | ko.observable(null) | harpoonApi.AwCollpurCoupon.dealId
purchaseId | Number | ko.observable(null) | harpoonApi.AwCollpurCoupon.purchaseId
couponCode | String | ko.observable(null) | harpoonApi.AwCollpurCoupon.couponCode
status | String | ko.observable(null) | harpoonApi.AwCollpurCoupon.status
couponDeliveryDatetime | Date | ko.observable(null) | harpoonApi.AwCollpurCoupon.couponDeliveryDatetime
couponDateUpdated | Date | ko.observable(null) | harpoonApi.AwCollpurCoupon.couponDateUpdated
redeemedAt | Date | ko.observable(null) | harpoonApi.AwCollpurCoupon.redeemedAt
redeemedByTerminal | String | ko.observable(null) | harpoonApi.AwCollpurCoupon.redeemedByTerminal
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AwCollpurCouponModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
