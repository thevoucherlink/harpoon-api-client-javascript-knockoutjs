# CoreWebsiteMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.CoreWebsite.id
code | String | cannot | can | harpoonApi.CoreWebsite.code
name | String | cannot | can | harpoonApi.CoreWebsite.name
sortOrder | Number | cannot | can | harpoonApi.CoreWebsite.sortOrder
defaultGroupId | Number | cannot | can | harpoonApi.CoreWebsite.defaultGroupId
isDefault | Number | cannot | can | harpoonApi.CoreWebsite.isDefault
