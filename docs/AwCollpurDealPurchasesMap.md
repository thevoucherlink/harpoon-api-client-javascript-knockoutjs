# AwCollpurDealPurchasesMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.id
dealId | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.dealId
orderId | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.orderId
orderItemId | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.orderItemId
qtyPurchased | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.qtyPurchased
qtyWithCoupons | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.qtyWithCoupons
customerName | String | cannot | can | harpoonApi.AwCollpurDealPurchases.customerName
customerId | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.customerId
purchaseDateTime | Date | cannot | can | harpoonApi.AwCollpurDealPurchases.purchaseDateTime
shippingAmount | String | cannot | can | harpoonApi.AwCollpurDealPurchases.shippingAmount
qtyOrdered | String | cannot | can | harpoonApi.AwCollpurDealPurchases.qtyOrdered
refundState | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.refundState
isSuccessedFlag | Number | cannot | can | harpoonApi.AwCollpurDealPurchases.isSuccessedFlag
awCollpurCoupon | AwCollpurCoupon | can | can | Property awCollpurCoupon of harpoonApi.AwCollpurDealPurchases describes a relationship hasOne harpoonApi.AwCollpurCoupon
