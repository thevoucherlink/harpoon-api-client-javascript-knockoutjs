# EmailAppCampaignDealDealEmailModel
## Usage
```javascript
var vm = EmailAppCampaignDealDealEmailModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
brandEmail | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.brandEmail
brandLogo | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.brandLogo
brandName | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.brandName
brandTel | number | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.brandTel
campaignBarcode | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignBarcode
campaignCode | number | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignCode
campaignDescription | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignDescription
campaignEndTime | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignEndTime
campaignImage | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignImage
campaignLocation | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignLocation
campaignName | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignName
campaignTermsAndConditions | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignTermsAndConditions
campaignUsage | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.campaignUsage
orderId | number | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.orderId
priceText | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.priceText
organiserName | string | ko.observable(null) | harpoonApi.EmailAppCampaignDealDealEmail.organiserName
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | EmailAppCampaignDealDealEmailModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
