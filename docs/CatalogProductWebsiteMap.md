# CatalogProductWebsiteMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
productId | Number | cannot | can | harpoonApi.CatalogProductWebsite.productId
websiteId | Number | cannot | can | harpoonApi.CatalogProductWebsite.websiteId
