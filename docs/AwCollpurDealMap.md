# AwCollpurDealMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwCollpurDeal.id
productId | Number | cannot | can | harpoonApi.AwCollpurDeal.productId
productName | String | cannot | can | harpoonApi.AwCollpurDeal.productName
storeIds | String | cannot | can | harpoonApi.AwCollpurDeal.storeIds
isActive | Number | cannot | can | harpoonApi.AwCollpurDeal.isActive
isSuccess | Number | cannot | can | harpoonApi.AwCollpurDeal.isSuccess
closeState | Number | cannot | can | harpoonApi.AwCollpurDeal.closeState
qtyToReachDeal | Number | cannot | can | harpoonApi.AwCollpurDeal.qtyToReachDeal
purchasesLeft | Number | cannot | can | harpoonApi.AwCollpurDeal.purchasesLeft
maximumAllowedPurchases | Number | cannot | can | harpoonApi.AwCollpurDeal.maximumAllowedPurchases
availableFrom | Date | cannot | can | harpoonApi.AwCollpurDeal.availableFrom
availableTo | Date | cannot | can | harpoonApi.AwCollpurDeal.availableTo
price | String | cannot | can | harpoonApi.AwCollpurDeal.price
autoClose | Number | cannot | can | harpoonApi.AwCollpurDeal.autoClose
name | String | cannot | can | harpoonApi.AwCollpurDeal.name
description | String | cannot | can | harpoonApi.AwCollpurDeal.description
fullDescription | String | cannot | can | harpoonApi.AwCollpurDeal.fullDescription
dealImage | String | cannot | can | harpoonApi.AwCollpurDeal.dealImage
isFeatured | Number | cannot | can | harpoonApi.AwCollpurDeal.isFeatured
enableCoupons | Number | cannot | can | harpoonApi.AwCollpurDeal.enableCoupons
couponPrefix | String | cannot | can | harpoonApi.AwCollpurDeal.couponPrefix
couponExpireAfterDays | Number | cannot | can | harpoonApi.AwCollpurDeal.couponExpireAfterDays
expiredFlag | Number | cannot | can | harpoonApi.AwCollpurDeal.expiredFlag
sentBeforeFlag | Number | cannot | can | harpoonApi.AwCollpurDeal.sentBeforeFlag
isSuccessedFlag | Number | cannot | can | harpoonApi.AwCollpurDeal.isSuccessedFlag
awCollpurDealPurchases | AwCollpurDealPurchases | can | can | Property awCollpurDealPurchases of harpoonApi.AwCollpurDeal describes a relationship hasMany harpoonApi.AwCollpurDealPurchases
