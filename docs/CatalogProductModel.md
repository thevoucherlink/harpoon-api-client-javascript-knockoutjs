# CatalogProductModel
## Usage
```javascript
var vm = CatalogProductModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.CatalogProduct.id
attributeSetId | Number | ko.observable(null) | harpoonApi.CatalogProduct.attributeSetId
typeId | String | ko.observable(null) | harpoonApi.CatalogProduct.typeId
actionText | String | ko.observable(null) | harpoonApi.CatalogProduct.actionText
agreementId | Number | ko.observable(null) | harpoonApi.CatalogProduct.agreementId
alias | String | ko.observable(null) | harpoonApi.CatalogProduct.alias
altLink | String | ko.observable(null) | harpoonApi.CatalogProduct.altLink
campaignId | String | ko.observable(null) | harpoonApi.CatalogProduct.campaignId
campaignLiveFrom | Date | ko.observable(null) | harpoonApi.CatalogProduct.campaignLiveFrom
campaignLiveTo | Date | ko.observable(null) | harpoonApi.CatalogProduct.campaignLiveTo
campaignShared | String | ko.observable(null) | harpoonApi.CatalogProduct.campaignShared
campaignStatus | String | ko.observable(null) | harpoonApi.CatalogProduct.campaignStatus
campaignType | String | ko.observable(null) | harpoonApi.CatalogProduct.campaignType
checkoutLink | String | ko.observable(null) | harpoonApi.CatalogProduct.checkoutLink
collectionNotes | String | ko.observable(null) | harpoonApi.CatalogProduct.collectionNotes
cost | String | ko.observable(null) | harpoonApi.CatalogProduct.cost
couponCodeText | String | ko.observable(null) | harpoonApi.CatalogProduct.couponCodeText
couponCodeType | String | ko.observable(null) | harpoonApi.CatalogProduct.couponCodeType
couponType | String | ko.observable(null) | harpoonApi.CatalogProduct.couponType
createdAt | Date | ko.observable(null) | harpoonApi.CatalogProduct.createdAt
creatorId | String | ko.observable(null) | harpoonApi.CatalogProduct.creatorId
description | String | ko.observable(null) | harpoonApi.CatalogProduct.description
externalId | String | ko.observable(null) | harpoonApi.CatalogProduct.externalId
facebookAttendingCount | String | ko.observable(null) | harpoonApi.CatalogProduct.facebookAttendingCount
facebookCategory | String | ko.observable(null) | harpoonApi.CatalogProduct.facebookCategory
facebookId | String | ko.observable(null) | harpoonApi.CatalogProduct.facebookId
facebookMaybeCount | String | ko.observable(null) | harpoonApi.CatalogProduct.facebookMaybeCount
facebookNode | String | ko.observable(null) | harpoonApi.CatalogProduct.facebookNode
facebookNoreplyCount | String | ko.observable(null) | harpoonApi.CatalogProduct.facebookNoreplyCount
facebookPlace | String | ko.observable(null) | harpoonApi.CatalogProduct.facebookPlace
facebookUpdatedTime | Date | ko.observable(null) | harpoonApi.CatalogProduct.facebookUpdatedTime
giftMessageAvailable | Number | ko.observable(null) | harpoonApi.CatalogProduct.giftMessageAvailable
hasOptions | Number | ko.observable(null) | harpoonApi.CatalogProduct.hasOptions
image | String | ko.observable(null) | harpoonApi.CatalogProduct.image
imageLabel | String | ko.observable(null) | harpoonApi.CatalogProduct.imageLabel
isRecurring | Number | ko.observable(null) | harpoonApi.CatalogProduct.isRecurring
linksExist | Number | ko.observable(null) | harpoonApi.CatalogProduct.linksExist
linksPurchasedSeparately | Number | ko.observable(null) | harpoonApi.CatalogProduct.linksPurchasedSeparately
linksTitle | String | ko.observable(null) | harpoonApi.CatalogProduct.linksTitle
locationLink | String | ko.observable(null) | harpoonApi.CatalogProduct.locationLink
msrp | String | ko.observable(null) | harpoonApi.CatalogProduct.msrp
msrpDisplayActualPriceType | String | ko.observable(null) | harpoonApi.CatalogProduct.msrpDisplayActualPriceType
msrpEnabled | Number | ko.observable(null) | harpoonApi.CatalogProduct.msrpEnabled
name | String | ko.observable(null) | harpoonApi.CatalogProduct.name
newsFromDate | Date | ko.observable(null) | harpoonApi.CatalogProduct.newsFromDate
newsToDate | Date | ko.observable(null) | harpoonApi.CatalogProduct.newsToDate
notificationBadge | String | ko.observable(null) | harpoonApi.CatalogProduct.notificationBadge
notificationInvisible | Number | ko.observable(null) | harpoonApi.CatalogProduct.notificationInvisible
notificationSilent | Number | ko.observable(null) | harpoonApi.CatalogProduct.notificationSilent
partnerId | String | ko.observable(null) | harpoonApi.CatalogProduct.partnerId
passedValidation | Number | ko.observable(null) | harpoonApi.CatalogProduct.passedValidation
price | String | ko.observable(null) | harpoonApi.CatalogProduct.price
priceText | String | ko.observable(null) | harpoonApi.CatalogProduct.priceText
priceType | Number | ko.observable(null) | harpoonApi.CatalogProduct.priceType
priceView | Number | ko.observable(null) | harpoonApi.CatalogProduct.priceView
pushwooshIds | String | ko.observable(null) | harpoonApi.CatalogProduct.pushwooshIds
pushwooshTokens | String | ko.observable(null) | harpoonApi.CatalogProduct.pushwooshTokens
recurringProfile | String | ko.observable(null) | harpoonApi.CatalogProduct.recurringProfile
redemptionType | String | ko.observable(null) | harpoonApi.CatalogProduct.redemptionType
requiredOptions | Number | ko.observable(null) | harpoonApi.CatalogProduct.requiredOptions
reviewHtml | String | ko.observable(null) | harpoonApi.CatalogProduct.reviewHtml
shipmentType | Number | ko.observable(null) | harpoonApi.CatalogProduct.shipmentType
shortDescription | String | ko.observable(null) | harpoonApi.CatalogProduct.shortDescription
sku | String | ko.observable(null) | harpoonApi.CatalogProduct.sku
skuType | Number | ko.observable(null) | harpoonApi.CatalogProduct.skuType
smallImage | String | ko.observable(null) | harpoonApi.CatalogProduct.smallImage
smallImageLabel | String | ko.observable(null) | harpoonApi.CatalogProduct.smallImageLabel
specialFromDate | Date | ko.observable(null) | harpoonApi.CatalogProduct.specialFromDate
specialPrice | String | ko.observable(null) | harpoonApi.CatalogProduct.specialPrice
specialToDate | Date | ko.observable(null) | harpoonApi.CatalogProduct.specialToDate
taxClassId | Number | ko.observable(null) | harpoonApi.CatalogProduct.taxClassId
thumbnail | String | ko.observable(null) | harpoonApi.CatalogProduct.thumbnail
thumbnailLabel | String | ko.observable(null) | harpoonApi.CatalogProduct.thumbnailLabel
udropshipCalculateRates | Number | ko.observable(null) | harpoonApi.CatalogProduct.udropshipCalculateRates
udropshipVendor | Number | ko.observable(null) | harpoonApi.CatalogProduct.udropshipVendor
udropshipVendorValue | String | ko.observable(null) | harpoonApi.CatalogProduct.udropshipVendorValue
udtiershipRates | String | ko.observable(null) | harpoonApi.CatalogProduct.udtiershipRates
udtiershipUseCustom | Number | ko.observable(null) | harpoonApi.CatalogProduct.udtiershipUseCustom
unlockTime | Number | ko.observable(null) | harpoonApi.CatalogProduct.unlockTime
updatedAt | Date | ko.observable(null) | harpoonApi.CatalogProduct.updatedAt
urlKey | String | ko.observable(null) | harpoonApi.CatalogProduct.urlKey
urlPath | String | ko.observable(null) | harpoonApi.CatalogProduct.urlPath
validationReport | String | ko.observable(null) | harpoonApi.CatalogProduct.validationReport
venueList | String | ko.observable(null) | harpoonApi.CatalogProduct.venueList
visibility | Number | ko.observable(null) | harpoonApi.CatalogProduct.visibility
visibleFrom | Date | ko.observable(null) | harpoonApi.CatalogProduct.visibleFrom
visibleTo | Date | ko.observable(null) | harpoonApi.CatalogProduct.visibleTo
weight | String | ko.observable(null) | harpoonApi.CatalogProduct.weight
weightType | Number | ko.observable(null) | harpoonApi.CatalogProduct.weightType
termsConditions | String | ko.observable(null) | harpoonApi.CatalogProduct.termsConditions
competitionQuestion | String | ko.observable(null) | harpoonApi.CatalogProduct.competitionQuestion
competitionAnswer | String | ko.observable(null) | harpoonApi.CatalogProduct.competitionAnswer
competitionWinnerType | String | ko.observable(null) | harpoonApi.CatalogProduct.competitionWinnerType
competitionMultijoinStatus | Boolean | ko.observable(null) | harpoonApi.CatalogProduct.competitionMultijoinStatus
competitionMultijoinReset | Date | ko.observable(null) | harpoonApi.CatalogProduct.competitionMultijoinReset
competitionMultijoinCooldown | String | ko.observable(null) | harpoonApi.CatalogProduct.competitionMultijoinCooldown
competitionWinnerEmail | Boolean | ko.observable(null) | harpoonApi.CatalogProduct.competitionWinnerEmail
competitionQuestionValidate | Boolean | ko.observable(null) | harpoonApi.CatalogProduct.competitionQuestionValidate
connectFacebookId | String | ko.observable(null) | harpoonApi.CatalogProduct.connectFacebookId
udropshipVendors | UdropshipVendor | ko.observableArray([]) | Property udropshipVendors of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.UdropshipVendor
productEventCompetition | AwEventbookingEvent | ko.observable(null) | Property productEventCompetition of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.AwEventbookingEvent
inventory | CatalogInventoryStockItem | ko.observable(null) | Property inventory of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.CatalogInventoryStockItem
catalogCategories | CatalogCategory | ko.observableArray([]) | Property catalogCategories of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.CatalogCategory
checkoutAgreement | object | ko.observable(null) | Property checkoutAgreement of harpoonApi.CatalogProduct describes a relationship belongsTo harpoonApi.CheckoutAgreement
awCollpurDeal | AwCollpurDeal | ko.observable(null) | Property awCollpurDeal of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.AwCollpurDeal
creator | object | ko.observable(null) | Property creator of harpoonApi.CatalogProduct describes a relationship belongsTo harpoonApi.UdropshipVendor
udropshipVendorProducts | UdropshipVendorProduct | ko.observableArray([]) | Property udropshipVendorProducts of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.UdropshipVendorProduct
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CatalogProductModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
