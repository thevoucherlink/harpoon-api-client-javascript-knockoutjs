# CouponPurchaseCodeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
type | string | cannot | can | harpoonApi.CouponPurchaseCode.type
text | string | cannot | can | harpoonApi.CouponPurchaseCode.text
image | string | cannot | can | harpoonApi.CouponPurchaseCode.image
