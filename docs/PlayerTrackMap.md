# PlayerTrackMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.PlayerTrack.id
file | string | cannot | can | Source file
type | string | cannot | can | Source type (used only in JS SDK)
label | string | cannot | can | Source label
default | boolean | cannot | can | If Source is the default Source
order | number | cannot | can | Sort order index
playlistItem | object | cannot | can | Property playlistItem of harpoonApi.PlayerTrack describes a relationship belongsTo harpoonApi.PlaylistItem
