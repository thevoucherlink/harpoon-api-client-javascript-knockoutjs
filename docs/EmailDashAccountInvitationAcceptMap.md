# EmailDashAccountInvitationAcceptMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
receiverName | string | cannot | can | harpoonApi.EmailDashAccountInvitationAccept.receiverName
reDirectLink | string | cannot | can | harpoonApi.EmailDashAccountInvitationAccept.reDirectLink
userName | string | cannot | can | harpoonApi.EmailDashAccountInvitationAccept.userName
