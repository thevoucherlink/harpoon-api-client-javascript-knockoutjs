# EmailDashAccountWelcomeNewUserMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
receiverFirstName | string | cannot | can | harpoonApi.EmailDashAccountWelcomeNewUser.receiverFirstName
unsubscribe | string | cannot | can | harpoonApi.EmailDashAccountWelcomeNewUser.unsubscribe
