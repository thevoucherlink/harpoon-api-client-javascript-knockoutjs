# HOAuthPermissionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appId | string | cannot | can | harpoonApi.HOAuthPermission.appId
userId | string | cannot | can | harpoonApi.HOAuthPermission.userId
issuedAt | date | cannot | can | harpoonApi.HOAuthPermission.issuedAt
expiresIn | number | cannot | can | harpoonApi.HOAuthPermission.expiresIn
expiredAt | date | cannot | can | harpoonApi.HOAuthPermission.expiredAt
scopes | string | can | can | harpoonApi.HOAuthPermission.scopes
application | object | cannot | can | Property application of harpoonApi.HOAuthPermission describes a relationship belongsTo harpoonApi.OAuthClientApplication
user | object | cannot | can | Property user of harpoonApi.HOAuthPermission describes a relationship belongsTo harpoonApi.User
