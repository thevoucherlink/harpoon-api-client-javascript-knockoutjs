# EmailDashCampaignFlowGoLiveMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.EmailDashCampaignFlowGoLive.appName
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowGoLive.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowGoLive.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowGoLive.reDirectLink
