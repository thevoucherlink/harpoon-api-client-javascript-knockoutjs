# HarpoonHpublicv12VendorAppCategoryMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.HarpoonHpublicv12VendorAppCategory.id
appId | Number | cannot | can | harpoonApi.HarpoonHpublicv12VendorAppCategory.appId
vendorId | Number | cannot | can | harpoonApi.HarpoonHpublicv12VendorAppCategory.vendorId
categoryId | Number | cannot | can | harpoonApi.HarpoonHpublicv12VendorAppCategory.categoryId
isPrimary | Number | cannot | can | harpoonApi.HarpoonHpublicv12VendorAppCategory.isPrimary
createdAt | Date | cannot | can | harpoonApi.HarpoonHpublicv12VendorAppCategory.createdAt
updatedAt | Date | cannot | can | harpoonApi.HarpoonHpublicv12VendorAppCategory.updatedAt
