# FacebookEventMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.FacebookEvent.id
node | string | cannot | can | harpoonApi.FacebookEvent.node
category | string | cannot | can | harpoonApi.FacebookEvent.category
updatedTime | string | cannot | can | harpoonApi.FacebookEvent.updatedTime
maybeCount | number | cannot | can | harpoonApi.FacebookEvent.maybeCount
noreplyCount | number | cannot | can | harpoonApi.FacebookEvent.noreplyCount
attendingCount | number | cannot | can | harpoonApi.FacebookEvent.attendingCount
place | object | cannot | can | harpoonApi.FacebookEvent.place
