# UdropshipVendorProductAssocMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
isUdmulti | Number | cannot | can | harpoonApi.UdropshipVendorProductAssoc.isUdmulti
id | number | cannot | can | harpoonApi.UdropshipVendorProductAssoc.id
isAttribute | Number | cannot | can | harpoonApi.UdropshipVendorProductAssoc.isAttribute
productId | Number | cannot | can | harpoonApi.UdropshipVendorProductAssoc.productId
vendorId | Number | cannot | can | harpoonApi.UdropshipVendorProductAssoc.vendorId
udropshipVendor | object | cannot | can | Property udropshipVendor of harpoonApi.UdropshipVendorProductAssoc describes a relationship belongsTo harpoonApi.UdropshipVendor
catalogProduct | object | cannot | can | Property catalogProduct of harpoonApi.UdropshipVendorProductAssoc describes a relationship belongsTo harpoonApi.CatalogProduct
