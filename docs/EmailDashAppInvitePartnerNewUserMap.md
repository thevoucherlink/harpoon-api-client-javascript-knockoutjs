# EmailDashAppInvitePartnerNewUserMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appList | string | cannot | can | harpoonApi.EmailDashAppInvitePartnerNewUser.appList
brandEmail | string | cannot | can | harpoonApi.EmailDashAppInvitePartnerNewUser.brandEmail
brandName | string | cannot | can | harpoonApi.EmailDashAppInvitePartnerNewUser.brandName
brandTel | number | cannot | can | harpoonApi.EmailDashAppInvitePartnerNewUser.brandTel
senderFirstName | string | cannot | can | harpoonApi.EmailDashAppInvitePartnerNewUser.senderFirstName
verificationLink | string | cannot | can | harpoonApi.EmailDashAppInvitePartnerNewUser.verificationLink
