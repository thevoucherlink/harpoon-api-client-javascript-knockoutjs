# NotificationFromMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
customer | Customer | can | can | harpoonApi.NotificationFrom.customer
brand | Brand | can | can | harpoonApi.NotificationFrom.brand
