# MemoryCacheMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.MemoryCache.id
key | string | cannot | can | harpoonApi.MemoryCache.key
value | object | cannot | can | harpoonApi.MemoryCache.value
