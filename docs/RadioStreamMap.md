# RadioStreamMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RadioStream.id
name | string | cannot | can | harpoonApi.RadioStream.name
description | string | cannot | can | harpoonApi.RadioStream.description
urlHQ | string | cannot | can | harpoonApi.RadioStream.urlHQ
urlLQ | string | cannot | can | harpoonApi.RadioStream.urlLQ
starts | date | cannot | can | When the Stream starts of being public
ends | date | cannot | can | When the Stream ceases of being public
metaDataUrl | string | cannot | can | harpoonApi.RadioStream.metaDataUrl
imgStream | string | cannot | can | harpoonApi.RadioStream.imgStream
sponsorTrack | string | cannot | can | Url of the sponsor MP3 to be played
tritonConfig | TritonConfig | can | can | harpoonApi.RadioStream.tritonConfig
radioShows | RadioShow | can | can | Property radioShows of harpoonApi.RadioStream describes a relationship hasMany harpoonApi.RadioShow
