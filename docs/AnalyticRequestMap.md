# AnalyticRequestMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.AnalyticRequest.id
requests | object | can | can | harpoonApi.AnalyticRequest.requests
scope | string | cannot | can | harpoonApi.AnalyticRequest.scope
