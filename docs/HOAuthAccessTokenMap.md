# HOAuthAccessTokenMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.HOAuthAccessToken.id
appId | string | cannot | can | harpoonApi.HOAuthAccessToken.appId
userId | string | cannot | can | harpoonApi.HOAuthAccessToken.userId
issuedAt | date | cannot | can | harpoonApi.HOAuthAccessToken.issuedAt
expiresIn | number | cannot | can | harpoonApi.HOAuthAccessToken.expiresIn
expiredAt | date | cannot | can | harpoonApi.HOAuthAccessToken.expiredAt
scopes | string | can | can | harpoonApi.HOAuthAccessToken.scopes
parameters | object | can | can | harpoonApi.HOAuthAccessToken.parameters
authorizationCode | string | cannot | can | harpoonApi.HOAuthAccessToken.authorizationCode
refreshToken | string | cannot | can | harpoonApi.HOAuthAccessToken.refreshToken
tokenType | string | cannot | can | harpoonApi.HOAuthAccessToken.tokenType
hash | string | cannot | can | harpoonApi.HOAuthAccessToken.hash
application | object | cannot | can | Property application of harpoonApi.HOAuthAccessToken describes a relationship belongsTo harpoonApi.OAuthClientApplication
user | object | cannot | can | Property user of harpoonApi.HOAuthAccessToken describes a relationship belongsTo harpoonApi.User
