# RadioPlaySessionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RadioPlaySession.id
sessionTime | number | cannot | can | harpoonApi.RadioPlaySession.sessionTime
playUpdateTime | date | cannot | can | harpoonApi.RadioPlaySession.playUpdateTime
radioStreamId | number | cannot | can | harpoonApi.RadioPlaySession.radioStreamId
radioShowId | number | cannot | can | harpoonApi.RadioPlaySession.radioShowId
playEndTime | date | cannot | can | harpoonApi.RadioPlaySession.playEndTime
playStartTime | date | cannot | can | harpoonApi.RadioPlaySession.playStartTime
customerId | number | cannot | can | harpoonApi.RadioPlaySession.customerId
