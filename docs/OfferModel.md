# OfferModel
## Usage
```javascript
var vm = OfferModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.Offer.id
name | string | ko.observable(null) | harpoonApi.Offer.name
description | string | ko.observable(null) | harpoonApi.Offer.description
cover | string | ko.observable(null) | harpoonApi.Offer.cover
coverUpload | MagentoImageUpload | new MagentoImageUploadModel() | harpoonApi.Offer.coverUpload
campaignType | Category | new CategoryModel() | harpoonApi.Offer.campaignType
category | Category | new CategoryModel() | harpoonApi.Offer.category
topic | Category | new CategoryModel() | harpoonApi.Offer.topic
alias | string | ko.observable(null) | harpoonApi.Offer.alias
from | date | ko.observable(null) | harpoonApi.Offer.from
to | date | ko.observable(null) | harpoonApi.Offer.to
baseCurrency | string | ko.observable(null) | harpoonApi.Offer.baseCurrency
priceText | string | ko.observable(null) | harpoonApi.Offer.priceText
bannerText | string | ko.observable(null) | harpoonApi.Offer.bannerText
checkoutLink | string | ko.observable(null) | harpoonApi.Offer.checkoutLink
nearestVenue | Venue | new VenueModel() | harpoonApi.Offer.nearestVenue
actionText | string | ko.observable(null) | harpoonApi.Offer.actionText
status | string | ko.observable(null) | harpoonApi.Offer.status
collectionNotes | string | ko.observable(null) | harpoonApi.Offer.collectionNotes
termsConditions | string | ko.observable(null) | harpoonApi.Offer.termsConditions
locationLink | string | ko.observable(null) | harpoonApi.Offer.locationLink
altLink | string | ko.observable(null) | harpoonApi.Offer.altLink
redemptionType | string | ko.observable(null) | harpoonApi.Offer.redemptionType
brand | Brand | new BrandModel() | harpoonApi.Offer.brand
closestPurchase | OfferClosestPurchase | new OfferClosestPurchaseModel() | harpoonApi.Offer.closestPurchase
isFeatured | boolean | ko.observable(null) | harpoonApi.Offer.isFeatured
qtyPerOrder | number | ko.observable(null) | harpoonApi.Offer.qtyPerOrder
shareLink | string | ko.observable(null) | harpoonApi.Offer.shareLink
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | OfferModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
