# UdropshipVendorProductMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.UdropshipVendorProduct.id
vendorId | Number | cannot | can | harpoonApi.UdropshipVendorProduct.vendorId
productId | Number | cannot | can | harpoonApi.UdropshipVendorProduct.productId
priority | Number | cannot | can | harpoonApi.UdropshipVendorProduct.priority
carrierCode | String | cannot | can | harpoonApi.UdropshipVendorProduct.carrierCode
vendorSku | String | cannot | can | harpoonApi.UdropshipVendorProduct.vendorSku
vendorCost | String | cannot | can | harpoonApi.UdropshipVendorProduct.vendorCost
stockQty | String | cannot | can | harpoonApi.UdropshipVendorProduct.stockQty
backorders | Number | cannot | can | harpoonApi.UdropshipVendorProduct.backorders
shippingPrice | String | cannot | can | harpoonApi.UdropshipVendorProduct.shippingPrice
status | Number | cannot | can | harpoonApi.UdropshipVendorProduct.status
reservedQty | String | cannot | can | harpoonApi.UdropshipVendorProduct.reservedQty
availState | String | cannot | can | harpoonApi.UdropshipVendorProduct.availState
availDate | Date | cannot | can | harpoonApi.UdropshipVendorProduct.availDate
relationshipStatus | String | cannot | can | harpoonApi.UdropshipVendorProduct.relationshipStatus
udropshipVendor | object | cannot | can | Property udropshipVendor of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.UdropshipVendor
catalogProduct | object | cannot | can | Property catalogProduct of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.CatalogProduct
