# MagentoImageUploadMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
file | string | cannot | can | harpoonApi.MagentoImageUpload.file
contentType | string | cannot | can | harpoonApi.MagentoImageUpload.contentType
