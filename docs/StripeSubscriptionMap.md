# StripeSubscriptionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
applicationFeePercent | number | cannot | can | harpoonApi.StripeSubscription.applicationFeePercent
cancelAtPeriodEnd | boolean | cannot | can | harpoonApi.StripeSubscription.cancelAtPeriodEnd
canceledAt | number | cannot | can | harpoonApi.StripeSubscription.canceledAt
currentPeriodEnd | number | cannot | can | harpoonApi.StripeSubscription.currentPeriodEnd
currentPeriodStart | number | cannot | can | harpoonApi.StripeSubscription.currentPeriodStart
metadata | string | cannot | can | harpoonApi.StripeSubscription.metadata
quantity | number | cannot | can | harpoonApi.StripeSubscription.quantity
start | number | cannot | can | harpoonApi.StripeSubscription.start
status | string | cannot | can | harpoonApi.StripeSubscription.status
taxPercent | number | cannot | can | harpoonApi.StripeSubscription.taxPercent
trialEnd | number | cannot | can | harpoonApi.StripeSubscription.trialEnd
trialStart | number | cannot | can | harpoonApi.StripeSubscription.trialStart
customer | string | cannot | can | harpoonApi.StripeSubscription.customer
discount | StripeDiscount | can | can | harpoonApi.StripeSubscription.discount
plan | StripePlan | can | can | harpoonApi.StripeSubscription.plan
object | string | cannot | can | harpoonApi.StripeSubscription.object
endedAt | number | cannot | can | harpoonApi.StripeSubscription.endedAt
