# TwitterConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.TwitterConfig.id
apiKey | string | cannot | can | harpoonApi.TwitterConfig.apiKey
apiSecret | string | cannot | can | harpoonApi.TwitterConfig.apiSecret
