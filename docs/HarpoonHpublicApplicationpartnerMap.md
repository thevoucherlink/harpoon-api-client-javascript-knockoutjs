# HarpoonHpublicApplicationpartnerMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.id
applicationId | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.applicationId
brandId | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.brandId
status | String | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.status
invitedEmail | String | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.invitedEmail
invitedAt | Date | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.invitedAt
acceptedAt | Date | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.acceptedAt
createdAt | Date | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.createdAt
updatedAt | Date | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.updatedAt
configList | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configList
configFeed | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configFeed
configNeedFollow | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configNeedFollow
configAcceptEvent | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptEvent
configAcceptCoupon | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptCoupon
configAcceptDealsimple | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptDealsimple
configAcceptDealgroup | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptDealgroup
configAcceptNotificationpush | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptNotificationpush
configAcceptNotificationbeacon | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptNotificationbeacon
isOwner | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.isOwner
configApproveEvent | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configApproveEvent
configApproveCoupon | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configApproveCoupon
configApproveDealsimple | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configApproveDealsimple
configApproveDealgroup | Number | cannot | can | harpoonApi.HarpoonHpublicApplicationpartner.configApproveDealgroup
udropshipVendor | object | cannot | can | Property udropshipVendor of harpoonApi.HarpoonHpublicApplicationpartner describes a relationship belongsTo harpoonApi.UdropshipVendor
