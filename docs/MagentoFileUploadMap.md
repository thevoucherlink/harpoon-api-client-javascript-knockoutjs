# MagentoFileUploadMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
file | string | cannot | can | File contents encoded as base64
contentType | string | cannot | can | The file's encode type e.g application/json
name | string | cannot | can | the file name including the extension
