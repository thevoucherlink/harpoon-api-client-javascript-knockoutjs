# EmailAppCampaignEventReminderMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
campaignEnd | string | cannot | can | harpoonApi.EmailAppCampaignEventReminder.campaignEnd
campaignEndTime | string | cannot | can | harpoonApi.EmailAppCampaignEventReminder.campaignEndTime
campaignImage | string | cannot | can | harpoonApi.EmailAppCampaignEventReminder.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignEventReminder.campaignName
campaignStart | string | cannot | can | harpoonApi.EmailAppCampaignEventReminder.campaignStart
campaignStartTime | string | cannot | can | harpoonApi.EmailAppCampaignEventReminder.campaignStartTime
userName | string | cannot | can | harpoonApi.EmailAppCampaignEventReminder.userName
reminderDays | number | cannot | can | harpoonApi.EmailAppCampaignEventReminder.reminderDays
