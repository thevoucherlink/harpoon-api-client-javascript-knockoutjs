# BranchConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.BranchConfig.id
branchLiveKey | string | cannot | can | harpoonApi.BranchConfig.branchLiveKey
branchTestKey | string | cannot | can | harpoonApi.BranchConfig.branchTestKey
branchLiveUrl | string | cannot | can | harpoonApi.BranchConfig.branchLiveUrl
branchTestUrl | string | cannot | can | harpoonApi.BranchConfig.branchTestUrl
