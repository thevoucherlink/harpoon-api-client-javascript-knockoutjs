# CompetitionWinnerMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CompetitionWinner.id
name | string | cannot | can | harpoonApi.CompetitionWinner.name
position | string | cannot | can | harpoonApi.CompetitionWinner.position
chosenAt | date | cannot | can | harpoonApi.CompetitionWinner.chosenAt
