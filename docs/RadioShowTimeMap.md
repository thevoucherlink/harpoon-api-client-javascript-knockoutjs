# RadioShowTimeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RadioShowTime.id
startTime | string | cannot | can | Time when the show starts, format HHmm
endTime | string | cannot | can | Time when the show ends, format HHmm
weekday | number | cannot | can | Day of the week when the show is live, 1 = Monday / 7 = Sunday
radioShow | object | cannot | can | Property radioShow of harpoonApi.RadioShowTime describes a relationship belongsTo harpoonApi.RadioShow
