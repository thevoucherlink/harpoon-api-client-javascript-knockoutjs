# EmailAppCampaignEventEventCancelledMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.appName
brandEmail | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.brandLogo
brandName | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.brandName
brandTel | number | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.brandTel
campaignImage | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.campaignImage
campaignLocation | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.campaignLocation
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.campaignName
campaignStart | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.campaignStart
userName | string | cannot | can | harpoonApi.EmailAppCampaignEventEventCancelled.userName
