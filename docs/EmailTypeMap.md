# EmailTypeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
template1 | string | cannot | can | harpoonApi.EmailType.template1
template2 | string | cannot | can | harpoonApi.EmailType.template2
template3 | string | cannot | can | harpoonApi.EmailType.template3
template4 | string | cannot | can | harpoonApi.EmailType.template4
template5 | string | cannot | can | harpoonApi.EmailType.template5
template6 | string | cannot | can | harpoonApi.EmailType.template6
template7 | string | cannot | can | harpoonApi.EmailType.template7
