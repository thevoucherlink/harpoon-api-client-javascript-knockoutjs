# GeoLocationMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
latitude | number | cannot | can | harpoonApi.GeoLocation.latitude
longitude | number | cannot | can | harpoonApi.GeoLocation.longitude
