# EmailDashCampaignInvitePartnerExistingMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerExisting.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerExisting.brandLogo
brandName | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerExisting.brandName
brandTel | number | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerExisting.brandTel
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerExisting.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerExisting.reDirectLink
