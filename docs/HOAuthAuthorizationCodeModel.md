# HOAuthAuthorizationCodeModel
## Usage
```javascript
var vm = HOAuthAuthorizationCodeModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.id
appId | string | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.appId
userId | string | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.userId
issuedAt | date | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.issuedAt
expiresIn | number | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.expiresIn
expiredAt | date | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.expiredAt
scopes | string | ko.observableArray([]) | harpoonApi.HOAuthAuthorizationCode.scopes
parameters | object | ko.observableArray([]) | harpoonApi.HOAuthAuthorizationCode.parameters
used | boolean | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.used
redirectURI | string | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.redirectURI
hash | string | ko.observable(null) | harpoonApi.HOAuthAuthorizationCode.hash
application | object | ko.observable(null) | Property application of harpoonApi.HOAuthAuthorizationCode describes a relationship belongsTo harpoonApi.OAuthClientApplication
user | object | ko.observable(null) | Property user of harpoonApi.HOAuthAuthorizationCode describes a relationship belongsTo harpoonApi.User
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | HOAuthAuthorizationCodeModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
