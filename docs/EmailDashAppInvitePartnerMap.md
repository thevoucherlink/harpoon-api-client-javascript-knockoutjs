# EmailDashAppInvitePartnerMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appList | string | cannot | can | harpoonApi.EmailDashAppInvitePartner.appList
senderFirstName | string | cannot | can | harpoonApi.EmailDashAppInvitePartner.senderFirstName
verificationLink | string | cannot | can | harpoonApi.EmailDashAppInvitePartner.verificationLink
