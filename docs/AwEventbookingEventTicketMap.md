# AwEventbookingEventTicketMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwEventbookingEventTicket.id
eventId | Number | cannot | can | harpoonApi.AwEventbookingEventTicket.eventId
price | String | cannot | can | harpoonApi.AwEventbookingEventTicket.price
priceType | String | cannot | can | harpoonApi.AwEventbookingEventTicket.priceType
sku | String | cannot | can | harpoonApi.AwEventbookingEventTicket.sku
qty | Number | cannot | can | harpoonApi.AwEventbookingEventTicket.qty
codeprefix | String | cannot | can | harpoonApi.AwEventbookingEventTicket.codeprefix
fee | String | cannot | can | harpoonApi.AwEventbookingEventTicket.fee
isPassOnFee | Number | cannot | can | harpoonApi.AwEventbookingEventTicket.isPassOnFee
chance | Number | cannot | can | harpoonApi.AwEventbookingEventTicket.chance
attributes | AwEventbookingEventTicketAttribute | can | can | Property attributes of harpoonApi.AwEventbookingEventTicket describes a relationship hasMany harpoonApi.AwEventbookingEventTicketAttribute
tickets | AwEventbookingTicket | can | can | Property tickets of harpoonApi.AwEventbookingEventTicket describes a relationship hasMany harpoonApi.AwEventbookingTicket
awEventbookingTicket | AwEventbookingTicket | can | can | Property awEventbookingTicket of harpoonApi.AwEventbookingEventTicket describes a relationship hasMany harpoonApi.AwEventbookingTicket
