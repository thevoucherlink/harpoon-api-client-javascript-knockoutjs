# CompetitionPurchaseMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CompetitionPurchase.id
orderId | number | cannot | can | harpoonApi.CompetitionPurchase.orderId
name | string | cannot | can | harpoonApi.CompetitionPurchase.name
competition | Competition | can | can | harpoonApi.CompetitionPurchase.competition
redemptionType | string | cannot | can | harpoonApi.CompetitionPurchase.redemptionType
unlockTime | number | cannot | can | harpoonApi.CompetitionPurchase.unlockTime
code | string | cannot | can | harpoonApi.CompetitionPurchase.code
qrcode | string | cannot | can | harpoonApi.CompetitionPurchase.qrcode
isAvailable | boolean | cannot | can | harpoonApi.CompetitionPurchase.isAvailable
status | string | cannot | can | harpoonApi.CompetitionPurchase.status
createdAt | date | cannot | can | harpoonApi.CompetitionPurchase.createdAt
expiredAt | date | cannot | can | harpoonApi.CompetitionPurchase.expiredAt
redeemedAt | date | cannot | can | harpoonApi.CompetitionPurchase.redeemedAt
redeemedByTerminal | string | cannot | can | harpoonApi.CompetitionPurchase.redeemedByTerminal
basePrice | number | cannot | can | harpoonApi.CompetitionPurchase.basePrice
ticketPrice | number | cannot | can | harpoonApi.CompetitionPurchase.ticketPrice
currency | string | cannot | can | harpoonApi.CompetitionPurchase.currency
attendee | Customer | can | can | harpoonApi.CompetitionPurchase.attendee
chanceCount | number | cannot | can | harpoonApi.CompetitionPurchase.chanceCount
winner | CompetitionWinner | can | can | harpoonApi.CompetitionPurchase.winner
