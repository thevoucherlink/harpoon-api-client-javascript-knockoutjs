# AwEventbookingOrderHistoryMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwEventbookingOrderHistory.id
eventTicketId | Number | cannot | can | harpoonApi.AwEventbookingOrderHistory.eventTicketId
orderItemId | Number | cannot | can | harpoonApi.AwEventbookingOrderHistory.orderItemId
