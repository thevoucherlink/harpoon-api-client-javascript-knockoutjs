# FormRowMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
metadataKey | string | cannot | can | harpoonApi.FormRow.metadataKey
type | string | cannot | can | harpoonApi.FormRow.type
title | string | cannot | can | harpoonApi.FormRow.title
defaultValue | string | cannot | can | harpoonApi.FormRow.defaultValue
placeholder | string | cannot | can | harpoonApi.FormRow.placeholder
hidden | boolean | cannot | can | harpoonApi.FormRow.hidden
required | boolean | cannot | can | harpoonApi.FormRow.required
validationRegExp | string | cannot | can | harpoonApi.FormRow.validationRegExp
validationMessage | string | cannot | can | harpoonApi.FormRow.validationMessage
selectorOptions | FormSelectorOption | can | can | harpoonApi.FormRow.selectorOptions
