# AuthTokenMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
accessToken | string | cannot | can | harpoonApi.AuthToken.accessToken
expiresIn | number | cannot | can | harpoonApi.AuthToken.expiresIn
scope | string | cannot | can | harpoonApi.AuthToken.scope
refreshToken | string | cannot | can | harpoonApi.AuthToken.refreshToken
tokenType | string | cannot | can | harpoonApi.AuthToken.tokenType
kid | string | cannot | can | harpoonApi.AuthToken.kid
macAlgorithm | string | cannot | can | harpoonApi.AuthToken.macAlgorithm
macKey | string | cannot | can | harpoonApi.AuthToken.macKey
