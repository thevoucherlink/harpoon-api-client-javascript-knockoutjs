# EmailAppCampaignEventBuyTicketTheTicketMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
campaignBarcode | string | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignBarcode
campaignEndTime | string | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignEndTime
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignName
campaignOrderDate | string | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignOrderDate
campaignStart | string | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignStart
ticketType | string | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.ticketType
userName | string | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.userName
orderId | number | cannot | can | harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.orderId
