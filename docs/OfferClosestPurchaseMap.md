# OfferClosestPurchaseMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
expiredAt | date | cannot | can | harpoonApi.OfferClosestPurchase.expiredAt
purchasedAt | date | cannot | can | harpoonApi.OfferClosestPurchase.purchasedAt
