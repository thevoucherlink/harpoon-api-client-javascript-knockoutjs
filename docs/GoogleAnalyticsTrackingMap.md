# GoogleAnalyticsTrackingMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
mobileTrackingId | string | cannot | can | harpoonApi.GoogleAnalyticsTracking.mobileTrackingId
webTrackingId | string | cannot | can | harpoonApi.GoogleAnalyticsTracking.webTrackingId
