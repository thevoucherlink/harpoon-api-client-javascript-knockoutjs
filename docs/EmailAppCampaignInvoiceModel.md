# EmailAppCampaignInvoiceModel
## Usage
```javascript
var vm = EmailAppCampaignInvoiceModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
appName | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.appName
billingAddressLineOne | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.billingAddressLineOne
billingAddressLineTwo | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.billingAddressLineTwo
billingAddressLineThree | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.billingAddressLineThree
brandEmail | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.brandEmail
campaignName | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.campaignName
campaignOrderDate | date | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.campaignOrderDate
campaignPayBy | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.campaignPayBy
cardholderName | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.cardholderName
orderId | number | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.orderId
price | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.price
quantity | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.quantity
ticketType | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.ticketType
totalPrice | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.totalPrice
userName | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.userName
sku | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.sku
tax | string | ko.observable(null) | harpoonApi.EmailAppCampaignInvoice.tax
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | EmailAppCampaignInvoiceModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
