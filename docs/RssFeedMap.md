# RssFeedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RssFeed.id
category | string | cannot | can | harpoonApi.RssFeed.category
subCategory | string | cannot | can | harpoonApi.RssFeed.subCategory
url | string | cannot | can | harpoonApi.RssFeed.url
name | string | cannot | can | harpoonApi.RssFeed.name
sortOrder | number | cannot | can | harpoonApi.RssFeed.sortOrder
styleCss | string | cannot | can | harpoonApi.RssFeed.styleCss
noAds | boolean | cannot | can | harpoonApi.RssFeed.noAds
adMRectVisible | boolean | cannot | can | harpoonApi.RssFeed.adMRectVisible
adMRectPosition | number | cannot | can | harpoonApi.RssFeed.adMRectPosition
rssFeedGroup | object | cannot | can | Property rssFeedGroup of harpoonApi.RssFeed describes a relationship belongsTo harpoonApi.RssFeedGroup
