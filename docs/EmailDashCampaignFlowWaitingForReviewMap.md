# EmailDashCampaignFlowWaitingForReviewMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailDashCampaignFlowWaitingForReview.brandEmail
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowWaitingForReview.brandName
brandTel | number | cannot | can | harpoonApi.EmailDashCampaignFlowWaitingForReview.brandTel
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowWaitingForReview.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowWaitingForReview.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowWaitingForReview.reDirectLink
