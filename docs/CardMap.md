# CardMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Card.id
name | string | cannot | can | harpoonApi.Card.name
type | string | cannot | can | harpoonApi.Card.type
funding | string | cannot | can | harpoonApi.Card.funding
lastDigits | string | cannot | can | harpoonApi.Card.lastDigits
exMonth | string | cannot | can | harpoonApi.Card.exMonth
exYear | string | cannot | can | harpoonApi.Card.exYear
cardholderName | string | cannot | can | harpoonApi.Card.cardholderName
