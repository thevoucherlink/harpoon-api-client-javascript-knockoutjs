# UrbanAirshipConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
developmentAppKey | string | cannot | can | harpoonApi.UrbanAirshipConfig.developmentAppKey
developmentAppSecret | string | cannot | can | harpoonApi.UrbanAirshipConfig.developmentAppSecret
developmentLogLevel | string | cannot | can | harpoonApi.UrbanAirshipConfig.developmentLogLevel
productionAppKey | string | cannot | can | harpoonApi.UrbanAirshipConfig.productionAppKey
productionAppSecret | string | cannot | can | harpoonApi.UrbanAirshipConfig.productionAppSecret
productionLogLevel | string | cannot | can | harpoonApi.UrbanAirshipConfig.productionLogLevel
inProduction | boolean | cannot | can | harpoonApi.UrbanAirshipConfig.inProduction
