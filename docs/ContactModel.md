# ContactModel
## Usage
```javascript
var vm = ContactModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.Contact.id
email | string | ko.observable(null) | Email address like hello@harpoonconnect.com
website | string | ko.observable(null) | URL like http://harpoonconnect.com or https://www.harpoonconnect.com
phone | string | ko.observable(null) | Telephone number like 08123456789
address | Address | new AddressModel() | harpoonApi.Contact.address
twitter | string | ko.observable(null) | URL starting with https://www.twitter.com/ followed by your username or id
facebook | string | ko.observable(null) | URL starting with https://www.facebook.com/ followed by your username or id
whatsapp | string | ko.observable(null) | Telephone number including country code like: +35381234567890
snapchat | string | ko.observable(null) | SnapChat username
googlePlus | string | ko.observable(null) | URL starting with https://plus.google.com/+ followed by your username or id
linkedIn | string | ko.observable(null) | LinkedIn profile URL
hashtag | string | ko.observable(null) | A string starting with # and followed by alphanumeric characters (both lower and upper case)
text | string | ko.observable(null) | harpoonApi.Contact.text
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | ContactModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
