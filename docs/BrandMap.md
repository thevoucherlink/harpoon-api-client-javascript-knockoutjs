# BrandMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Brand.id
name | string | cannot | can | harpoonApi.Brand.name
logo | string | cannot | can | harpoonApi.Brand.logo
logoUpload | MagentoImageUpload | can | can | harpoonApi.Brand.logoUpload
cover | string | cannot | can | harpoonApi.Brand.cover
coverUpload | MagentoImageUpload | can | can | harpoonApi.Brand.coverUpload
description | string | cannot | can | harpoonApi.Brand.description
email | string | cannot | can | harpoonApi.Brand.email
phone | string | cannot | can | harpoonApi.Brand.phone
address | string | cannot | can | harpoonApi.Brand.address
managerName | string | cannot | can | harpoonApi.Brand.managerName
followerCount | number | cannot | can | harpoonApi.Brand.followerCount
isFollowed | boolean | cannot | can | harpoonApi.Brand.isFollowed
category | Category | can | can | harpoonApi.Brand.category
categories | Category | can | can | harpoonApi.Brand.categories
website | string | cannot | can | harpoonApi.Brand.website
