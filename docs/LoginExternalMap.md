# LoginExternalMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
apiKey | string | cannot | can | harpoonApi.LoginExternal.apiKey
version | string | cannot | can | harpoonApi.LoginExternal.version
requestUrl | string | cannot | can | harpoonApi.LoginExternal.requestUrl
authorizeUrl | string | cannot | can | harpoonApi.LoginExternal.authorizeUrl
accessUrl | string | cannot | can | harpoonApi.LoginExternal.accessUrl
