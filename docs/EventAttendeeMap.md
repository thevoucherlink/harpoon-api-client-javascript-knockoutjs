# EventAttendeeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.EventAttendee.id
joinedAt | date | cannot | can | harpoonApi.EventAttendee.joinedAt
