# MenuItemMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.MenuItem.id
name | string | cannot | can | harpoonApi.MenuItem.name
url | string | cannot | can | harpoonApi.MenuItem.url
image | string | cannot | can | harpoonApi.MenuItem.image
visibility | boolean | cannot | can | harpoonApi.MenuItem.visibility
sortOrder | number | cannot | can | harpoonApi.MenuItem.sortOrder
pageAction | boolean | cannot | can | harpoonApi.MenuItem.pageAction
pageActionIcon | string | cannot | can | harpoonApi.MenuItem.pageActionIcon
pageActionLink | string | cannot | can | harpoonApi.MenuItem.pageActionLink
styleCss | string | cannot | can | harpoonApi.MenuItem.styleCss
featureRadioDontShowOnCamera | boolean | cannot | can | harpoonApi.MenuItem.featureRadioDontShowOnCamera
