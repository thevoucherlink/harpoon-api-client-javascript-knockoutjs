# HarpoonHpublicApplicationpartnerModel
## Usage
```javascript
var vm = HarpoonHpublicApplicationpartnerModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.id
applicationId | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.applicationId
brandId | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.brandId
status | String | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.status
invitedEmail | String | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.invitedEmail
invitedAt | Date | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.invitedAt
acceptedAt | Date | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.acceptedAt
createdAt | Date | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.createdAt
updatedAt | Date | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.updatedAt
configList | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configList
configFeed | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configFeed
configNeedFollow | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configNeedFollow
configAcceptEvent | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptEvent
configAcceptCoupon | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptCoupon
configAcceptDealsimple | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptDealsimple
configAcceptDealgroup | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptDealgroup
configAcceptNotificationpush | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptNotificationpush
configAcceptNotificationbeacon | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configAcceptNotificationbeacon
isOwner | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.isOwner
configApproveEvent | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configApproveEvent
configApproveCoupon | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configApproveCoupon
configApproveDealsimple | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configApproveDealsimple
configApproveDealgroup | Number | ko.observable(null) | harpoonApi.HarpoonHpublicApplicationpartner.configApproveDealgroup
udropshipVendor | object | ko.observable(null) | Property udropshipVendor of harpoonApi.HarpoonHpublicApplicationpartner describes a relationship belongsTo harpoonApi.UdropshipVendor
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | HarpoonHpublicApplicationpartnerModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
