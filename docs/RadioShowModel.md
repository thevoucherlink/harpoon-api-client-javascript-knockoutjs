# RadioShowModel
## Usage
```javascript
var vm = RadioShowModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.RadioShow.id
name | string | ko.observable(null) | harpoonApi.RadioShow.name
description | string | ko.observable(null) | harpoonApi.RadioShow.description
content | string | ko.observable(null) | harpoonApi.RadioShow.content
contentType | string | ko.observable(null) | harpoonApi.RadioShow.contentType
contact | Contact | new ContactModel() | Contacts for this show
starts | date | ko.observable(null) | When the Show starts of being public
ends | date | ko.observable(null) | When the Show ceases of being public
type | string | ko.observable(null) | harpoonApi.RadioShow.type
radioShowTimeCurrent | RadioShowTime | new RadioShowTimeModel() | harpoonApi.RadioShow.radioShowTimeCurrent
imgShow | string | ko.observable(null) | harpoonApi.RadioShow.imgShow
sponsorTrack | string | ko.observable(null) | Url of the sponsor MP3 to be played
radioPresenters | RadioPresenter | ko.observableArray([]) | Property radioPresenters of harpoonApi.RadioShow describes a relationship hasMany harpoonApi.RadioPresenter
radioStream | object | ko.observable(null) | Property radioStream of harpoonApi.RadioShow describes a relationship belongsTo harpoonApi.RadioStream
radioShowTimes | RadioShowTime | ko.observableArray([]) | Property radioShowTimes of harpoonApi.RadioShow describes a relationship hasMany harpoonApi.RadioShowTime
playlistItem | object | ko.observable(null) | Property playlistItem of harpoonApi.RadioShow describes a relationship belongsTo harpoonApi.PlaylistItem
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | RadioShowModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
