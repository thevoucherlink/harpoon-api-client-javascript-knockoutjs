# EmailDashCampaignFlowSuccessMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowSuccess.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowSuccess.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowSuccess.reDirectLink
userName | string | cannot | can | harpoonApi.EmailDashCampaignFlowSuccess.userName
