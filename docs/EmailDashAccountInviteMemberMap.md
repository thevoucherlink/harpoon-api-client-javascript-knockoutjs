# EmailDashAccountInviteMemberMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandLogo | string | cannot | can | harpoonApi.EmailDashAccountInviteMember.brandLogo
brandName | string | cannot | can | harpoonApi.EmailDashAccountInviteMember.brandName
password | string | cannot | can | harpoonApi.EmailDashAccountInviteMember.password
reDirectLink | string | cannot | can | harpoonApi.EmailDashAccountInviteMember.reDirectLink
senderFirstName | string | cannot | can | harpoonApi.EmailDashAccountInviteMember.senderFirstName
userName | string | cannot | can | harpoonApi.EmailDashAccountInviteMember.userName
