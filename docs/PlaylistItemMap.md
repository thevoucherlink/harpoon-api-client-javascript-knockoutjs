# PlaylistItemMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.PlaylistItem.id
title | string | cannot | can | Stream title
shortDescription | string | cannot | can | Stream short description (on SDKs is under desc)
image | string | cannot | can | Playlist image
file | string | cannot | can | Stream file
type | string | cannot | can | Stream type (used only in JS SDK)
mediaType | string | cannot | can | Stream media type (e.g. video , audio , radioStream)
mediaId | number | cannot | can | Ad media id (on SDKs is under mediaid)
order | number | cannot | can | Sort order index
playlist | object | cannot | can | Property playlist of harpoonApi.PlaylistItem describes a relationship belongsTo harpoonApi.Playlist
playerSources | PlayerSource | can | can | Property playerSources of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.PlayerSource
playerTracks | PlayerTrack | can | can | Property playerTracks of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.PlayerTrack
radioShows | RadioShow | can | can | Property radioShows of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.RadioShow
