# CustomerFollowerMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CustomerFollower.id
customerId | number | cannot | can | harpoonApi.CustomerFollower.customerId
followingId | number | cannot | can | harpoonApi.CustomerFollower.followingId
follower | object | cannot | can | Property follower of harpoonApi.CustomerFollower describes a relationship belongsTo harpoonApi.Customer
following | object | cannot | can | Property following of harpoonApi.CustomerFollower describes a relationship belongsTo harpoonApi.Customer
