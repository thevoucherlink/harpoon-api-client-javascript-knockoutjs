# HarpoonHpublicBrandvenueMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.id
id | Number | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.id
brandId | Number | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.brandId
name | String | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.name
address | String | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.address
latitude | Number | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.latitude
longitude | Number | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.longitude
createdAt | Date | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.createdAt
updatedAt | Date | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.updatedAt
email | String | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.email
phone | String | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.phone
country | String | cannot | can | harpoonApi.HarpoonHpublicBrandvenue.country
