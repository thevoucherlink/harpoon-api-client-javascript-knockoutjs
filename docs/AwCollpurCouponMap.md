# AwCollpurCouponMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwCollpurCoupon.id
dealId | Number | cannot | can | harpoonApi.AwCollpurCoupon.dealId
purchaseId | Number | cannot | can | harpoonApi.AwCollpurCoupon.purchaseId
couponCode | String | cannot | can | harpoonApi.AwCollpurCoupon.couponCode
status | String | cannot | can | harpoonApi.AwCollpurCoupon.status
couponDeliveryDatetime | Date | cannot | can | harpoonApi.AwCollpurCoupon.couponDeliveryDatetime
couponDateUpdated | Date | cannot | can | harpoonApi.AwCollpurCoupon.couponDateUpdated
redeemedAt | Date | cannot | can | harpoonApi.AwCollpurCoupon.redeemedAt
redeemedByTerminal | String | cannot | can | harpoonApi.AwCollpurCoupon.redeemedByTerminal
