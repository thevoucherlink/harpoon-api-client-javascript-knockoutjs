# EmailDashCampaignFlowStoppedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailDashCampaignFlowStopped.brandEmail
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowStopped.brandName
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowStopped.campaignImage
brandTel | number | cannot | can | harpoonApi.EmailDashCampaignFlowStopped.brandTel
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowStopped.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowStopped.reDirectLink
userName | string | cannot | can | harpoonApi.EmailDashCampaignFlowStopped.userName
