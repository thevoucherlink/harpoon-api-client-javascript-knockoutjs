# CustomerBadgeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
offer | number | cannot | can | harpoonApi.CustomerBadge.offer
event | number | cannot | can | harpoonApi.CustomerBadge.event
dealAll | number | cannot | can | harpoonApi.CustomerBadge.dealAll
dealCoupon | number | cannot | can | harpoonApi.CustomerBadge.dealCoupon
dealSimple | number | cannot | can | harpoonApi.CustomerBadge.dealSimple
dealGroup | number | cannot | can | harpoonApi.CustomerBadge.dealGroup
competition | number | cannot | can | harpoonApi.CustomerBadge.competition
walletOfferAvailable | number | cannot | can | harpoonApi.CustomerBadge.walletOfferAvailable
walletOfferNew | number | cannot | can | harpoonApi.CustomerBadge.walletOfferNew
walletOfferExpiring | number | cannot | can | harpoonApi.CustomerBadge.walletOfferExpiring
walletOfferExpired | number | cannot | can | harpoonApi.CustomerBadge.walletOfferExpired
notificationUnread | number | cannot | can | harpoonApi.CustomerBadge.notificationUnread
brandFeed | number | cannot | can | harpoonApi.CustomerBadge.brandFeed
brand | number | cannot | can | harpoonApi.CustomerBadge.brand
brandActivity | number | cannot | can | harpoonApi.CustomerBadge.brandActivity
all | number | cannot | can | harpoonApi.CustomerBadge.all
