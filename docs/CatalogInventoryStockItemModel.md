# CatalogInventoryStockItemModel
## Usage
```javascript
var vm = CatalogInventoryStockItemModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
backorders | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.backorders
enableQtyIncrements | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.enableQtyIncrements
isDecimalDivided | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.isDecimalDivided
isQtyDecimal | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.isQtyDecimal
isInStock | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.isInStock
id | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.id
lowStockDate | Date | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.lowStockDate
manageStock | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.manageStock
minQty | String | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.minQty
maxSaleQty | String | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.maxSaleQty
minSaleQty | String | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.minSaleQty
notifyStockQty | String | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.notifyStockQty
productId | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.productId
qty | String | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.qty
stockId | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.stockId
useConfigBackorders | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigBackorders
qtyIncrements | String | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.qtyIncrements
stockStatusChangedAuto | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.stockStatusChangedAuto
useConfigEnableQtyInc | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigEnableQtyInc
useConfigMaxSaleQty | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigMaxSaleQty
useConfigManageStock | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigManageStock
useConfigMinQty | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigMinQty
useConfigNotifyStockQty | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigNotifyStockQty
useConfigMinSaleQty | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigMinSaleQty
useConfigQtyIncrements | Number | ko.observable(null) | harpoonApi.CatalogInventoryStockItem.useConfigQtyIncrements
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CatalogInventoryStockItemModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
