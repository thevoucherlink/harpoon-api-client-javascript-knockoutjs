# RadioPresenterRadioShowMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RadioPresenterRadioShow.id
radioPresenter | object | cannot | can | Property radioPresenter of harpoonApi.RadioPresenterRadioShow describes a relationship belongsTo harpoonApi.RadioPresenter
radioShow | object | cannot | can | Property radioShow of harpoonApi.RadioPresenterRadioShow describes a relationship belongsTo harpoonApi.RadioShow
