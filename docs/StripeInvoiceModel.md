# StripeInvoiceModel
## Usage
```javascript
var vm = StripeInvoiceModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
object | string | ko.observable(null) | harpoonApi.StripeInvoice.object
amountDue | number | ko.observable(null) | harpoonApi.StripeInvoice.amountDue
applicationFee | number | ko.observable(null) | harpoonApi.StripeInvoice.applicationFee
attemptCount | number | ko.observable(null) | harpoonApi.StripeInvoice.attemptCount
attempted | boolean | ko.observable(null) | harpoonApi.StripeInvoice.attempted
charge | string | ko.observable(null) | harpoonApi.StripeInvoice.charge
closed | boolean | ko.observable(null) | harpoonApi.StripeInvoice.closed
currency | string | ko.observable(null) | harpoonApi.StripeInvoice.currency
date | number | ko.observable(null) | harpoonApi.StripeInvoice.date
description | string | ko.observable(null) | harpoonApi.StripeInvoice.description
endingBalance | number | ko.observable(null) | harpoonApi.StripeInvoice.endingBalance
forgiven | boolean | ko.observable(null) | harpoonApi.StripeInvoice.forgiven
livemode | boolean | ko.observable(null) | harpoonApi.StripeInvoice.livemode
metadata | string | ko.observable(null) | harpoonApi.StripeInvoice.metadata
nextPaymentAttempt | number | ko.observable(null) | harpoonApi.StripeInvoice.nextPaymentAttempt
paid | boolean | ko.observable(null) | harpoonApi.StripeInvoice.paid
periodEnd | number | ko.observable(null) | harpoonApi.StripeInvoice.periodEnd
periodStart | number | ko.observable(null) | harpoonApi.StripeInvoice.periodStart
receiptNumber | string | ko.observable(null) | harpoonApi.StripeInvoice.receiptNumber
subscriptionProrationDate | number | ko.observable(null) | harpoonApi.StripeInvoice.subscriptionProrationDate
subtotal | number | ko.observable(null) | harpoonApi.StripeInvoice.subtotal
tax | number | ko.observable(null) | harpoonApi.StripeInvoice.tax
taxPercent | number | ko.observable(null) | harpoonApi.StripeInvoice.taxPercent
total | number | ko.observable(null) | harpoonApi.StripeInvoice.total
webhooksDeliveredAt | number | ko.observable(null) | harpoonApi.StripeInvoice.webhooksDeliveredAt
customer | string | ko.observable(null) | harpoonApi.StripeInvoice.customer
discount | StripeDiscount | new StripeDiscountModel() | harpoonApi.StripeInvoice.discount
statmentDescriptor | string | ko.observable(null) | harpoonApi.StripeInvoice.statmentDescriptor
subscription | StripeSubscription | new StripeSubscriptionModel() | harpoonApi.StripeInvoice.subscription
lines | StripeInvoiceItem | ko.observableArray([]) | harpoonApi.StripeInvoice.lines
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | StripeInvoiceModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
