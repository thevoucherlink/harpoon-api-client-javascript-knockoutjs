# EventPurchaseMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.EventPurchase.id
orderId | number | cannot | can | harpoonApi.EventPurchase.orderId
name | string | cannot | can | harpoonApi.EventPurchase.name
event | Event | can | can | harpoonApi.EventPurchase.event
redemptionType | string | cannot | can | harpoonApi.EventPurchase.redemptionType
unlockTime | number | cannot | can | harpoonApi.EventPurchase.unlockTime
code | string | cannot | can | harpoonApi.EventPurchase.code
qrcode | string | cannot | can | harpoonApi.EventPurchase.qrcode
isAvailable | boolean | cannot | can | harpoonApi.EventPurchase.isAvailable
status | string | cannot | can | harpoonApi.EventPurchase.status
createdAt | date | cannot | can | harpoonApi.EventPurchase.createdAt
expiredAt | date | cannot | can | harpoonApi.EventPurchase.expiredAt
redeemedAt | date | cannot | can | harpoonApi.EventPurchase.redeemedAt
redeemedByTerminal | string | cannot | can | harpoonApi.EventPurchase.redeemedByTerminal
basePrice | number | cannot | can | harpoonApi.EventPurchase.basePrice
ticketPrice | number | cannot | can | harpoonApi.EventPurchase.ticketPrice
currency | string | cannot | can | harpoonApi.EventPurchase.currency
attendee | Customer | can | can | harpoonApi.EventPurchase.attendee
