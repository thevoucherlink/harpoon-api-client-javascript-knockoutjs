# HOAuthAuthorizationCodeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.HOAuthAuthorizationCode.id
appId | string | cannot | can | harpoonApi.HOAuthAuthorizationCode.appId
userId | string | cannot | can | harpoonApi.HOAuthAuthorizationCode.userId
issuedAt | date | cannot | can | harpoonApi.HOAuthAuthorizationCode.issuedAt
expiresIn | number | cannot | can | harpoonApi.HOAuthAuthorizationCode.expiresIn
expiredAt | date | cannot | can | harpoonApi.HOAuthAuthorizationCode.expiredAt
scopes | string | can | can | harpoonApi.HOAuthAuthorizationCode.scopes
parameters | object | can | can | harpoonApi.HOAuthAuthorizationCode.parameters
used | boolean | cannot | can | harpoonApi.HOAuthAuthorizationCode.used
redirectURI | string | cannot | can | harpoonApi.HOAuthAuthorizationCode.redirectURI
hash | string | cannot | can | harpoonApi.HOAuthAuthorizationCode.hash
application | object | cannot | can | Property application of harpoonApi.HOAuthAuthorizationCode describes a relationship belongsTo harpoonApi.OAuthClientApplication
user | object | cannot | can | Property user of harpoonApi.HOAuthAuthorizationCode describes a relationship belongsTo harpoonApi.User
