# AppConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.AppConfig.id
imgAppLogo | string | cannot | can | App logo
imgSplashScreen | string | cannot | can | App splash-screen
imgBrandLogo | string | cannot | can | harpoonApi.AppConfig.imgBrandLogo
imgNavBarLogo | string | cannot | can | harpoonApi.AppConfig.imgNavBarLogo
imgBrandPlaceholder | string | cannot | can | harpoonApi.AppConfig.imgBrandPlaceholder
imgMenuBackground | string | cannot | can | harpoonApi.AppConfig.imgMenuBackground
featureRadio | boolean | cannot | can | Enable or disable the Radio feature within the app
featureRadioAutoPlay | boolean | cannot | can | Enable or disable the Radio autoplay feature within the app
featureNews | boolean | cannot | can | harpoonApi.AppConfig.featureNews
featureEvents | boolean | cannot | can | harpoonApi.AppConfig.featureEvents
featureEventsCategorized | boolean | cannot | can | If true the Events list is categorised
featureEventsAttendeeList | boolean | cannot | can | If true the user will have the possibility to see Events' attendees
featureCommunity | boolean | cannot | can | harpoonApi.AppConfig.featureCommunity
featureCommunityCategorized | boolean | cannot | can | If true the Brand list is categories
featureCompetitions | boolean | cannot | can | harpoonApi.AppConfig.featureCompetitions
featureCompetitionsAttendeeList | boolean | cannot | can | If true the user will have the possibility to see Competitions' attendees
featureOffers | boolean | cannot | can | harpoonApi.AppConfig.featureOffers
featureOffersCategorized | boolean | cannot | can | If true the Offer list is categories
featureBrandFollow | boolean | cannot | can | If true the user will have the possibility to follow or unfollow Brands
featureBrandFollowerList | boolean | cannot | can | If true the user will have the possibility to see Brands' followers
featureWordpressConnect | boolean | cannot | can | harpoonApi.AppConfig.featureWordpressConnect
featureLogin | boolean | cannot | can | If true the user will be asked to login before getting access to the app
featureHarpoonLogin | boolean | cannot | can | harpoonApi.AppConfig.featureHarpoonLogin
featureFacebookLogin | boolean | cannot | can | If true the user will have the possibility to login with Facebook
featurePhoneVerification | boolean | cannot | can | If true the user will need to verify its account using a phone number
featurePlayer | boolean | cannot | can | Show / Hide media player
featurePlayerMainButton | boolean | cannot | can | Show / Hide main button
featureGoogleAnalytics | boolean | cannot | can | If true the user actions will be tracked while using the app
playerMainButtonTitle | string | cannot | can | Media Player Main Button title
playerConfig | PlayerConfig | can | can | Media Player config
menu | MenuItem | can | can | harpoonApi.AppConfig.menu
color | string | cannot | can | harpoonApi.AppConfig.color
colors | AppColor | can | can | harpoonApi.AppConfig.colors
googleAppId | string | cannot | can | harpoonApi.AppConfig.googleAppId
iOSAppId | string | cannot | can | harpoonApi.AppConfig.iOSAppId
googleAnalyticsTracking | GoogleAnalyticsTracking | can | can | harpoonApi.AppConfig.googleAnalyticsTracking
loginExternal | LoginExternal | can | can | harpoonApi.AppConfig.loginExternal
rssWebViewCss | string | cannot | can | harpoonApi.AppConfig.rssWebViewCss
ads | AppAd | can | can | harpoonApi.AppConfig.ads
iOSAds | AppAd | can | can | harpoonApi.AppConfig.iOSAds
androidAds | AppAd | can | can | harpoonApi.AppConfig.androidAds
pushNotificationProvider | string | cannot | can | harpoonApi.AppConfig.pushNotificationProvider
batchConfig | BatchConfig | can | can | harpoonApi.AppConfig.batchConfig
urbanAirshipConfig | UrbanAirshipConfig | can | can | harpoonApi.AppConfig.urbanAirshipConfig
pushWooshConfig | PushWooshConfig | can | can | harpoonApi.AppConfig.pushWooshConfig
facebookConfig | FacebookConfig | can | can | harpoonApi.AppConfig.facebookConfig
twitterConfig | TwitterConfig | can | can | harpoonApi.AppConfig.twitterConfig
rssTags | RssTags | can | can | harpoonApi.AppConfig.rssTags
connectTo | AppConnectTo | can | can | harpoonApi.AppConfig.connectTo
iOSVersion | string | cannot | can | harpoonApi.AppConfig.iOSVersion
iOSBuild | string | cannot | can | harpoonApi.AppConfig.iOSBuild
iOSGoogleServices | string | cannot | can | harpoonApi.AppConfig.iOSGoogleServices
iOSTeamId | string | cannot | can | harpoonApi.AppConfig.iOSTeamId
iOSAppleId | string | cannot | can | harpoonApi.AppConfig.iOSAppleId
iOSPushNotificationPrivateKey | string | cannot | can | harpoonApi.AppConfig.iOSPushNotificationPrivateKey
iOSPushNotificationCertificate | string | cannot | can | harpoonApi.AppConfig.iOSPushNotificationCertificate
iOSPushNotificationPem | string | cannot | can | harpoonApi.AppConfig.iOSPushNotificationPem
iOSPushNotificationPassword | string | cannot | can | harpoonApi.AppConfig.iOSPushNotificationPassword
iOSReleaseNotes | string | cannot | can | harpoonApi.AppConfig.iOSReleaseNotes
androidBuild | string | cannot | can | harpoonApi.AppConfig.androidBuild
androidVersion | string | cannot | can | harpoonApi.AppConfig.androidVersion
androidGoogleServices | string | cannot | can | harpoonApi.AppConfig.androidGoogleServices
androidGoogleApiKey | string | cannot | can | harpoonApi.AppConfig.androidGoogleApiKey
androidGoogleCloudMessagingSenderId | string | cannot | can | harpoonApi.AppConfig.androidGoogleCloudMessagingSenderId
androidKey | string | cannot | can | harpoonApi.AppConfig.androidKey
androidKeystore | string | cannot | can | harpoonApi.AppConfig.androidKeystore
androidKeystoreConfig | AndroidKeystoreConfig | can | can | harpoonApi.AppConfig.androidKeystoreConfig
androidApk | string | cannot | can | harpoonApi.AppConfig.androidApk
customerDetailsForm | FormSection | can | can | harpoonApi.AppConfig.customerDetailsForm
contact | Contact | can | can | harpoonApi.AppConfig.contact
imgSponsorMenu | string | cannot | can | harpoonApi.AppConfig.imgSponsorMenu
imgSponsorSplash | string | cannot | can | harpoonApi.AppConfig.imgSponsorSplash
audioSponsorPreRollAd | string | cannot | can | harpoonApi.AppConfig.audioSponsorPreRollAd
name | string | cannot | can | harpoonApi.AppConfig.name
isChildConfig | boolean | cannot | can | harpoonApi.AppConfig.isChildConfig
typeNewsFocusSource | string | cannot | can | harpoonApi.AppConfig.typeNewsFocusSource
sponsorSplashImgDisplayTime | number | cannot | can | harpoonApi.AppConfig.sponsorSplashImgDisplayTime
radioSessionInterval | number | cannot | can | harpoonApi.AppConfig.radioSessionInterval
featureRadioSession | boolean | cannot | can | harpoonApi.AppConfig.featureRadioSession
wordpressShareUrlStub | string | cannot | can | harpoonApi.AppConfig.wordpressShareUrlStub
iosUriScheme | string | cannot | can | harpoonApi.AppConfig.iosUriScheme
androidUriScheme | string | cannot | can | harpoonApi.AppConfig.androidUriScheme
branchConfig | BranchConfig | can | can | harpoonApi.AppConfig.branchConfig
defaultRadioStreamId | number | cannot | can | harpoonApi.AppConfig.defaultRadioStreamId
featureTritonPlayer | boolean | cannot | can | Enable or disable the Triton Player in App
app | object | cannot | can | Property app of harpoonApi.AppConfig describes a relationship belongsTo harpoonApi.App
