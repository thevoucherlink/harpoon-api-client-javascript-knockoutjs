# CustomerModel
## Usage
```javascript
var vm = CustomerModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.Customer.id
email | string | ko.observable(null) | harpoonApi.Customer.email
firstName | string | ko.observable(null) | harpoonApi.Customer.firstName
lastName | string | ko.observable(null) | harpoonApi.Customer.lastName
dob | string | ko.observable(null) | harpoonApi.Customer.dob
gender | string | ko.observable(null) | harpoonApi.Customer.gender
password | string | ko.observable(null) | harpoonApi.Customer.password
profilePicture | string | ko.observable(null) | harpoonApi.Customer.profilePicture
cover | string | ko.observable(null) | harpoonApi.Customer.cover
profilePictureUpload | MagentoImageUpload | new MagentoImageUploadModel() | harpoonApi.Customer.profilePictureUpload
coverUpload | MagentoImageUpload | new MagentoImageUploadModel() | harpoonApi.Customer.coverUpload
metadata | object | ko.observable(null) | harpoonApi.Customer.metadata
connection | CustomerConnection | new CustomerConnectionModel() | harpoonApi.Customer.connection
privacy | CustomerPrivacy | new CustomerPrivacyModel() | harpoonApi.Customer.privacy
followingCount | number | ko.observable(null) | harpoonApi.Customer.followingCount
followerCount | number | ko.observable(null) | harpoonApi.Customer.followerCount
isFollowed | boolean | ko.observable(null) | harpoonApi.Customer.isFollowed
isFollower | boolean | ko.observable(null) | harpoonApi.Customer.isFollower
notificationCount | number | ko.observable(null) | harpoonApi.Customer.notificationCount
badge | CustomerBadge | new CustomerBadgeModel() | harpoonApi.Customer.badge
authorizationCode | string | ko.observable(null) | harpoonApi.Customer.authorizationCode
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CustomerModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
