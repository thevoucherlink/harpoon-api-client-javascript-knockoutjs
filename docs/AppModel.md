# AppModel
## Usage
```javascript
var vm = AppModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.App.id
internalId | number | ko.observable(null) | Id used internally in Harpoon
vendorId | number | ko.observable(null) | Vendor App owner
name | string | ko.observable(null) | Name of the application
category | string | ko.observable(null) | harpoonApi.App.category
description | string | ko.observable(null) | harpoonApi.App.description
keywords | string | ko.observable(null) | harpoonApi.App.keywords
copyright | string | ko.observable(null) | harpoonApi.App.copyright
realm | string | ko.observable(null) | harpoonApi.App.realm
bundle | string | ko.observable(null) | Bundle Identifier of the application, com.{company name}.{project name}
urlScheme | string | ko.observable(null) | URL Scheme used for deeplinking
brandFollowOffer | boolean | ko.observable(null) | Customer need to follow the Brand to display the Offers outside of the Brand profile page
privacyUrl | string | ko.observable(null) | URL linking to the Privacy page for the app
termsOfServiceUrl | string | ko.observable(null) | URL linking to the Terms of Service page for the app
restrictionAge | number | ko.observable(null) | Restrict the use of the app to just the customer with at least the value specified, 0 = no limit
storeAndroidUrl | string | ko.observable(null) | URL linking to the Google Play page for the app
storeIosUrl | string | ko.observable(null) | URL linking to the iTunes App Store page for the app
typeNewsFeed | string | ko.observable(null) | News Feed source, nativeFeed/rss
clientSecret | string | ko.observable(null) | harpoonApi.App.clientSecret
grantTypes | string | ko.observableArray([]) | harpoonApi.App.grantTypes
scopes | string | ko.observableArray([]) | harpoonApi.App.scopes
tokenType | string | ko.observable(null) | harpoonApi.App.tokenType
anonymousAllowed | boolean | ko.observable(null) | harpoonApi.App.anonymousAllowed
status | string | ko.observable(null) | Status of the application, production/sandbox/disabled
created | date | ko.observable(null) | harpoonApi.App.created
modified | date | ko.observable(null) | harpoonApi.App.modified
styleOfferList | string | ko.observable(null) | Size of the Offer List Item, big/small
appConfig | AppConfig | new AppConfigModel() | harpoonApi.App.appConfig
clientToken | string | ko.observable(null) | harpoonApi.App.clientToken
multiConfigTitle | string | ko.observable(null) | harpoonApi.App.multiConfigTitle
multiConfig | AppConfig | ko.observableArray([]) | harpoonApi.App.multiConfig
rssFeeds | RssFeed | ko.observableArray([]) | Property rssFeeds of harpoonApi.App describes a relationship hasMany harpoonApi.RssFeed
rssFeedGroups | RssFeedGroup | ko.observableArray([]) | Property rssFeedGroups of harpoonApi.App describes a relationship hasMany harpoonApi.RssFeedGroup
radioStreams | RadioStream | ko.observableArray([]) | Property radioStreams of harpoonApi.App describes a relationship hasMany harpoonApi.RadioStream
customers | Customer | ko.observableArray([]) | Property customers of harpoonApi.App describes a relationship hasMany harpoonApi.Customer
appConfigs | AppConfig | ko.observableArray([]) | Property appConfigs of harpoonApi.App describes a relationship hasMany harpoonApi.AppConfig
playlists | Playlist | ko.observableArray([]) | Property playlists of harpoonApi.App describes a relationship hasMany harpoonApi.Playlist
apps | App | ko.observableArray([]) | Property apps of harpoonApi.App describes a relationship hasMany harpoonApi.App
parent | object | ko.observable(null) | Property parent of harpoonApi.App describes a relationship belongsTo harpoonApi.App
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AppModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
