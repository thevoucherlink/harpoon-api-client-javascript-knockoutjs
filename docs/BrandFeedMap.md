# BrandFeedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.BrandFeed.id
brand | Brand | can | can | harpoonApi.BrandFeed.brand
