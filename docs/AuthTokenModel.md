# AuthTokenModel
## Usage
```javascript
var vm = AuthTokenModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
accessToken | string | ko.observable(null) | harpoonApi.AuthToken.accessToken
expiresIn | number | ko.observable(null) | harpoonApi.AuthToken.expiresIn
scope | string | ko.observable(null) | harpoonApi.AuthToken.scope
refreshToken | string | ko.observable(null) | harpoonApi.AuthToken.refreshToken
tokenType | string | ko.observable(null) | harpoonApi.AuthToken.tokenType
kid | string | ko.observable(null) | harpoonApi.AuthToken.kid
macAlgorithm | string | ko.observable(null) | harpoonApi.AuthToken.macAlgorithm
macKey | string | ko.observable(null) | harpoonApi.AuthToken.macKey
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AuthTokenModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
