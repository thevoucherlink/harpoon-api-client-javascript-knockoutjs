# AppAdMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.AppAd.id
unitId | string | cannot | can | harpoonApi.AppAd.unitId
unitName | string | cannot | can | harpoonApi.AppAd.unitName
unitType | string | cannot | can | harpoonApi.AppAd.unitType
