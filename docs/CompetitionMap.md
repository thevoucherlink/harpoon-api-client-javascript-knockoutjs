# CompetitionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Competition.id
basePrice | number | cannot | can | harpoonApi.Competition.basePrice
attendee | CompetitionAttendee | can | can | harpoonApi.Competition.attendee
attendees | CompetitionAttendee | can | can | harpoonApi.Competition.attendees
attendeeCount | number | cannot | can | harpoonApi.Competition.attendeeCount
hasJoined | boolean | cannot | can | harpoonApi.Competition.hasJoined
chance | CompetitionChance | can | can | harpoonApi.Competition.chance
chanceCount | number | cannot | can | harpoonApi.Competition.chanceCount
isWinner | boolean | cannot | can | harpoonApi.Competition.isWinner
facebook | FacebookEvent | can | can | harpoonApi.Competition.facebook
chances | CompetitionChance | can | can | harpoonApi.Competition.chances
earnMoreChancesURL | string | cannot | can | harpoonApi.Competition.earnMoreChancesURL
type | Category | can | can | harpoonApi.Competition.type
earnMoreChances | boolean | cannot | can | harpoonApi.Competition.earnMoreChances
competitionQuestion | string | cannot | can | harpoonApi.Competition.competitionQuestion
competitionAnswer | string | cannot | can | harpoonApi.Competition.competitionAnswer
