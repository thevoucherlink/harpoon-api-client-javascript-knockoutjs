# EmailAppCampaignCouponsTheCouponMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.appName
brandEmail | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandLogo
brandName | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandName
brandTel | number | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandTel
campaignBarcode | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignBarcode
campaignCode | number | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignCode
campaignDescription | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignDescription
campaignEndTime | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignEndTime
campaignImage | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignImage
campaignLocation | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignLocation
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignName
organizerName | string | cannot | can | harpoonApi.EmailAppCampaignCouponsTheCoupon.organizerName
