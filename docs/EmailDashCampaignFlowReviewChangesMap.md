# EmailDashCampaignFlowReviewChangesMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowReviewChanges.brandName
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowReviewChanges.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowReviewChanges.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowReviewChanges.reDirectLink
senderFirstName | string | cannot | can | harpoonApi.EmailDashCampaignFlowReviewChanges.senderFirstName
customMessage | string | cannot | can | harpoonApi.EmailDashCampaignFlowReviewChanges.customMessage
