# EmailAppCampaignDealDealEmailMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.brandLogo
brandName | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.brandName
brandTel | number | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.brandTel
campaignBarcode | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignBarcode
campaignCode | number | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignCode
campaignDescription | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignDescription
campaignEndTime | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignEndTime
campaignImage | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignImage
campaignLocation | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignLocation
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignName
campaignTermsAndConditions | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignTermsAndConditions
campaignUsage | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.campaignUsage
orderId | number | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.orderId
priceText | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.priceText
organiserName | string | cannot | can | harpoonApi.EmailAppCampaignDealDealEmail.organiserName
