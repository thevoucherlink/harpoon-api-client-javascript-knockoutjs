# HOAuthScopeMappingMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
scope | string | cannot | can | The scope name
route | string | cannot | can | The route as [verb] /api/users
