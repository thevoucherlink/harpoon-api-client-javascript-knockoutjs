# EmailDashAppInvitationAcceptedExistingUserMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
receiverName | string | cannot | can | harpoonApi.EmailDashAppInvitationAcceptedExistingUser.receiverName
reDirectLink | string | cannot | can | harpoonApi.EmailDashAppInvitationAcceptedExistingUser.reDirectLink
userName | string | cannot | can | harpoonApi.EmailDashAppInvitationAcceptedExistingUser.userName
