# PlayerConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.PlayerConfig.id
mute | boolean | cannot | can | Available on the following SDKs: JS, iOS
autostart | boolean | cannot | can | Available on the following SDKs: JS, iOS, Android
repeat | boolean | cannot | can | Available on the following SDKs: JS, iOS, Android
controls | boolean | cannot | can | Available on the following SDKs: JS, iOS, Android
visualPlaylist | boolean | cannot | can | Available on the following SDKs: JS (as visualplaylist)
displayTitle | boolean | cannot | can | Available on the following SDKs: JS (as displaytitle)
displayDescription | boolean | cannot | can | Available on the following SDKs: JS (as displaydescription)
stretching | string | cannot | can | Available on the following SDKs: JS, iOS, Android (as stretch)
hlshtml | boolean | cannot | can | Available on the following SDKs: JS
primary | string | cannot | can | Available on the following SDKs: JS
flashPlayer | string | cannot | can | Available on the following SDKs: JS (as flashplayer)
baseSkinPath | string | cannot | can | Available on the following SDKs: JS (as base)
preload | string | cannot | can | Available on the following SDKs: JS
playerAdConfig | object | cannot | can | Available on the following SDKs: JS, iOS, Android
playerCaptionConfig | object | cannot | can | Available on the following SDKs: JS, iOS, Android
skinName | string | cannot | can | Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin)
skinCss | string | cannot | can | Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin)
skinActive | string | cannot | can | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS
skinInactive | string | cannot | can | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS
skinBackground | string | cannot | can | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS
