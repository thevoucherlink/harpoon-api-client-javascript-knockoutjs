# DealMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Deal.id
type | Category | can | can | harpoonApi.Deal.type
price | number | cannot | can | harpoonApi.Deal.price
hasClaimed | boolean | cannot | can | harpoonApi.Deal.hasClaimed
qtyLeft | number | cannot | can | harpoonApi.Deal.qtyLeft
qtyTotal | number | cannot | can | harpoonApi.Deal.qtyTotal
qtyClaimed | number | cannot | can | harpoonApi.Deal.qtyClaimed
