# BrandModel
## Usage
```javascript
var vm = BrandModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.Brand.id
name | string | ko.observable(null) | harpoonApi.Brand.name
logo | string | ko.observable(null) | harpoonApi.Brand.logo
logoUpload | MagentoImageUpload | new MagentoImageUploadModel() | harpoonApi.Brand.logoUpload
cover | string | ko.observable(null) | harpoonApi.Brand.cover
coverUpload | MagentoImageUpload | new MagentoImageUploadModel() | harpoonApi.Brand.coverUpload
description | string | ko.observable(null) | harpoonApi.Brand.description
email | string | ko.observable(null) | harpoonApi.Brand.email
phone | string | ko.observable(null) | harpoonApi.Brand.phone
address | string | ko.observable(null) | harpoonApi.Brand.address
managerName | string | ko.observable(null) | harpoonApi.Brand.managerName
followerCount | number | ko.observable(null) | harpoonApi.Brand.followerCount
isFollowed | boolean | ko.observable(null) | harpoonApi.Brand.isFollowed
category | Category | ko.observableArray([]) | harpoonApi.Brand.category
categories | Category | ko.observableArray([]) | harpoonApi.Brand.categories
website | string | ko.observable(null) | harpoonApi.Brand.website
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | BrandModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
