# CustomerConnectionTwitterMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CustomerConnectionTwitter.id
userId | string | cannot | can | harpoonApi.CustomerConnectionTwitter.userId
