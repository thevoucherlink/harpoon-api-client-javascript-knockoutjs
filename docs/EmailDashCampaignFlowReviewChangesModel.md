# EmailDashCampaignFlowReviewChangesModel
## Usage
```javascript
var vm = EmailDashCampaignFlowReviewChangesModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
brandName | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowReviewChanges.brandName
campaignImage | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowReviewChanges.campaignImage
campaignName | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowReviewChanges.campaignName
reDirectLink | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowReviewChanges.reDirectLink
senderFirstName | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowReviewChanges.senderFirstName
customMessage | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowReviewChanges.customMessage
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | EmailDashCampaignFlowReviewChangesModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
