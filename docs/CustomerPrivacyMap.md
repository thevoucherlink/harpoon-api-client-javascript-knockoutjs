# CustomerPrivacyMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
activity | string | cannot | can | harpoonApi.CustomerPrivacy.activity
notificationPush | boolean | cannot | can | harpoonApi.CustomerPrivacy.notificationPush
notificationBeacon | boolean | cannot | can | harpoonApi.CustomerPrivacy.notificationBeacon
notificationLocation | boolean | cannot | can | harpoonApi.CustomerPrivacy.notificationLocation
