# EmailAppCampaignInvoiceMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.appName
billingAddressLineOne | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.billingAddressLineOne
billingAddressLineTwo | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.billingAddressLineTwo
billingAddressLineThree | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.billingAddressLineThree
brandEmail | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.brandEmail
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.campaignName
campaignOrderDate | date | cannot | can | harpoonApi.EmailAppCampaignInvoice.campaignOrderDate
campaignPayBy | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.campaignPayBy
cardholderName | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.cardholderName
orderId | number | cannot | can | harpoonApi.EmailAppCampaignInvoice.orderId
price | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.price
quantity | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.quantity
ticketType | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.ticketType
totalPrice | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.totalPrice
userName | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.userName
sku | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.sku
tax | string | cannot | can | harpoonApi.EmailAppCampaignInvoice.tax
