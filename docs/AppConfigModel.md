# AppConfigModel
## Usage
```javascript
var vm = AppConfigModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.AppConfig.id
imgAppLogo | string | ko.observable(null) | App logo
imgSplashScreen | string | ko.observable(null) | App splash-screen
imgBrandLogo | string | ko.observable(null) | harpoonApi.AppConfig.imgBrandLogo
imgNavBarLogo | string | ko.observable(null) | harpoonApi.AppConfig.imgNavBarLogo
imgBrandPlaceholder | string | ko.observable(null) | harpoonApi.AppConfig.imgBrandPlaceholder
imgMenuBackground | string | ko.observable(null) | harpoonApi.AppConfig.imgMenuBackground
featureRadio | boolean | ko.observable(null) | Enable or disable the Radio feature within the app
featureRadioAutoPlay | boolean | ko.observable(null) | Enable or disable the Radio autoplay feature within the app
featureNews | boolean | ko.observable(null) | harpoonApi.AppConfig.featureNews
featureEvents | boolean | ko.observable(null) | harpoonApi.AppConfig.featureEvents
featureEventsCategorized | boolean | ko.observable(null) | If true the Events list is categorised
featureEventsAttendeeList | boolean | ko.observable(null) | If true the user will have the possibility to see Events' attendees
featureCommunity | boolean | ko.observable(null) | harpoonApi.AppConfig.featureCommunity
featureCommunityCategorized | boolean | ko.observable(null) | If true the Brand list is categories
featureCompetitions | boolean | ko.observable(null) | harpoonApi.AppConfig.featureCompetitions
featureCompetitionsAttendeeList | boolean | ko.observable(null) | If true the user will have the possibility to see Competitions' attendees
featureOffers | boolean | ko.observable(null) | harpoonApi.AppConfig.featureOffers
featureOffersCategorized | boolean | ko.observable(null) | If true the Offer list is categories
featureBrandFollow | boolean | ko.observable(null) | If true the user will have the possibility to follow or unfollow Brands
featureBrandFollowerList | boolean | ko.observable(null) | If true the user will have the possibility to see Brands' followers
featureWordpressConnect | boolean | ko.observable(null) | harpoonApi.AppConfig.featureWordpressConnect
featureLogin | boolean | ko.observable(null) | If true the user will be asked to login before getting access to the app
featureHarpoonLogin | boolean | ko.observable(null) | harpoonApi.AppConfig.featureHarpoonLogin
featureFacebookLogin | boolean | ko.observable(null) | If true the user will have the possibility to login with Facebook
featurePhoneVerification | boolean | ko.observable(null) | If true the user will need to verify its account using a phone number
featurePlayer | boolean | ko.observable(null) | Show / Hide media player
featurePlayerMainButton | boolean | ko.observable(null) | Show / Hide main button
featureGoogleAnalytics | boolean | ko.observable(null) | If true the user actions will be tracked while using the app
playerMainButtonTitle | string | ko.observable(null) | Media Player Main Button title
playerConfig | PlayerConfig | new PlayerConfigModel() | Media Player config
menu | MenuItem | ko.observableArray([]) | harpoonApi.AppConfig.menu
color | string | ko.observable(null) | harpoonApi.AppConfig.color
colors | AppColor | new AppColorModel() | harpoonApi.AppConfig.colors
googleAppId | string | ko.observable(null) | harpoonApi.AppConfig.googleAppId
iOSAppId | string | ko.observable(null) | harpoonApi.AppConfig.iOSAppId
googleAnalyticsTracking | GoogleAnalyticsTracking | new GoogleAnalyticsTrackingModel() | harpoonApi.AppConfig.googleAnalyticsTracking
loginExternal | LoginExternal | new LoginExternalModel() | harpoonApi.AppConfig.loginExternal
rssWebViewCss | string | ko.observable(null) | harpoonApi.AppConfig.rssWebViewCss
ads | AppAd | ko.observableArray([]) | harpoonApi.AppConfig.ads
iOSAds | AppAd | ko.observableArray([]) | harpoonApi.AppConfig.iOSAds
androidAds | AppAd | ko.observableArray([]) | harpoonApi.AppConfig.androidAds
pushNotificationProvider | string | ko.observable(null) | harpoonApi.AppConfig.pushNotificationProvider
batchConfig | BatchConfig | new BatchConfigModel() | harpoonApi.AppConfig.batchConfig
urbanAirshipConfig | UrbanAirshipConfig | new UrbanAirshipConfigModel() | harpoonApi.AppConfig.urbanAirshipConfig
pushWooshConfig | PushWooshConfig | new PushWooshConfigModel() | harpoonApi.AppConfig.pushWooshConfig
facebookConfig | FacebookConfig | new FacebookConfigModel() | harpoonApi.AppConfig.facebookConfig
twitterConfig | TwitterConfig | new TwitterConfigModel() | harpoonApi.AppConfig.twitterConfig
rssTags | RssTags | new RssTagsModel() | harpoonApi.AppConfig.rssTags
connectTo | AppConnectTo | new AppConnectToModel() | harpoonApi.AppConfig.connectTo
iOSVersion | string | ko.observable(null) | harpoonApi.AppConfig.iOSVersion
iOSBuild | string | ko.observable(null) | harpoonApi.AppConfig.iOSBuild
iOSGoogleServices | string | ko.observable(null) | harpoonApi.AppConfig.iOSGoogleServices
iOSTeamId | string | ko.observable(null) | harpoonApi.AppConfig.iOSTeamId
iOSAppleId | string | ko.observable(null) | harpoonApi.AppConfig.iOSAppleId
iOSPushNotificationPrivateKey | string | ko.observable(null) | harpoonApi.AppConfig.iOSPushNotificationPrivateKey
iOSPushNotificationCertificate | string | ko.observable(null) | harpoonApi.AppConfig.iOSPushNotificationCertificate
iOSPushNotificationPem | string | ko.observable(null) | harpoonApi.AppConfig.iOSPushNotificationPem
iOSPushNotificationPassword | string | ko.observable(null) | harpoonApi.AppConfig.iOSPushNotificationPassword
iOSReleaseNotes | string | ko.observable(null) | harpoonApi.AppConfig.iOSReleaseNotes
androidBuild | string | ko.observable(null) | harpoonApi.AppConfig.androidBuild
androidVersion | string | ko.observable(null) | harpoonApi.AppConfig.androidVersion
androidGoogleServices | string | ko.observable(null) | harpoonApi.AppConfig.androidGoogleServices
androidGoogleApiKey | string | ko.observable(null) | harpoonApi.AppConfig.androidGoogleApiKey
androidGoogleCloudMessagingSenderId | string | ko.observable(null) | harpoonApi.AppConfig.androidGoogleCloudMessagingSenderId
androidKey | string | ko.observable(null) | harpoonApi.AppConfig.androidKey
androidKeystore | string | ko.observable(null) | harpoonApi.AppConfig.androidKeystore
androidKeystoreConfig | AndroidKeystoreConfig | new AndroidKeystoreConfigModel() | harpoonApi.AppConfig.androidKeystoreConfig
androidApk | string | ko.observable(null) | harpoonApi.AppConfig.androidApk
customerDetailsForm | FormSection | ko.observableArray([]) | harpoonApi.AppConfig.customerDetailsForm
contact | Contact | new ContactModel() | harpoonApi.AppConfig.contact
imgSponsorMenu | string | ko.observable(null) | harpoonApi.AppConfig.imgSponsorMenu
imgSponsorSplash | string | ko.observable(null) | harpoonApi.AppConfig.imgSponsorSplash
audioSponsorPreRollAd | string | ko.observable(null) | harpoonApi.AppConfig.audioSponsorPreRollAd
name | string | ko.observable(null) | harpoonApi.AppConfig.name
isChildConfig | boolean | ko.observable(null) | harpoonApi.AppConfig.isChildConfig
typeNewsFocusSource | string | ko.observable(null) | harpoonApi.AppConfig.typeNewsFocusSource
sponsorSplashImgDisplayTime | number | ko.observable(null) | harpoonApi.AppConfig.sponsorSplashImgDisplayTime
radioSessionInterval | number | ko.observable(null) | harpoonApi.AppConfig.radioSessionInterval
featureRadioSession | boolean | ko.observable(null) | harpoonApi.AppConfig.featureRadioSession
wordpressShareUrlStub | string | ko.observable(null) | harpoonApi.AppConfig.wordpressShareUrlStub
iosUriScheme | string | ko.observable(null) | harpoonApi.AppConfig.iosUriScheme
androidUriScheme | string | ko.observable(null) | harpoonApi.AppConfig.androidUriScheme
branchConfig | BranchConfig | new BranchConfigModel() | harpoonApi.AppConfig.branchConfig
defaultRadioStreamId | number | ko.observable(null) | harpoonApi.AppConfig.defaultRadioStreamId
featureTritonPlayer | boolean | ko.observable(null) | Enable or disable the Triton Player in App
app | object | ko.observable(null) | Property app of harpoonApi.AppConfig describes a relationship belongsTo harpoonApi.App
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AppConfigModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
