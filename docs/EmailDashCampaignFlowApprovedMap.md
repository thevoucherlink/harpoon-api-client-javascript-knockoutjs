# EmailDashCampaignFlowApprovedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.EmailDashCampaignFlowApproved.appName
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowApproved.brandName
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowApproved.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowApproved.campaignName
campaignStartDate | string | cannot | can | harpoonApi.EmailDashCampaignFlowApproved.campaignStartDate
campaignStartTime | string | cannot | can | harpoonApi.EmailDashCampaignFlowApproved.campaignStartTime
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowApproved.reDirectLink
