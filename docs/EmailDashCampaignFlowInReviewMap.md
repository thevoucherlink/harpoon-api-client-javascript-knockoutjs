# EmailDashCampaignFlowInReviewMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailDashCampaignFlowInReview.brandEmail
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowInReview.brandName
brandTel | number | cannot | can | harpoonApi.EmailDashCampaignFlowInReview.brandTel
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowInReview.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowInReview.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowInReview.reDirectLink
