# CategoryMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Category.id
name | string | cannot | can | harpoonApi.Category.name
productCount | number | cannot | can | harpoonApi.Category.productCount
hasChildren | boolean | cannot | can | harpoonApi.Category.hasChildren
parent | Category | can | can | harpoonApi.Category.parent
isPrimary | boolean | cannot | can | harpoonApi.Category.isPrimary
children | Category | can | can | harpoonApi.Category.children
