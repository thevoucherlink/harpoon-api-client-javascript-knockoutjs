# VenueMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Venue.id
name | string | cannot | can | harpoonApi.Venue.name
address | string | cannot | can | harpoonApi.Venue.address
city | string | cannot | can | harpoonApi.Venue.city
country | string | cannot | can | harpoonApi.Venue.country
coordinates | GeoLocation | can | can | harpoonApi.Venue.coordinates
phone | string | cannot | can | harpoonApi.Venue.phone
email | string | cannot | can | harpoonApi.Venue.email
brand | Brand | can | can | harpoonApi.Venue.brand
