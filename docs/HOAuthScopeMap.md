# HOAuthScopeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
scope | string | cannot | can | harpoonApi.HOAuthScope.scope
description | string | cannot | can | harpoonApi.HOAuthScope.description
iconURL | string | cannot | can | harpoonApi.HOAuthScope.iconURL
ttl | number | cannot | can | harpoonApi.HOAuthScope.ttl
