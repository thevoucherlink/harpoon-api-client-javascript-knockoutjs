# FormSectionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.FormSection.id
title | string | cannot | can | harpoonApi.FormSection.title
rows | FormRow | can | can | harpoonApi.FormSection.rows
