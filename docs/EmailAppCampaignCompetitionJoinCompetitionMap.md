# EmailAppCampaignCompetitionJoinCompetitionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandLogo
brandName | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandName
brandTel | number | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandTel
campaignDescription | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignDescription
campaignEndTime | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignEndTime
campaignImage | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignImage
campaignLink | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignLink
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignName
campaignTermsAndConditions | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignTermsAndConditions
