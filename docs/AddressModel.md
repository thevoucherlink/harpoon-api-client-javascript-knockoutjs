# AddressModel
## Usage
```javascript
var vm = AddressModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.Address.id
streetAddress | string | ko.observable(null) | Main street line for the address (first line)
streetAddressComp | string | ko.observable(null) | Line to complete the address (second line)
city | string | ko.observable(null) | harpoonApi.Address.city
region | string | ko.observable(null) | Region, County, Province or State
countryCode | string | ko.observable(null) | Country ISO code (must be max of 2 capital letter)
postcode | string | ko.observable(null) | harpoonApi.Address.postcode
attnFirstName | string | ko.observable(null) | Postal Attn {person first name}
attnLastName | string | ko.observable(null) | Postal Attn {person last name}
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AddressModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
