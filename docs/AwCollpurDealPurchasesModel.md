# AwCollpurDealPurchasesModel
## Usage
```javascript
var vm = AwCollpurDealPurchasesModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.id
dealId | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.dealId
orderId | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.orderId
orderItemId | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.orderItemId
qtyPurchased | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.qtyPurchased
qtyWithCoupons | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.qtyWithCoupons
customerName | String | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.customerName
customerId | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.customerId
purchaseDateTime | Date | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.purchaseDateTime
shippingAmount | String | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.shippingAmount
qtyOrdered | String | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.qtyOrdered
refundState | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.refundState
isSuccessedFlag | Number | ko.observable(null) | harpoonApi.AwCollpurDealPurchases.isSuccessedFlag
awCollpurCoupon | AwCollpurCoupon | ko.observable(null) | Property awCollpurCoupon of harpoonApi.AwCollpurDealPurchases describes a relationship hasOne harpoonApi.AwCollpurCoupon
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AwCollpurDealPurchasesModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
