# PlaylistItemModel
## Usage
```javascript
var vm = PlaylistItemModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.PlaylistItem.id
title | string | ko.observable(null) | Stream title
shortDescription | string | ko.observable(null) | Stream short description (on SDKs is under desc)
image | string | ko.observable(null) | Playlist image
file | string | ko.observable(null) | Stream file
type | string | ko.observable(null) | Stream type (used only in JS SDK)
mediaType | string | ko.observable(null) | Stream media type (e.g. video , audio , radioStream)
mediaId | number | ko.observable(null) | Ad media id (on SDKs is under mediaid)
order | number | ko.observable(null) | Sort order index
playlist | object | ko.observable(null) | Property playlist of harpoonApi.PlaylistItem describes a relationship belongsTo harpoonApi.Playlist
playerSources | PlayerSource | ko.observableArray([]) | Property playerSources of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.PlayerSource
playerTracks | PlayerTrack | ko.observableArray([]) | Property playerTracks of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.PlayerTrack
radioShows | RadioShow | ko.observableArray([]) | Property radioShows of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.RadioShow
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | PlaylistItemModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
