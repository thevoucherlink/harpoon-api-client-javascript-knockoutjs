# PiwikCredentialsMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.PiwikCredentials.id
apiToken | string | cannot | can | harpoonApi.PiwikCredentials.apiToken
appId | string | cannot | can | harpoonApi.PiwikCredentials.appId
password | string | cannot | can | harpoonApi.PiwikCredentials.password
username | string | cannot | can | harpoonApi.PiwikCredentials.username
scope | string | cannot | can | harpoonApi.PiwikCredentials.scope
created | date | cannot | can | harpoonApi.PiwikCredentials.created
modified | date | cannot | can | harpoonApi.PiwikCredentials.modified
