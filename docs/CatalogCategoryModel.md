# CatalogCategoryModel
## Usage
```javascript
var vm = CatalogCategoryModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.CatalogCategory.id
parentId | Number | ko.observable(null) | harpoonApi.CatalogCategory.parentId
createdAt | Date | ko.observable(null) | harpoonApi.CatalogCategory.createdAt
updatedAt | Date | ko.observable(null) | harpoonApi.CatalogCategory.updatedAt
path | String | ko.observable(null) | harpoonApi.CatalogCategory.path
position | Number | ko.observable(null) | harpoonApi.CatalogCategory.position
level | Number | ko.observable(null) | harpoonApi.CatalogCategory.level
childrenCount | Number | ko.observable(null) | harpoonApi.CatalogCategory.childrenCount
storeId | Number | ko.observable(null) | harpoonApi.CatalogCategory.storeId
allChildren | String | ko.observable(null) | harpoonApi.CatalogCategory.allChildren
availableSortBy | String | ko.observable(null) | harpoonApi.CatalogCategory.availableSortBy
children | String | ko.observable(null) | harpoonApi.CatalogCategory.children
customApplyToProducts | Number | ko.observable(null) | harpoonApi.CatalogCategory.customApplyToProducts
customDesign | String | ko.observable(null) | harpoonApi.CatalogCategory.customDesign
customDesignFrom | Date | ko.observable(null) | harpoonApi.CatalogCategory.customDesignFrom
customDesignTo | Date | ko.observable(null) | harpoonApi.CatalogCategory.customDesignTo
customLayoutUpdate | String | ko.observable(null) | harpoonApi.CatalogCategory.customLayoutUpdate
customUseParentSettings | Number | ko.observable(null) | harpoonApi.CatalogCategory.customUseParentSettings
defaultSortBy | String | ko.observable(null) | harpoonApi.CatalogCategory.defaultSortBy
description | String | ko.observable(null) | harpoonApi.CatalogCategory.description
displayMode | String | ko.observable(null) | harpoonApi.CatalogCategory.displayMode
filterPriceRange | String | ko.observable(null) | harpoonApi.CatalogCategory.filterPriceRange
image | String | ko.observable(null) | harpoonApi.CatalogCategory.image
includeInMenu | Number | ko.observable(null) | harpoonApi.CatalogCategory.includeInMenu
isActive | Number | ko.observable(null) | harpoonApi.CatalogCategory.isActive
isAnchor | Number | ko.observable(null) | harpoonApi.CatalogCategory.isAnchor
landingPage | Number | ko.observable(null) | harpoonApi.CatalogCategory.landingPage
metaDescription | String | ko.observable(null) | harpoonApi.CatalogCategory.metaDescription
metaKeywords | String | ko.observable(null) | harpoonApi.CatalogCategory.metaKeywords
metaTitle | String | ko.observable(null) | harpoonApi.CatalogCategory.metaTitle
name | String | ko.observable(null) | harpoonApi.CatalogCategory.name
pageLayout | String | ko.observable(null) | harpoonApi.CatalogCategory.pageLayout
pathInStore | String | ko.observable(null) | harpoonApi.CatalogCategory.pathInStore
thumbnail | String | ko.observable(null) | harpoonApi.CatalogCategory.thumbnail
ummCatLabel | String | ko.observable(null) | harpoonApi.CatalogCategory.ummCatLabel
ummCatTarget | String | ko.observable(null) | harpoonApi.CatalogCategory.ummCatTarget
ummDdBlocks | String | ko.observable(null) | harpoonApi.CatalogCategory.ummDdBlocks
ummDdColumns | Number | ko.observable(null) | harpoonApi.CatalogCategory.ummDdColumns
ummDdProportions | String | ko.observable(null) | harpoonApi.CatalogCategory.ummDdProportions
ummDdType | String | ko.observable(null) | harpoonApi.CatalogCategory.ummDdType
ummDdWidth | String | ko.observable(null) | harpoonApi.CatalogCategory.ummDdWidth
urlKey | String | ko.observable(null) | harpoonApi.CatalogCategory.urlKey
urlPath | String | ko.observable(null) | harpoonApi.CatalogCategory.urlPath
catalogProducts | CatalogProduct | ko.observableArray([]) | Property catalogProducts of harpoonApi.CatalogCategory describes a relationship hasMany harpoonApi.CatalogProduct
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CatalogCategoryModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
