# RssFeedModel
## Usage
```javascript
var vm = RssFeedModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.RssFeed.id
category | string | ko.observable(null) | harpoonApi.RssFeed.category
subCategory | string | ko.observable(null) | harpoonApi.RssFeed.subCategory
url | string | ko.observable(null) | harpoonApi.RssFeed.url
name | string | ko.observable(null) | harpoonApi.RssFeed.name
sortOrder | number | ko.observable(null) | harpoonApi.RssFeed.sortOrder
styleCss | string | ko.observable(null) | harpoonApi.RssFeed.styleCss
noAds | boolean | ko.observable(null) | harpoonApi.RssFeed.noAds
adMRectVisible | boolean | ko.observable(null) | harpoonApi.RssFeed.adMRectVisible
adMRectPosition | number | ko.observable(null) | harpoonApi.RssFeed.adMRectPosition
rssFeedGroup | object | ko.observable(null) | Property rssFeedGroup of harpoonApi.RssFeed describes a relationship belongsTo harpoonApi.RssFeedGroup
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | RssFeedModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
