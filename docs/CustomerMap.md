# CustomerMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Customer.id
email | string | cannot | can | harpoonApi.Customer.email
firstName | string | cannot | can | harpoonApi.Customer.firstName
lastName | string | cannot | can | harpoonApi.Customer.lastName
dob | string | cannot | can | harpoonApi.Customer.dob
gender | string | cannot | can | harpoonApi.Customer.gender
password | string | cannot | can | harpoonApi.Customer.password
profilePicture | string | cannot | can | harpoonApi.Customer.profilePicture
cover | string | cannot | can | harpoonApi.Customer.cover
profilePictureUpload | MagentoImageUpload | can | can | harpoonApi.Customer.profilePictureUpload
coverUpload | MagentoImageUpload | can | can | harpoonApi.Customer.coverUpload
metadata | object | cannot | can | harpoonApi.Customer.metadata
connection | CustomerConnection | can | can | harpoonApi.Customer.connection
privacy | CustomerPrivacy | can | can | harpoonApi.Customer.privacy
followingCount | number | cannot | can | harpoonApi.Customer.followingCount
followerCount | number | cannot | can | harpoonApi.Customer.followerCount
isFollowed | boolean | cannot | can | harpoonApi.Customer.isFollowed
isFollower | boolean | cannot | can | harpoonApi.Customer.isFollower
notificationCount | number | cannot | can | harpoonApi.Customer.notificationCount
badge | CustomerBadge | can | can | harpoonApi.Customer.badge
authorizationCode | string | cannot | can | harpoonApi.Customer.authorizationCode
