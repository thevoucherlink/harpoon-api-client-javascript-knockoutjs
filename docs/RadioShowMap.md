# RadioShowMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RadioShow.id
name | string | cannot | can | harpoonApi.RadioShow.name
description | string | cannot | can | harpoonApi.RadioShow.description
content | string | cannot | can | harpoonApi.RadioShow.content
contentType | string | cannot | can | harpoonApi.RadioShow.contentType
contact | Contact | can | can | Contacts for this show
starts | date | cannot | can | When the Show starts of being public
ends | date | cannot | can | When the Show ceases of being public
type | string | cannot | can | harpoonApi.RadioShow.type
radioShowTimeCurrent | RadioShowTime | can | can | harpoonApi.RadioShow.radioShowTimeCurrent
imgShow | string | cannot | can | harpoonApi.RadioShow.imgShow
sponsorTrack | string | cannot | can | Url of the sponsor MP3 to be played
radioPresenters | RadioPresenter | can | can | Property radioPresenters of harpoonApi.RadioShow describes a relationship hasMany harpoonApi.RadioPresenter
radioStream | object | cannot | can | Property radioStream of harpoonApi.RadioShow describes a relationship belongsTo harpoonApi.RadioStream
radioShowTimes | RadioShowTime | can | can | Property radioShowTimes of harpoonApi.RadioShow describes a relationship hasMany harpoonApi.RadioShowTime
playlistItem | object | cannot | can | Property playlistItem of harpoonApi.RadioShow describes a relationship belongsTo harpoonApi.PlaylistItem
