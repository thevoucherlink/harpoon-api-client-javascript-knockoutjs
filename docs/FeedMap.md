# FeedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Feed.id
message | string | cannot | can | harpoonApi.Feed.message
cover | string | cannot | can | harpoonApi.Feed.cover
coverUpload | MagentoImageUpload | can | can | harpoonApi.Feed.coverUpload
related | object | cannot | can | harpoonApi.Feed.related
link | string | cannot | can | harpoonApi.Feed.link
actionCode | string | cannot | can | harpoonApi.Feed.actionCode
privacyCode | string | cannot | can | harpoonApi.Feed.privacyCode
postedAt | date | cannot | can | harpoonApi.Feed.postedAt
