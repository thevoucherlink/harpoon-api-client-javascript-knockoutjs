# RssFeedGroupMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RssFeedGroup.id
name | string | cannot | can | harpoonApi.RssFeedGroup.name
sortOrder | number | cannot | can | harpoonApi.RssFeedGroup.sortOrder
rssFeeds | RssFeed | can | can | Property rssFeeds of harpoonApi.RssFeedGroup describes a relationship hasMany harpoonApi.RssFeed
