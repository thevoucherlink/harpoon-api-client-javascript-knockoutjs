# AppColorModel
## Usage
```javascript
var vm = AppColorModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
appPrimary | string | ko.observable(null) | harpoonApi.AppColor.appPrimary
appSecondary | string | ko.observable(null) | harpoonApi.AppColor.appSecondary
textPrimary | string | ko.observable(null) | harpoonApi.AppColor.textPrimary
textSecondary | string | ko.observable(null) | harpoonApi.AppColor.textSecondary
radioNavBar | string | ko.observable(null) | harpoonApi.AppColor.radioNavBar
radioPlayer | string | ko.observable(null) | harpoonApi.AppColor.radioPlayer
radioProgressBar | string | ko.observable(null) | harpoonApi.AppColor.radioProgressBar
menuBackground | string | ko.observable(null) | harpoonApi.AppColor.menuBackground
radioTextSecondary | string | ko.observable(null) | harpoonApi.AppColor.radioTextSecondary
radioTextPrimary | string | ko.observable(null) | harpoonApi.AppColor.radioTextPrimary
menuItem | string | ko.observable(null) | harpoonApi.AppColor.menuItem
appGradientPrimary | string | ko.observable(null) | harpoonApi.AppColor.appGradientPrimary
feedSegmentIndicator | string | ko.observable(null) | harpoonApi.AppColor.feedSegmentIndicator
radioPlayButton | string | ko.observable(null) | harpoonApi.AppColor.radioPlayButton
radioPauseButton | string | ko.observable(null) | harpoonApi.AppColor.radioPauseButton
radioPlayButtonBackground | string | ko.observable(null) | harpoonApi.AppColor.radioPlayButtonBackground
radioPauseButtonBackground | string | ko.observable(null) | harpoonApi.AppColor.radioPauseButtonBackground
radioPlayerItem | string | ko.observable(null) | harpoonApi.AppColor.radioPlayerItem
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AppColorModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
