# CustomerActivityMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CustomerActivity.id
customer | Customer | can | can | harpoonApi.CustomerActivity.customer
