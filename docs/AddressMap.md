# AddressMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Address.id
streetAddress | string | cannot | can | Main street line for the address (first line)
streetAddressComp | string | cannot | can | Line to complete the address (second line)
city | string | cannot | can | harpoonApi.Address.city
region | string | cannot | can | Region, County, Province or State
countryCode | string | cannot | can | Country ISO code (must be max of 2 capital letter)
postcode | string | cannot | can | harpoonApi.Address.postcode
attnFirstName | string | cannot | can | Postal Attn {person first name}
attnLastName | string | cannot | can | Postal Attn {person last name}
