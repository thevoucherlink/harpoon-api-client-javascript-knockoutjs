# ProductMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Product.id
name | string | cannot | can | harpoonApi.Product.name
description | string | cannot | can | harpoonApi.Product.description
cover | string | cannot | can | harpoonApi.Product.cover
price | number | cannot | can | harpoonApi.Product.price
base_currency | string | cannot | can | harpoonApi.Product.base_currency
