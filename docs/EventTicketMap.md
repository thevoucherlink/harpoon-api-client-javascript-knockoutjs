# EventTicketMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.EventTicket.id
name | string | cannot | can | harpoonApi.EventTicket.name
price | number | cannot | can | harpoonApi.EventTicket.price
qtyBought | number | cannot | can | harpoonApi.EventTicket.qtyBought
qtyTotal | number | cannot | can | harpoonApi.EventTicket.qtyTotal
qtyLeft | number | cannot | can | harpoonApi.EventTicket.qtyLeft
