# EmailAppCampaignCompetitionWinCompetitionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandLogo
brandName | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandName
brandTel | number | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandTel
campaignImage | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.campaignImage
campaignLink | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.campaignLink
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.campaignName
prizeCollection | string | cannot | can | harpoonApi.EmailAppCampaignCompetitionWinCompetition.prizeCollection
