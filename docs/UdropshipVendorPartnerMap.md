# UdropshipVendorPartnerMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
status | String | cannot | can | harpoonApi.UdropshipVendorPartner.status
invitedEmail | String | cannot | can | harpoonApi.UdropshipVendorPartner.invitedEmail
invitedAt | Date | cannot | can | harpoonApi.UdropshipVendorPartner.invitedAt
acceptedAt | Date | cannot | can | harpoonApi.UdropshipVendorPartner.acceptedAt
createdAt | Date | cannot | can | harpoonApi.UdropshipVendorPartner.createdAt
updatedAt | Date | cannot | can | harpoonApi.UdropshipVendorPartner.updatedAt
configList | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configList
configFeed | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configFeed
configNeedFollow | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configNeedFollow
configAcceptEvent | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configAcceptEvent
configAcceptCoupon | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configAcceptCoupon
configAcceptDealsimple | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configAcceptDealsimple
configAcceptDealgroup | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configAcceptDealgroup
configAcceptNotificationpush | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configAcceptNotificationpush
configAcceptNotificationbeacon | Number | cannot | can | harpoonApi.UdropshipVendorPartner.configAcceptNotificationbeacon
isOwner | Number | cannot | can | harpoonApi.UdropshipVendorPartner.isOwner
vendorId | Number | cannot | can | harpoonApi.UdropshipVendorPartner.vendorId
partnerId | Number | cannot | can | harpoonApi.UdropshipVendorPartner.partnerId
id | Number | cannot | can | harpoonApi.UdropshipVendorPartner.id
udropshipVendor | object | cannot | can | Property udropshipVendor of harpoonApi.UdropshipVendorPartner describes a relationship belongsTo harpoonApi.UdropshipVendor
