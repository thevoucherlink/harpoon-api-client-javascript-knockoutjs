# StripePlanMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
amount | string | cannot | can | harpoonApi.StripePlan.amount
currency | string | cannot | can | harpoonApi.StripePlan.currency
interval | string | cannot | can | harpoonApi.StripePlan.interval
intervalCount | number | cannot | can | harpoonApi.StripePlan.intervalCount
metadata | string | cannot | can | harpoonApi.StripePlan.metadata
name | string | cannot | can | harpoonApi.StripePlan.name
statement_descriptor | string | cannot | can | harpoonApi.StripePlan.statement_descriptor
trial_period_days | number | cannot | can | harpoonApi.StripePlan.trial_period_days
