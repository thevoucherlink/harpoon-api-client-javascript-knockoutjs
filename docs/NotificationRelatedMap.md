# NotificationRelatedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
customerId | number | cannot | can | harpoonApi.NotificationRelated.customerId
brandId | number | cannot | can | harpoonApi.NotificationRelated.brandId
venueId | number | cannot | can | harpoonApi.NotificationRelated.venueId
competitionId | number | cannot | can | harpoonApi.NotificationRelated.competitionId
couponId | number | cannot | can | harpoonApi.NotificationRelated.couponId
eventId | number | cannot | can | harpoonApi.NotificationRelated.eventId
dealPaidId | number | cannot | can | harpoonApi.NotificationRelated.dealPaidId
dealGroupId | number | cannot | can | harpoonApi.NotificationRelated.dealGroupId
