# RssTagsMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
imgTumbnail | string | cannot | can | harpoonApi.RssTags.imgTumbnail
imgMain | string | cannot | can | harpoonApi.RssTags.imgMain
link | string | cannot | can | harpoonApi.RssTags.link
postDateTime | string | cannot | can | harpoonApi.RssTags.postDateTime
description | string | cannot | can | harpoonApi.RssTags.description
category | string | cannot | can | harpoonApi.RssTags.category
title | string | cannot | can | harpoonApi.RssTags.title
