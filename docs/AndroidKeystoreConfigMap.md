# AndroidKeystoreConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.AndroidKeystoreConfig.id
storePassword | string | cannot | can | harpoonApi.AndroidKeystoreConfig.storePassword
keyAlias | string | cannot | can | harpoonApi.AndroidKeystoreConfig.keyAlias
keyPassword | string | cannot | can | harpoonApi.AndroidKeystoreConfig.keyPassword
hashKey | string | cannot | can | harpoonApi.AndroidKeystoreConfig.hashKey
