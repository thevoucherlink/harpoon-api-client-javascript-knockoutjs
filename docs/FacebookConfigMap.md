# FacebookConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appId | string | cannot | can | harpoonApi.FacebookConfig.appId
appToken | string | cannot | can | harpoonApi.FacebookConfig.appToken
scopes | string | cannot | can | harpoonApi.FacebookConfig.scopes
