# CompetitionChanceMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CompetitionChance.id
name | string | cannot | can | harpoonApi.CompetitionChance.name
price | number | cannot | can | harpoonApi.CompetitionChance.price
chanceCount | number | cannot | can | harpoonApi.CompetitionChance.chanceCount
qtyBought | number | cannot | can | harpoonApi.CompetitionChance.qtyBought
qtyTotal | number | cannot | can | harpoonApi.CompetitionChance.qtyTotal
qtyLeft | number | cannot | can | harpoonApi.CompetitionChance.qtyLeft
