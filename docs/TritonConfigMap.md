# TritonConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
stationId | string | cannot | can | harpoonApi.TritonConfig.stationId
stationName | string | cannot | can | harpoonApi.TritonConfig.stationName
mp3Mount | string | cannot | can | harpoonApi.TritonConfig.mp3Mount
aacMount | string | cannot | can | harpoonApi.TritonConfig.aacMount
