# RadioStreamModel
## Usage
```javascript
var vm = RadioStreamModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.RadioStream.id
name | string | ko.observable(null) | harpoonApi.RadioStream.name
description | string | ko.observable(null) | harpoonApi.RadioStream.description
urlHQ | string | ko.observable(null) | harpoonApi.RadioStream.urlHQ
urlLQ | string | ko.observable(null) | harpoonApi.RadioStream.urlLQ
starts | date | ko.observable(null) | When the Stream starts of being public
ends | date | ko.observable(null) | When the Stream ceases of being public
metaDataUrl | string | ko.observable(null) | harpoonApi.RadioStream.metaDataUrl
imgStream | string | ko.observable(null) | harpoonApi.RadioStream.imgStream
sponsorTrack | string | ko.observable(null) | Url of the sponsor MP3 to be played
tritonConfig | TritonConfig | new TritonConfigModel() | harpoonApi.RadioStream.tritonConfig
radioShows | RadioShow | ko.observableArray([]) | Property radioShows of harpoonApi.RadioStream describes a relationship hasMany harpoonApi.RadioShow
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | RadioStreamModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
