# EmailAppCampaignCouponsTheCouponModel
## Usage
```javascript
var vm = EmailAppCampaignCouponsTheCouponModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
appName | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.appName
brandEmail | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandEmail
brandLogo | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandLogo
brandName | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandName
brandTel | number | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.brandTel
campaignBarcode | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignBarcode
campaignCode | number | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignCode
campaignDescription | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignDescription
campaignEndTime | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignEndTime
campaignImage | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignImage
campaignLocation | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignLocation
campaignName | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignName
organizerName | string | ko.observable(null) | harpoonApi.EmailAppCampaignCouponsTheCoupon.organizerName
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | EmailAppCampaignCouponsTheCouponModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
