# EmailDashAppInvitationAcceptedNewUserMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailDashAppInvitationAcceptedNewUser.brandEmail
brandName | string | cannot | can | harpoonApi.EmailDashAppInvitationAcceptedNewUser.brandName
userName | string | cannot | can | harpoonApi.EmailDashAppInvitationAcceptedNewUser.userName
