# EmailDashCampaignInvitePartnerNewMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerNew.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerNew.brandLogo
brandName | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerNew.brandName
brandTel | number | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerNew.brandTel
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerNew.campaignName
campaignType | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerNew.campaignType
verificationLink | string | cannot | can | harpoonApi.EmailDashCampaignInvitePartnerNew.verificationLink
