# ContactMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Contact.id
email | string | cannot | can | Email address like hello@harpoonconnect.com
website | string | cannot | can | URL like http://harpoonconnect.com or https://www.harpoonconnect.com
phone | string | cannot | can | Telephone number like 08123456789
address | Address | can | can | harpoonApi.Contact.address
twitter | string | cannot | can | URL starting with https://www.twitter.com/ followed by your username or id
facebook | string | cannot | can | URL starting with https://www.facebook.com/ followed by your username or id
whatsapp | string | cannot | can | Telephone number including country code like: +35381234567890
snapchat | string | cannot | can | SnapChat username
googlePlus | string | cannot | can | URL starting with https://plus.google.com/+ followed by your username or id
linkedIn | string | cannot | can | LinkedIn profile URL
hashtag | string | cannot | can | A string starting with # and followed by alphanumeric characters (both lower and upper case)
text | string | cannot | can | harpoonApi.Contact.text
