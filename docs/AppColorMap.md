# AppColorMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appPrimary | string | cannot | can | harpoonApi.AppColor.appPrimary
appSecondary | string | cannot | can | harpoonApi.AppColor.appSecondary
textPrimary | string | cannot | can | harpoonApi.AppColor.textPrimary
textSecondary | string | cannot | can | harpoonApi.AppColor.textSecondary
radioNavBar | string | cannot | can | harpoonApi.AppColor.radioNavBar
radioPlayer | string | cannot | can | harpoonApi.AppColor.radioPlayer
radioProgressBar | string | cannot | can | harpoonApi.AppColor.radioProgressBar
menuBackground | string | cannot | can | harpoonApi.AppColor.menuBackground
radioTextSecondary | string | cannot | can | harpoonApi.AppColor.radioTextSecondary
radioTextPrimary | string | cannot | can | harpoonApi.AppColor.radioTextPrimary
menuItem | string | cannot | can | harpoonApi.AppColor.menuItem
appGradientPrimary | string | cannot | can | harpoonApi.AppColor.appGradientPrimary
feedSegmentIndicator | string | cannot | can | harpoonApi.AppColor.feedSegmentIndicator
radioPlayButton | string | cannot | can | harpoonApi.AppColor.radioPlayButton
radioPauseButton | string | cannot | can | harpoonApi.AppColor.radioPauseButton
radioPlayButtonBackground | string | cannot | can | harpoonApi.AppColor.radioPlayButtonBackground
radioPauseButtonBackground | string | cannot | can | harpoonApi.AppColor.radioPauseButtonBackground
radioPlayerItem | string | cannot | can | harpoonApi.AppColor.radioPlayerItem
