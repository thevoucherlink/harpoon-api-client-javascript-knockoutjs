# AwEventbookingEventModel
## Usage
```javascript
var vm = AwEventbookingEventModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.AwEventbookingEvent.id
productId | Number | ko.observable(null) | harpoonApi.AwEventbookingEvent.productId
isEnabled | Number | ko.observable(null) | harpoonApi.AwEventbookingEvent.isEnabled
eventStartDate | Date | ko.observable(null) | harpoonApi.AwEventbookingEvent.eventStartDate
eventEndDate | Date | ko.observable(null) | harpoonApi.AwEventbookingEvent.eventEndDate
dayCountBeforeSendReminderLetter | Number | ko.observable(null) | harpoonApi.AwEventbookingEvent.dayCountBeforeSendReminderLetter
isReminderSend | Number | ko.observable(null) | harpoonApi.AwEventbookingEvent.isReminderSend
isTermsEnabled | Number | ko.observable(null) | harpoonApi.AwEventbookingEvent.isTermsEnabled
generatePdfTickets | Number | ko.observable(null) | harpoonApi.AwEventbookingEvent.generatePdfTickets
redeemRoles | String | ko.observable(null) | harpoonApi.AwEventbookingEvent.redeemRoles
location | String | ko.observable(null) | harpoonApi.AwEventbookingEvent.location
tickets | AwEventbookingEventTicket | ko.observableArray([]) | Property tickets of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingEventTicket
attributes | AwEventbookingEventAttribute | ko.observableArray([]) | Property attributes of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingEventAttribute
purchasedTickets | AwEventbookingTicket | ko.observableArray([]) | Property purchasedTickets of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingTicket
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AwEventbookingEventModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
