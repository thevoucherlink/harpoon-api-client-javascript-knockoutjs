# FormSelectorOptionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.FormSelectorOption.id
title | string | cannot | can | harpoonApi.FormSelectorOption.title
value | string | cannot | can | harpoonApi.FormSelectorOption.value
format | string | cannot | can | harpoonApi.FormSelectorOption.format
