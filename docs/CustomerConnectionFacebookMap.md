# CustomerConnectionFacebookMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CustomerConnectionFacebook.id
userId | string | cannot | can | harpoonApi.CustomerConnectionFacebook.userId
