# EmailDashAccountEmailConfirmationMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
userName | string | cannot | can | harpoonApi.EmailDashAccountEmailConfirmation.userName
verificationLink | string | cannot | can | harpoonApi.EmailDashAccountEmailConfirmation.verificationLink
