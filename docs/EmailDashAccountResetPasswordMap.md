# EmailDashAccountResetPasswordMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
password | string | cannot | can | harpoonApi.EmailDashAccountResetPassword.password
userName | string | cannot | can | harpoonApi.EmailDashAccountResetPassword.userName
