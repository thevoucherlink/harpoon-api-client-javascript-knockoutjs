# CompetitionAttendeeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CompetitionAttendee.id
joinedAt | date | cannot | can | harpoonApi.CompetitionAttendee.joinedAt
