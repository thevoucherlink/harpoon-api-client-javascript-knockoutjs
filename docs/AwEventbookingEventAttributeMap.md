# AwEventbookingEventAttributeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwEventbookingEventAttribute.id
eventId | Number | cannot | can | harpoonApi.AwEventbookingEventAttribute.eventId
storeId | Number | cannot | can | harpoonApi.AwEventbookingEventAttribute.storeId
attributeCode | String | cannot | can | harpoonApi.AwEventbookingEventAttribute.attributeCode
value | String | cannot | can | harpoonApi.AwEventbookingEventAttribute.value
