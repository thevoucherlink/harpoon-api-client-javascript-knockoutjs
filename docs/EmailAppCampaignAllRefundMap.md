# EmailAppCampaignAllRefundMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.appName
brandCover | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.brandCover
brandEmail | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.brandLogo
brandName | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.brandName
brandTel | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.brandTel
campaignName | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.campaignName
orderId | number | cannot | can | harpoonApi.EmailAppCampaignAllRefund.orderId
organizerName | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.organizerName
refundAmount | number | cannot | can | harpoonApi.EmailAppCampaignAllRefund.refundAmount
userName | string | cannot | can | harpoonApi.EmailAppCampaignAllRefund.userName
