# EmailAppSettingsPasswordForgotPasswordResetMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandCover | string | cannot | can | harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandCover
brandEmail | string | cannot | can | harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandLogo
brandName | string | cannot | can | harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandName
brandTel | number | cannot | can | harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandTel
userName | string | cannot | can | harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.userName
verificationLink | string | cannot | can | harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.verificationLink
