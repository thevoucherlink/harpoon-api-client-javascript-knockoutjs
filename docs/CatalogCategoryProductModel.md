# CatalogCategoryProductModel
## Usage
```javascript
var vm = CatalogCategoryProductModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
categoryId | Number | ko.observable(null) | harpoonApi.CatalogCategoryProduct.categoryId
productId | Number | ko.observable(null) | harpoonApi.CatalogCategoryProduct.productId
position | Number | ko.observable(null) | harpoonApi.CatalogCategoryProduct.position
catalogProduct | object | ko.observable(null) | Property catalogProduct of harpoonApi.CatalogCategoryProduct describes a relationship belongsTo harpoonApi.CatalogProduct
catalogCategory | object | ko.observable(null) | Property catalogCategory of harpoonApi.CatalogCategoryProduct describes a relationship belongsTo harpoonApi.CatalogCategory
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CatalogCategoryProductModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
