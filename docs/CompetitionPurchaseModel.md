# CompetitionPurchaseModel
## Usage
```javascript
var vm = CompetitionPurchaseModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.CompetitionPurchase.id
orderId | number | ko.observable(null) | harpoonApi.CompetitionPurchase.orderId
name | string | ko.observable(null) | harpoonApi.CompetitionPurchase.name
competition | Competition | new CompetitionModel() | harpoonApi.CompetitionPurchase.competition
redemptionType | string | ko.observable(null) | harpoonApi.CompetitionPurchase.redemptionType
unlockTime | number | ko.observable(null) | harpoonApi.CompetitionPurchase.unlockTime
code | string | ko.observable(null) | harpoonApi.CompetitionPurchase.code
qrcode | string | ko.observable(null) | harpoonApi.CompetitionPurchase.qrcode
isAvailable | boolean | ko.observable(null) | harpoonApi.CompetitionPurchase.isAvailable
status | string | ko.observable(null) | harpoonApi.CompetitionPurchase.status
createdAt | date | ko.observable(null) | harpoonApi.CompetitionPurchase.createdAt
expiredAt | date | ko.observable(null) | harpoonApi.CompetitionPurchase.expiredAt
redeemedAt | date | ko.observable(null) | harpoonApi.CompetitionPurchase.redeemedAt
redeemedByTerminal | string | ko.observable(null) | harpoonApi.CompetitionPurchase.redeemedByTerminal
basePrice | number | ko.observable(null) | harpoonApi.CompetitionPurchase.basePrice
ticketPrice | number | ko.observable(null) | harpoonApi.CompetitionPurchase.ticketPrice
currency | string | ko.observable(null) | harpoonApi.CompetitionPurchase.currency
attendee | Customer | new CustomerModel() | harpoonApi.CompetitionPurchase.attendee
chanceCount | number | ko.observable(null) | harpoonApi.CompetitionPurchase.chanceCount
winner | CompetitionWinner | new CompetitionWinnerModel() | harpoonApi.CompetitionPurchase.winner
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CompetitionPurchaseModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
