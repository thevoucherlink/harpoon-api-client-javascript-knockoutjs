# OfferCheckoutMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
cartId | number | cannot | can | harpoonApi.OfferCheckout.cartId
url | string | cannot | can | harpoonApi.OfferCheckout.url
status | string | cannot | can | harpoonApi.OfferCheckout.status
