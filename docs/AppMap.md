# AppMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.App.id
internalId | number | cannot | can | Id used internally in Harpoon
vendorId | number | cannot | can | Vendor App owner
name | string | cannot | can | Name of the application
category | string | cannot | can | harpoonApi.App.category
description | string | cannot | can | harpoonApi.App.description
keywords | string | cannot | can | harpoonApi.App.keywords
copyright | string | cannot | can | harpoonApi.App.copyright
realm | string | cannot | can | harpoonApi.App.realm
bundle | string | cannot | can | Bundle Identifier of the application, com.{company name}.{project name}
urlScheme | string | cannot | can | URL Scheme used for deeplinking
brandFollowOffer | boolean | cannot | can | Customer need to follow the Brand to display the Offers outside of the Brand profile page
privacyUrl | string | cannot | can | URL linking to the Privacy page for the app
termsOfServiceUrl | string | cannot | can | URL linking to the Terms of Service page for the app
restrictionAge | number | cannot | can | Restrict the use of the app to just the customer with at least the value specified, 0 = no limit
storeAndroidUrl | string | cannot | can | URL linking to the Google Play page for the app
storeIosUrl | string | cannot | can | URL linking to the iTunes App Store page for the app
typeNewsFeed | string | cannot | can | News Feed source, nativeFeed/rss
clientSecret | string | cannot | can | harpoonApi.App.clientSecret
grantTypes | string | can | can | harpoonApi.App.grantTypes
scopes | string | can | can | harpoonApi.App.scopes
tokenType | string | cannot | can | harpoonApi.App.tokenType
anonymousAllowed | boolean | cannot | can | harpoonApi.App.anonymousAllowed
status | string | cannot | can | Status of the application, production/sandbox/disabled
created | date | cannot | can | harpoonApi.App.created
modified | date | cannot | can | harpoonApi.App.modified
styleOfferList | string | cannot | can | Size of the Offer List Item, big/small
appConfig | AppConfig | can | can | harpoonApi.App.appConfig
clientToken | string | cannot | can | harpoonApi.App.clientToken
multiConfigTitle | string | cannot | can | harpoonApi.App.multiConfigTitle
multiConfig | AppConfig | can | can | harpoonApi.App.multiConfig
rssFeeds | RssFeed | can | can | Property rssFeeds of harpoonApi.App describes a relationship hasMany harpoonApi.RssFeed
rssFeedGroups | RssFeedGroup | can | can | Property rssFeedGroups of harpoonApi.App describes a relationship hasMany harpoonApi.RssFeedGroup
radioStreams | RadioStream | can | can | Property radioStreams of harpoonApi.App describes a relationship hasMany harpoonApi.RadioStream
customers | Customer | can | can | Property customers of harpoonApi.App describes a relationship hasMany harpoonApi.Customer
appConfigs | AppConfig | can | can | Property appConfigs of harpoonApi.App describes a relationship hasMany harpoonApi.AppConfig
playlists | Playlist | can | can | Property playlists of harpoonApi.App describes a relationship hasMany harpoonApi.Playlist
apps | App | can | can | Property apps of harpoonApi.App describes a relationship hasMany harpoonApi.App
parent | object | cannot | can | Property parent of harpoonApi.App describes a relationship belongsTo harpoonApi.App
