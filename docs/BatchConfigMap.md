# BatchConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.BatchConfig.id
apiKey | string | cannot | can | harpoonApi.BatchConfig.apiKey
iOSApiKey | string | cannot | can | harpoonApi.BatchConfig.iOSApiKey
androidApiKey | string | cannot | can | harpoonApi.BatchConfig.androidApiKey
