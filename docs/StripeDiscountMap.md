# StripeDiscountMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
object | string | cannot | can | harpoonApi.StripeDiscount.object
customer | string | cannot | can | harpoonApi.StripeDiscount.customer
end | number | cannot | can | harpoonApi.StripeDiscount.end
start | number | cannot | can | harpoonApi.StripeDiscount.start
subscription | string | cannot | can | harpoonApi.StripeDiscount.subscription
coupon | StripeCoupon | can | can | harpoonApi.StripeDiscount.coupon
