# AwEventbookingEventTicketAttributeMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwEventbookingEventTicketAttribute.id
ticketId | Number | cannot | can | harpoonApi.AwEventbookingEventTicketAttribute.ticketId
storeId | Number | cannot | can | harpoonApi.AwEventbookingEventTicketAttribute.storeId
attributeCode | String | cannot | can | harpoonApi.AwEventbookingEventTicketAttribute.attributeCode
value | String | cannot | can | harpoonApi.AwEventbookingEventTicketAttribute.value
