# EventMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Event.id
basePrice | number | cannot | can | harpoonApi.Event.basePrice
attendee | EventAttendee | can | can | harpoonApi.Event.attendee
attendees | EventAttendee | can | can | harpoonApi.Event.attendees
attendeeCount | number | cannot | can | harpoonApi.Event.attendeeCount
isGoing | boolean | cannot | can | harpoonApi.Event.isGoing
ticket | EventTicket | can | can | harpoonApi.Event.ticket
tickets | EventTicket | can | can | harpoonApi.Event.tickets
termsConditions | string | cannot | can | harpoonApi.Event.termsConditions
facebook | FacebookEvent | can | can | harpoonApi.Event.facebook
connectFacebookId | string | cannot | can | harpoonApi.Event.connectFacebookId
