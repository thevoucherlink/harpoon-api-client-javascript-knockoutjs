# EmailDashCampaignFlowDeletedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowDeleted.brandName
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowDeleted.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowDeleted.campaignName
userName | string | cannot | can | harpoonApi.EmailDashCampaignFlowDeleted.userName
