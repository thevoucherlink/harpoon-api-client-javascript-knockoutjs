# EventPurchaseModel
## Usage
```javascript
var vm = EventPurchaseModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.EventPurchase.id
orderId | number | ko.observable(null) | harpoonApi.EventPurchase.orderId
name | string | ko.observable(null) | harpoonApi.EventPurchase.name
event | Event | new EventModel() | harpoonApi.EventPurchase.event
redemptionType | string | ko.observable(null) | harpoonApi.EventPurchase.redemptionType
unlockTime | number | ko.observable(null) | harpoonApi.EventPurchase.unlockTime
code | string | ko.observable(null) | harpoonApi.EventPurchase.code
qrcode | string | ko.observable(null) | harpoonApi.EventPurchase.qrcode
isAvailable | boolean | ko.observable(null) | harpoonApi.EventPurchase.isAvailable
status | string | ko.observable(null) | harpoonApi.EventPurchase.status
createdAt | date | ko.observable(null) | harpoonApi.EventPurchase.createdAt
expiredAt | date | ko.observable(null) | harpoonApi.EventPurchase.expiredAt
redeemedAt | date | ko.observable(null) | harpoonApi.EventPurchase.redeemedAt
redeemedByTerminal | string | ko.observable(null) | harpoonApi.EventPurchase.redeemedByTerminal
basePrice | number | ko.observable(null) | harpoonApi.EventPurchase.basePrice
ticketPrice | number | ko.observable(null) | harpoonApi.EventPurchase.ticketPrice
currency | string | ko.observable(null) | harpoonApi.EventPurchase.currency
attendee | Customer | new CustomerModel() | harpoonApi.EventPurchase.attendee
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | EventPurchaseModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
