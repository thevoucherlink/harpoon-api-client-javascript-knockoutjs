# AwEventbookingEventMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwEventbookingEvent.id
productId | Number | cannot | can | harpoonApi.AwEventbookingEvent.productId
isEnabled | Number | cannot | can | harpoonApi.AwEventbookingEvent.isEnabled
eventStartDate | Date | cannot | can | harpoonApi.AwEventbookingEvent.eventStartDate
eventEndDate | Date | cannot | can | harpoonApi.AwEventbookingEvent.eventEndDate
dayCountBeforeSendReminderLetter | Number | cannot | can | harpoonApi.AwEventbookingEvent.dayCountBeforeSendReminderLetter
isReminderSend | Number | cannot | can | harpoonApi.AwEventbookingEvent.isReminderSend
isTermsEnabled | Number | cannot | can | harpoonApi.AwEventbookingEvent.isTermsEnabled
generatePdfTickets | Number | cannot | can | harpoonApi.AwEventbookingEvent.generatePdfTickets
redeemRoles | String | cannot | can | harpoonApi.AwEventbookingEvent.redeemRoles
location | String | cannot | can | harpoonApi.AwEventbookingEvent.location
tickets | AwEventbookingEventTicket | can | can | Property tickets of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingEventTicket
attributes | AwEventbookingEventAttribute | can | can | Property attributes of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingEventAttribute
purchasedTickets | AwEventbookingTicket | can | can | Property purchasedTickets of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingTicket
