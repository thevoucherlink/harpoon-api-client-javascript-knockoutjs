# HarpoonHpublicBrandvenueModel
## Usage
```javascript
var vm = HarpoonHpublicBrandvenueModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.id
id | Number | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.id
brandId | Number | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.brandId
name | String | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.name
address | String | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.address
latitude | Number | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.latitude
longitude | Number | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.longitude
createdAt | Date | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.createdAt
updatedAt | Date | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.updatedAt
email | String | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.email
phone | String | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.phone
country | String | ko.observable(null) | harpoonApi.HarpoonHpublicBrandvenue.country
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | HarpoonHpublicBrandvenueModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
