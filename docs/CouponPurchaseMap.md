# CouponPurchaseMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CouponPurchase.id
orderId | number | cannot | can | harpoonApi.CouponPurchase.orderId
name | string | cannot | can | harpoonApi.CouponPurchase.name
code | CouponPurchaseCode | can | can | harpoonApi.CouponPurchase.code
isAvailable | boolean | cannot | can | harpoonApi.CouponPurchase.isAvailable
createdAt | date | cannot | can | harpoonApi.CouponPurchase.createdAt
expiredAt | date | cannot | can | harpoonApi.CouponPurchase.expiredAt
redeemedAt | date | cannot | can | harpoonApi.CouponPurchase.redeemedAt
redeemedByTerminal | string | cannot | can | harpoonApi.CouponPurchase.redeemedByTerminal
redemptionType | string | cannot | can | harpoonApi.CouponPurchase.redemptionType
status | string | cannot | can | harpoonApi.CouponPurchase.status
unlockTime | number | cannot | can | harpoonApi.CouponPurchase.unlockTime
