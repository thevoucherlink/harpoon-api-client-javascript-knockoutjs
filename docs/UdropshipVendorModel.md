# UdropshipVendorModel
## Usage
```javascript
var vm = UdropshipVendorModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.UdropshipVendor.id
vendorName | String | ko.observable(null) | harpoonApi.UdropshipVendor.vendorName
vendorAttn | String | ko.observable(null) | harpoonApi.UdropshipVendor.vendorAttn
email | String | ko.observable(null) | harpoonApi.UdropshipVendor.email
street | String | ko.observable(null) | harpoonApi.UdropshipVendor.street
city | String | ko.observable(null) | harpoonApi.UdropshipVendor.city
zip | String | ko.observable(null) | harpoonApi.UdropshipVendor.zip
countryId | String | ko.observable(null) | harpoonApi.UdropshipVendor.countryId
regionId | Number | ko.observable(null) | harpoonApi.UdropshipVendor.regionId
region | String | ko.observable(null) | harpoonApi.UdropshipVendor.region
telephone | String | ko.observable(null) | harpoonApi.UdropshipVendor.telephone
fax | String | ko.observable(null) | harpoonApi.UdropshipVendor.fax
status | Boolean | ko.observable(null) | harpoonApi.UdropshipVendor.status
password | String | ko.observable(null) | harpoonApi.UdropshipVendor.password
passwordHash | String | ko.observable(null) | harpoonApi.UdropshipVendor.passwordHash
passwordEnc | String | ko.observable(null) | harpoonApi.UdropshipVendor.passwordEnc
carrierCode | String | ko.observable(null) | harpoonApi.UdropshipVendor.carrierCode
notifyNewOrder | Number | ko.observable(null) | harpoonApi.UdropshipVendor.notifyNewOrder
labelType | String | ko.observable(null) | harpoonApi.UdropshipVendor.labelType
testMode | Number | ko.observable(null) | harpoonApi.UdropshipVendor.testMode
handlingFee | String | ko.observable(null) | harpoonApi.UdropshipVendor.handlingFee
upsShipperNumber | String | ko.observable(null) | harpoonApi.UdropshipVendor.upsShipperNumber
customDataCombined | String | ko.observable(null) | harpoonApi.UdropshipVendor.customDataCombined
customVarsCombined | String | ko.observable(null) | harpoonApi.UdropshipVendor.customVarsCombined
emailTemplate | Number | ko.observable(null) | harpoonApi.UdropshipVendor.emailTemplate
urlKey | String | ko.observable(null) | harpoonApi.UdropshipVendor.urlKey
randomHash | String | ko.observable(null) | harpoonApi.UdropshipVendor.randomHash
createdAt | Date | ko.observable(null) | harpoonApi.UdropshipVendor.createdAt
notifyLowstock | Number | ko.observable(null) | harpoonApi.UdropshipVendor.notifyLowstock
notifyLowstockQty | String | ko.observable(null) | harpoonApi.UdropshipVendor.notifyLowstockQty
useHandlingFee | Number | ko.observable(null) | harpoonApi.UdropshipVendor.useHandlingFee
useRatesFallback | Number | ko.observable(null) | harpoonApi.UdropshipVendor.useRatesFallback
allowShippingExtraCharge | Number | ko.observable(null) | harpoonApi.UdropshipVendor.allowShippingExtraCharge
defaultShippingExtraChargeSuffix | String | ko.observable(null) | harpoonApi.UdropshipVendor.defaultShippingExtraChargeSuffix
defaultShippingExtraChargeType | String | ko.observable(null) | harpoonApi.UdropshipVendor.defaultShippingExtraChargeType
defaultShippingExtraCharge | String | ko.observable(null) | harpoonApi.UdropshipVendor.defaultShippingExtraCharge
isExtraChargeShippingDefault | Number | ko.observable(null) | harpoonApi.UdropshipVendor.isExtraChargeShippingDefault
defaultShippingId | Number | ko.observable(null) | harpoonApi.UdropshipVendor.defaultShippingId
billingUseShipping | Number | ko.observable(null) | harpoonApi.UdropshipVendor.billingUseShipping
billingEmail | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingEmail
billingTelephone | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingTelephone
billingFax | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingFax
billingVendorAttn | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingVendorAttn
billingStreet | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingStreet
billingCity | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingCity
billingZip | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingZip
billingCountryId | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingCountryId
billingRegionId | Number | ko.observable(null) | harpoonApi.UdropshipVendor.billingRegionId
billingRegion | String | ko.observable(null) | harpoonApi.UdropshipVendor.billingRegion
subdomainLevel | Number | ko.observable(null) | harpoonApi.UdropshipVendor.subdomainLevel
updateStoreBaseUrl | Number | ko.observable(null) | harpoonApi.UdropshipVendor.updateStoreBaseUrl
confirmation | String | ko.observable(null) | harpoonApi.UdropshipVendor.confirmation
confirmationSent | Number | ko.observable(null) | harpoonApi.UdropshipVendor.confirmationSent
rejectReason | String | ko.observable(null) | harpoonApi.UdropshipVendor.rejectReason
backorderByAvailability | Number | ko.observable(null) | harpoonApi.UdropshipVendor.backorderByAvailability
useReservedQty | Number | ko.observable(null) | harpoonApi.UdropshipVendor.useReservedQty
tiercomRates | String | ko.observable(null) | harpoonApi.UdropshipVendor.tiercomRates
tiercomFixedRule | String | ko.observable(null) | harpoonApi.UdropshipVendor.tiercomFixedRule
tiercomFixedRates | String | ko.observable(null) | harpoonApi.UdropshipVendor.tiercomFixedRates
tiercomFixedCalcType | String | ko.observable(null) | harpoonApi.UdropshipVendor.tiercomFixedCalcType
tiershipRates | String | ko.observable(null) | harpoonApi.UdropshipVendor.tiershipRates
tiershipSimpleRates | String | ko.observable(null) | harpoonApi.UdropshipVendor.tiershipSimpleRates
tiershipUseV2Rates | Number | ko.observable(null) | harpoonApi.UdropshipVendor.tiershipUseV2Rates
vacationMode | Number | ko.observable(null) | harpoonApi.UdropshipVendor.vacationMode
vacationEnd | Date | ko.observable(null) | harpoonApi.UdropshipVendor.vacationEnd
vacationMessage | String | ko.observable(null) | harpoonApi.UdropshipVendor.vacationMessage
udmemberLimitProducts | Number | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberLimitProducts
udmemberProfileId | Number | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberProfileId
udmemberProfileRefid | String | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberProfileRefid
udmemberMembershipCode | String | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberMembershipCode
udmemberMembershipTitle | String | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberMembershipTitle
udmemberBillingType | String | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberBillingType
udmemberHistory | String | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberHistory
udmemberProfileSyncOff | Number | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberProfileSyncOff
udmemberAllowMicrosite | Number | ko.observable(null) | harpoonApi.UdropshipVendor.udmemberAllowMicrosite
udprodTemplateSku | String | ko.observable(null) | harpoonApi.UdropshipVendor.udprodTemplateSku
vendorTaxClass | String | ko.observable(null) | harpoonApi.UdropshipVendor.vendorTaxClass
catalogProducts | CatalogProduct | ko.observableArray([]) | Property catalogProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.CatalogProduct
vendorLocations | HarpoonHpublicBrandvenue | ko.observableArray([]) | Property vendorLocations of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicBrandvenue
config | HarpoonHpublicVendorconfig | ko.observable(null) | Property config of harpoonApi.UdropshipVendor describes a relationship hasOne harpoonApi.HarpoonHpublicVendorconfig
partners | UdropshipVendor | ko.observableArray([]) | Property partners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendor
udropshipVendorProducts | UdropshipVendorProduct | ko.observableArray([]) | Property udropshipVendorProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendorProduct
harpoonHpublicApplicationpartners | HarpoonHpublicApplicationpartner | ko.observableArray([]) | Property harpoonHpublicApplicationpartners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicApplicationpartner
harpoonHpublicv12VendorAppCategories | HarpoonHpublicv12VendorAppCategory | ko.observableArray([]) | Property harpoonHpublicv12VendorAppCategories of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicv12VendorAppCategory
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | UdropshipVendorModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
