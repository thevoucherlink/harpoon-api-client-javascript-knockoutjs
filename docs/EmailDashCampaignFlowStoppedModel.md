# EmailDashCampaignFlowStoppedModel
## Usage
```javascript
var vm = EmailDashCampaignFlowStoppedModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
brandEmail | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowStopped.brandEmail
brandName | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowStopped.brandName
campaignImage | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowStopped.campaignImage
brandTel | number | ko.observable(null) | harpoonApi.EmailDashCampaignFlowStopped.brandTel
campaignName | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowStopped.campaignName
reDirectLink | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowStopped.reDirectLink
userName | string | ko.observable(null) | harpoonApi.EmailDashCampaignFlowStopped.userName
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | EmailDashCampaignFlowStoppedModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
