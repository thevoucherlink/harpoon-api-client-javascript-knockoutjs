# EmailDashCampaignFlowChangesAcceptedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowChangesAccepted.brandName
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowChangesAccepted.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowChangesAccepted.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowChangesAccepted.reDirectLink
senderFirstName | string | cannot | can | harpoonApi.EmailDashCampaignFlowChangesAccepted.senderFirstName
