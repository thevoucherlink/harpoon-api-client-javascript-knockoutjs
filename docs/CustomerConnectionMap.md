# CustomerConnectionMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CustomerConnection.id
facebook | CustomerConnectionFacebook | can | can | harpoonApi.CustomerConnection.facebook
twitter | CustomerConnectionTwitter | can | can | harpoonApi.CustomerConnection.twitter
