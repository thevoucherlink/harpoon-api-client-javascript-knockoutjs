# CompetitionModel
## Usage
```javascript
var vm = CompetitionModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.Competition.id
basePrice | number | ko.observable(null) | harpoonApi.Competition.basePrice
attendee | CompetitionAttendee | ko.observableArray([]) | harpoonApi.Competition.attendee
attendees | CompetitionAttendee | ko.observableArray([]) | harpoonApi.Competition.attendees
attendeeCount | number | ko.observable(null) | harpoonApi.Competition.attendeeCount
hasJoined | boolean | ko.observable(null) | harpoonApi.Competition.hasJoined
chance | CompetitionChance | ko.observableArray([]) | harpoonApi.Competition.chance
chanceCount | number | ko.observable(null) | harpoonApi.Competition.chanceCount
isWinner | boolean | ko.observable(null) | harpoonApi.Competition.isWinner
facebook | FacebookEvent | new FacebookEventModel() | harpoonApi.Competition.facebook
chances | CompetitionChance | ko.observableArray([]) | harpoonApi.Competition.chances
earnMoreChancesURL | string | ko.observable(null) | harpoonApi.Competition.earnMoreChancesURL
type | Category | new CategoryModel() | harpoonApi.Competition.type
earnMoreChances | boolean | ko.observable(null) | harpoonApi.Competition.earnMoreChances
competitionQuestion | string | ko.observable(null) | harpoonApi.Competition.competitionQuestion
competitionAnswer | string | ko.observable(null) | harpoonApi.Competition.competitionAnswer
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CompetitionModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
