# HOAuthClientApplicationModel
## Usage
```javascript
var vm = HOAuthClientApplicationModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.id
clientType | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.clientType
redirectURIs | string | ko.observableArray([]) | harpoonApi.HOAuthClientApplication.redirectURIs
tokenEndpointAuthMethod | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.tokenEndpointAuthMethod
grantTypes | object | ko.observableArray([]) | harpoonApi.HOAuthClientApplication.grantTypes
responseTypes | object | ko.observableArray([]) | harpoonApi.HOAuthClientApplication.responseTypes
tokenType | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.tokenType
clientSecret | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.clientSecret
clientName | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.clientName
clientURI | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.clientURI
logoURI | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.logoURI
scopes | string | ko.observableArray([]) | harpoonApi.HOAuthClientApplication.scopes
contacts | string | ko.observableArray([]) | harpoonApi.HOAuthClientApplication.contacts
tosURI | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.tosURI
policyURI | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.policyURI
jwksURI | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.jwksURI
jwks | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.jwks
softwareId | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.softwareId
softwareVersion | string | ko.observable(null) | harpoonApi.HOAuthClientApplication.softwareVersion
callbackUrls | boolean | ko.observable(null) | harpoonApi.HOAuthClientApplication.callbackUrls
permissions | boolean | ko.observable(null) | harpoonApi.HOAuthClientApplication.permissions
authenticationEnabled | boolean | ko.observable(null) | harpoonApi.HOAuthClientApplication.authenticationEnabled
anonymousAllowed | boolean | ko.observable(null) | harpoonApi.HOAuthClientApplication.anonymousAllowed
authenticationSchemes | boolean | ko.observable(null) | harpoonApi.HOAuthClientApplication.authenticationSchemes
icon | boolean | ko.observable(null) | harpoonApi.HOAuthClientApplication.icon
url | boolean | ko.observable(null) | harpoonApi.HOAuthClientApplication.url
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | HOAuthClientApplicationModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
