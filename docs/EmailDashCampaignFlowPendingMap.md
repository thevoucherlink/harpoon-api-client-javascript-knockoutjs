# EmailDashCampaignFlowPendingMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandEmail | string | cannot | can | harpoonApi.EmailDashCampaignFlowPending.brandEmail
brandName | string | cannot | can | harpoonApi.EmailDashCampaignFlowPending.brandName
brandTel | number | cannot | can | harpoonApi.EmailDashCampaignFlowPending.brandTel
campaignImage | string | cannot | can | harpoonApi.EmailDashCampaignFlowPending.campaignImage
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignFlowPending.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignFlowPending.reDirectLink
