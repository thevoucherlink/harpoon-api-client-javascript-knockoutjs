# PlayerConfigModel
## Usage
```javascript
var vm = PlayerConfigModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.PlayerConfig.id
mute | boolean | ko.observable(null) | Available on the following SDKs: JS, iOS
autostart | boolean | ko.observable(null) | Available on the following SDKs: JS, iOS, Android
repeat | boolean | ko.observable(null) | Available on the following SDKs: JS, iOS, Android
controls | boolean | ko.observable(null) | Available on the following SDKs: JS, iOS, Android
visualPlaylist | boolean | ko.observable(null) | Available on the following SDKs: JS (as visualplaylist)
displayTitle | boolean | ko.observable(null) | Available on the following SDKs: JS (as displaytitle)
displayDescription | boolean | ko.observable(null) | Available on the following SDKs: JS (as displaydescription)
stretching | string | ko.observable(null) | Available on the following SDKs: JS, iOS, Android (as stretch)
hlshtml | boolean | ko.observable(null) | Available on the following SDKs: JS
primary | string | ko.observable(null) | Available on the following SDKs: JS
flashPlayer | string | ko.observable(null) | Available on the following SDKs: JS (as flashplayer)
baseSkinPath | string | ko.observable(null) | Available on the following SDKs: JS (as base)
preload | string | ko.observable(null) | Available on the following SDKs: JS
playerAdConfig | object | ko.observable(null) | Available on the following SDKs: JS, iOS, Android
playerCaptionConfig | object | ko.observable(null) | Available on the following SDKs: JS, iOS, Android
skinName | string | ko.observable(null) | Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin)
skinCss | string | ko.observable(null) | Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin)
skinActive | string | ko.observable(null) | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS
skinInactive | string | ko.observable(null) | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS
skinBackground | string | ko.observable(null) | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | PlayerConfigModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
