# UdropshipVendorMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.UdropshipVendor.id
vendorName | String | cannot | can | harpoonApi.UdropshipVendor.vendorName
vendorAttn | String | cannot | can | harpoonApi.UdropshipVendor.vendorAttn
email | String | cannot | can | harpoonApi.UdropshipVendor.email
street | String | cannot | can | harpoonApi.UdropshipVendor.street
city | String | cannot | can | harpoonApi.UdropshipVendor.city
zip | String | cannot | can | harpoonApi.UdropshipVendor.zip
countryId | String | cannot | can | harpoonApi.UdropshipVendor.countryId
regionId | Number | cannot | can | harpoonApi.UdropshipVendor.regionId
region | String | cannot | can | harpoonApi.UdropshipVendor.region
telephone | String | cannot | can | harpoonApi.UdropshipVendor.telephone
fax | String | cannot | can | harpoonApi.UdropshipVendor.fax
status | Boolean | cannot | can | harpoonApi.UdropshipVendor.status
password | String | cannot | can | harpoonApi.UdropshipVendor.password
passwordHash | String | cannot | can | harpoonApi.UdropshipVendor.passwordHash
passwordEnc | String | cannot | can | harpoonApi.UdropshipVendor.passwordEnc
carrierCode | String | cannot | can | harpoonApi.UdropshipVendor.carrierCode
notifyNewOrder | Number | cannot | can | harpoonApi.UdropshipVendor.notifyNewOrder
labelType | String | cannot | can | harpoonApi.UdropshipVendor.labelType
testMode | Number | cannot | can | harpoonApi.UdropshipVendor.testMode
handlingFee | String | cannot | can | harpoonApi.UdropshipVendor.handlingFee
upsShipperNumber | String | cannot | can | harpoonApi.UdropshipVendor.upsShipperNumber
customDataCombined | String | cannot | can | harpoonApi.UdropshipVendor.customDataCombined
customVarsCombined | String | cannot | can | harpoonApi.UdropshipVendor.customVarsCombined
emailTemplate | Number | cannot | can | harpoonApi.UdropshipVendor.emailTemplate
urlKey | String | cannot | can | harpoonApi.UdropshipVendor.urlKey
randomHash | String | cannot | can | harpoonApi.UdropshipVendor.randomHash
createdAt | Date | cannot | can | harpoonApi.UdropshipVendor.createdAt
notifyLowstock | Number | cannot | can | harpoonApi.UdropshipVendor.notifyLowstock
notifyLowstockQty | String | cannot | can | harpoonApi.UdropshipVendor.notifyLowstockQty
useHandlingFee | Number | cannot | can | harpoonApi.UdropshipVendor.useHandlingFee
useRatesFallback | Number | cannot | can | harpoonApi.UdropshipVendor.useRatesFallback
allowShippingExtraCharge | Number | cannot | can | harpoonApi.UdropshipVendor.allowShippingExtraCharge
defaultShippingExtraChargeSuffix | String | cannot | can | harpoonApi.UdropshipVendor.defaultShippingExtraChargeSuffix
defaultShippingExtraChargeType | String | cannot | can | harpoonApi.UdropshipVendor.defaultShippingExtraChargeType
defaultShippingExtraCharge | String | cannot | can | harpoonApi.UdropshipVendor.defaultShippingExtraCharge
isExtraChargeShippingDefault | Number | cannot | can | harpoonApi.UdropshipVendor.isExtraChargeShippingDefault
defaultShippingId | Number | cannot | can | harpoonApi.UdropshipVendor.defaultShippingId
billingUseShipping | Number | cannot | can | harpoonApi.UdropshipVendor.billingUseShipping
billingEmail | String | cannot | can | harpoonApi.UdropshipVendor.billingEmail
billingTelephone | String | cannot | can | harpoonApi.UdropshipVendor.billingTelephone
billingFax | String | cannot | can | harpoonApi.UdropshipVendor.billingFax
billingVendorAttn | String | cannot | can | harpoonApi.UdropshipVendor.billingVendorAttn
billingStreet | String | cannot | can | harpoonApi.UdropshipVendor.billingStreet
billingCity | String | cannot | can | harpoonApi.UdropshipVendor.billingCity
billingZip | String | cannot | can | harpoonApi.UdropshipVendor.billingZip
billingCountryId | String | cannot | can | harpoonApi.UdropshipVendor.billingCountryId
billingRegionId | Number | cannot | can | harpoonApi.UdropshipVendor.billingRegionId
billingRegion | String | cannot | can | harpoonApi.UdropshipVendor.billingRegion
subdomainLevel | Number | cannot | can | harpoonApi.UdropshipVendor.subdomainLevel
updateStoreBaseUrl | Number | cannot | can | harpoonApi.UdropshipVendor.updateStoreBaseUrl
confirmation | String | cannot | can | harpoonApi.UdropshipVendor.confirmation
confirmationSent | Number | cannot | can | harpoonApi.UdropshipVendor.confirmationSent
rejectReason | String | cannot | can | harpoonApi.UdropshipVendor.rejectReason
backorderByAvailability | Number | cannot | can | harpoonApi.UdropshipVendor.backorderByAvailability
useReservedQty | Number | cannot | can | harpoonApi.UdropshipVendor.useReservedQty
tiercomRates | String | cannot | can | harpoonApi.UdropshipVendor.tiercomRates
tiercomFixedRule | String | cannot | can | harpoonApi.UdropshipVendor.tiercomFixedRule
tiercomFixedRates | String | cannot | can | harpoonApi.UdropshipVendor.tiercomFixedRates
tiercomFixedCalcType | String | cannot | can | harpoonApi.UdropshipVendor.tiercomFixedCalcType
tiershipRates | String | cannot | can | harpoonApi.UdropshipVendor.tiershipRates
tiershipSimpleRates | String | cannot | can | harpoonApi.UdropshipVendor.tiershipSimpleRates
tiershipUseV2Rates | Number | cannot | can | harpoonApi.UdropshipVendor.tiershipUseV2Rates
vacationMode | Number | cannot | can | harpoonApi.UdropshipVendor.vacationMode
vacationEnd | Date | cannot | can | harpoonApi.UdropshipVendor.vacationEnd
vacationMessage | String | cannot | can | harpoonApi.UdropshipVendor.vacationMessage
udmemberLimitProducts | Number | cannot | can | harpoonApi.UdropshipVendor.udmemberLimitProducts
udmemberProfileId | Number | cannot | can | harpoonApi.UdropshipVendor.udmemberProfileId
udmemberProfileRefid | String | cannot | can | harpoonApi.UdropshipVendor.udmemberProfileRefid
udmemberMembershipCode | String | cannot | can | harpoonApi.UdropshipVendor.udmemberMembershipCode
udmemberMembershipTitle | String | cannot | can | harpoonApi.UdropshipVendor.udmemberMembershipTitle
udmemberBillingType | String | cannot | can | harpoonApi.UdropshipVendor.udmemberBillingType
udmemberHistory | String | cannot | can | harpoonApi.UdropshipVendor.udmemberHistory
udmemberProfileSyncOff | Number | cannot | can | harpoonApi.UdropshipVendor.udmemberProfileSyncOff
udmemberAllowMicrosite | Number | cannot | can | harpoonApi.UdropshipVendor.udmemberAllowMicrosite
udprodTemplateSku | String | cannot | can | harpoonApi.UdropshipVendor.udprodTemplateSku
vendorTaxClass | String | cannot | can | harpoonApi.UdropshipVendor.vendorTaxClass
catalogProducts | CatalogProduct | can | can | Property catalogProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.CatalogProduct
vendorLocations | HarpoonHpublicBrandvenue | can | can | Property vendorLocations of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicBrandvenue
config | HarpoonHpublicVendorconfig | can | can | Property config of harpoonApi.UdropshipVendor describes a relationship hasOne harpoonApi.HarpoonHpublicVendorconfig
partners | UdropshipVendor | can | can | Property partners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendor
udropshipVendorProducts | UdropshipVendorProduct | can | can | Property udropshipVendorProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendorProduct
harpoonHpublicApplicationpartners | HarpoonHpublicApplicationpartner | can | can | Property harpoonHpublicApplicationpartners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicApplicationpartner
harpoonHpublicv12VendorAppCategories | HarpoonHpublicv12VendorAppCategory | can | can | Property harpoonHpublicv12VendorAppCategories of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicv12VendorAppCategory
