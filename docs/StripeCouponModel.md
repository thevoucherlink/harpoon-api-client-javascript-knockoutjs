# StripeCouponModel
## Usage
```javascript
var vm = StripeCouponModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
object | string | ko.observable(null) | harpoonApi.StripeCoupon.object
amountOff | number | ko.observable(null) | harpoonApi.StripeCoupon.amountOff
created | number | ko.observable(null) | harpoonApi.StripeCoupon.created
currency | string | ko.observable(null) | harpoonApi.StripeCoupon.currency
durationInMonths | number | ko.observable(null) | harpoonApi.StripeCoupon.durationInMonths
livemode | boolean | ko.observable(null) | harpoonApi.StripeCoupon.livemode
maxRedemptions | number | ko.observable(null) | harpoonApi.StripeCoupon.maxRedemptions
metadata | string | ko.observable(null) | harpoonApi.StripeCoupon.metadata
percentOff | number | ko.observable(null) | harpoonApi.StripeCoupon.percentOff
redeemBy | number | ko.observable(null) | harpoonApi.StripeCoupon.redeemBy
timesRedeemed | number | ko.observable(null) | harpoonApi.StripeCoupon.timesRedeemed
valid | boolean | ko.observable(null) | harpoonApi.StripeCoupon.valid
duration | string | ko.observable(null) | harpoonApi.StripeCoupon.duration
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | StripeCouponModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
