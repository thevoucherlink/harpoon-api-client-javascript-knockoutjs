# PlaylistMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.Playlist.id
name | string | cannot | can | Playlist name
shortDescription | string | cannot | can | Playlist short description
image | string | cannot | can | Playlist image
isMain | boolean | cannot | can | If Playlist is included in Main
playlistItems | PlaylistItem | can | can | Property playlistItems of harpoonApi.Playlist describes a relationship hasMany harpoonApi.PlaylistItem
