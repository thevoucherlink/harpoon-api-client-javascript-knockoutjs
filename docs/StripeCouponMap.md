# StripeCouponMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
object | string | cannot | can | harpoonApi.StripeCoupon.object
amountOff | number | cannot | can | harpoonApi.StripeCoupon.amountOff
created | number | cannot | can | harpoonApi.StripeCoupon.created
currency | string | cannot | can | harpoonApi.StripeCoupon.currency
durationInMonths | number | cannot | can | harpoonApi.StripeCoupon.durationInMonths
livemode | boolean | cannot | can | harpoonApi.StripeCoupon.livemode
maxRedemptions | number | cannot | can | harpoonApi.StripeCoupon.maxRedemptions
metadata | string | cannot | can | harpoonApi.StripeCoupon.metadata
percentOff | number | cannot | can | harpoonApi.StripeCoupon.percentOff
redeemBy | number | cannot | can | harpoonApi.StripeCoupon.redeemBy
timesRedeemed | number | cannot | can | harpoonApi.StripeCoupon.timesRedeemed
valid | boolean | cannot | can | harpoonApi.StripeCoupon.valid
duration | string | cannot | can | harpoonApi.StripeCoupon.duration
