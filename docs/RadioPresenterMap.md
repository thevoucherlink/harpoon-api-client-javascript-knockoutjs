# RadioPresenterMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.RadioPresenter.id
name | string | cannot | can | harpoonApi.RadioPresenter.name
image | string | cannot | can | harpoonApi.RadioPresenter.image
contact | Contact | can | can | Contacts for this presenter
radioShows | RadioShow | can | can | Property radioShows of harpoonApi.RadioPresenter describes a relationship hasMany harpoonApi.RadioShow
