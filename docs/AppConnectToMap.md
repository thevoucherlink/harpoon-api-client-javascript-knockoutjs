# AppConnectToMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.AppConnectTo.id
token | string | cannot | can | harpoonApi.AppConnectTo.token
heading | string | cannot | can | harpoonApi.AppConnectTo.heading
description | string | cannot | can | harpoonApi.AppConnectTo.description
link | string | cannot | can | harpoonApi.AppConnectTo.link
