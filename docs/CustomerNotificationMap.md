# CustomerNotificationMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.CustomerNotification.id
message | string | cannot | can | harpoonApi.CustomerNotification.message
cover | string | cannot | can | harpoonApi.CustomerNotification.cover
coverUpload | MagentoImageUpload | can | can | harpoonApi.CustomerNotification.coverUpload
from | NotificationFrom | can | can | harpoonApi.CustomerNotification.from
related | NotificationRelated | can | can | harpoonApi.CustomerNotification.related
link | string | cannot | can | harpoonApi.CustomerNotification.link
actionCode | string | cannot | can | harpoonApi.CustomerNotification.actionCode
status | string | cannot | can | harpoonApi.CustomerNotification.status
sentAt | date | cannot | can | harpoonApi.CustomerNotification.sentAt
