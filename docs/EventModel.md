# EventModel
## Usage
```javascript
var vm = EventModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | string | ko.observable(null) | harpoonApi.Event.id
basePrice | number | ko.observable(null) | harpoonApi.Event.basePrice
attendee | EventAttendee | ko.observableArray([]) | harpoonApi.Event.attendee
attendees | EventAttendee | ko.observableArray([]) | harpoonApi.Event.attendees
attendeeCount | number | ko.observable(null) | harpoonApi.Event.attendeeCount
isGoing | boolean | ko.observable(null) | harpoonApi.Event.isGoing
ticket | EventTicket | ko.observableArray([]) | harpoonApi.Event.ticket
tickets | EventTicket | ko.observableArray([]) | harpoonApi.Event.tickets
termsConditions | string | ko.observable(null) | harpoonApi.Event.termsConditions
facebook | FacebookEvent | new FacebookEventModel() | harpoonApi.Event.facebook
connectFacebookId | string | ko.observable(null) | harpoonApi.Event.connectFacebookId
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | EventModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
