# PushWooshConfigMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.PushWooshConfig.appName
appId | string | cannot | can | harpoonApi.PushWooshConfig.appId
appToken | string | cannot | can | harpoonApi.PushWooshConfig.appToken
