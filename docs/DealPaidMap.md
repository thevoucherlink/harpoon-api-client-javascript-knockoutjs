# DealPaidMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | string | cannot | can | harpoonApi.DealPaid.id
product | Product | can | can | harpoonApi.DealPaid.product
