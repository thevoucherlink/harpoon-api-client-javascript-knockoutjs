# AwEventbookingTicketMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
id | Number | cannot | can | harpoonApi.AwEventbookingTicket.id
eventTicketId | Number | cannot | can | harpoonApi.AwEventbookingTicket.eventTicketId
orderItemId | Number | cannot | can | harpoonApi.AwEventbookingTicket.orderItemId
code | String | cannot | can | harpoonApi.AwEventbookingTicket.code
redeemed | Number | cannot | can | harpoonApi.AwEventbookingTicket.redeemed
paymentStatus | Number | cannot | can | harpoonApi.AwEventbookingTicket.paymentStatus
createdAt | Date | cannot | can | harpoonApi.AwEventbookingTicket.createdAt
redeemedAt | Date | cannot | can | harpoonApi.AwEventbookingTicket.redeemedAt
redeemedByTerminal | String | cannot | can | harpoonApi.AwEventbookingTicket.redeemedByTerminal
fee | String | cannot | can | harpoonApi.AwEventbookingTicket.fee
isPassOnFee | Number | cannot | can | harpoonApi.AwEventbookingTicket.isPassOnFee
competitionwinnerId | Number | cannot | can | harpoonApi.AwEventbookingTicket.competitionwinnerId
