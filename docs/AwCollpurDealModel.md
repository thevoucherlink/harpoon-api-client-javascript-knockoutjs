# AwCollpurDealModel
## Usage
```javascript
var vm = AwCollpurDealModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.id
productId | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.productId
productName | String | ko.observable(null) | harpoonApi.AwCollpurDeal.productName
storeIds | String | ko.observable(null) | harpoonApi.AwCollpurDeal.storeIds
isActive | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.isActive
isSuccess | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.isSuccess
closeState | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.closeState
qtyToReachDeal | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.qtyToReachDeal
purchasesLeft | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.purchasesLeft
maximumAllowedPurchases | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.maximumAllowedPurchases
availableFrom | Date | ko.observable(null) | harpoonApi.AwCollpurDeal.availableFrom
availableTo | Date | ko.observable(null) | harpoonApi.AwCollpurDeal.availableTo
price | String | ko.observable(null) | harpoonApi.AwCollpurDeal.price
autoClose | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.autoClose
name | String | ko.observable(null) | harpoonApi.AwCollpurDeal.name
description | String | ko.observable(null) | harpoonApi.AwCollpurDeal.description
fullDescription | String | ko.observable(null) | harpoonApi.AwCollpurDeal.fullDescription
dealImage | String | ko.observable(null) | harpoonApi.AwCollpurDeal.dealImage
isFeatured | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.isFeatured
enableCoupons | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.enableCoupons
couponPrefix | String | ko.observable(null) | harpoonApi.AwCollpurDeal.couponPrefix
couponExpireAfterDays | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.couponExpireAfterDays
expiredFlag | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.expiredFlag
sentBeforeFlag | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.sentBeforeFlag
isSuccessedFlag | Number | ko.observable(null) | harpoonApi.AwCollpurDeal.isSuccessedFlag
awCollpurDealPurchases | AwCollpurDealPurchases | ko.observableArray([]) | Property awCollpurDealPurchases of harpoonApi.AwCollpurDeal describes a relationship hasMany harpoonApi.AwCollpurDealPurchases
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | AwCollpurDealModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
