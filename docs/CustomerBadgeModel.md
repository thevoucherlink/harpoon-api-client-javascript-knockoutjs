# CustomerBadgeModel
## Usage
```javascript
var vm = CustomerBadgeModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
offer | number | ko.observable(null) | harpoonApi.CustomerBadge.offer
event | number | ko.observable(null) | harpoonApi.CustomerBadge.event
dealAll | number | ko.observable(null) | harpoonApi.CustomerBadge.dealAll
dealCoupon | number | ko.observable(null) | harpoonApi.CustomerBadge.dealCoupon
dealSimple | number | ko.observable(null) | harpoonApi.CustomerBadge.dealSimple
dealGroup | number | ko.observable(null) | harpoonApi.CustomerBadge.dealGroup
competition | number | ko.observable(null) | harpoonApi.CustomerBadge.competition
walletOfferAvailable | number | ko.observable(null) | harpoonApi.CustomerBadge.walletOfferAvailable
walletOfferNew | number | ko.observable(null) | harpoonApi.CustomerBadge.walletOfferNew
walletOfferExpiring | number | ko.observable(null) | harpoonApi.CustomerBadge.walletOfferExpiring
walletOfferExpired | number | ko.observable(null) | harpoonApi.CustomerBadge.walletOfferExpired
notificationUnread | number | ko.observable(null) | harpoonApi.CustomerBadge.notificationUnread
brandFeed | number | ko.observable(null) | harpoonApi.CustomerBadge.brandFeed
brand | number | ko.observable(null) | harpoonApi.CustomerBadge.brand
brandActivity | number | ko.observable(null) | harpoonApi.CustomerBadge.brandActivity
all | number | ko.observable(null) | harpoonApi.CustomerBadge.all
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | CustomerBadgeModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
