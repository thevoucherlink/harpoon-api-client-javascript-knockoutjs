# EmailDashCampaignInvitationAcceptedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
brandName | string | cannot | can | harpoonApi.EmailDashCampaignInvitationAccepted.brandName
campaignName | string | cannot | can | harpoonApi.EmailDashCampaignInvitationAccepted.campaignName
reDirectLink | string | cannot | can | harpoonApi.EmailDashCampaignInvitationAccepted.reDirectLink
userName | string | cannot | can | harpoonApi.EmailDashCampaignInvitationAccepted.userName
