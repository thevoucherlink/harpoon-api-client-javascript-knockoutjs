# CheckoutAgreementMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
name | String | cannot | can | harpoonApi.CheckoutAgreement.name
contentHeight | String | cannot | can | harpoonApi.CheckoutAgreement.contentHeight
checkboxText | String | cannot | can | harpoonApi.CheckoutAgreement.checkboxText
isActive | Number | cannot | can | harpoonApi.CheckoutAgreement.isActive
isHtml | Number | cannot | can | harpoonApi.CheckoutAgreement.isHtml
id | Number | cannot | can | harpoonApi.CheckoutAgreement.id
content | String | cannot | can | harpoonApi.CheckoutAgreement.content
