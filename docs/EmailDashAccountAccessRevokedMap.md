# EmailDashAccountAccessRevokedMap
## Properties
Name | Type | Create | Update | Description
------------ | ------------- | ------------- | ------------- | -------------
appName | string | cannot | can | harpoonApi.EmailDashAccountAccessRevoked.appName
brandEmail | string | cannot | can | harpoonApi.EmailDashAccountAccessRevoked.brandEmail
brandLogo | string | cannot | can | harpoonApi.EmailDashAccountAccessRevoked.brandLogo
brandName | string | cannot | can | harpoonApi.EmailDashAccountAccessRevoked.brandName
brandTel | number | cannot | can | harpoonApi.EmailDashAccountAccessRevoked.brandTel
userName | string | cannot | can | harpoonApi.EmailDashAccountAccessRevoked.userName
