# UdropshipVendorProductModel
## Usage
```javascript
var vm = UdropshipVendorProductModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
id | Number | ko.observable(null) | harpoonApi.UdropshipVendorProduct.id
vendorId | Number | ko.observable(null) | harpoonApi.UdropshipVendorProduct.vendorId
productId | Number | ko.observable(null) | harpoonApi.UdropshipVendorProduct.productId
priority | Number | ko.observable(null) | harpoonApi.UdropshipVendorProduct.priority
carrierCode | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.carrierCode
vendorSku | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.vendorSku
vendorCost | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.vendorCost
stockQty | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.stockQty
backorders | Number | ko.observable(null) | harpoonApi.UdropshipVendorProduct.backorders
shippingPrice | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.shippingPrice
status | Number | ko.observable(null) | harpoonApi.UdropshipVendorProduct.status
reservedQty | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.reservedQty
availState | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.availState
availDate | Date | ko.observable(null) | harpoonApi.UdropshipVendorProduct.availDate
relationshipStatus | String | ko.observable(null) | harpoonApi.UdropshipVendorProduct.relationshipStatus
udropshipVendor | object | ko.observable(null) | Property udropshipVendor of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.UdropshipVendor
catalogProduct | object | ko.observable(null) | Property catalogProduct of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.CatalogProduct
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | UdropshipVendorProductModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
