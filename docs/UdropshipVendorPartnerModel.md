# UdropshipVendorPartnerModel
## Usage
```javascript
var vm = UdropshipVendorPartnerModel();
vm.update({"id": 123});
```
## Observable Properties
Name | Type | Observable Type | Description
------------ | ------------- | ------------- | -------------
status | String | ko.observable(null) | harpoonApi.UdropshipVendorPartner.status
invitedEmail | String | ko.observable(null) | harpoonApi.UdropshipVendorPartner.invitedEmail
invitedAt | Date | ko.observable(null) | harpoonApi.UdropshipVendorPartner.invitedAt
acceptedAt | Date | ko.observable(null) | harpoonApi.UdropshipVendorPartner.acceptedAt
createdAt | Date | ko.observable(null) | harpoonApi.UdropshipVendorPartner.createdAt
updatedAt | Date | ko.observable(null) | harpoonApi.UdropshipVendorPartner.updatedAt
configList | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configList
configFeed | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configFeed
configNeedFollow | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configNeedFollow
configAcceptEvent | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configAcceptEvent
configAcceptCoupon | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configAcceptCoupon
configAcceptDealsimple | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configAcceptDealsimple
configAcceptDealgroup | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configAcceptDealgroup
configAcceptNotificationpush | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configAcceptNotificationpush
configAcceptNotificationbeacon | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.configAcceptNotificationbeacon
isOwner | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.isOwner
vendorId | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.vendorId
partnerId | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.partnerId
id | Number | ko.observable(null) | harpoonApi.UdropshipVendorPartner.id
udropshipVendor | object | ko.observable(null) | Property udropshipVendor of harpoonApi.UdropshipVendorPartner describes a relationship belongsTo harpoonApi.UdropshipVendor
## Constant Properties
Name | Type | Description
------------ | ------------- | -------------
\_\_map\_\_ | object | UdropshipVendorPartnerModel map for Knockout mapping
\_\_properties\_default_value\_\_ | object |  Default values for model properties name
\_\_relational\_properties\_\_ | array | List of model properties name used for relationships with other models
\_\_ignored_properties\_\_ | array | List of model properties name to be ignored during the export
## Methods
Name | Arguments | Return | Description
------------ | ------------- | ------------- | -------------
update | data | undefined | Update model instance
getJSObject | | object | Turn model instance into JS object
getJSONString | | string | Turn model instance into JSON object
getRelationalProperties | | [string] | Get model properties name used for relationships with other models
getIgnoredProperties | | [string] | Get model properties name to be ignored during the export
getPropertyDefaultValue | propertyName | mixed | Get default value for model property name
setPropertyToDefault | propertyName | undefined | Reset property default value
setPropertiesToDefault | | undefined | Reset all properties to default valye
