/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CustomerFollower Map
 * @type {object}
 */
var CustomerFollowerMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CustomerFollower.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CustomerFollowerMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerFollowerModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CustomerFollowerMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerFollower.customerId
	 * @type {number}
	 */
	"customerId": {
		"create": function(options){
			Console.log("CustomerFollowerMap - customerId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerFollowerModel.customerId in during mapping");
		},
		"update": function(options){
			Console.log("CustomerFollowerMap - customerId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customerId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerFollower.followingId
	 * @type {number}
	 */
	"followingId": {
		"create": function(options){
			Console.log("CustomerFollowerMap - followingId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerFollowerModel.followingId in during mapping");
		},
		"update": function(options){
			Console.log("CustomerFollowerMap - followingId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("followingId");
			}
			return options.target;
		}
	},
	/**
	 * Property follower of harpoonApi.CustomerFollower describes a relationship belongsTo harpoonApi.Customer
	 * @type {object}
	 */
	"follower": {
		"create": function(options){
			Console.log("CustomerFollowerMap - follower - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerFollowerModel.follower in during mapping");
		},
		"update": function(options){
			Console.log("CustomerFollowerMap - follower - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("follower");
			}
			return options.target;
		}
	},
	/**
	 * Property following of harpoonApi.CustomerFollower describes a relationship belongsTo harpoonApi.Customer
	 * @type {object}
	 */
	"following": {
		"create": function(options){
			Console.log("CustomerFollowerMap - following - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerFollowerModel.following in during mapping");
		},
		"update": function(options){
			Console.log("CustomerFollowerMap - following - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("following");
			}
			return options.target;
		}
	},
};
