/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * FormSelectorOption Map
 * @type {object}
 */
var FormSelectorOptionMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.FormSelectorOption.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("FormSelectorOptionMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormSelectorOptionModel.id in during mapping");
		},
		"update": function(options){
			Console.log("FormSelectorOptionMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormSelectorOption.title
	 * @type {string}
	 */
	"title": {
		"create": function(options){
			Console.log("FormSelectorOptionMap - title - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormSelectorOptionModel.title in during mapping");
		},
		"update": function(options){
			Console.log("FormSelectorOptionMap - title - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("title");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormSelectorOption.value
	 * @type {string}
	 */
	"value": {
		"create": function(options){
			Console.log("FormSelectorOptionMap - value - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormSelectorOptionModel.value in during mapping");
		},
		"update": function(options){
			Console.log("FormSelectorOptionMap - value - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("value");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormSelectorOption.format
	 * @type {string}
	 */
	"format": {
		"create": function(options){
			Console.log("FormSelectorOptionMap - format - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormSelectorOptionModel.format in during mapping");
		},
		"update": function(options){
			Console.log("FormSelectorOptionMap - format - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("format");
			}
			return options.target;
		}
	},
};
