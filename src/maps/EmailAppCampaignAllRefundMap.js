/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignAllRefund Map
 * @type {object}
 */
var EmailAppCampaignAllRefundMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.appName
	 * @type {string}
	 */
	"appName": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - appName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.appName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - appName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.brandCover
	 * @type {string}
	 */
	"brandCover": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandCover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.brandCover in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandCover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandCover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.brandLogo
	 * @type {string}
	 */
	"brandLogo": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.brandLogo in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.brandTel
	 * @type {string}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.orderId
	 * @type {number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.organizerName
	 * @type {string}
	 */
	"organizerName": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - organizerName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.organizerName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - organizerName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("organizerName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.refundAmount
	 * @type {number}
	 */
	"refundAmount": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - refundAmount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.refundAmount in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - refundAmount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("refundAmount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignAllRefund.userName
	 * @type {string}
	 */
	"userName": {
		"create": function(options){
			Console.log("EmailAppCampaignAllRefundMap - userName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignAllRefundModel.userName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignAllRefundMap - userName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userName");
			}
			return options.target;
		}
	},
};
