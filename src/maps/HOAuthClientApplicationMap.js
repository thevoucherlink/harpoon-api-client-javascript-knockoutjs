/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HOAuthClientApplication Map
 * @type {object}
 */
var HOAuthClientApplicationMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HOAuthClientApplication.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.clientType
	 * @type {string}
	 */
	"clientType": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - clientType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.clientType in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - clientType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("clientType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.redirectURIs
	 * @type {string}
	 */
	"redirectURIs": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - redirectURIs - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - redirectURIs - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redirectURIs");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.tokenEndpointAuthMethod
	 * @type {string}
	 */
	"tokenEndpointAuthMethod": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - tokenEndpointAuthMethod - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.tokenEndpointAuthMethod in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - tokenEndpointAuthMethod - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tokenEndpointAuthMethod");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.grantTypes
	 * @type {object}
	 */
	"grantTypes": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - grantTypes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - grantTypes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("grantTypes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.responseTypes
	 * @type {object}
	 */
	"responseTypes": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - responseTypes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - responseTypes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("responseTypes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.tokenType
	 * @type {string}
	 */
	"tokenType": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - tokenType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.tokenType in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - tokenType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tokenType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.clientSecret
	 * @type {string}
	 */
	"clientSecret": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - clientSecret - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.clientSecret in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - clientSecret - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("clientSecret");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.clientName
	 * @type {string}
	 */
	"clientName": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - clientName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.clientName in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - clientName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("clientName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.clientURI
	 * @type {string}
	 */
	"clientURI": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - clientURI - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.clientURI in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - clientURI - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("clientURI");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.logoURI
	 * @type {string}
	 */
	"logoURI": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - logoURI - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.logoURI in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - logoURI - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("logoURI");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.scopes
	 * @type {string}
	 */
	"scopes": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - scopes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - scopes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scopes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.contacts
	 * @type {string}
	 */
	"contacts": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - contacts - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - contacts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("contacts");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.tosURI
	 * @type {string}
	 */
	"tosURI": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - tosURI - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.tosURI in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - tosURI - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tosURI");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.policyURI
	 * @type {string}
	 */
	"policyURI": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - policyURI - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.policyURI in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - policyURI - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("policyURI");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.jwksURI
	 * @type {string}
	 */
	"jwksURI": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - jwksURI - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.jwksURI in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - jwksURI - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("jwksURI");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.jwks
	 * @type {string}
	 */
	"jwks": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - jwks - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.jwks in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - jwks - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("jwks");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.softwareId
	 * @type {string}
	 */
	"softwareId": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - softwareId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.softwareId in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - softwareId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("softwareId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.softwareVersion
	 * @type {string}
	 */
	"softwareVersion": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - softwareVersion - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.softwareVersion in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - softwareVersion - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("softwareVersion");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.callbackUrls
	 * @type {boolean}
	 */
	"callbackUrls": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - callbackUrls - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.callbackUrls in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - callbackUrls - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("callbackUrls");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.permissions
	 * @type {boolean}
	 */
	"permissions": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - permissions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.permissions in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - permissions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("permissions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.authenticationEnabled
	 * @type {boolean}
	 */
	"authenticationEnabled": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - authenticationEnabled - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.authenticationEnabled in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - authenticationEnabled - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("authenticationEnabled");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.anonymousAllowed
	 * @type {boolean}
	 */
	"anonymousAllowed": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - anonymousAllowed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.anonymousAllowed in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - anonymousAllowed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("anonymousAllowed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.authenticationSchemes
	 * @type {boolean}
	 */
	"authenticationSchemes": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - authenticationSchemes - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.authenticationSchemes in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - authenticationSchemes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("authenticationSchemes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.icon
	 * @type {boolean}
	 */
	"icon": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - icon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.icon in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - icon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("icon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthClientApplication.url
	 * @type {boolean}
	 */
	"url": {
		"create": function(options){
			Console.log("HOAuthClientApplicationMap - url - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthClientApplicationModel.url in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthClientApplicationMap - url - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("url");
			}
			return options.target;
		}
	},
};
