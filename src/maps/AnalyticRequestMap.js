/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AnalyticRequest Map
 * @type {object}
 */
var AnalyticRequestMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AnalyticRequest.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("AnalyticRequestMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AnalyticRequestModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AnalyticRequestMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AnalyticRequest.requests
	 * @type {object}
	 */
	"requests": {
		"create": function(options){
			Console.log("AnalyticRequestMap - requests - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AnalyticRequestMap - requests - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("requests");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AnalyticRequest.scope
	 * @type {string}
	 */
	"scope": {
		"create": function(options){
			Console.log("AnalyticRequestMap - scope - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AnalyticRequestModel.scope in during mapping");
		},
		"update": function(options){
			Console.log("AnalyticRequestMap - scope - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scope");
			}
			return options.target;
		}
	},
};
