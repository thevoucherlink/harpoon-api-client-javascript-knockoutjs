/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CustomerBadge Map
 * @type {object}
 */
var CustomerBadgeMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CustomerBadge.offer
	 * @type {number}
	 */
	"offer": {
		"create": function(options){
			Console.log("CustomerBadgeMap - offer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.offer in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - offer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("offer");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.event
	 * @type {number}
	 */
	"event": {
		"create": function(options){
			Console.log("CustomerBadgeMap - event - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.event in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - event - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("event");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.dealAll
	 * @type {number}
	 */
	"dealAll": {
		"create": function(options){
			Console.log("CustomerBadgeMap - dealAll - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.dealAll in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - dealAll - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealAll");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.dealCoupon
	 * @type {number}
	 */
	"dealCoupon": {
		"create": function(options){
			Console.log("CustomerBadgeMap - dealCoupon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.dealCoupon in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - dealCoupon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealCoupon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.dealSimple
	 * @type {number}
	 */
	"dealSimple": {
		"create": function(options){
			Console.log("CustomerBadgeMap - dealSimple - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.dealSimple in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - dealSimple - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealSimple");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.dealGroup
	 * @type {number}
	 */
	"dealGroup": {
		"create": function(options){
			Console.log("CustomerBadgeMap - dealGroup - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.dealGroup in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - dealGroup - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealGroup");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.competition
	 * @type {number}
	 */
	"competition": {
		"create": function(options){
			Console.log("CustomerBadgeMap - competition - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.competition in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - competition - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competition");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.walletOfferAvailable
	 * @type {number}
	 */
	"walletOfferAvailable": {
		"create": function(options){
			Console.log("CustomerBadgeMap - walletOfferAvailable - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.walletOfferAvailable in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - walletOfferAvailable - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("walletOfferAvailable");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.walletOfferNew
	 * @type {number}
	 */
	"walletOfferNew": {
		"create": function(options){
			Console.log("CustomerBadgeMap - walletOfferNew - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.walletOfferNew in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - walletOfferNew - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("walletOfferNew");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.walletOfferExpiring
	 * @type {number}
	 */
	"walletOfferExpiring": {
		"create": function(options){
			Console.log("CustomerBadgeMap - walletOfferExpiring - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.walletOfferExpiring in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - walletOfferExpiring - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("walletOfferExpiring");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.walletOfferExpired
	 * @type {number}
	 */
	"walletOfferExpired": {
		"create": function(options){
			Console.log("CustomerBadgeMap - walletOfferExpired - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.walletOfferExpired in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - walletOfferExpired - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("walletOfferExpired");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.notificationUnread
	 * @type {number}
	 */
	"notificationUnread": {
		"create": function(options){
			Console.log("CustomerBadgeMap - notificationUnread - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.notificationUnread in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - notificationUnread - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationUnread");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.brandFeed
	 * @type {number}
	 */
	"brandFeed": {
		"create": function(options){
			Console.log("CustomerBadgeMap - brandFeed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.brandFeed in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - brandFeed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandFeed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.brand
	 * @type {number}
	 */
	"brand": {
		"create": function(options){
			Console.log("CustomerBadgeMap - brand - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.brand in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - brand - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brand");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.brandActivity
	 * @type {number}
	 */
	"brandActivity": {
		"create": function(options){
			Console.log("CustomerBadgeMap - brandActivity - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.brandActivity in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - brandActivity - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandActivity");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerBadge.all
	 * @type {number}
	 */
	"all": {
		"create": function(options){
			Console.log("CustomerBadgeMap - all - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerBadgeModel.all in during mapping");
		},
		"update": function(options){
			Console.log("CustomerBadgeMap - all - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("all");
			}
			return options.target;
		}
	},
};
