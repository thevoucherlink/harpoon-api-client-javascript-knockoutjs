/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * PlayerSource Map
 * @type {object}
 */
var PlayerSourceMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.PlayerSource.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("PlayerSourceMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerSourceModel.id in during mapping");
		},
		"update": function(options){
			Console.log("PlayerSourceMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Source file
	 * @type {string}
	 */
	"file": {
		"create": function(options){
			Console.log("PlayerSourceMap - file - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerSourceModel.file in during mapping");
		},
		"update": function(options){
			Console.log("PlayerSourceMap - file - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("file");
			}
			return options.target;
		}
	},
	/**
	 * Source type (used only in JS SDK)
	 * @type {string}
	 */
	"type": {
		"create": function(options){
			Console.log("PlayerSourceMap - type - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerSourceModel.type in during mapping");
		},
		"update": function(options){
			Console.log("PlayerSourceMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * Source label
	 * @type {string}
	 */
	"label": {
		"create": function(options){
			Console.log("PlayerSourceMap - label - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerSourceModel.label in during mapping");
		},
		"update": function(options){
			Console.log("PlayerSourceMap - label - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("label");
			}
			return options.target;
		}
	},
	/**
	 * If Source is the default Source
	 * @type {boolean}
	 */
	"default": {
		"create": function(options){
			Console.log("PlayerSourceMap - default - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerSourceModel.default in during mapping");
		},
		"update": function(options){
			Console.log("PlayerSourceMap - default - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("default");
			}
			return options.target;
		}
	},
	/**
	 * Sort order index
	 * @type {number}
	 */
	"order": {
		"create": function(options){
			Console.log("PlayerSourceMap - order - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerSourceModel.order in during mapping");
		},
		"update": function(options){
			Console.log("PlayerSourceMap - order - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("order");
			}
			return options.target;
		}
	},
	/**
	 * Property playlistItem of harpoonApi.PlayerSource describes a relationship belongsTo harpoonApi.PlaylistItem
	 * @type {object}
	 */
	"playlistItem": {
		"create": function(options){
			Console.log("PlayerSourceMap - playlistItem - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerSourceModel.playlistItem in during mapping");
		},
		"update": function(options){
			Console.log("PlayerSourceMap - playlistItem - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playlistItem");
			}
			return options.target;
		}
	},
};
