/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Contact Map
 * @type {object}
 */
var ContactMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Contact.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("ContactMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.id in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Email address like hello@harpoonconnect.com
	 * @type {string}
	 */
	"email": {
		"create": function(options){
			Console.log("ContactMap - email - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.email in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - email - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("email");
			}
			return options.target;
		}
	},
	/**
	 * URL like http://harpoonconnect.com or https://www.harpoonconnect.com
	 * @type {string}
	 */
	"website": {
		"create": function(options){
			Console.log("ContactMap - website - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.website in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - website - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("website");
			}
			return options.target;
		}
	},
	/**
	 * Telephone number like 08123456789
	 * @type {string}
	 */
	"phone": {
		"create": function(options){
			Console.log("ContactMap - phone - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.phone in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - phone - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("phone");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Contact.address
	 * @type {Address}
	 */
	"address": {
		"create": function(options){
			Console.log("ContactMap - address - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("ContactMap - address - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("address");
			}
			return options.target;
		}
	},
	/**
	 * URL starting with https://www.twitter.com/ followed by your username or id
	 * @type {string}
	 */
	"twitter": {
		"create": function(options){
			Console.log("ContactMap - twitter - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.twitter in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - twitter - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("twitter");
			}
			return options.target;
		}
	},
	/**
	 * URL starting with https://www.facebook.com/ followed by your username or id
	 * @type {string}
	 */
	"facebook": {
		"create": function(options){
			Console.log("ContactMap - facebook - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.facebook in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - facebook - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebook");
			}
			return options.target;
		}
	},
	/**
	 * Telephone number including country code like: +35381234567890
	 * @type {string}
	 */
	"whatsapp": {
		"create": function(options){
			Console.log("ContactMap - whatsapp - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.whatsapp in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - whatsapp - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("whatsapp");
			}
			return options.target;
		}
	},
	/**
	 * SnapChat username
	 * @type {string}
	 */
	"snapchat": {
		"create": function(options){
			Console.log("ContactMap - snapchat - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.snapchat in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - snapchat - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("snapchat");
			}
			return options.target;
		}
	},
	/**
	 * URL starting with https://plus.google.com/+ followed by your username or id
	 * @type {string}
	 */
	"googlePlus": {
		"create": function(options){
			Console.log("ContactMap - googlePlus - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.googlePlus in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - googlePlus - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("googlePlus");
			}
			return options.target;
		}
	},
	/**
	 * LinkedIn profile URL
	 * @type {string}
	 */
	"linkedIn": {
		"create": function(options){
			Console.log("ContactMap - linkedIn - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.linkedIn in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - linkedIn - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("linkedIn");
			}
			return options.target;
		}
	},
	/**
	 * A string starting with # and followed by alphanumeric characters (both lower and upper case)
	 * @type {string}
	 */
	"hashtag": {
		"create": function(options){
			Console.log("ContactMap - hashtag - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.hashtag in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - hashtag - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hashtag");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Contact.text
	 * @type {string}
	 */
	"text": {
		"create": function(options){
			Console.log("ContactMap - text - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ContactModel.text in during mapping");
		},
		"update": function(options){
			Console.log("ContactMap - text - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("text");
			}
			return options.target;
		}
	},
};
