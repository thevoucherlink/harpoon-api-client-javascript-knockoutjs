/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CustomerNotification Map
 * @type {object}
 */
var CustomerNotificationMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CustomerNotification.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CustomerNotificationMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerNotificationModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.message
	 * @type {string}
	 */
	"message": {
		"create": function(options){
			Console.log("CustomerNotificationMap - message - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerNotificationModel.message in during mapping");
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - message - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("message");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.cover
	 * @type {string}
	 */
	"cover": {
		"create": function(options){
			Console.log("CustomerNotificationMap - cover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerNotificationModel.cover in during mapping");
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - cover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.coverUpload
	 * @type {MagentoImageUpload}
	 */
	"coverUpload": {
		"create": function(options){
			Console.log("CustomerNotificationMap - coverUpload - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - coverUpload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("coverUpload");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.from
	 * @type {NotificationFrom}
	 */
	"from": {
		"create": function(options){
			Console.log("CustomerNotificationMap - from - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - from - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("from");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.related
	 * @type {NotificationRelated}
	 */
	"related": {
		"create": function(options){
			Console.log("CustomerNotificationMap - related - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - related - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("related");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.link
	 * @type {string}
	 */
	"link": {
		"create": function(options){
			Console.log("CustomerNotificationMap - link - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerNotificationModel.link in during mapping");
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - link - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("link");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.actionCode
	 * @type {string}
	 */
	"actionCode": {
		"create": function(options){
			Console.log("CustomerNotificationMap - actionCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerNotificationModel.actionCode in during mapping");
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - actionCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("actionCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.status
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("CustomerNotificationMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerNotificationModel.status in during mapping");
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerNotification.sentAt
	 * @type {date}
	 */
	"sentAt": {
		"create": function(options){
			Console.log("CustomerNotificationMap - sentAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerNotificationModel.sentAt in during mapping");
		},
		"update": function(options){
			Console.log("CustomerNotificationMap - sentAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sentAt");
			}
			return options.target;
		}
	},
};
