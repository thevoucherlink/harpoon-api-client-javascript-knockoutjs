/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignCompetitionJoinCompetition Map
 * @type {object}
 */
var EmailAppCampaignCompetitionJoinCompetitionMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandLogo
	 * @type {string}
	 */
	"brandLogo": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.brandLogo in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignDescription
	 * @type {string}
	 */
	"campaignDescription": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.campaignDescription in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignDescription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignEndTime
	 * @type {string}
	 */
	"campaignEndTime": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignEndTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.campaignEndTime in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignEndTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignEndTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignLink
	 * @type {string}
	 */
	"campaignLink": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.campaignLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionJoinCompetition.campaignTermsAndConditions
	 * @type {string}
	 */
	"campaignTermsAndConditions": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignTermsAndConditions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionJoinCompetitionModel.campaignTermsAndConditions in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionJoinCompetitionMap - campaignTermsAndConditions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignTermsAndConditions");
			}
			return options.target;
		}
	},
};
