/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CatalogInventoryStockItem Map
 * @type {object}
 */
var CatalogInventoryStockItemMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CatalogInventoryStockItem.backorders
	 * @type {Number}
	 */
	"backorders": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - backorders - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.backorders in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - backorders - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("backorders");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.enableQtyIncrements
	 * @type {Number}
	 */
	"enableQtyIncrements": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - enableQtyIncrements - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.enableQtyIncrements in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - enableQtyIncrements - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("enableQtyIncrements");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.isDecimalDivided
	 * @type {Number}
	 */
	"isDecimalDivided": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - isDecimalDivided - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.isDecimalDivided in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - isDecimalDivided - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isDecimalDivided");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.isQtyDecimal
	 * @type {Number}
	 */
	"isQtyDecimal": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - isQtyDecimal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.isQtyDecimal in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - isQtyDecimal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isQtyDecimal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.isInStock
	 * @type {Number}
	 */
	"isInStock": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - isInStock - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.isInStock in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - isInStock - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isInStock");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.lowStockDate
	 * @type {Date}
	 */
	"lowStockDate": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - lowStockDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.lowStockDate in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - lowStockDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("lowStockDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.manageStock
	 * @type {Number}
	 */
	"manageStock": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - manageStock - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.manageStock in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - manageStock - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("manageStock");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.minQty
	 * @type {String}
	 */
	"minQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - minQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.minQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - minQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("minQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.maxSaleQty
	 * @type {String}
	 */
	"maxSaleQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - maxSaleQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.maxSaleQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - maxSaleQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("maxSaleQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.minSaleQty
	 * @type {String}
	 */
	"minSaleQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - minSaleQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.minSaleQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - minSaleQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("minSaleQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.notifyStockQty
	 * @type {String}
	 */
	"notifyStockQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - notifyStockQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.notifyStockQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - notifyStockQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notifyStockQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.productId
	 * @type {Number}
	 */
	"productId": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - productId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.productId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - productId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.qty
	 * @type {String}
	 */
	"qty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - qty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.qty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - qty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.stockId
	 * @type {Number}
	 */
	"stockId": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - stockId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.stockId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - stockId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stockId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigBackorders
	 * @type {Number}
	 */
	"useConfigBackorders": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigBackorders - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigBackorders in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigBackorders - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigBackorders");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.qtyIncrements
	 * @type {String}
	 */
	"qtyIncrements": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - qtyIncrements - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.qtyIncrements in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - qtyIncrements - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyIncrements");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.stockStatusChangedAuto
	 * @type {Number}
	 */
	"stockStatusChangedAuto": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - stockStatusChangedAuto - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.stockStatusChangedAuto in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - stockStatusChangedAuto - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stockStatusChangedAuto");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigEnableQtyInc
	 * @type {Number}
	 */
	"useConfigEnableQtyInc": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigEnableQtyInc - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigEnableQtyInc in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigEnableQtyInc - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigEnableQtyInc");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigMaxSaleQty
	 * @type {Number}
	 */
	"useConfigMaxSaleQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigMaxSaleQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigMaxSaleQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigMaxSaleQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigMaxSaleQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigManageStock
	 * @type {Number}
	 */
	"useConfigManageStock": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigManageStock - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigManageStock in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigManageStock - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigManageStock");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigMinQty
	 * @type {Number}
	 */
	"useConfigMinQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigMinQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigMinQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigMinQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigMinQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigNotifyStockQty
	 * @type {Number}
	 */
	"useConfigNotifyStockQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigNotifyStockQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigNotifyStockQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigNotifyStockQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigNotifyStockQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigMinSaleQty
	 * @type {Number}
	 */
	"useConfigMinSaleQty": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigMinSaleQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigMinSaleQty in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigMinSaleQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigMinSaleQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogInventoryStockItem.useConfigQtyIncrements
	 * @type {Number}
	 */
	"useConfigQtyIncrements": {
		"create": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigQtyIncrements - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogInventoryStockItemModel.useConfigQtyIncrements in during mapping");
		},
		"update": function(options){
			Console.log("CatalogInventoryStockItemMap - useConfigQtyIncrements - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useConfigQtyIncrements");
			}
			return options.target;
		}
	},
};
