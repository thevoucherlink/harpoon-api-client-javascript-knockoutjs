/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CatalogProductWebsite Map
 * @type {object}
 */
var CatalogProductWebsiteMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CatalogProductWebsite.productId
	 * @type {Number}
	 */
	"productId": {
		"create": function(options){
			Console.log("CatalogProductWebsiteMap - productId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductWebsiteModel.productId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductWebsiteMap - productId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProductWebsite.websiteId
	 * @type {Number}
	 */
	"websiteId": {
		"create": function(options){
			Console.log("CatalogProductWebsiteMap - websiteId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductWebsiteModel.websiteId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductWebsiteMap - websiteId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("websiteId");
			}
			return options.target;
		}
	},
};
