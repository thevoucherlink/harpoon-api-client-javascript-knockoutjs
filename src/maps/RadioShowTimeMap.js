/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RadioShowTime Map
 * @type {object}
 */
var RadioShowTimeMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RadioShowTime.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("RadioShowTimeMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowTimeModel.id in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowTimeMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Time when the show starts, format HHmm
	 * @type {string}
	 */
	"startTime": {
		"create": function(options){
			Console.log("RadioShowTimeMap - startTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowTimeModel.startTime in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowTimeMap - startTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("startTime");
			}
			return options.target;
		}
	},
	/**
	 * Time when the show ends, format HHmm
	 * @type {string}
	 */
	"endTime": {
		"create": function(options){
			Console.log("RadioShowTimeMap - endTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowTimeModel.endTime in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowTimeMap - endTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("endTime");
			}
			return options.target;
		}
	},
	/**
	 * Day of the week when the show is live, 1 = Monday / 7 = Sunday
	 * @type {number}
	 */
	"weekday": {
		"create": function(options){
			Console.log("RadioShowTimeMap - weekday - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowTimeModel.weekday in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowTimeMap - weekday - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("weekday");
			}
			return options.target;
		}
	},
	/**
	 * Property radioShow of harpoonApi.RadioShowTime describes a relationship belongsTo harpoonApi.RadioShow
	 * @type {object}
	 */
	"radioShow": {
		"create": function(options){
			Console.log("RadioShowTimeMap - radioShow - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowTimeModel.radioShow in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowTimeMap - radioShow - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioShow");
			}
			return options.target;
		}
	},
};
