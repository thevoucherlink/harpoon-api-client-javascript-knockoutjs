/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * LoginExternal Map
 * @type {object}
 */
var LoginExternalMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.LoginExternal.apiKey
	 * @type {string}
	 */
	"apiKey": {
		"create": function(options){
			Console.log("LoginExternalMap - apiKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create LoginExternalModel.apiKey in during mapping");
		},
		"update": function(options){
			Console.log("LoginExternalMap - apiKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("apiKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.LoginExternal.version
	 * @type {string}
	 */
	"version": {
		"create": function(options){
			Console.log("LoginExternalMap - version - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create LoginExternalModel.version in during mapping");
		},
		"update": function(options){
			Console.log("LoginExternalMap - version - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("version");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.LoginExternal.requestUrl
	 * @type {string}
	 */
	"requestUrl": {
		"create": function(options){
			Console.log("LoginExternalMap - requestUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create LoginExternalModel.requestUrl in during mapping");
		},
		"update": function(options){
			Console.log("LoginExternalMap - requestUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("requestUrl");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.LoginExternal.authorizeUrl
	 * @type {string}
	 */
	"authorizeUrl": {
		"create": function(options){
			Console.log("LoginExternalMap - authorizeUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create LoginExternalModel.authorizeUrl in during mapping");
		},
		"update": function(options){
			Console.log("LoginExternalMap - authorizeUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("authorizeUrl");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.LoginExternal.accessUrl
	 * @type {string}
	 */
	"accessUrl": {
		"create": function(options){
			Console.log("LoginExternalMap - accessUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create LoginExternalModel.accessUrl in during mapping");
		},
		"update": function(options){
			Console.log("LoginExternalMap - accessUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("accessUrl");
			}
			return options.target;
		}
	},
};
