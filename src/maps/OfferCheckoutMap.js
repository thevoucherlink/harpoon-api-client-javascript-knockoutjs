/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * OfferCheckout Map
 * @type {object}
 */
var OfferCheckoutMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.OfferCheckout.cartId
	 * @type {number}
	 */
	"cartId": {
		"create": function(options){
			Console.log("OfferCheckoutMap - cartId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferCheckoutModel.cartId in during mapping");
		},
		"update": function(options){
			Console.log("OfferCheckoutMap - cartId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cartId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.OfferCheckout.url
	 * @type {string}
	 */
	"url": {
		"create": function(options){
			Console.log("OfferCheckoutMap - url - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferCheckoutModel.url in during mapping");
		},
		"update": function(options){
			Console.log("OfferCheckoutMap - url - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("url");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.OfferCheckout.status
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("OfferCheckoutMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferCheckoutModel.status in during mapping");
		},
		"update": function(options){
			Console.log("OfferCheckoutMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
};
