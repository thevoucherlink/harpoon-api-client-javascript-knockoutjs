/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CheckoutAgreement Map
 * @type {object}
 */
var CheckoutAgreementMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CheckoutAgreement.name
	 * @type {String}
	 */
	"name": {
		"create": function(options){
			Console.log("CheckoutAgreementMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CheckoutAgreementModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CheckoutAgreementMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CheckoutAgreement.contentHeight
	 * @type {String}
	 */
	"contentHeight": {
		"create": function(options){
			Console.log("CheckoutAgreementMap - contentHeight - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CheckoutAgreementModel.contentHeight in during mapping");
		},
		"update": function(options){
			Console.log("CheckoutAgreementMap - contentHeight - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("contentHeight");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CheckoutAgreement.checkboxText
	 * @type {String}
	 */
	"checkboxText": {
		"create": function(options){
			Console.log("CheckoutAgreementMap - checkboxText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CheckoutAgreementModel.checkboxText in during mapping");
		},
		"update": function(options){
			Console.log("CheckoutAgreementMap - checkboxText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("checkboxText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CheckoutAgreement.isActive
	 * @type {Number}
	 */
	"isActive": {
		"create": function(options){
			Console.log("CheckoutAgreementMap - isActive - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CheckoutAgreementModel.isActive in during mapping");
		},
		"update": function(options){
			Console.log("CheckoutAgreementMap - isActive - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isActive");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CheckoutAgreement.isHtml
	 * @type {Number}
	 */
	"isHtml": {
		"create": function(options){
			Console.log("CheckoutAgreementMap - isHtml - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CheckoutAgreementModel.isHtml in during mapping");
		},
		"update": function(options){
			Console.log("CheckoutAgreementMap - isHtml - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isHtml");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CheckoutAgreement.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("CheckoutAgreementMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CheckoutAgreementModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CheckoutAgreementMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CheckoutAgreement.content
	 * @type {String}
	 */
	"content": {
		"create": function(options){
			Console.log("CheckoutAgreementMap - content - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CheckoutAgreementModel.content in during mapping");
		},
		"update": function(options){
			Console.log("CheckoutAgreementMap - content - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("content");
			}
			return options.target;
		}
	},
};
