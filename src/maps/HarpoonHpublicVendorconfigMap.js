/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HarpoonHpublicVendorconfig Map
 * @type {object}
 */
var HarpoonHpublicVendorconfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.type
	 * @type {Number}
	 */
	"type": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - type - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.type in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.category
	 * @type {String}
	 */
	"category": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - category - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.category in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - category - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("category");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.logo
	 * @type {String}
	 */
	"logo": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - logo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.logo in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - logo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("logo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.cover
	 * @type {String}
	 */
	"cover": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - cover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.cover in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - cover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookPageId
	 * @type {String}
	 */
	"facebookPageId": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookPageId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookPageId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookPageId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookPageId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookPageToken
	 * @type {String}
	 */
	"facebookPageToken": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookPageToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookPageToken in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookPageToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookPageToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookEventEnabled
	 * @type {Number}
	 */
	"facebookEventEnabled": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookEventEnabled - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookEventEnabled in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookEventEnabled - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookEventEnabled");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookEventUpdatedAt
	 * @type {Date}
	 */
	"facebookEventUpdatedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookEventUpdatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookEventUpdatedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookEventUpdatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookEventUpdatedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookEventCount
	 * @type {Number}
	 */
	"facebookEventCount": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookEventCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookEventCount in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookEventCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookEventCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookFeedEnabled
	 * @type {Number}
	 */
	"facebookFeedEnabled": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookFeedEnabled - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookFeedEnabled in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookFeedEnabled - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookFeedEnabled");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookFeedUpdatedAt
	 * @type {Date}
	 */
	"facebookFeedUpdatedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookFeedUpdatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookFeedUpdatedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookFeedUpdatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookFeedUpdatedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.facebookFeedCount
	 * @type {Number}
	 */
	"facebookFeedCount": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookFeedCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.facebookFeedCount in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - facebookFeedCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookFeedCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.stripePublishableKey
	 * @type {String}
	 */
	"stripePublishableKey": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripePublishableKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.stripePublishableKey in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripePublishableKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stripePublishableKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.stripeRefreshToken
	 * @type {String}
	 */
	"stripeRefreshToken": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripeRefreshToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.stripeRefreshToken in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripeRefreshToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stripeRefreshToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.stripeAccessToken
	 * @type {String}
	 */
	"stripeAccessToken": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripeAccessToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.stripeAccessToken in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripeAccessToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stripeAccessToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.feesEventTicket
	 * @type {String}
	 */
	"feesEventTicket": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - feesEventTicket - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.feesEventTicket in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - feesEventTicket - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("feesEventTicket");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.stripeUserId
	 * @type {String}
	 */
	"stripeUserId": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripeUserId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.stripeUserId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - stripeUserId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stripeUserId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.vendorconfigId
	 * @type {Number}
	 */
	"vendorconfigId": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - vendorconfigId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.vendorconfigId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - vendorconfigId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorconfigId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicVendorconfig.updatedAt
	 * @type {Date}
	 */
	"updatedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - updatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicVendorconfigModel.updatedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicVendorconfigMap - updatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedAt");
			}
			return options.target;
		}
	},
};
