/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * UdropshipVendorProduct Map
 * @type {object}
 */
var UdropshipVendorProductMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.UdropshipVendorProduct.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.id in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.vendorId
	 * @type {Number}
	 */
	"vendorId": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - vendorId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.vendorId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - vendorId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.productId
	 * @type {Number}
	 */
	"productId": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - productId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.productId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - productId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.priority
	 * @type {Number}
	 */
	"priority": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - priority - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.priority in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - priority - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("priority");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.carrierCode
	 * @type {String}
	 */
	"carrierCode": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - carrierCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.carrierCode in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - carrierCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("carrierCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.vendorSku
	 * @type {String}
	 */
	"vendorSku": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - vendorSku - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.vendorSku in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - vendorSku - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorSku");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.vendorCost
	 * @type {String}
	 */
	"vendorCost": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - vendorCost - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.vendorCost in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - vendorCost - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorCost");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.stockQty
	 * @type {String}
	 */
	"stockQty": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - stockQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.stockQty in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - stockQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stockQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.backorders
	 * @type {Number}
	 */
	"backorders": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - backorders - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.backorders in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - backorders - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("backorders");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.shippingPrice
	 * @type {String}
	 */
	"shippingPrice": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - shippingPrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.shippingPrice in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - shippingPrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("shippingPrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.status
	 * @type {Number}
	 */
	"status": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.status in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.reservedQty
	 * @type {String}
	 */
	"reservedQty": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - reservedQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.reservedQty in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - reservedQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("reservedQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.availState
	 * @type {String}
	 */
	"availState": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - availState - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.availState in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - availState - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("availState");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.availDate
	 * @type {Date}
	 */
	"availDate": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - availDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.availDate in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - availDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("availDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProduct.relationshipStatus
	 * @type {String}
	 */
	"relationshipStatus": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - relationshipStatus - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.relationshipStatus in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - relationshipStatus - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("relationshipStatus");
			}
			return options.target;
		}
	},
	/**
	 * Property udropshipVendor of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.UdropshipVendor
	 * @type {object}
	 */
	"udropshipVendor": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - udropshipVendor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.udropshipVendor in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - udropshipVendor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendor");
			}
			return options.target;
		}
	},
	/**
	 * Property catalogProduct of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.CatalogProduct
	 * @type {object}
	 */
	"catalogProduct": {
		"create": function(options){
			Console.log("UdropshipVendorProductMap - catalogProduct - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductModel.catalogProduct in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductMap - catalogProduct - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("catalogProduct");
			}
			return options.target;
		}
	},
};
