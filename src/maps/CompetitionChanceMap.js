/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CompetitionChance Map
 * @type {object}
 */
var CompetitionChanceMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CompetitionChance.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CompetitionChanceMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionChanceModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionChanceMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionChance.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("CompetitionChanceMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionChanceModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionChanceMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionChance.price
	 * @type {number}
	 */
	"price": {
		"create": function(options){
			Console.log("CompetitionChanceMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionChanceModel.price in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionChanceMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionChance.chanceCount
	 * @type {number}
	 */
	"chanceCount": {
		"create": function(options){
			Console.log("CompetitionChanceMap - chanceCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionChanceModel.chanceCount in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionChanceMap - chanceCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("chanceCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionChance.qtyBought
	 * @type {number}
	 */
	"qtyBought": {
		"create": function(options){
			Console.log("CompetitionChanceMap - qtyBought - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionChanceModel.qtyBought in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionChanceMap - qtyBought - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyBought");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionChance.qtyTotal
	 * @type {number}
	 */
	"qtyTotal": {
		"create": function(options){
			Console.log("CompetitionChanceMap - qtyTotal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionChanceModel.qtyTotal in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionChanceMap - qtyTotal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyTotal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionChance.qtyLeft
	 * @type {number}
	 */
	"qtyLeft": {
		"create": function(options){
			Console.log("CompetitionChanceMap - qtyLeft - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionChanceModel.qtyLeft in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionChanceMap - qtyLeft - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyLeft");
			}
			return options.target;
		}
	},
};
