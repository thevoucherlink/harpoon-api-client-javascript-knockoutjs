/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * UdropshipVendorPartner Map
 * @type {object}
 */
var UdropshipVendorPartnerMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.UdropshipVendorPartner.status
	 * @type {String}
	 */
	"status": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.status in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.invitedEmail
	 * @type {String}
	 */
	"invitedEmail": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - invitedEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.invitedEmail in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - invitedEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("invitedEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.invitedAt
	 * @type {Date}
	 */
	"invitedAt": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - invitedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.invitedAt in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - invitedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("invitedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.acceptedAt
	 * @type {Date}
	 */
	"acceptedAt": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - acceptedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.acceptedAt in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - acceptedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("acceptedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.updatedAt
	 * @type {Date}
	 */
	"updatedAt": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - updatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.updatedAt in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - updatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configList
	 * @type {Number}
	 */
	"configList": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configList in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configFeed
	 * @type {Number}
	 */
	"configFeed": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configFeed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configFeed in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configFeed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configFeed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configNeedFollow
	 * @type {Number}
	 */
	"configNeedFollow": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configNeedFollow - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configNeedFollow in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configNeedFollow - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configNeedFollow");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configAcceptEvent
	 * @type {Number}
	 */
	"configAcceptEvent": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptEvent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configAcceptEvent in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptEvent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptEvent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configAcceptCoupon
	 * @type {Number}
	 */
	"configAcceptCoupon": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptCoupon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configAcceptCoupon in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptCoupon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptCoupon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configAcceptDealsimple
	 * @type {Number}
	 */
	"configAcceptDealsimple": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptDealsimple - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configAcceptDealsimple in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptDealsimple - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptDealsimple");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configAcceptDealgroup
	 * @type {Number}
	 */
	"configAcceptDealgroup": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptDealgroup - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configAcceptDealgroup in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptDealgroup - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptDealgroup");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configAcceptNotificationpush
	 * @type {Number}
	 */
	"configAcceptNotificationpush": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptNotificationpush - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configAcceptNotificationpush in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptNotificationpush - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptNotificationpush");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.configAcceptNotificationbeacon
	 * @type {Number}
	 */
	"configAcceptNotificationbeacon": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptNotificationbeacon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.configAcceptNotificationbeacon in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - configAcceptNotificationbeacon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptNotificationbeacon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.isOwner
	 * @type {Number}
	 */
	"isOwner": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - isOwner - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.isOwner in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - isOwner - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isOwner");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.vendorId
	 * @type {Number}
	 */
	"vendorId": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - vendorId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.vendorId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - vendorId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.partnerId
	 * @type {Number}
	 */
	"partnerId": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - partnerId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.partnerId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - partnerId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("partnerId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorPartner.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.id in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Property udropshipVendor of harpoonApi.UdropshipVendorPartner describes a relationship belongsTo harpoonApi.UdropshipVendor
	 * @type {object}
	 */
	"udropshipVendor": {
		"create": function(options){
			Console.log("UdropshipVendorPartnerMap - udropshipVendor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorPartnerModel.udropshipVendor in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorPartnerMap - udropshipVendor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendor");
			}
			return options.target;
		}
	},
};
