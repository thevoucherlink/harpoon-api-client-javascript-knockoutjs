/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CompetitionPurchase Map
 * @type {object}
 */
var CompetitionPurchaseMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CompetitionPurchase.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.orderId
	 * @type {number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.competition
	 * @type {Competition}
	 */
	"competition": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - competition - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - competition - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competition");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.redemptionType
	 * @type {string}
	 */
	"redemptionType": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - redemptionType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.redemptionType in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - redemptionType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redemptionType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.unlockTime
	 * @type {number}
	 */
	"unlockTime": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - unlockTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.unlockTime in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - unlockTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("unlockTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.code
	 * @type {string}
	 */
	"code": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - code - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.code in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - code - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("code");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.qrcode
	 * @type {string}
	 */
	"qrcode": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - qrcode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.qrcode in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - qrcode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qrcode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.isAvailable
	 * @type {boolean}
	 */
	"isAvailable": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - isAvailable - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.isAvailable in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - isAvailable - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isAvailable");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.status
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.status in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.createdAt
	 * @type {date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.expiredAt
	 * @type {date}
	 */
	"expiredAt": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - expiredAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.expiredAt in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - expiredAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiredAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.redeemedAt
	 * @type {date}
	 */
	"redeemedAt": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - redeemedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.redeemedAt in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - redeemedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.redeemedByTerminal
	 * @type {string}
	 */
	"redeemedByTerminal": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - redeemedByTerminal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.redeemedByTerminal in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - redeemedByTerminal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedByTerminal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.basePrice
	 * @type {number}
	 */
	"basePrice": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - basePrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.basePrice in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - basePrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("basePrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.ticketPrice
	 * @type {number}
	 */
	"ticketPrice": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - ticketPrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.ticketPrice in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - ticketPrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ticketPrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.currency
	 * @type {string}
	 */
	"currency": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - currency - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.currency in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - currency - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("currency");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.attendee
	 * @type {Customer}
	 */
	"attendee": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - attendee - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - attendee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.chanceCount
	 * @type {number}
	 */
	"chanceCount": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - chanceCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionPurchaseModel.chanceCount in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - chanceCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("chanceCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionPurchase.winner
	 * @type {CompetitionWinner}
	 */
	"winner": {
		"create": function(options){
			Console.log("CompetitionPurchaseMap - winner - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CompetitionPurchaseMap - winner - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("winner");
			}
			return options.target;
		}
	},
};
