/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RadioPresenter Map
 * @type {object}
 */
var RadioPresenterMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RadioPresenter.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("RadioPresenterMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPresenterModel.id in during mapping");
		},
		"update": function(options){
			Console.log("RadioPresenterMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPresenter.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("RadioPresenterMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPresenterModel.name in during mapping");
		},
		"update": function(options){
			Console.log("RadioPresenterMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPresenter.image
	 * @type {string}
	 */
	"image": {
		"create": function(options){
			Console.log("RadioPresenterMap - image - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPresenterModel.image in during mapping");
		},
		"update": function(options){
			Console.log("RadioPresenterMap - image - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("image");
			}
			return options.target;
		}
	},
	/**
	 * Contacts for this presenter
	 * @type {Contact}
	 */
	"contact": {
		"create": function(options){
			Console.log("RadioPresenterMap - contact - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("RadioPresenterMap - contact - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("contact");
			}
			return options.target;
		}
	},
	/**
	 * Property radioShows of harpoonApi.RadioPresenter describes a relationship hasMany harpoonApi.RadioShow
	 * @type {RadioShow}
	 */
	"radioShows": {
		"create": function(options){
			Console.log("RadioPresenterMap - radioShows - create");
			Console.debug(JSON.stringify(options));
			return new RadioShowModel();
		},
		"update": function(options){
			Console.log("RadioPresenterMap - radioShows - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioShows");
			}
			return options.target;
		}
	},
};
