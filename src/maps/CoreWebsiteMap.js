/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CoreWebsite Map
 * @type {object}
 */
var CoreWebsiteMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CoreWebsite.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("CoreWebsiteMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CoreWebsiteModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CoreWebsiteMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CoreWebsite.code
	 * @type {String}
	 */
	"code": {
		"create": function(options){
			Console.log("CoreWebsiteMap - code - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CoreWebsiteModel.code in during mapping");
		},
		"update": function(options){
			Console.log("CoreWebsiteMap - code - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("code");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CoreWebsite.name
	 * @type {String}
	 */
	"name": {
		"create": function(options){
			Console.log("CoreWebsiteMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CoreWebsiteModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CoreWebsiteMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CoreWebsite.sortOrder
	 * @type {Number}
	 */
	"sortOrder": {
		"create": function(options){
			Console.log("CoreWebsiteMap - sortOrder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CoreWebsiteModel.sortOrder in during mapping");
		},
		"update": function(options){
			Console.log("CoreWebsiteMap - sortOrder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sortOrder");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CoreWebsite.defaultGroupId
	 * @type {Number}
	 */
	"defaultGroupId": {
		"create": function(options){
			Console.log("CoreWebsiteMap - defaultGroupId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CoreWebsiteModel.defaultGroupId in during mapping");
		},
		"update": function(options){
			Console.log("CoreWebsiteMap - defaultGroupId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultGroupId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CoreWebsite.isDefault
	 * @type {Number}
	 */
	"isDefault": {
		"create": function(options){
			Console.log("CoreWebsiteMap - isDefault - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CoreWebsiteModel.isDefault in during mapping");
		},
		"update": function(options){
			Console.log("CoreWebsiteMap - isDefault - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isDefault");
			}
			return options.target;
		}
	},
};
