/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwEventbookingEventTicket Map
 * @type {object}
 */
var AwEventbookingEventTicketMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwEventbookingEventTicket.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.eventId
	 * @type {Number}
	 */
	"eventId": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - eventId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.eventId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - eventId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("eventId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.price
	 * @type {String}
	 */
	"price": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.price in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.priceType
	 * @type {String}
	 */
	"priceType": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - priceType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.priceType in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - priceType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("priceType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.sku
	 * @type {String}
	 */
	"sku": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - sku - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.sku in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - sku - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sku");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.qty
	 * @type {Number}
	 */
	"qty": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - qty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.qty in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - qty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.codeprefix
	 * @type {String}
	 */
	"codeprefix": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - codeprefix - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.codeprefix in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - codeprefix - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("codeprefix");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.fee
	 * @type {String}
	 */
	"fee": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - fee - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.fee in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - fee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("fee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.isPassOnFee
	 * @type {Number}
	 */
	"isPassOnFee": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - isPassOnFee - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.isPassOnFee in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - isPassOnFee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isPassOnFee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicket.chance
	 * @type {Number}
	 */
	"chance": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - chance - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketModel.chance in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - chance - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("chance");
			}
			return options.target;
		}
	},
	/**
	 * Property attributes of harpoonApi.AwEventbookingEventTicket describes a relationship hasMany harpoonApi.AwEventbookingEventTicketAttribute
	 * @type {AwEventbookingEventTicketAttribute}
	 */
	"attributes": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - attributes - create");
			Console.debug(JSON.stringify(options));
			return new AwEventbookingEventTicketAttributeModel();
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - attributes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attributes");
			}
			return options.target;
		}
	},
	/**
	 * Property tickets of harpoonApi.AwEventbookingEventTicket describes a relationship hasMany harpoonApi.AwEventbookingTicket
	 * @type {AwEventbookingTicket}
	 */
	"tickets": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - tickets - create");
			Console.debug(JSON.stringify(options));
			return new AwEventbookingTicketModel();
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - tickets - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tickets");
			}
			return options.target;
		}
	},
	/**
	 * Property awEventbookingTicket of harpoonApi.AwEventbookingEventTicket describes a relationship hasMany harpoonApi.AwEventbookingTicket
	 * @type {AwEventbookingTicket}
	 */
	"awEventbookingTicket": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketMap - awEventbookingTicket - create");
			Console.debug(JSON.stringify(options));
			return new AwEventbookingTicketModel();
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketMap - awEventbookingTicket - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("awEventbookingTicket");
			}
			return options.target;
		}
	},
};
