/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Brand Map
 * @type {object}
 */
var BrandMap = {
	// Ignore
	"ignore": ["category","coverUpload","logoUpload"],
	// Properties
	/**
	 * harpoonApi.Brand.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("BrandMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.id in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("BrandMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.name in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.logo
	 * @type {string}
	 */
	"logo": {
		"create": function(options){
			Console.log("BrandMap - logo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.logo in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - logo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("logo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.logoUpload
	 * @type {MagentoImageUpload}
	 */
	"logoUpload": {
		"create": function(options){
			Console.log("BrandMap - logoUpload - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("BrandMap - logoUpload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("logoUpload");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.cover
	 * @type {string}
	 */
	"cover": {
		"create": function(options){
			Console.log("BrandMap - cover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.cover in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - cover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.coverUpload
	 * @type {MagentoImageUpload}
	 */
	"coverUpload": {
		"create": function(options){
			Console.log("BrandMap - coverUpload - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("BrandMap - coverUpload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("coverUpload");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("BrandMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.description in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.email
	 * @type {string}
	 */
	"email": {
		"create": function(options){
			Console.log("BrandMap - email - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.email in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - email - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("email");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.phone
	 * @type {string}
	 */
	"phone": {
		"create": function(options){
			Console.log("BrandMap - phone - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.phone in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - phone - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("phone");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.address
	 * @type {string}
	 */
	"address": {
		"create": function(options){
			Console.log("BrandMap - address - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.address in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - address - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("address");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.managerName
	 * @type {string}
	 */
	"managerName": {
		"create": function(options){
			Console.log("BrandMap - managerName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.managerName in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - managerName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("managerName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.followerCount
	 * @type {number}
	 */
	"followerCount": {
		"create": function(options){
			Console.log("BrandMap - followerCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.followerCount in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - followerCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("followerCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.isFollowed
	 * @type {boolean}
	 */
	"isFollowed": {
		"create": function(options){
			Console.log("BrandMap - isFollowed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.isFollowed in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - isFollowed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isFollowed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.category
	 * @type {Category}
	 */
	"category": {
		"create": function(options){
			Console.log("BrandMap - category - create");
			Console.debug(JSON.stringify(options));
			return new CategoryModel();
		},
		"update": function(options){
			Console.log("BrandMap - category - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("category");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.categories
	 * @type {Category}
	 */
	"categories": {
		"create": function(options){
			Console.log("BrandMap - categories - create");
			Console.debug(JSON.stringify(options));
			return new CategoryModel();
		},
		"update": function(options){
			Console.log("BrandMap - categories - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("categories");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Brand.website
	 * @type {string}
	 */
	"website": {
		"create": function(options){
			Console.log("BrandMap - website - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BrandModel.website in during mapping");
		},
		"update": function(options){
			Console.log("BrandMap - website - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("website");
			}
			return options.target;
		}
	},
};
