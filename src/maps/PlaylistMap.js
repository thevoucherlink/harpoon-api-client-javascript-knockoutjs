/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Playlist Map
 * @type {object}
 */
var PlaylistMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Playlist.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("PlaylistMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistModel.id in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Playlist name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("PlaylistMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistModel.name in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * Playlist short description
	 * @type {string}
	 */
	"shortDescription": {
		"create": function(options){
			Console.log("PlaylistMap - shortDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistModel.shortDescription in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistMap - shortDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("shortDescription");
			}
			return options.target;
		}
	},
	/**
	 * Playlist image
	 * @type {string}
	 */
	"image": {
		"create": function(options){
			Console.log("PlaylistMap - image - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistModel.image in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistMap - image - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("image");
			}
			return options.target;
		}
	},
	/**
	 * If Playlist is included in Main
	 * @type {boolean}
	 */
	"isMain": {
		"create": function(options){
			Console.log("PlaylistMap - isMain - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistModel.isMain in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistMap - isMain - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isMain");
			}
			return options.target;
		}
	},
	/**
	 * Property playlistItems of harpoonApi.Playlist describes a relationship hasMany harpoonApi.PlaylistItem
	 * @type {PlaylistItem}
	 */
	"playlistItems": {
		"create": function(options){
			Console.log("PlaylistMap - playlistItems - create");
			Console.debug(JSON.stringify(options));
			return new PlaylistItemModel();
		},
		"update": function(options){
			Console.log("PlaylistMap - playlistItems - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playlistItems");
			}
			return options.target;
		}
	},
};
