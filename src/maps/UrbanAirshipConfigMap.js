/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * UrbanAirshipConfig Map
 * @type {object}
 */
var UrbanAirshipConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.UrbanAirshipConfig.developmentAppKey
	 * @type {string}
	 */
	"developmentAppKey": {
		"create": function(options){
			Console.log("UrbanAirshipConfigMap - developmentAppKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UrbanAirshipConfigModel.developmentAppKey in during mapping");
		},
		"update": function(options){
			Console.log("UrbanAirshipConfigMap - developmentAppKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("developmentAppKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UrbanAirshipConfig.developmentAppSecret
	 * @type {string}
	 */
	"developmentAppSecret": {
		"create": function(options){
			Console.log("UrbanAirshipConfigMap - developmentAppSecret - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UrbanAirshipConfigModel.developmentAppSecret in during mapping");
		},
		"update": function(options){
			Console.log("UrbanAirshipConfigMap - developmentAppSecret - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("developmentAppSecret");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UrbanAirshipConfig.developmentLogLevel
	 * @type {string}
	 */
	"developmentLogLevel": {
		"create": function(options){
			Console.log("UrbanAirshipConfigMap - developmentLogLevel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UrbanAirshipConfigModel.developmentLogLevel in during mapping");
		},
		"update": function(options){
			Console.log("UrbanAirshipConfigMap - developmentLogLevel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("developmentLogLevel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UrbanAirshipConfig.productionAppKey
	 * @type {string}
	 */
	"productionAppKey": {
		"create": function(options){
			Console.log("UrbanAirshipConfigMap - productionAppKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UrbanAirshipConfigModel.productionAppKey in during mapping");
		},
		"update": function(options){
			Console.log("UrbanAirshipConfigMap - productionAppKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productionAppKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UrbanAirshipConfig.productionAppSecret
	 * @type {string}
	 */
	"productionAppSecret": {
		"create": function(options){
			Console.log("UrbanAirshipConfigMap - productionAppSecret - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UrbanAirshipConfigModel.productionAppSecret in during mapping");
		},
		"update": function(options){
			Console.log("UrbanAirshipConfigMap - productionAppSecret - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productionAppSecret");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UrbanAirshipConfig.productionLogLevel
	 * @type {string}
	 */
	"productionLogLevel": {
		"create": function(options){
			Console.log("UrbanAirshipConfigMap - productionLogLevel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UrbanAirshipConfigModel.productionLogLevel in during mapping");
		},
		"update": function(options){
			Console.log("UrbanAirshipConfigMap - productionLogLevel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productionLogLevel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UrbanAirshipConfig.inProduction
	 * @type {boolean}
	 */
	"inProduction": {
		"create": function(options){
			Console.log("UrbanAirshipConfigMap - inProduction - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UrbanAirshipConfigModel.inProduction in during mapping");
		},
		"update": function(options){
			Console.log("UrbanAirshipConfigMap - inProduction - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("inProduction");
			}
			return options.target;
		}
	},
};
