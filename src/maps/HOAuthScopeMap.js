/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HOAuthScope Map
 * @type {object}
 */
var HOAuthScopeMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HOAuthScope.scope
	 * @type {string}
	 */
	"scope": {
		"create": function(options){
			Console.log("HOAuthScopeMap - scope - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthScopeModel.scope in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthScopeMap - scope - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scope");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthScope.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("HOAuthScopeMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthScopeModel.description in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthScopeMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthScope.iconURL
	 * @type {string}
	 */
	"iconURL": {
		"create": function(options){
			Console.log("HOAuthScopeMap - iconURL - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthScopeModel.iconURL in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthScopeMap - iconURL - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iconURL");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthScope.ttl
	 * @type {number}
	 */
	"ttl": {
		"create": function(options){
			Console.log("HOAuthScopeMap - ttl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthScopeModel.ttl in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthScopeMap - ttl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ttl");
			}
			return options.target;
		}
	},
};
