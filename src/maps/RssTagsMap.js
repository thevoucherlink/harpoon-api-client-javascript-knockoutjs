/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RssTags Map
 * @type {object}
 */
var RssTagsMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RssTags.imgTumbnail
	 * @type {string}
	 */
	"imgTumbnail": {
		"create": function(options){
			Console.log("RssTagsMap - imgTumbnail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssTagsModel.imgTumbnail in during mapping");
		},
		"update": function(options){
			Console.log("RssTagsMap - imgTumbnail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgTumbnail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssTags.imgMain
	 * @type {string}
	 */
	"imgMain": {
		"create": function(options){
			Console.log("RssTagsMap - imgMain - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssTagsModel.imgMain in during mapping");
		},
		"update": function(options){
			Console.log("RssTagsMap - imgMain - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgMain");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssTags.link
	 * @type {string}
	 */
	"link": {
		"create": function(options){
			Console.log("RssTagsMap - link - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssTagsModel.link in during mapping");
		},
		"update": function(options){
			Console.log("RssTagsMap - link - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("link");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssTags.postDateTime
	 * @type {string}
	 */
	"postDateTime": {
		"create": function(options){
			Console.log("RssTagsMap - postDateTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssTagsModel.postDateTime in during mapping");
		},
		"update": function(options){
			Console.log("RssTagsMap - postDateTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("postDateTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssTags.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("RssTagsMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssTagsModel.description in during mapping");
		},
		"update": function(options){
			Console.log("RssTagsMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssTags.category
	 * @type {string}
	 */
	"category": {
		"create": function(options){
			Console.log("RssTagsMap - category - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssTagsModel.category in during mapping");
		},
		"update": function(options){
			Console.log("RssTagsMap - category - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("category");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssTags.title
	 * @type {string}
	 */
	"title": {
		"create": function(options){
			Console.log("RssTagsMap - title - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssTagsModel.title in during mapping");
		},
		"update": function(options){
			Console.log("RssTagsMap - title - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("title");
			}
			return options.target;
		}
	},
};
