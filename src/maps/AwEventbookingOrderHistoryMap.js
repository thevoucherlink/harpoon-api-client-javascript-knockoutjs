/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwEventbookingOrderHistory Map
 * @type {object}
 */
var AwEventbookingOrderHistoryMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwEventbookingOrderHistory.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwEventbookingOrderHistoryMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingOrderHistoryModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingOrderHistoryMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingOrderHistory.eventTicketId
	 * @type {Number}
	 */
	"eventTicketId": {
		"create": function(options){
			Console.log("AwEventbookingOrderHistoryMap - eventTicketId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingOrderHistoryModel.eventTicketId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingOrderHistoryMap - eventTicketId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("eventTicketId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingOrderHistory.orderItemId
	 * @type {Number}
	 */
	"orderItemId": {
		"create": function(options){
			Console.log("AwEventbookingOrderHistoryMap - orderItemId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingOrderHistoryModel.orderItemId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingOrderHistoryMap - orderItemId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderItemId");
			}
			return options.target;
		}
	},
};
