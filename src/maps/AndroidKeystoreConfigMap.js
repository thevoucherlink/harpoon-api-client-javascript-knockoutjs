/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AndroidKeystoreConfig Map
 * @type {object}
 */
var AndroidKeystoreConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AndroidKeystoreConfig.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("AndroidKeystoreConfigMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AndroidKeystoreConfigModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AndroidKeystoreConfigMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AndroidKeystoreConfig.storePassword
	 * @type {string}
	 */
	"storePassword": {
		"create": function(options){
			Console.log("AndroidKeystoreConfigMap - storePassword - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AndroidKeystoreConfigModel.storePassword in during mapping");
		},
		"update": function(options){
			Console.log("AndroidKeystoreConfigMap - storePassword - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("storePassword");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AndroidKeystoreConfig.keyAlias
	 * @type {string}
	 */
	"keyAlias": {
		"create": function(options){
			Console.log("AndroidKeystoreConfigMap - keyAlias - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AndroidKeystoreConfigModel.keyAlias in during mapping");
		},
		"update": function(options){
			Console.log("AndroidKeystoreConfigMap - keyAlias - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("keyAlias");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AndroidKeystoreConfig.keyPassword
	 * @type {string}
	 */
	"keyPassword": {
		"create": function(options){
			Console.log("AndroidKeystoreConfigMap - keyPassword - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AndroidKeystoreConfigModel.keyPassword in during mapping");
		},
		"update": function(options){
			Console.log("AndroidKeystoreConfigMap - keyPassword - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("keyPassword");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AndroidKeystoreConfig.hashKey
	 * @type {string}
	 */
	"hashKey": {
		"create": function(options){
			Console.log("AndroidKeystoreConfigMap - hashKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AndroidKeystoreConfigModel.hashKey in during mapping");
		},
		"update": function(options){
			Console.log("AndroidKeystoreConfigMap - hashKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hashKey");
			}
			return options.target;
		}
	},
};
