/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HarpoonHpublicBrandvenue Map
 * @type {object}
 */
var HarpoonHpublicBrandvenueMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.brandId
	 * @type {Number}
	 */
	"brandId": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - brandId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.brandId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - brandId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.name
	 * @type {String}
	 */
	"name": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.name in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.address
	 * @type {String}
	 */
	"address": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - address - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.address in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - address - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("address");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.latitude
	 * @type {Number}
	 */
	"latitude": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - latitude - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.latitude in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - latitude - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("latitude");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.longitude
	 * @type {Number}
	 */
	"longitude": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - longitude - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.longitude in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - longitude - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("longitude");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.updatedAt
	 * @type {Date}
	 */
	"updatedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - updatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.updatedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - updatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.email
	 * @type {String}
	 */
	"email": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - email - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.email in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - email - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("email");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.phone
	 * @type {String}
	 */
	"phone": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - phone - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.phone in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - phone - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("phone");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicBrandvenue.country
	 * @type {String}
	 */
	"country": {
		"create": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - country - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicBrandvenueModel.country in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicBrandvenueMap - country - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("country");
			}
			return options.target;
		}
	},
};
