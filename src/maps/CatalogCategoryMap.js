/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CatalogCategory Map
 * @type {object}
 */
var CatalogCategoryMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CatalogCategory.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("CatalogCategoryMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.parentId
	 * @type {Number}
	 */
	"parentId": {
		"create": function(options){
			Console.log("CatalogCategoryMap - parentId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.parentId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - parentId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("parentId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("CatalogCategoryMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.updatedAt
	 * @type {Date}
	 */
	"updatedAt": {
		"create": function(options){
			Console.log("CatalogCategoryMap - updatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.updatedAt in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - updatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.path
	 * @type {String}
	 */
	"path": {
		"create": function(options){
			Console.log("CatalogCategoryMap - path - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.path in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - path - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("path");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.position
	 * @type {Number}
	 */
	"position": {
		"create": function(options){
			Console.log("CatalogCategoryMap - position - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.position in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - position - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("position");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.level
	 * @type {Number}
	 */
	"level": {
		"create": function(options){
			Console.log("CatalogCategoryMap - level - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.level in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - level - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("level");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.childrenCount
	 * @type {Number}
	 */
	"childrenCount": {
		"create": function(options){
			Console.log("CatalogCategoryMap - childrenCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.childrenCount in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - childrenCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("childrenCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.storeId
	 * @type {Number}
	 */
	"storeId": {
		"create": function(options){
			Console.log("CatalogCategoryMap - storeId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.storeId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - storeId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("storeId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.allChildren
	 * @type {String}
	 */
	"allChildren": {
		"create": function(options){
			Console.log("CatalogCategoryMap - allChildren - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.allChildren in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - allChildren - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("allChildren");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.availableSortBy
	 * @type {String}
	 */
	"availableSortBy": {
		"create": function(options){
			Console.log("CatalogCategoryMap - availableSortBy - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.availableSortBy in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - availableSortBy - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("availableSortBy");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.children
	 * @type {String}
	 */
	"children": {
		"create": function(options){
			Console.log("CatalogCategoryMap - children - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.children in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - children - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("children");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.customApplyToProducts
	 * @type {Number}
	 */
	"customApplyToProducts": {
		"create": function(options){
			Console.log("CatalogCategoryMap - customApplyToProducts - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.customApplyToProducts in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - customApplyToProducts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customApplyToProducts");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.customDesign
	 * @type {String}
	 */
	"customDesign": {
		"create": function(options){
			Console.log("CatalogCategoryMap - customDesign - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.customDesign in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - customDesign - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customDesign");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.customDesignFrom
	 * @type {Date}
	 */
	"customDesignFrom": {
		"create": function(options){
			Console.log("CatalogCategoryMap - customDesignFrom - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.customDesignFrom in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - customDesignFrom - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customDesignFrom");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.customDesignTo
	 * @type {Date}
	 */
	"customDesignTo": {
		"create": function(options){
			Console.log("CatalogCategoryMap - customDesignTo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.customDesignTo in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - customDesignTo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customDesignTo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.customLayoutUpdate
	 * @type {String}
	 */
	"customLayoutUpdate": {
		"create": function(options){
			Console.log("CatalogCategoryMap - customLayoutUpdate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.customLayoutUpdate in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - customLayoutUpdate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customLayoutUpdate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.customUseParentSettings
	 * @type {Number}
	 */
	"customUseParentSettings": {
		"create": function(options){
			Console.log("CatalogCategoryMap - customUseParentSettings - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.customUseParentSettings in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - customUseParentSettings - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customUseParentSettings");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.defaultSortBy
	 * @type {String}
	 */
	"defaultSortBy": {
		"create": function(options){
			Console.log("CatalogCategoryMap - defaultSortBy - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.defaultSortBy in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - defaultSortBy - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultSortBy");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.description
	 * @type {String}
	 */
	"description": {
		"create": function(options){
			Console.log("CatalogCategoryMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.description in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.displayMode
	 * @type {String}
	 */
	"displayMode": {
		"create": function(options){
			Console.log("CatalogCategoryMap - displayMode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.displayMode in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - displayMode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("displayMode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.filterPriceRange
	 * @type {String}
	 */
	"filterPriceRange": {
		"create": function(options){
			Console.log("CatalogCategoryMap - filterPriceRange - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.filterPriceRange in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - filterPriceRange - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("filterPriceRange");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.image
	 * @type {String}
	 */
	"image": {
		"create": function(options){
			Console.log("CatalogCategoryMap - image - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.image in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - image - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("image");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.includeInMenu
	 * @type {Number}
	 */
	"includeInMenu": {
		"create": function(options){
			Console.log("CatalogCategoryMap - includeInMenu - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.includeInMenu in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - includeInMenu - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("includeInMenu");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.isActive
	 * @type {Number}
	 */
	"isActive": {
		"create": function(options){
			Console.log("CatalogCategoryMap - isActive - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.isActive in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - isActive - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isActive");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.isAnchor
	 * @type {Number}
	 */
	"isAnchor": {
		"create": function(options){
			Console.log("CatalogCategoryMap - isAnchor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.isAnchor in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - isAnchor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isAnchor");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.landingPage
	 * @type {Number}
	 */
	"landingPage": {
		"create": function(options){
			Console.log("CatalogCategoryMap - landingPage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.landingPage in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - landingPage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("landingPage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.metaDescription
	 * @type {String}
	 */
	"metaDescription": {
		"create": function(options){
			Console.log("CatalogCategoryMap - metaDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.metaDescription in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - metaDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metaDescription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.metaKeywords
	 * @type {String}
	 */
	"metaKeywords": {
		"create": function(options){
			Console.log("CatalogCategoryMap - metaKeywords - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.metaKeywords in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - metaKeywords - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metaKeywords");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.metaTitle
	 * @type {String}
	 */
	"metaTitle": {
		"create": function(options){
			Console.log("CatalogCategoryMap - metaTitle - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.metaTitle in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - metaTitle - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metaTitle");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.name
	 * @type {String}
	 */
	"name": {
		"create": function(options){
			Console.log("CatalogCategoryMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.pageLayout
	 * @type {String}
	 */
	"pageLayout": {
		"create": function(options){
			Console.log("CatalogCategoryMap - pageLayout - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.pageLayout in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - pageLayout - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pageLayout");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.pathInStore
	 * @type {String}
	 */
	"pathInStore": {
		"create": function(options){
			Console.log("CatalogCategoryMap - pathInStore - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.pathInStore in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - pathInStore - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pathInStore");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.thumbnail
	 * @type {String}
	 */
	"thumbnail": {
		"create": function(options){
			Console.log("CatalogCategoryMap - thumbnail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.thumbnail in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - thumbnail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("thumbnail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.ummCatLabel
	 * @type {String}
	 */
	"ummCatLabel": {
		"create": function(options){
			Console.log("CatalogCategoryMap - ummCatLabel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.ummCatLabel in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - ummCatLabel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ummCatLabel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.ummCatTarget
	 * @type {String}
	 */
	"ummCatTarget": {
		"create": function(options){
			Console.log("CatalogCategoryMap - ummCatTarget - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.ummCatTarget in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - ummCatTarget - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ummCatTarget");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.ummDdBlocks
	 * @type {String}
	 */
	"ummDdBlocks": {
		"create": function(options){
			Console.log("CatalogCategoryMap - ummDdBlocks - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.ummDdBlocks in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - ummDdBlocks - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ummDdBlocks");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.ummDdColumns
	 * @type {Number}
	 */
	"ummDdColumns": {
		"create": function(options){
			Console.log("CatalogCategoryMap - ummDdColumns - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.ummDdColumns in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - ummDdColumns - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ummDdColumns");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.ummDdProportions
	 * @type {String}
	 */
	"ummDdProportions": {
		"create": function(options){
			Console.log("CatalogCategoryMap - ummDdProportions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.ummDdProportions in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - ummDdProportions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ummDdProportions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.ummDdType
	 * @type {String}
	 */
	"ummDdType": {
		"create": function(options){
			Console.log("CatalogCategoryMap - ummDdType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.ummDdType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - ummDdType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ummDdType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.ummDdWidth
	 * @type {String}
	 */
	"ummDdWidth": {
		"create": function(options){
			Console.log("CatalogCategoryMap - ummDdWidth - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.ummDdWidth in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - ummDdWidth - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ummDdWidth");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.urlKey
	 * @type {String}
	 */
	"urlKey": {
		"create": function(options){
			Console.log("CatalogCategoryMap - urlKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.urlKey in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - urlKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategory.urlPath
	 * @type {String}
	 */
	"urlPath": {
		"create": function(options){
			Console.log("CatalogCategoryMap - urlPath - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryModel.urlPath in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - urlPath - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlPath");
			}
			return options.target;
		}
	},
	/**
	 * Property catalogProducts of harpoonApi.CatalogCategory describes a relationship hasMany harpoonApi.CatalogProduct
	 * @type {CatalogProduct}
	 */
	"catalogProducts": {
		"create": function(options){
			Console.log("CatalogCategoryMap - catalogProducts - create");
			Console.debug(JSON.stringify(options));
			return new CatalogProductModel();
		},
		"update": function(options){
			Console.log("CatalogCategoryMap - catalogProducts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("catalogProducts");
			}
			return options.target;
		}
	},
};
