/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CouponPurchase Map
 * @type {object}
 */
var CouponPurchaseMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CouponPurchase.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CouponPurchaseMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.orderId
	 * @type {number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("CouponPurchaseMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("CouponPurchaseMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.code
	 * @type {CouponPurchaseCode}
	 */
	"code": {
		"create": function(options){
			Console.log("CouponPurchaseMap - code - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - code - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("code");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.isAvailable
	 * @type {boolean}
	 */
	"isAvailable": {
		"create": function(options){
			Console.log("CouponPurchaseMap - isAvailable - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.isAvailable in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - isAvailable - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isAvailable");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.createdAt
	 * @type {date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("CouponPurchaseMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.expiredAt
	 * @type {date}
	 */
	"expiredAt": {
		"create": function(options){
			Console.log("CouponPurchaseMap - expiredAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.expiredAt in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - expiredAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiredAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.redeemedAt
	 * @type {date}
	 */
	"redeemedAt": {
		"create": function(options){
			Console.log("CouponPurchaseMap - redeemedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.redeemedAt in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - redeemedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.redeemedByTerminal
	 * @type {string}
	 */
	"redeemedByTerminal": {
		"create": function(options){
			Console.log("CouponPurchaseMap - redeemedByTerminal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.redeemedByTerminal in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - redeemedByTerminal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedByTerminal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.redemptionType
	 * @type {string}
	 */
	"redemptionType": {
		"create": function(options){
			Console.log("CouponPurchaseMap - redemptionType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.redemptionType in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - redemptionType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redemptionType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.status
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("CouponPurchaseMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.status in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CouponPurchase.unlockTime
	 * @type {number}
	 */
	"unlockTime": {
		"create": function(options){
			Console.log("CouponPurchaseMap - unlockTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CouponPurchaseModel.unlockTime in during mapping");
		},
		"update": function(options){
			Console.log("CouponPurchaseMap - unlockTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("unlockTime");
			}
			return options.target;
		}
	},
};
