/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RadioPlaySession Map
 * @type {object}
 */
var RadioPlaySessionMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RadioPlaySession.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.id in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPlaySession.sessionTime
	 * @type {number}
	 */
	"sessionTime": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - sessionTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.sessionTime in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - sessionTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sessionTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPlaySession.playUpdateTime
	 * @type {date}
	 */
	"playUpdateTime": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - playUpdateTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.playUpdateTime in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - playUpdateTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playUpdateTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPlaySession.radioStreamId
	 * @type {number}
	 */
	"radioStreamId": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - radioStreamId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.radioStreamId in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - radioStreamId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioStreamId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPlaySession.radioShowId
	 * @type {number}
	 */
	"radioShowId": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - radioShowId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.radioShowId in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - radioShowId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioShowId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPlaySession.playEndTime
	 * @type {date}
	 */
	"playEndTime": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - playEndTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.playEndTime in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - playEndTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playEndTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPlaySession.playStartTime
	 * @type {date}
	 */
	"playStartTime": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - playStartTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.playStartTime in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - playStartTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playStartTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioPlaySession.customerId
	 * @type {number}
	 */
	"customerId": {
		"create": function(options){
			Console.log("RadioPlaySessionMap - customerId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioPlaySessionModel.customerId in during mapping");
		},
		"update": function(options){
			Console.log("RadioPlaySessionMap - customerId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customerId");
			}
			return options.target;
		}
	},
};
