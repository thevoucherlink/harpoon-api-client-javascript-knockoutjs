/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwEventbookingTicket Map
 * @type {object}
 */
var AwEventbookingTicketMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwEventbookingTicket.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.eventTicketId
	 * @type {Number}
	 */
	"eventTicketId": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - eventTicketId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.eventTicketId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - eventTicketId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("eventTicketId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.orderItemId
	 * @type {Number}
	 */
	"orderItemId": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - orderItemId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.orderItemId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - orderItemId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderItemId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.code
	 * @type {String}
	 */
	"code": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - code - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.code in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - code - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("code");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.redeemed
	 * @type {Number}
	 */
	"redeemed": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - redeemed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.redeemed in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - redeemed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.paymentStatus
	 * @type {Number}
	 */
	"paymentStatus": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - paymentStatus - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.paymentStatus in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - paymentStatus - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("paymentStatus");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.redeemedAt
	 * @type {Date}
	 */
	"redeemedAt": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - redeemedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.redeemedAt in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - redeemedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.redeemedByTerminal
	 * @type {String}
	 */
	"redeemedByTerminal": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - redeemedByTerminal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.redeemedByTerminal in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - redeemedByTerminal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedByTerminal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.fee
	 * @type {String}
	 */
	"fee": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - fee - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.fee in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - fee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("fee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.isPassOnFee
	 * @type {Number}
	 */
	"isPassOnFee": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - isPassOnFee - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.isPassOnFee in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - isPassOnFee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isPassOnFee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingTicket.competitionwinnerId
	 * @type {Number}
	 */
	"competitionwinnerId": {
		"create": function(options){
			Console.log("AwEventbookingTicketMap - competitionwinnerId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingTicketModel.competitionwinnerId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingTicketMap - competitionwinnerId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionwinnerId");
			}
			return options.target;
		}
	},
};
