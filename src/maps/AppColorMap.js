/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AppColor Map
 * @type {object}
 */
var AppColorMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AppColor.appPrimary
	 * @type {string}
	 */
	"appPrimary": {
		"create": function(options){
			Console.log("AppColorMap - appPrimary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.appPrimary in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - appPrimary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appPrimary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.appSecondary
	 * @type {string}
	 */
	"appSecondary": {
		"create": function(options){
			Console.log("AppColorMap - appSecondary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.appSecondary in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - appSecondary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appSecondary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.textPrimary
	 * @type {string}
	 */
	"textPrimary": {
		"create": function(options){
			Console.log("AppColorMap - textPrimary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.textPrimary in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - textPrimary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("textPrimary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.textSecondary
	 * @type {string}
	 */
	"textSecondary": {
		"create": function(options){
			Console.log("AppColorMap - textSecondary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.textSecondary in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - textSecondary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("textSecondary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioNavBar
	 * @type {string}
	 */
	"radioNavBar": {
		"create": function(options){
			Console.log("AppColorMap - radioNavBar - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioNavBar in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioNavBar - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioNavBar");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioPlayer
	 * @type {string}
	 */
	"radioPlayer": {
		"create": function(options){
			Console.log("AppColorMap - radioPlayer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioPlayer in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioPlayer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioPlayer");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioProgressBar
	 * @type {string}
	 */
	"radioProgressBar": {
		"create": function(options){
			Console.log("AppColorMap - radioProgressBar - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioProgressBar in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioProgressBar - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioProgressBar");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.menuBackground
	 * @type {string}
	 */
	"menuBackground": {
		"create": function(options){
			Console.log("AppColorMap - menuBackground - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.menuBackground in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - menuBackground - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("menuBackground");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioTextSecondary
	 * @type {string}
	 */
	"radioTextSecondary": {
		"create": function(options){
			Console.log("AppColorMap - radioTextSecondary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioTextSecondary in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioTextSecondary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioTextSecondary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioTextPrimary
	 * @type {string}
	 */
	"radioTextPrimary": {
		"create": function(options){
			Console.log("AppColorMap - radioTextPrimary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioTextPrimary in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioTextPrimary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioTextPrimary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.menuItem
	 * @type {string}
	 */
	"menuItem": {
		"create": function(options){
			Console.log("AppColorMap - menuItem - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.menuItem in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - menuItem - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("menuItem");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.appGradientPrimary
	 * @type {string}
	 */
	"appGradientPrimary": {
		"create": function(options){
			Console.log("AppColorMap - appGradientPrimary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.appGradientPrimary in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - appGradientPrimary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appGradientPrimary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.feedSegmentIndicator
	 * @type {string}
	 */
	"feedSegmentIndicator": {
		"create": function(options){
			Console.log("AppColorMap - feedSegmentIndicator - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.feedSegmentIndicator in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - feedSegmentIndicator - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("feedSegmentIndicator");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioPlayButton
	 * @type {string}
	 */
	"radioPlayButton": {
		"create": function(options){
			Console.log("AppColorMap - radioPlayButton - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioPlayButton in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioPlayButton - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioPlayButton");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioPauseButton
	 * @type {string}
	 */
	"radioPauseButton": {
		"create": function(options){
			Console.log("AppColorMap - radioPauseButton - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioPauseButton in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioPauseButton - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioPauseButton");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioPlayButtonBackground
	 * @type {string}
	 */
	"radioPlayButtonBackground": {
		"create": function(options){
			Console.log("AppColorMap - radioPlayButtonBackground - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioPlayButtonBackground in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioPlayButtonBackground - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioPlayButtonBackground");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioPauseButtonBackground
	 * @type {string}
	 */
	"radioPauseButtonBackground": {
		"create": function(options){
			Console.log("AppColorMap - radioPauseButtonBackground - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioPauseButtonBackground in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioPauseButtonBackground - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioPauseButtonBackground");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppColor.radioPlayerItem
	 * @type {string}
	 */
	"radioPlayerItem": {
		"create": function(options){
			Console.log("AppColorMap - radioPlayerItem - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppColorModel.radioPlayerItem in during mapping");
		},
		"update": function(options){
			Console.log("AppColorMap - radioPlayerItem - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioPlayerItem");
			}
			return options.target;
		}
	},
};
