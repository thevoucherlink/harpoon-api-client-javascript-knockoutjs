/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailDashAppInvitePartnerNewUser Map
 * @type {object}
 */
var EmailDashAppInvitePartnerNewUserMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailDashAppInvitePartnerNewUser.appList
	 * @type {string}
	 */
	"appList": {
		"create": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - appList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashAppInvitePartnerNewUserModel.appList in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - appList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashAppInvitePartnerNewUser.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashAppInvitePartnerNewUserModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashAppInvitePartnerNewUser.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashAppInvitePartnerNewUserModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashAppInvitePartnerNewUser.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashAppInvitePartnerNewUserModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashAppInvitePartnerNewUser.senderFirstName
	 * @type {string}
	 */
	"senderFirstName": {
		"create": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - senderFirstName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashAppInvitePartnerNewUserModel.senderFirstName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - senderFirstName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("senderFirstName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashAppInvitePartnerNewUser.verificationLink
	 * @type {string}
	 */
	"verificationLink": {
		"create": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - verificationLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashAppInvitePartnerNewUserModel.verificationLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashAppInvitePartnerNewUserMap - verificationLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("verificationLink");
			}
			return options.target;
		}
	},
};
