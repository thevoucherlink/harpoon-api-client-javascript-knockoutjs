/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignInvoice Map
 * @type {object}
 */
var EmailAppCampaignInvoiceMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignInvoice.appName
	 * @type {string}
	 */
	"appName": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - appName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.appName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - appName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.billingAddressLineOne
	 * @type {string}
	 */
	"billingAddressLineOne": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - billingAddressLineOne - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.billingAddressLineOne in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - billingAddressLineOne - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingAddressLineOne");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.billingAddressLineTwo
	 * @type {string}
	 */
	"billingAddressLineTwo": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - billingAddressLineTwo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.billingAddressLineTwo in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - billingAddressLineTwo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingAddressLineTwo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.billingAddressLineThree
	 * @type {string}
	 */
	"billingAddressLineThree": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - billingAddressLineThree - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.billingAddressLineThree in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - billingAddressLineThree - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingAddressLineThree");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.campaignOrderDate
	 * @type {date}
	 */
	"campaignOrderDate": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - campaignOrderDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.campaignOrderDate in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - campaignOrderDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignOrderDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.campaignPayBy
	 * @type {string}
	 */
	"campaignPayBy": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - campaignPayBy - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.campaignPayBy in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - campaignPayBy - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignPayBy");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.cardholderName
	 * @type {string}
	 */
	"cardholderName": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - cardholderName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.cardholderName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - cardholderName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cardholderName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.orderId
	 * @type {number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.price
	 * @type {string}
	 */
	"price": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.price in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.quantity
	 * @type {string}
	 */
	"quantity": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - quantity - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.quantity in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - quantity - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("quantity");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.ticketType
	 * @type {string}
	 */
	"ticketType": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - ticketType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.ticketType in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - ticketType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ticketType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.totalPrice
	 * @type {string}
	 */
	"totalPrice": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - totalPrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.totalPrice in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - totalPrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("totalPrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.userName
	 * @type {string}
	 */
	"userName": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - userName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.userName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - userName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.sku
	 * @type {string}
	 */
	"sku": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - sku - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.sku in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - sku - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sku");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignInvoice.tax
	 * @type {string}
	 */
	"tax": {
		"create": function(options){
			Console.log("EmailAppCampaignInvoiceMap - tax - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignInvoiceModel.tax in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignInvoiceMap - tax - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tax");
			}
			return options.target;
		}
	},
};
