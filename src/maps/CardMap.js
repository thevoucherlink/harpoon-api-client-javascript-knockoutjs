/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Card Map
 * @type {object}
 */
var CardMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Card.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CardMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Card.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("CardMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Card.type
	 * @type {string}
	 */
	"type": {
		"create": function(options){
			Console.log("CardMap - type - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.type in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Card.funding
	 * @type {string}
	 */
	"funding": {
		"create": function(options){
			Console.log("CardMap - funding - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.funding in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - funding - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("funding");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Card.lastDigits
	 * @type {string}
	 */
	"lastDigits": {
		"create": function(options){
			Console.log("CardMap - lastDigits - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.lastDigits in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - lastDigits - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("lastDigits");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Card.exMonth
	 * @type {string}
	 */
	"exMonth": {
		"create": function(options){
			Console.log("CardMap - exMonth - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.exMonth in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - exMonth - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("exMonth");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Card.exYear
	 * @type {string}
	 */
	"exYear": {
		"create": function(options){
			Console.log("CardMap - exYear - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.exYear in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - exYear - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("exYear");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Card.cardholderName
	 * @type {string}
	 */
	"cardholderName": {
		"create": function(options){
			Console.log("CardMap - cardholderName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CardModel.cardholderName in during mapping");
		},
		"update": function(options){
			Console.log("CardMap - cardholderName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cardholderName");
			}
			return options.target;
		}
	},
};
