/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Customer Map
 * @type {object}
 */
var CustomerMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Customer.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CustomerMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.email
	 * @type {string}
	 */
	"email": {
		"create": function(options){
			Console.log("CustomerMap - email - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.email in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - email - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("email");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.firstName
	 * @type {string}
	 */
	"firstName": {
		"create": function(options){
			Console.log("CustomerMap - firstName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.firstName in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - firstName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("firstName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.lastName
	 * @type {string}
	 */
	"lastName": {
		"create": function(options){
			Console.log("CustomerMap - lastName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.lastName in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - lastName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("lastName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.dob
	 * @type {string}
	 */
	"dob": {
		"create": function(options){
			Console.log("CustomerMap - dob - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.dob in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - dob - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dob");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.gender
	 * @type {string}
	 */
	"gender": {
		"create": function(options){
			Console.log("CustomerMap - gender - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.gender in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - gender - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("gender");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.password
	 * @type {string}
	 */
	"password": {
		"create": function(options){
			Console.log("CustomerMap - password - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.password in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - password - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("password");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.profilePicture
	 * @type {string}
	 */
	"profilePicture": {
		"create": function(options){
			Console.log("CustomerMap - profilePicture - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.profilePicture in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - profilePicture - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("profilePicture");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.cover
	 * @type {string}
	 */
	"cover": {
		"create": function(options){
			Console.log("CustomerMap - cover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.cover in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - cover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.profilePictureUpload
	 * @type {MagentoImageUpload}
	 */
	"profilePictureUpload": {
		"create": function(options){
			Console.log("CustomerMap - profilePictureUpload - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerMap - profilePictureUpload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("profilePictureUpload");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.coverUpload
	 * @type {MagentoImageUpload}
	 */
	"coverUpload": {
		"create": function(options){
			Console.log("CustomerMap - coverUpload - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerMap - coverUpload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("coverUpload");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.metadata
	 * @type {object}
	 */
	"metadata": {
		"create": function(options){
			Console.log("CustomerMap - metadata - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.metadata in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - metadata - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metadata");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.connection
	 * @type {CustomerConnection}
	 */
	"connection": {
		"create": function(options){
			Console.log("CustomerMap - connection - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerMap - connection - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("connection");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.privacy
	 * @type {CustomerPrivacy}
	 */
	"privacy": {
		"create": function(options){
			Console.log("CustomerMap - privacy - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerMap - privacy - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("privacy");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.followingCount
	 * @type {number}
	 */
	"followingCount": {
		"create": function(options){
			Console.log("CustomerMap - followingCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.followingCount in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - followingCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("followingCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.followerCount
	 * @type {number}
	 */
	"followerCount": {
		"create": function(options){
			Console.log("CustomerMap - followerCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.followerCount in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - followerCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("followerCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.isFollowed
	 * @type {boolean}
	 */
	"isFollowed": {
		"create": function(options){
			Console.log("CustomerMap - isFollowed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.isFollowed in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - isFollowed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isFollowed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.isFollower
	 * @type {boolean}
	 */
	"isFollower": {
		"create": function(options){
			Console.log("CustomerMap - isFollower - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.isFollower in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - isFollower - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isFollower");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.notificationCount
	 * @type {number}
	 */
	"notificationCount": {
		"create": function(options){
			Console.log("CustomerMap - notificationCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.notificationCount in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - notificationCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.badge
	 * @type {CustomerBadge}
	 */
	"badge": {
		"create": function(options){
			Console.log("CustomerMap - badge - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CustomerMap - badge - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("badge");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Customer.authorizationCode
	 * @type {string}
	 */
	"authorizationCode": {
		"create": function(options){
			Console.log("CustomerMap - authorizationCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerModel.authorizationCode in during mapping");
		},
		"update": function(options){
			Console.log("CustomerMap - authorizationCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("authorizationCode");
			}
			return options.target;
		}
	},
};
