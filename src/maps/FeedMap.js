/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Feed Map
 * @type {object}
 */
var FeedMap = {
	// Ignore
	"ignore": ["coverUpload"],
	// Properties
	/**
	 * harpoonApi.Feed.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("FeedMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.id in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.message
	 * @type {string}
	 */
	"message": {
		"create": function(options){
			Console.log("FeedMap - message - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.message in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - message - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("message");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.cover
	 * @type {string}
	 */
	"cover": {
		"create": function(options){
			Console.log("FeedMap - cover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.cover in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - cover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.coverUpload
	 * @type {MagentoImageUpload}
	 */
	"coverUpload": {
		"create": function(options){
			Console.log("FeedMap - coverUpload - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("FeedMap - coverUpload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("coverUpload");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.related
	 * @type {object}
	 */
	"related": {
		"create": function(options){
			Console.log("FeedMap - related - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.related in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - related - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("related");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.link
	 * @type {string}
	 */
	"link": {
		"create": function(options){
			Console.log("FeedMap - link - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.link in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - link - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("link");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.actionCode
	 * @type {string}
	 */
	"actionCode": {
		"create": function(options){
			Console.log("FeedMap - actionCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.actionCode in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - actionCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("actionCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.privacyCode
	 * @type {string}
	 */
	"privacyCode": {
		"create": function(options){
			Console.log("FeedMap - privacyCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.privacyCode in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - privacyCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("privacyCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Feed.postedAt
	 * @type {date}
	 */
	"postedAt": {
		"create": function(options){
			Console.log("FeedMap - postedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FeedModel.postedAt in during mapping");
		},
		"update": function(options){
			Console.log("FeedMap - postedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("postedAt");
			}
			return options.target;
		}
	},
};
