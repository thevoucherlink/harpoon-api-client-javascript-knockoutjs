/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * StripeDiscount Map
 * @type {object}
 */
var StripeDiscountMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.StripeDiscount.object
	 * @type {string}
	 */
	"object": {
		"create": function(options){
			Console.log("StripeDiscountMap - object - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeDiscountModel.object in during mapping");
		},
		"update": function(options){
			Console.log("StripeDiscountMap - object - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("object");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeDiscount.customer
	 * @type {string}
	 */
	"customer": {
		"create": function(options){
			Console.log("StripeDiscountMap - customer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeDiscountModel.customer in during mapping");
		},
		"update": function(options){
			Console.log("StripeDiscountMap - customer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customer");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeDiscount.end
	 * @type {number}
	 */
	"end": {
		"create": function(options){
			Console.log("StripeDiscountMap - end - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeDiscountModel.end in during mapping");
		},
		"update": function(options){
			Console.log("StripeDiscountMap - end - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("end");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeDiscount.start
	 * @type {number}
	 */
	"start": {
		"create": function(options){
			Console.log("StripeDiscountMap - start - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeDiscountModel.start in during mapping");
		},
		"update": function(options){
			Console.log("StripeDiscountMap - start - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("start");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeDiscount.subscription
	 * @type {string}
	 */
	"subscription": {
		"create": function(options){
			Console.log("StripeDiscountMap - subscription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeDiscountModel.subscription in during mapping");
		},
		"update": function(options){
			Console.log("StripeDiscountMap - subscription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("subscription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeDiscount.coupon
	 * @type {StripeCoupon}
	 */
	"coupon": {
		"create": function(options){
			Console.log("StripeDiscountMap - coupon - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("StripeDiscountMap - coupon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("coupon");
			}
			return options.target;
		}
	},
};
