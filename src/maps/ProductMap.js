/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Product Map
 * @type {object}
 */
var ProductMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Product.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("ProductMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ProductModel.id in during mapping");
		},
		"update": function(options){
			Console.log("ProductMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Product.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("ProductMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ProductModel.name in during mapping");
		},
		"update": function(options){
			Console.log("ProductMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Product.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("ProductMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ProductModel.description in during mapping");
		},
		"update": function(options){
			Console.log("ProductMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Product.cover
	 * @type {string}
	 */
	"cover": {
		"create": function(options){
			Console.log("ProductMap - cover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ProductModel.cover in during mapping");
		},
		"update": function(options){
			Console.log("ProductMap - cover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Product.price
	 * @type {number}
	 */
	"price": {
		"create": function(options){
			Console.log("ProductMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ProductModel.price in during mapping");
		},
		"update": function(options){
			Console.log("ProductMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Product.base_currency
	 * @type {string}
	 */
	"base_currency": {
		"create": function(options){
			Console.log("ProductMap - base_currency - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create ProductModel.base_currency in during mapping");
		},
		"update": function(options){
			Console.log("ProductMap - base_currency - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("base_currency");
			}
			return options.target;
		}
	},
};
