/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * StripeSubscription Map
 * @type {object}
 */
var StripeSubscriptionMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.StripeSubscription.applicationFeePercent
	 * @type {number}
	 */
	"applicationFeePercent": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - applicationFeePercent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.applicationFeePercent in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - applicationFeePercent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("applicationFeePercent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.cancelAtPeriodEnd
	 * @type {boolean}
	 */
	"cancelAtPeriodEnd": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - cancelAtPeriodEnd - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.cancelAtPeriodEnd in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - cancelAtPeriodEnd - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cancelAtPeriodEnd");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.canceledAt
	 * @type {number}
	 */
	"canceledAt": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - canceledAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.canceledAt in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - canceledAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("canceledAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.currentPeriodEnd
	 * @type {number}
	 */
	"currentPeriodEnd": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - currentPeriodEnd - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.currentPeriodEnd in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - currentPeriodEnd - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("currentPeriodEnd");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.currentPeriodStart
	 * @type {number}
	 */
	"currentPeriodStart": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - currentPeriodStart - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.currentPeriodStart in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - currentPeriodStart - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("currentPeriodStart");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.metadata
	 * @type {string}
	 */
	"metadata": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - metadata - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.metadata in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - metadata - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metadata");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.quantity
	 * @type {number}
	 */
	"quantity": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - quantity - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.quantity in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - quantity - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("quantity");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.start
	 * @type {number}
	 */
	"start": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - start - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.start in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - start - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("start");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.status
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.status in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.taxPercent
	 * @type {number}
	 */
	"taxPercent": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - taxPercent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.taxPercent in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - taxPercent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("taxPercent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.trialEnd
	 * @type {number}
	 */
	"trialEnd": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - trialEnd - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.trialEnd in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - trialEnd - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("trialEnd");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.trialStart
	 * @type {number}
	 */
	"trialStart": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - trialStart - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.trialStart in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - trialStart - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("trialStart");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.customer
	 * @type {string}
	 */
	"customer": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - customer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.customer in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - customer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customer");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.discount
	 * @type {StripeDiscount}
	 */
	"discount": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - discount - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - discount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("discount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.plan
	 * @type {StripePlan}
	 */
	"plan": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - plan - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - plan - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("plan");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.object
	 * @type {string}
	 */
	"object": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - object - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.object in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - object - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("object");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeSubscription.endedAt
	 * @type {number}
	 */
	"endedAt": {
		"create": function(options){
			Console.log("StripeSubscriptionMap - endedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeSubscriptionModel.endedAt in during mapping");
		},
		"update": function(options){
			Console.log("StripeSubscriptionMap - endedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("endedAt");
			}
			return options.target;
		}
	},
};
