/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RadioStream Map
 * @type {object}
 */
var RadioStreamMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RadioStream.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("RadioStreamMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.id in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioStream.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("RadioStreamMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.name in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioStream.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("RadioStreamMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.description in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioStream.urlHQ
	 * @type {string}
	 */
	"urlHQ": {
		"create": function(options){
			Console.log("RadioStreamMap - urlHQ - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.urlHQ in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - urlHQ - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlHQ");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioStream.urlLQ
	 * @type {string}
	 */
	"urlLQ": {
		"create": function(options){
			Console.log("RadioStreamMap - urlLQ - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.urlLQ in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - urlLQ - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlLQ");
			}
			return options.target;
		}
	},
	/**
	 * When the Stream starts of being public
	 * @type {date}
	 */
	"starts": {
		"create": function(options){
			Console.log("RadioStreamMap - starts - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.starts in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - starts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("starts");
			}
			return options.target;
		}
	},
	/**
	 * When the Stream ceases of being public
	 * @type {date}
	 */
	"ends": {
		"create": function(options){
			Console.log("RadioStreamMap - ends - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.ends in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - ends - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ends");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioStream.metaDataUrl
	 * @type {string}
	 */
	"metaDataUrl": {
		"create": function(options){
			Console.log("RadioStreamMap - metaDataUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.metaDataUrl in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - metaDataUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metaDataUrl");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioStream.imgStream
	 * @type {string}
	 */
	"imgStream": {
		"create": function(options){
			Console.log("RadioStreamMap - imgStream - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.imgStream in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - imgStream - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgStream");
			}
			return options.target;
		}
	},
	/**
	 * Url of the sponsor MP3 to be played
	 * @type {string}
	 */
	"sponsorTrack": {
		"create": function(options){
			Console.log("RadioStreamMap - sponsorTrack - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioStreamModel.sponsorTrack in during mapping");
		},
		"update": function(options){
			Console.log("RadioStreamMap - sponsorTrack - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sponsorTrack");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioStream.tritonConfig
	 * @type {TritonConfig}
	 */
	"tritonConfig": {
		"create": function(options){
			Console.log("RadioStreamMap - tritonConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("RadioStreamMap - tritonConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tritonConfig");
			}
			return options.target;
		}
	},
	/**
	 * Property radioShows of harpoonApi.RadioStream describes a relationship hasMany harpoonApi.RadioShow
	 * @type {RadioShow}
	 */
	"radioShows": {
		"create": function(options){
			Console.log("RadioStreamMap - radioShows - create");
			Console.debug(JSON.stringify(options));
			return new RadioShowModel();
		},
		"update": function(options){
			Console.log("RadioStreamMap - radioShows - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioShows");
			}
			return options.target;
		}
	},
};
