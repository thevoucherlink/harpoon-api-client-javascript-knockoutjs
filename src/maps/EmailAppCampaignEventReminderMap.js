/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignEventReminder Map
 * @type {object}
 */
var EmailAppCampaignEventReminderMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.campaignEnd
	 * @type {string}
	 */
	"campaignEnd": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignEnd - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.campaignEnd in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignEnd - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignEnd");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.campaignEndTime
	 * @type {string}
	 */
	"campaignEndTime": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignEndTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.campaignEndTime in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignEndTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignEndTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.campaignStart
	 * @type {string}
	 */
	"campaignStart": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignStart - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.campaignStart in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignStart - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignStart");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.campaignStartTime
	 * @type {string}
	 */
	"campaignStartTime": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignStartTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.campaignStartTime in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - campaignStartTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignStartTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.userName
	 * @type {string}
	 */
	"userName": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - userName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.userName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - userName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventReminder.reminderDays
	 * @type {number}
	 */
	"reminderDays": {
		"create": function(options){
			Console.log("EmailAppCampaignEventReminderMap - reminderDays - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventReminderModel.reminderDays in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventReminderMap - reminderDays - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("reminderDays");
			}
			return options.target;
		}
	},
};
