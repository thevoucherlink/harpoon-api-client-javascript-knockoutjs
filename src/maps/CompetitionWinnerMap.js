/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CompetitionWinner Map
 * @type {object}
 */
var CompetitionWinnerMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CompetitionWinner.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CompetitionWinnerMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionWinnerModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionWinnerMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionWinner.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("CompetitionWinnerMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionWinnerModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionWinnerMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionWinner.position
	 * @type {string}
	 */
	"position": {
		"create": function(options){
			Console.log("CompetitionWinnerMap - position - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionWinnerModel.position in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionWinnerMap - position - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("position");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CompetitionWinner.chosenAt
	 * @type {date}
	 */
	"chosenAt": {
		"create": function(options){
			Console.log("CompetitionWinnerMap - chosenAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionWinnerModel.chosenAt in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionWinnerMap - chosenAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("chosenAt");
			}
			return options.target;
		}
	},
};
