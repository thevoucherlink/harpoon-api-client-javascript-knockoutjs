/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AppAd Map
 * @type {object}
 */
var AppAdMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AppAd.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("AppAdMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppAdModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AppAdMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppAd.unitId
	 * @type {string}
	 */
	"unitId": {
		"create": function(options){
			Console.log("AppAdMap - unitId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppAdModel.unitId in during mapping");
		},
		"update": function(options){
			Console.log("AppAdMap - unitId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("unitId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppAd.unitName
	 * @type {string}
	 */
	"unitName": {
		"create": function(options){
			Console.log("AppAdMap - unitName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppAdModel.unitName in during mapping");
		},
		"update": function(options){
			Console.log("AppAdMap - unitName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("unitName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppAd.unitType
	 * @type {string}
	 */
	"unitType": {
		"create": function(options){
			Console.log("AppAdMap - unitType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppAdModel.unitType in during mapping");
		},
		"update": function(options){
			Console.log("AppAdMap - unitType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("unitType");
			}
			return options.target;
		}
	},
};
