/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EventPurchase Map
 * @type {object}
 */
var EventPurchaseMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EventPurchase.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("EventPurchaseMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.id in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.orderId
	 * @type {number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("EventPurchaseMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("EventPurchaseMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.name in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.event
	 * @type {Event}
	 */
	"event": {
		"create": function(options){
			Console.log("EventPurchaseMap - event - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("EventPurchaseMap - event - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("event");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.redemptionType
	 * @type {string}
	 */
	"redemptionType": {
		"create": function(options){
			Console.log("EventPurchaseMap - redemptionType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.redemptionType in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - redemptionType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redemptionType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.unlockTime
	 * @type {number}
	 */
	"unlockTime": {
		"create": function(options){
			Console.log("EventPurchaseMap - unlockTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.unlockTime in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - unlockTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("unlockTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.code
	 * @type {string}
	 */
	"code": {
		"create": function(options){
			Console.log("EventPurchaseMap - code - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.code in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - code - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("code");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.qrcode
	 * @type {string}
	 */
	"qrcode": {
		"create": function(options){
			Console.log("EventPurchaseMap - qrcode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.qrcode in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - qrcode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qrcode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.isAvailable
	 * @type {boolean}
	 */
	"isAvailable": {
		"create": function(options){
			Console.log("EventPurchaseMap - isAvailable - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.isAvailable in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - isAvailable - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isAvailable");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.status
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("EventPurchaseMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.status in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.createdAt
	 * @type {date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("EventPurchaseMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.expiredAt
	 * @type {date}
	 */
	"expiredAt": {
		"create": function(options){
			Console.log("EventPurchaseMap - expiredAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.expiredAt in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - expiredAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiredAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.redeemedAt
	 * @type {date}
	 */
	"redeemedAt": {
		"create": function(options){
			Console.log("EventPurchaseMap - redeemedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.redeemedAt in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - redeemedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.redeemedByTerminal
	 * @type {string}
	 */
	"redeemedByTerminal": {
		"create": function(options){
			Console.log("EventPurchaseMap - redeemedByTerminal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.redeemedByTerminal in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - redeemedByTerminal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedByTerminal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.basePrice
	 * @type {number}
	 */
	"basePrice": {
		"create": function(options){
			Console.log("EventPurchaseMap - basePrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.basePrice in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - basePrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("basePrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.ticketPrice
	 * @type {number}
	 */
	"ticketPrice": {
		"create": function(options){
			Console.log("EventPurchaseMap - ticketPrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.ticketPrice in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - ticketPrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ticketPrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.currency
	 * @type {string}
	 */
	"currency": {
		"create": function(options){
			Console.log("EventPurchaseMap - currency - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventPurchaseModel.currency in during mapping");
		},
		"update": function(options){
			Console.log("EventPurchaseMap - currency - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("currency");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventPurchase.attendee
	 * @type {Customer}
	 */
	"attendee": {
		"create": function(options){
			Console.log("EventPurchaseMap - attendee - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("EventPurchaseMap - attendee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendee");
			}
			return options.target;
		}
	},
};
