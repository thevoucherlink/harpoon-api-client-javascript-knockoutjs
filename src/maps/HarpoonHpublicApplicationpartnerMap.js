/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HarpoonHpublicApplicationpartner Map
 * @type {object}
 */
var HarpoonHpublicApplicationpartnerMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.applicationId
	 * @type {Number}
	 */
	"applicationId": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - applicationId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.applicationId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - applicationId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("applicationId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.brandId
	 * @type {Number}
	 */
	"brandId": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - brandId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.brandId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - brandId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.status
	 * @type {String}
	 */
	"status": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.status in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.invitedEmail
	 * @type {String}
	 */
	"invitedEmail": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - invitedEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.invitedEmail in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - invitedEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("invitedEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.invitedAt
	 * @type {Date}
	 */
	"invitedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - invitedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.invitedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - invitedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("invitedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.acceptedAt
	 * @type {Date}
	 */
	"acceptedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - acceptedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.acceptedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - acceptedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("acceptedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.updatedAt
	 * @type {Date}
	 */
	"updatedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - updatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.updatedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - updatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configList
	 * @type {Number}
	 */
	"configList": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configList in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configFeed
	 * @type {Number}
	 */
	"configFeed": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configFeed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configFeed in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configFeed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configFeed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configNeedFollow
	 * @type {Number}
	 */
	"configNeedFollow": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configNeedFollow - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configNeedFollow in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configNeedFollow - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configNeedFollow");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configAcceptEvent
	 * @type {Number}
	 */
	"configAcceptEvent": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptEvent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configAcceptEvent in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptEvent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptEvent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configAcceptCoupon
	 * @type {Number}
	 */
	"configAcceptCoupon": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptCoupon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configAcceptCoupon in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptCoupon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptCoupon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configAcceptDealsimple
	 * @type {Number}
	 */
	"configAcceptDealsimple": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptDealsimple - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configAcceptDealsimple in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptDealsimple - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptDealsimple");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configAcceptDealgroup
	 * @type {Number}
	 */
	"configAcceptDealgroup": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptDealgroup - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configAcceptDealgroup in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptDealgroup - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptDealgroup");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configAcceptNotificationpush
	 * @type {Number}
	 */
	"configAcceptNotificationpush": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptNotificationpush - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configAcceptNotificationpush in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptNotificationpush - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptNotificationpush");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configAcceptNotificationbeacon
	 * @type {Number}
	 */
	"configAcceptNotificationbeacon": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptNotificationbeacon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configAcceptNotificationbeacon in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configAcceptNotificationbeacon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configAcceptNotificationbeacon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.isOwner
	 * @type {Number}
	 */
	"isOwner": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - isOwner - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.isOwner in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - isOwner - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isOwner");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configApproveEvent
	 * @type {Number}
	 */
	"configApproveEvent": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveEvent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configApproveEvent in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveEvent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configApproveEvent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configApproveCoupon
	 * @type {Number}
	 */
	"configApproveCoupon": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveCoupon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configApproveCoupon in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveCoupon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configApproveCoupon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configApproveDealsimple
	 * @type {Number}
	 */
	"configApproveDealsimple": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveDealsimple - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configApproveDealsimple in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveDealsimple - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configApproveDealsimple");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicApplicationpartner.configApproveDealgroup
	 * @type {Number}
	 */
	"configApproveDealgroup": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveDealgroup - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.configApproveDealgroup in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - configApproveDealgroup - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("configApproveDealgroup");
			}
			return options.target;
		}
	},
	/**
	 * Property udropshipVendor of harpoonApi.HarpoonHpublicApplicationpartner describes a relationship belongsTo harpoonApi.UdropshipVendor
	 * @type {object}
	 */
	"udropshipVendor": {
		"create": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - udropshipVendor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicApplicationpartnerModel.udropshipVendor in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicApplicationpartnerMap - udropshipVendor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendor");
			}
			return options.target;
		}
	},
};
