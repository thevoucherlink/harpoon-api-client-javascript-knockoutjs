/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * PlaylistItem Map
 * @type {object}
 */
var PlaylistItemMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.PlaylistItem.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("PlaylistItemMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.id in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Stream title
	 * @type {string}
	 */
	"title": {
		"create": function(options){
			Console.log("PlaylistItemMap - title - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.title in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - title - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("title");
			}
			return options.target;
		}
	},
	/**
	 * Stream short description (on SDKs is under desc)
	 * @type {string}
	 */
	"shortDescription": {
		"create": function(options){
			Console.log("PlaylistItemMap - shortDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.shortDescription in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - shortDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("shortDescription");
			}
			return options.target;
		}
	},
	/**
	 * Playlist image
	 * @type {string}
	 */
	"image": {
		"create": function(options){
			Console.log("PlaylistItemMap - image - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.image in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - image - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("image");
			}
			return options.target;
		}
	},
	/**
	 * Stream file
	 * @type {string}
	 */
	"file": {
		"create": function(options){
			Console.log("PlaylistItemMap - file - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.file in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - file - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("file");
			}
			return options.target;
		}
	},
	/**
	 * Stream type (used only in JS SDK)
	 * @type {string}
	 */
	"type": {
		"create": function(options){
			Console.log("PlaylistItemMap - type - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.type in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * Stream media type (e.g. video , audio , radioStream)
	 * @type {string}
	 */
	"mediaType": {
		"create": function(options){
			Console.log("PlaylistItemMap - mediaType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.mediaType in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - mediaType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("mediaType");
			}
			return options.target;
		}
	},
	/**
	 * Ad media id (on SDKs is under mediaid)
	 * @type {number}
	 */
	"mediaId": {
		"create": function(options){
			Console.log("PlaylistItemMap - mediaId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.mediaId in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - mediaId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("mediaId");
			}
			return options.target;
		}
	},
	/**
	 * Sort order index
	 * @type {number}
	 */
	"order": {
		"create": function(options){
			Console.log("PlaylistItemMap - order - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.order in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - order - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("order");
			}
			return options.target;
		}
	},
	/**
	 * Property playlist of harpoonApi.PlaylistItem describes a relationship belongsTo harpoonApi.Playlist
	 * @type {object}
	 */
	"playlist": {
		"create": function(options){
			Console.log("PlaylistItemMap - playlist - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlaylistItemModel.playlist in during mapping");
		},
		"update": function(options){
			Console.log("PlaylistItemMap - playlist - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playlist");
			}
			return options.target;
		}
	},
	/**
	 * Property playerSources of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.PlayerSource
	 * @type {PlayerSource}
	 */
	"playerSources": {
		"create": function(options){
			Console.log("PlaylistItemMap - playerSources - create");
			Console.debug(JSON.stringify(options));
			return new PlayerSourceModel();
		},
		"update": function(options){
			Console.log("PlaylistItemMap - playerSources - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playerSources");
			}
			return options.target;
		}
	},
	/**
	 * Property playerTracks of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.PlayerTrack
	 * @type {PlayerTrack}
	 */
	"playerTracks": {
		"create": function(options){
			Console.log("PlaylistItemMap - playerTracks - create");
			Console.debug(JSON.stringify(options));
			return new PlayerTrackModel();
		},
		"update": function(options){
			Console.log("PlaylistItemMap - playerTracks - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playerTracks");
			}
			return options.target;
		}
	},
	/**
	 * Property radioShows of harpoonApi.PlaylistItem describes a relationship hasMany harpoonApi.RadioShow
	 * @type {RadioShow}
	 */
	"radioShows": {
		"create": function(options){
			Console.log("PlaylistItemMap - radioShows - create");
			Console.debug(JSON.stringify(options));
			return new RadioShowModel();
		},
		"update": function(options){
			Console.log("PlaylistItemMap - radioShows - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioShows");
			}
			return options.target;
		}
	},
};
