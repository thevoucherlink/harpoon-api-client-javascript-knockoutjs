/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HarpoonHpublicv12VendorAppCategory Map
 * @type {object}
 */
var HarpoonHpublicv12VendorAppCategoryMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HarpoonHpublicv12VendorAppCategory.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicv12VendorAppCategoryModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicv12VendorAppCategory.appId
	 * @type {Number}
	 */
	"appId": {
		"create": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - appId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicv12VendorAppCategoryModel.appId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - appId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicv12VendorAppCategory.vendorId
	 * @type {Number}
	 */
	"vendorId": {
		"create": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - vendorId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicv12VendorAppCategoryModel.vendorId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - vendorId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicv12VendorAppCategory.categoryId
	 * @type {Number}
	 */
	"categoryId": {
		"create": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - categoryId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicv12VendorAppCategoryModel.categoryId in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - categoryId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("categoryId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicv12VendorAppCategory.isPrimary
	 * @type {Number}
	 */
	"isPrimary": {
		"create": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - isPrimary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicv12VendorAppCategoryModel.isPrimary in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - isPrimary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isPrimary");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicv12VendorAppCategory.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicv12VendorAppCategoryModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HarpoonHpublicv12VendorAppCategory.updatedAt
	 * @type {Date}
	 */
	"updatedAt": {
		"create": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - updatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HarpoonHpublicv12VendorAppCategoryModel.updatedAt in during mapping");
		},
		"update": function(options){
			Console.log("HarpoonHpublicv12VendorAppCategoryMap - updatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedAt");
			}
			return options.target;
		}
	},
};
