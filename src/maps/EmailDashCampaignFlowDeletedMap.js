/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailDashCampaignFlowDeleted Map
 * @type {object}
 */
var EmailDashCampaignFlowDeletedMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailDashCampaignFlowDeleted.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowDeletedModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowDeleted.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowDeletedModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowDeleted.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowDeletedModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowDeleted.userName
	 * @type {string}
	 */
	"userName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - userName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowDeletedModel.userName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowDeletedMap - userName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userName");
			}
			return options.target;
		}
	},
};
