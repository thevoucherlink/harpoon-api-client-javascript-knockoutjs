/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CatalogProduct Map
 * @type {object}
 */
var CatalogProductMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CatalogProduct.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("CatalogProductMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.attributeSetId
	 * @type {Number}
	 */
	"attributeSetId": {
		"create": function(options){
			Console.log("CatalogProductMap - attributeSetId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.attributeSetId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - attributeSetId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attributeSetId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.typeId
	 * @type {String}
	 */
	"typeId": {
		"create": function(options){
			Console.log("CatalogProductMap - typeId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.typeId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - typeId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("typeId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.actionText
	 * @type {String}
	 */
	"actionText": {
		"create": function(options){
			Console.log("CatalogProductMap - actionText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.actionText in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - actionText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("actionText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.agreementId
	 * @type {Number}
	 */
	"agreementId": {
		"create": function(options){
			Console.log("CatalogProductMap - agreementId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.agreementId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - agreementId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("agreementId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.alias
	 * @type {String}
	 */
	"alias": {
		"create": function(options){
			Console.log("CatalogProductMap - alias - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.alias in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - alias - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("alias");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.altLink
	 * @type {String}
	 */
	"altLink": {
		"create": function(options){
			Console.log("CatalogProductMap - altLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.altLink in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - altLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("altLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.campaignId
	 * @type {String}
	 */
	"campaignId": {
		"create": function(options){
			Console.log("CatalogProductMap - campaignId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.campaignId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - campaignId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.campaignLiveFrom
	 * @type {Date}
	 */
	"campaignLiveFrom": {
		"create": function(options){
			Console.log("CatalogProductMap - campaignLiveFrom - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.campaignLiveFrom in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - campaignLiveFrom - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignLiveFrom");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.campaignLiveTo
	 * @type {Date}
	 */
	"campaignLiveTo": {
		"create": function(options){
			Console.log("CatalogProductMap - campaignLiveTo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.campaignLiveTo in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - campaignLiveTo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignLiveTo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.campaignShared
	 * @type {String}
	 */
	"campaignShared": {
		"create": function(options){
			Console.log("CatalogProductMap - campaignShared - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.campaignShared in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - campaignShared - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignShared");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.campaignStatus
	 * @type {String}
	 */
	"campaignStatus": {
		"create": function(options){
			Console.log("CatalogProductMap - campaignStatus - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.campaignStatus in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - campaignStatus - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignStatus");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.campaignType
	 * @type {String}
	 */
	"campaignType": {
		"create": function(options){
			Console.log("CatalogProductMap - campaignType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.campaignType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - campaignType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.checkoutLink
	 * @type {String}
	 */
	"checkoutLink": {
		"create": function(options){
			Console.log("CatalogProductMap - checkoutLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.checkoutLink in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - checkoutLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("checkoutLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.collectionNotes
	 * @type {String}
	 */
	"collectionNotes": {
		"create": function(options){
			Console.log("CatalogProductMap - collectionNotes - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.collectionNotes in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - collectionNotes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("collectionNotes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.cost
	 * @type {String}
	 */
	"cost": {
		"create": function(options){
			Console.log("CatalogProductMap - cost - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.cost in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - cost - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cost");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.couponCodeText
	 * @type {String}
	 */
	"couponCodeText": {
		"create": function(options){
			Console.log("CatalogProductMap - couponCodeText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.couponCodeText in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - couponCodeText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponCodeText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.couponCodeType
	 * @type {String}
	 */
	"couponCodeType": {
		"create": function(options){
			Console.log("CatalogProductMap - couponCodeType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.couponCodeType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - couponCodeType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponCodeType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.couponType
	 * @type {String}
	 */
	"couponType": {
		"create": function(options){
			Console.log("CatalogProductMap - couponType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.couponType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - couponType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("CatalogProductMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.creatorId
	 * @type {String}
	 */
	"creatorId": {
		"create": function(options){
			Console.log("CatalogProductMap - creatorId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.creatorId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - creatorId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("creatorId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.description
	 * @type {String}
	 */
	"description": {
		"create": function(options){
			Console.log("CatalogProductMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.description in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.externalId
	 * @type {String}
	 */
	"externalId": {
		"create": function(options){
			Console.log("CatalogProductMap - externalId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.externalId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - externalId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("externalId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookAttendingCount
	 * @type {String}
	 */
	"facebookAttendingCount": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookAttendingCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookAttendingCount in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookAttendingCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookAttendingCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookCategory
	 * @type {String}
	 */
	"facebookCategory": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookCategory - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookCategory in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookCategory - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookCategory");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookId
	 * @type {String}
	 */
	"facebookId": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookMaybeCount
	 * @type {String}
	 */
	"facebookMaybeCount": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookMaybeCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookMaybeCount in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookMaybeCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookMaybeCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookNode
	 * @type {String}
	 */
	"facebookNode": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookNode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookNode in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookNode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookNode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookNoreplyCount
	 * @type {String}
	 */
	"facebookNoreplyCount": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookNoreplyCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookNoreplyCount in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookNoreplyCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookNoreplyCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookPlace
	 * @type {String}
	 */
	"facebookPlace": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookPlace - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookPlace in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookPlace - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookPlace");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.facebookUpdatedTime
	 * @type {Date}
	 */
	"facebookUpdatedTime": {
		"create": function(options){
			Console.log("CatalogProductMap - facebookUpdatedTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.facebookUpdatedTime in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - facebookUpdatedTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookUpdatedTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.giftMessageAvailable
	 * @type {Number}
	 */
	"giftMessageAvailable": {
		"create": function(options){
			Console.log("CatalogProductMap - giftMessageAvailable - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.giftMessageAvailable in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - giftMessageAvailable - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("giftMessageAvailable");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.hasOptions
	 * @type {Number}
	 */
	"hasOptions": {
		"create": function(options){
			Console.log("CatalogProductMap - hasOptions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.hasOptions in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - hasOptions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hasOptions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.image
	 * @type {String}
	 */
	"image": {
		"create": function(options){
			Console.log("CatalogProductMap - image - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.image in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - image - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("image");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.imageLabel
	 * @type {String}
	 */
	"imageLabel": {
		"create": function(options){
			Console.log("CatalogProductMap - imageLabel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.imageLabel in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - imageLabel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imageLabel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.isRecurring
	 * @type {Number}
	 */
	"isRecurring": {
		"create": function(options){
			Console.log("CatalogProductMap - isRecurring - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.isRecurring in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - isRecurring - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isRecurring");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.linksExist
	 * @type {Number}
	 */
	"linksExist": {
		"create": function(options){
			Console.log("CatalogProductMap - linksExist - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.linksExist in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - linksExist - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("linksExist");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.linksPurchasedSeparately
	 * @type {Number}
	 */
	"linksPurchasedSeparately": {
		"create": function(options){
			Console.log("CatalogProductMap - linksPurchasedSeparately - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.linksPurchasedSeparately in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - linksPurchasedSeparately - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("linksPurchasedSeparately");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.linksTitle
	 * @type {String}
	 */
	"linksTitle": {
		"create": function(options){
			Console.log("CatalogProductMap - linksTitle - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.linksTitle in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - linksTitle - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("linksTitle");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.locationLink
	 * @type {String}
	 */
	"locationLink": {
		"create": function(options){
			Console.log("CatalogProductMap - locationLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.locationLink in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - locationLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("locationLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.msrp
	 * @type {String}
	 */
	"msrp": {
		"create": function(options){
			Console.log("CatalogProductMap - msrp - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.msrp in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - msrp - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("msrp");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.msrpDisplayActualPriceType
	 * @type {String}
	 */
	"msrpDisplayActualPriceType": {
		"create": function(options){
			Console.log("CatalogProductMap - msrpDisplayActualPriceType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.msrpDisplayActualPriceType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - msrpDisplayActualPriceType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("msrpDisplayActualPriceType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.msrpEnabled
	 * @type {Number}
	 */
	"msrpEnabled": {
		"create": function(options){
			Console.log("CatalogProductMap - msrpEnabled - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.msrpEnabled in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - msrpEnabled - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("msrpEnabled");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.name
	 * @type {String}
	 */
	"name": {
		"create": function(options){
			Console.log("CatalogProductMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.name in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.newsFromDate
	 * @type {Date}
	 */
	"newsFromDate": {
		"create": function(options){
			Console.log("CatalogProductMap - newsFromDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.newsFromDate in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - newsFromDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("newsFromDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.newsToDate
	 * @type {Date}
	 */
	"newsToDate": {
		"create": function(options){
			Console.log("CatalogProductMap - newsToDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.newsToDate in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - newsToDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("newsToDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.notificationBadge
	 * @type {String}
	 */
	"notificationBadge": {
		"create": function(options){
			Console.log("CatalogProductMap - notificationBadge - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.notificationBadge in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - notificationBadge - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationBadge");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.notificationInvisible
	 * @type {Number}
	 */
	"notificationInvisible": {
		"create": function(options){
			Console.log("CatalogProductMap - notificationInvisible - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.notificationInvisible in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - notificationInvisible - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationInvisible");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.notificationSilent
	 * @type {Number}
	 */
	"notificationSilent": {
		"create": function(options){
			Console.log("CatalogProductMap - notificationSilent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.notificationSilent in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - notificationSilent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationSilent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.partnerId
	 * @type {String}
	 */
	"partnerId": {
		"create": function(options){
			Console.log("CatalogProductMap - partnerId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.partnerId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - partnerId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("partnerId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.passedValidation
	 * @type {Number}
	 */
	"passedValidation": {
		"create": function(options){
			Console.log("CatalogProductMap - passedValidation - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.passedValidation in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - passedValidation - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("passedValidation");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.price
	 * @type {String}
	 */
	"price": {
		"create": function(options){
			Console.log("CatalogProductMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.price in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.priceText
	 * @type {String}
	 */
	"priceText": {
		"create": function(options){
			Console.log("CatalogProductMap - priceText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.priceText in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - priceText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("priceText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.priceType
	 * @type {Number}
	 */
	"priceType": {
		"create": function(options){
			Console.log("CatalogProductMap - priceType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.priceType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - priceType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("priceType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.priceView
	 * @type {Number}
	 */
	"priceView": {
		"create": function(options){
			Console.log("CatalogProductMap - priceView - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.priceView in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - priceView - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("priceView");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.pushwooshIds
	 * @type {String}
	 */
	"pushwooshIds": {
		"create": function(options){
			Console.log("CatalogProductMap - pushwooshIds - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.pushwooshIds in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - pushwooshIds - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pushwooshIds");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.pushwooshTokens
	 * @type {String}
	 */
	"pushwooshTokens": {
		"create": function(options){
			Console.log("CatalogProductMap - pushwooshTokens - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.pushwooshTokens in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - pushwooshTokens - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pushwooshTokens");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.recurringProfile
	 * @type {String}
	 */
	"recurringProfile": {
		"create": function(options){
			Console.log("CatalogProductMap - recurringProfile - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.recurringProfile in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - recurringProfile - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("recurringProfile");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.redemptionType
	 * @type {String}
	 */
	"redemptionType": {
		"create": function(options){
			Console.log("CatalogProductMap - redemptionType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.redemptionType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - redemptionType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redemptionType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.requiredOptions
	 * @type {Number}
	 */
	"requiredOptions": {
		"create": function(options){
			Console.log("CatalogProductMap - requiredOptions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.requiredOptions in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - requiredOptions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("requiredOptions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.reviewHtml
	 * @type {String}
	 */
	"reviewHtml": {
		"create": function(options){
			Console.log("CatalogProductMap - reviewHtml - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.reviewHtml in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - reviewHtml - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("reviewHtml");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.shipmentType
	 * @type {Number}
	 */
	"shipmentType": {
		"create": function(options){
			Console.log("CatalogProductMap - shipmentType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.shipmentType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - shipmentType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("shipmentType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.shortDescription
	 * @type {String}
	 */
	"shortDescription": {
		"create": function(options){
			Console.log("CatalogProductMap - shortDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.shortDescription in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - shortDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("shortDescription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.sku
	 * @type {String}
	 */
	"sku": {
		"create": function(options){
			Console.log("CatalogProductMap - sku - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.sku in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - sku - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sku");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.skuType
	 * @type {Number}
	 */
	"skuType": {
		"create": function(options){
			Console.log("CatalogProductMap - skuType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.skuType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - skuType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("skuType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.smallImage
	 * @type {String}
	 */
	"smallImage": {
		"create": function(options){
			Console.log("CatalogProductMap - smallImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.smallImage in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - smallImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("smallImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.smallImageLabel
	 * @type {String}
	 */
	"smallImageLabel": {
		"create": function(options){
			Console.log("CatalogProductMap - smallImageLabel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.smallImageLabel in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - smallImageLabel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("smallImageLabel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.specialFromDate
	 * @type {Date}
	 */
	"specialFromDate": {
		"create": function(options){
			Console.log("CatalogProductMap - specialFromDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.specialFromDate in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - specialFromDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("specialFromDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.specialPrice
	 * @type {String}
	 */
	"specialPrice": {
		"create": function(options){
			Console.log("CatalogProductMap - specialPrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.specialPrice in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - specialPrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("specialPrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.specialToDate
	 * @type {Date}
	 */
	"specialToDate": {
		"create": function(options){
			Console.log("CatalogProductMap - specialToDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.specialToDate in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - specialToDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("specialToDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.taxClassId
	 * @type {Number}
	 */
	"taxClassId": {
		"create": function(options){
			Console.log("CatalogProductMap - taxClassId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.taxClassId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - taxClassId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("taxClassId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.thumbnail
	 * @type {String}
	 */
	"thumbnail": {
		"create": function(options){
			Console.log("CatalogProductMap - thumbnail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.thumbnail in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - thumbnail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("thumbnail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.thumbnailLabel
	 * @type {String}
	 */
	"thumbnailLabel": {
		"create": function(options){
			Console.log("CatalogProductMap - thumbnailLabel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.thumbnailLabel in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - thumbnailLabel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("thumbnailLabel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.udropshipCalculateRates
	 * @type {Number}
	 */
	"udropshipCalculateRates": {
		"create": function(options){
			Console.log("CatalogProductMap - udropshipCalculateRates - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.udropshipCalculateRates in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - udropshipCalculateRates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipCalculateRates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.udropshipVendor
	 * @type {Number}
	 */
	"udropshipVendor": {
		"create": function(options){
			Console.log("CatalogProductMap - udropshipVendor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.udropshipVendor in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - udropshipVendor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendor");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.udropshipVendorValue
	 * @type {String}
	 */
	"udropshipVendorValue": {
		"create": function(options){
			Console.log("CatalogProductMap - udropshipVendorValue - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.udropshipVendorValue in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - udropshipVendorValue - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendorValue");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.udtiershipRates
	 * @type {String}
	 */
	"udtiershipRates": {
		"create": function(options){
			Console.log("CatalogProductMap - udtiershipRates - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.udtiershipRates in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - udtiershipRates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udtiershipRates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.udtiershipUseCustom
	 * @type {Number}
	 */
	"udtiershipUseCustom": {
		"create": function(options){
			Console.log("CatalogProductMap - udtiershipUseCustom - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.udtiershipUseCustom in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - udtiershipUseCustom - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udtiershipUseCustom");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.unlockTime
	 * @type {Number}
	 */
	"unlockTime": {
		"create": function(options){
			Console.log("CatalogProductMap - unlockTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.unlockTime in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - unlockTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("unlockTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.updatedAt
	 * @type {Date}
	 */
	"updatedAt": {
		"create": function(options){
			Console.log("CatalogProductMap - updatedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.updatedAt in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - updatedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.urlKey
	 * @type {String}
	 */
	"urlKey": {
		"create": function(options){
			Console.log("CatalogProductMap - urlKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.urlKey in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - urlKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.urlPath
	 * @type {String}
	 */
	"urlPath": {
		"create": function(options){
			Console.log("CatalogProductMap - urlPath - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.urlPath in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - urlPath - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlPath");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.validationReport
	 * @type {String}
	 */
	"validationReport": {
		"create": function(options){
			Console.log("CatalogProductMap - validationReport - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.validationReport in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - validationReport - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("validationReport");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.venueList
	 * @type {String}
	 */
	"venueList": {
		"create": function(options){
			Console.log("CatalogProductMap - venueList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.venueList in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - venueList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("venueList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.visibility
	 * @type {Number}
	 */
	"visibility": {
		"create": function(options){
			Console.log("CatalogProductMap - visibility - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.visibility in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - visibility - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("visibility");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.visibleFrom
	 * @type {Date}
	 */
	"visibleFrom": {
		"create": function(options){
			Console.log("CatalogProductMap - visibleFrom - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.visibleFrom in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - visibleFrom - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("visibleFrom");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.visibleTo
	 * @type {Date}
	 */
	"visibleTo": {
		"create": function(options){
			Console.log("CatalogProductMap - visibleTo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.visibleTo in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - visibleTo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("visibleTo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.weight
	 * @type {String}
	 */
	"weight": {
		"create": function(options){
			Console.log("CatalogProductMap - weight - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.weight in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - weight - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("weight");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.weightType
	 * @type {Number}
	 */
	"weightType": {
		"create": function(options){
			Console.log("CatalogProductMap - weightType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.weightType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - weightType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("weightType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.termsConditions
	 * @type {String}
	 */
	"termsConditions": {
		"create": function(options){
			Console.log("CatalogProductMap - termsConditions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.termsConditions in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - termsConditions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("termsConditions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionQuestion
	 * @type {String}
	 */
	"competitionQuestion": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionQuestion - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionQuestion in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionQuestion - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionQuestion");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionAnswer
	 * @type {String}
	 */
	"competitionAnswer": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionAnswer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionAnswer in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionAnswer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionAnswer");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionWinnerType
	 * @type {String}
	 */
	"competitionWinnerType": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionWinnerType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionWinnerType in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionWinnerType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionWinnerType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionMultijoinStatus
	 * @type {Boolean}
	 */
	"competitionMultijoinStatus": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionMultijoinStatus - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionMultijoinStatus in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionMultijoinStatus - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionMultijoinStatus");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionMultijoinReset
	 * @type {Date}
	 */
	"competitionMultijoinReset": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionMultijoinReset - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionMultijoinReset in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionMultijoinReset - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionMultijoinReset");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionMultijoinCooldown
	 * @type {String}
	 */
	"competitionMultijoinCooldown": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionMultijoinCooldown - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionMultijoinCooldown in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionMultijoinCooldown - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionMultijoinCooldown");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionWinnerEmail
	 * @type {Boolean}
	 */
	"competitionWinnerEmail": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionWinnerEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionWinnerEmail in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionWinnerEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionWinnerEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.competitionQuestionValidate
	 * @type {Boolean}
	 */
	"competitionQuestionValidate": {
		"create": function(options){
			Console.log("CatalogProductMap - competitionQuestionValidate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.competitionQuestionValidate in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - competitionQuestionValidate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionQuestionValidate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogProduct.connectFacebookId
	 * @type {String}
	 */
	"connectFacebookId": {
		"create": function(options){
			Console.log("CatalogProductMap - connectFacebookId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.connectFacebookId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - connectFacebookId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("connectFacebookId");
			}
			return options.target;
		}
	},
	/**
	 * Property udropshipVendors of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.UdropshipVendor
	 * @type {UdropshipVendor}
	 */
	"udropshipVendors": {
		"create": function(options){
			Console.log("CatalogProductMap - udropshipVendors - create");
			Console.debug(JSON.stringify(options));
			return new UdropshipVendorModel();
		},
		"update": function(options){
			Console.log("CatalogProductMap - udropshipVendors - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendors");
			}
			return options.target;
		}
	},
	/**
	 * Property productEventCompetition of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.AwEventbookingEvent
	 * @type {AwEventbookingEvent}
	 */
	"productEventCompetition": {
		"create": function(options){
			Console.log("CatalogProductMap - productEventCompetition - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CatalogProductMap - productEventCompetition - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productEventCompetition");
			}
			return options.target;
		}
	},
	/**
	 * Property inventory of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.CatalogInventoryStockItem
	 * @type {CatalogInventoryStockItem}
	 */
	"inventory": {
		"create": function(options){
			Console.log("CatalogProductMap - inventory - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CatalogProductMap - inventory - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("inventory");
			}
			return options.target;
		}
	},
	/**
	 * Property catalogCategories of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.CatalogCategory
	 * @type {CatalogCategory}
	 */
	"catalogCategories": {
		"create": function(options){
			Console.log("CatalogProductMap - catalogCategories - create");
			Console.debug(JSON.stringify(options));
			return new CatalogCategoryModel();
		},
		"update": function(options){
			Console.log("CatalogProductMap - catalogCategories - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("catalogCategories");
			}
			return options.target;
		}
	},
	/**
	 * Property checkoutAgreement of harpoonApi.CatalogProduct describes a relationship belongsTo harpoonApi.CheckoutAgreement
	 * @type {object}
	 */
	"checkoutAgreement": {
		"create": function(options){
			Console.log("CatalogProductMap - checkoutAgreement - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.checkoutAgreement in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - checkoutAgreement - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("checkoutAgreement");
			}
			return options.target;
		}
	},
	/**
	 * Property awCollpurDeal of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.AwCollpurDeal
	 * @type {AwCollpurDeal}
	 */
	"awCollpurDeal": {
		"create": function(options){
			Console.log("CatalogProductMap - awCollpurDeal - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CatalogProductMap - awCollpurDeal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("awCollpurDeal");
			}
			return options.target;
		}
	},
	/**
	 * Property creator of harpoonApi.CatalogProduct describes a relationship belongsTo harpoonApi.UdropshipVendor
	 * @type {object}
	 */
	"creator": {
		"create": function(options){
			Console.log("CatalogProductMap - creator - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogProductModel.creator in during mapping");
		},
		"update": function(options){
			Console.log("CatalogProductMap - creator - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("creator");
			}
			return options.target;
		}
	},
	/**
	 * Property udropshipVendorProducts of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.UdropshipVendorProduct
	 * @type {UdropshipVendorProduct}
	 */
	"udropshipVendorProducts": {
		"create": function(options){
			Console.log("CatalogProductMap - udropshipVendorProducts - create");
			Console.debug(JSON.stringify(options));
			return new UdropshipVendorProductModel();
		},
		"update": function(options){
			Console.log("CatalogProductMap - udropshipVendorProducts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendorProducts");
			}
			return options.target;
		}
	},
};
