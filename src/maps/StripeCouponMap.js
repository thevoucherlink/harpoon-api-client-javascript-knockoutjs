/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * StripeCoupon Map
 * @type {object}
 */
var StripeCouponMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.StripeCoupon.object
	 * @type {string}
	 */
	"object": {
		"create": function(options){
			Console.log("StripeCouponMap - object - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.object in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - object - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("object");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.amountOff
	 * @type {number}
	 */
	"amountOff": {
		"create": function(options){
			Console.log("StripeCouponMap - amountOff - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.amountOff in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - amountOff - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("amountOff");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.created
	 * @type {number}
	 */
	"created": {
		"create": function(options){
			Console.log("StripeCouponMap - created - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.created in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - created - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("created");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.currency
	 * @type {string}
	 */
	"currency": {
		"create": function(options){
			Console.log("StripeCouponMap - currency - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.currency in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - currency - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("currency");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.durationInMonths
	 * @type {number}
	 */
	"durationInMonths": {
		"create": function(options){
			Console.log("StripeCouponMap - durationInMonths - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.durationInMonths in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - durationInMonths - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("durationInMonths");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.livemode
	 * @type {boolean}
	 */
	"livemode": {
		"create": function(options){
			Console.log("StripeCouponMap - livemode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.livemode in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - livemode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("livemode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.maxRedemptions
	 * @type {number}
	 */
	"maxRedemptions": {
		"create": function(options){
			Console.log("StripeCouponMap - maxRedemptions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.maxRedemptions in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - maxRedemptions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("maxRedemptions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.metadata
	 * @type {string}
	 */
	"metadata": {
		"create": function(options){
			Console.log("StripeCouponMap - metadata - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.metadata in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - metadata - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metadata");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.percentOff
	 * @type {number}
	 */
	"percentOff": {
		"create": function(options){
			Console.log("StripeCouponMap - percentOff - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.percentOff in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - percentOff - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("percentOff");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.redeemBy
	 * @type {number}
	 */
	"redeemBy": {
		"create": function(options){
			Console.log("StripeCouponMap - redeemBy - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.redeemBy in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - redeemBy - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemBy");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.timesRedeemed
	 * @type {number}
	 */
	"timesRedeemed": {
		"create": function(options){
			Console.log("StripeCouponMap - timesRedeemed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.timesRedeemed in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - timesRedeemed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("timesRedeemed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.valid
	 * @type {boolean}
	 */
	"valid": {
		"create": function(options){
			Console.log("StripeCouponMap - valid - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.valid in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - valid - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("valid");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeCoupon.duration
	 * @type {string}
	 */
	"duration": {
		"create": function(options){
			Console.log("StripeCouponMap - duration - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeCouponModel.duration in during mapping");
		},
		"update": function(options){
			Console.log("StripeCouponMap - duration - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("duration");
			}
			return options.target;
		}
	},
};
