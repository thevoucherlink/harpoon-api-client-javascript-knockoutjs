/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwCollpurDealPurchases Map
 * @type {object}
 */
var AwCollpurDealPurchasesMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwCollpurDealPurchases.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.dealId
	 * @type {Number}
	 */
	"dealId": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - dealId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.dealId in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - dealId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.orderId
	 * @type {Number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.orderItemId
	 * @type {Number}
	 */
	"orderItemId": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - orderItemId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.orderItemId in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - orderItemId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderItemId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.qtyPurchased
	 * @type {Number}
	 */
	"qtyPurchased": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - qtyPurchased - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.qtyPurchased in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - qtyPurchased - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyPurchased");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.qtyWithCoupons
	 * @type {Number}
	 */
	"qtyWithCoupons": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - qtyWithCoupons - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.qtyWithCoupons in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - qtyWithCoupons - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyWithCoupons");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.customerName
	 * @type {String}
	 */
	"customerName": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - customerName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.customerName in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - customerName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customerName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.customerId
	 * @type {Number}
	 */
	"customerId": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - customerId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.customerId in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - customerId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customerId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.purchaseDateTime
	 * @type {Date}
	 */
	"purchaseDateTime": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - purchaseDateTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.purchaseDateTime in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - purchaseDateTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("purchaseDateTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.shippingAmount
	 * @type {String}
	 */
	"shippingAmount": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - shippingAmount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.shippingAmount in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - shippingAmount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("shippingAmount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.qtyOrdered
	 * @type {String}
	 */
	"qtyOrdered": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - qtyOrdered - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.qtyOrdered in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - qtyOrdered - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyOrdered");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.refundState
	 * @type {Number}
	 */
	"refundState": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - refundState - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.refundState in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - refundState - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("refundState");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDealPurchases.isSuccessedFlag
	 * @type {Number}
	 */
	"isSuccessedFlag": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - isSuccessedFlag - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealPurchasesModel.isSuccessedFlag in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - isSuccessedFlag - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isSuccessedFlag");
			}
			return options.target;
		}
	},
	/**
	 * Property awCollpurCoupon of harpoonApi.AwCollpurDealPurchases describes a relationship hasOne harpoonApi.AwCollpurCoupon
	 * @type {AwCollpurCoupon}
	 */
	"awCollpurCoupon": {
		"create": function(options){
			Console.log("AwCollpurDealPurchasesMap - awCollpurCoupon - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AwCollpurDealPurchasesMap - awCollpurCoupon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("awCollpurCoupon");
			}
			return options.target;
		}
	},
};
