/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppSettingsPasswordForgotPasswordReset Map
 * @type {object}
 */
var EmailAppSettingsPasswordForgotPasswordResetMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandCover
	 * @type {string}
	 */
	"brandCover": {
		"create": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandCover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppSettingsPasswordForgotPasswordResetModel.brandCover in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandCover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandCover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppSettingsPasswordForgotPasswordResetModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandLogo
	 * @type {string}
	 */
	"brandLogo": {
		"create": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppSettingsPasswordForgotPasswordResetModel.brandLogo in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppSettingsPasswordForgotPasswordResetModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppSettingsPasswordForgotPasswordResetModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.userName
	 * @type {string}
	 */
	"userName": {
		"create": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - userName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppSettingsPasswordForgotPasswordResetModel.userName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - userName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppSettingsPasswordForgotPasswordReset.verificationLink
	 * @type {string}
	 */
	"verificationLink": {
		"create": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - verificationLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppSettingsPasswordForgotPasswordResetModel.verificationLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppSettingsPasswordForgotPasswordResetMap - verificationLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("verificationLink");
			}
			return options.target;
		}
	},
};
