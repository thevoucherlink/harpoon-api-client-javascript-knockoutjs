/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Event Map
 * @type {object}
 */
var EventMap = {
	// Ignore
	"ignore": ["attendee","ticket"],
	// Properties
	/**
	 * harpoonApi.Event.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("EventMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventModel.id in during mapping");
		},
		"update": function(options){
			Console.log("EventMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.basePrice
	 * @type {number}
	 */
	"basePrice": {
		"create": function(options){
			Console.log("EventMap - basePrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventModel.basePrice in during mapping");
		},
		"update": function(options){
			Console.log("EventMap - basePrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("basePrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.attendee
	 * @type {EventAttendee}
	 */
	"attendee": {
		"create": function(options){
			Console.log("EventMap - attendee - create");
			Console.debug(JSON.stringify(options));
			return new EventAttendeeModel();
		},
		"update": function(options){
			Console.log("EventMap - attendee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.attendees
	 * @type {EventAttendee}
	 */
	"attendees": {
		"create": function(options){
			Console.log("EventMap - attendees - create");
			Console.debug(JSON.stringify(options));
			return new EventAttendeeModel();
		},
		"update": function(options){
			Console.log("EventMap - attendees - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendees");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.attendeeCount
	 * @type {number}
	 */
	"attendeeCount": {
		"create": function(options){
			Console.log("EventMap - attendeeCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventModel.attendeeCount in during mapping");
		},
		"update": function(options){
			Console.log("EventMap - attendeeCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendeeCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.isGoing
	 * @type {boolean}
	 */
	"isGoing": {
		"create": function(options){
			Console.log("EventMap - isGoing - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventModel.isGoing in during mapping");
		},
		"update": function(options){
			Console.log("EventMap - isGoing - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isGoing");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.ticket
	 * @type {EventTicket}
	 */
	"ticket": {
		"create": function(options){
			Console.log("EventMap - ticket - create");
			Console.debug(JSON.stringify(options));
			return new EventTicketModel();
		},
		"update": function(options){
			Console.log("EventMap - ticket - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ticket");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.tickets
	 * @type {EventTicket}
	 */
	"tickets": {
		"create": function(options){
			Console.log("EventMap - tickets - create");
			Console.debug(JSON.stringify(options));
			return new EventTicketModel();
		},
		"update": function(options){
			Console.log("EventMap - tickets - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tickets");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.termsConditions
	 * @type {string}
	 */
	"termsConditions": {
		"create": function(options){
			Console.log("EventMap - termsConditions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventModel.termsConditions in during mapping");
		},
		"update": function(options){
			Console.log("EventMap - termsConditions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("termsConditions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.facebook
	 * @type {FacebookEvent}
	 */
	"facebook": {
		"create": function(options){
			Console.log("EventMap - facebook - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("EventMap - facebook - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebook");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Event.connectFacebookId
	 * @type {string}
	 */
	"connectFacebookId": {
		"create": function(options){
			Console.log("EventMap - connectFacebookId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventModel.connectFacebookId in during mapping");
		},
		"update": function(options){
			Console.log("EventMap - connectFacebookId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("connectFacebookId");
			}
			return options.target;
		}
	},
};
