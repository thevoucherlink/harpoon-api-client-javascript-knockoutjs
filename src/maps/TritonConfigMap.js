/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * TritonConfig Map
 * @type {object}
 */
var TritonConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.TritonConfig.stationId
	 * @type {string}
	 */
	"stationId": {
		"create": function(options){
			Console.log("TritonConfigMap - stationId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create TritonConfigModel.stationId in during mapping");
		},
		"update": function(options){
			Console.log("TritonConfigMap - stationId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stationId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.TritonConfig.stationName
	 * @type {string}
	 */
	"stationName": {
		"create": function(options){
			Console.log("TritonConfigMap - stationName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create TritonConfigModel.stationName in during mapping");
		},
		"update": function(options){
			Console.log("TritonConfigMap - stationName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stationName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.TritonConfig.mp3Mount
	 * @type {string}
	 */
	"mp3Mount": {
		"create": function(options){
			Console.log("TritonConfigMap - mp3Mount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create TritonConfigModel.mp3Mount in during mapping");
		},
		"update": function(options){
			Console.log("TritonConfigMap - mp3Mount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("mp3Mount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.TritonConfig.aacMount
	 * @type {string}
	 */
	"aacMount": {
		"create": function(options){
			Console.log("TritonConfigMap - aacMount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create TritonConfigModel.aacMount in during mapping");
		},
		"update": function(options){
			Console.log("TritonConfigMap - aacMount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("aacMount");
			}
			return options.target;
		}
	},
};
