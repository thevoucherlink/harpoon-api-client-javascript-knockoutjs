/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailDashCampaignFlowStopped Map
 * @type {object}
 */
var EmailDashCampaignFlowStoppedMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailDashCampaignFlowStopped.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowStoppedModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowStopped.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowStoppedModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowStopped.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowStoppedModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowStopped.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowStoppedModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowStopped.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowStoppedModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowStopped.reDirectLink
	 * @type {string}
	 */
	"reDirectLink": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - reDirectLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowStoppedModel.reDirectLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - reDirectLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("reDirectLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowStopped.userName
	 * @type {string}
	 */
	"userName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - userName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowStoppedModel.userName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowStoppedMap - userName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userName");
			}
			return options.target;
		}
	},
};
