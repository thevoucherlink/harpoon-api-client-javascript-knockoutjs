/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HOAuthAuthorizationCode Map
 * @type {object}
 */
var HOAuthAuthorizationCodeMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HOAuthAuthorizationCode.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.appId
	 * @type {string}
	 */
	"appId": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - appId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.appId in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - appId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.userId
	 * @type {string}
	 */
	"userId": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - userId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.userId in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - userId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.issuedAt
	 * @type {date}
	 */
	"issuedAt": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - issuedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.issuedAt in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - issuedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("issuedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.expiresIn
	 * @type {number}
	 */
	"expiresIn": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - expiresIn - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.expiresIn in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - expiresIn - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiresIn");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.expiredAt
	 * @type {date}
	 */
	"expiredAt": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - expiredAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.expiredAt in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - expiredAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiredAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.scopes
	 * @type {string}
	 */
	"scopes": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - scopes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - scopes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scopes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.parameters
	 * @type {object}
	 */
	"parameters": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - parameters - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - parameters - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("parameters");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.used
	 * @type {boolean}
	 */
	"used": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - used - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.used in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - used - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("used");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.redirectURI
	 * @type {string}
	 */
	"redirectURI": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - redirectURI - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.redirectURI in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - redirectURI - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redirectURI");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAuthorizationCode.hash
	 * @type {string}
	 */
	"hash": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - hash - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.hash in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - hash - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hash");
			}
			return options.target;
		}
	},
	/**
	 * Property application of harpoonApi.HOAuthAuthorizationCode describes a relationship belongsTo harpoonApi.OAuthClientApplication
	 * @type {object}
	 */
	"application": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - application - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.application in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - application - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("application");
			}
			return options.target;
		}
	},
	/**
	 * Property user of harpoonApi.HOAuthAuthorizationCode describes a relationship belongsTo harpoonApi.User
	 * @type {object}
	 */
	"user": {
		"create": function(options){
			Console.log("HOAuthAuthorizationCodeMap - user - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAuthorizationCodeModel.user in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAuthorizationCodeMap - user - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("user");
			}
			return options.target;
		}
	},
};
