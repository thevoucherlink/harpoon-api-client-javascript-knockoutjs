/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HOAuthAccessToken Map
 * @type {object}
 */
var HOAuthAccessTokenMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HOAuthAccessToken.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.id in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.appId
	 * @type {string}
	 */
	"appId": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - appId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.appId in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - appId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.userId
	 * @type {string}
	 */
	"userId": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - userId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.userId in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - userId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.issuedAt
	 * @type {date}
	 */
	"issuedAt": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - issuedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.issuedAt in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - issuedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("issuedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.expiresIn
	 * @type {number}
	 */
	"expiresIn": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - expiresIn - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.expiresIn in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - expiresIn - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiresIn");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.expiredAt
	 * @type {date}
	 */
	"expiredAt": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - expiredAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.expiredAt in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - expiredAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiredAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.scopes
	 * @type {string}
	 */
	"scopes": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - scopes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - scopes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scopes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.parameters
	 * @type {object}
	 */
	"parameters": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - parameters - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - parameters - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("parameters");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.authorizationCode
	 * @type {string}
	 */
	"authorizationCode": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - authorizationCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.authorizationCode in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - authorizationCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("authorizationCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.refreshToken
	 * @type {string}
	 */
	"refreshToken": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - refreshToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.refreshToken in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - refreshToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("refreshToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.tokenType
	 * @type {string}
	 */
	"tokenType": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - tokenType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.tokenType in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - tokenType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tokenType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthAccessToken.hash
	 * @type {string}
	 */
	"hash": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - hash - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.hash in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - hash - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hash");
			}
			return options.target;
		}
	},
	/**
	 * Property application of harpoonApi.HOAuthAccessToken describes a relationship belongsTo harpoonApi.OAuthClientApplication
	 * @type {object}
	 */
	"application": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - application - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.application in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - application - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("application");
			}
			return options.target;
		}
	},
	/**
	 * Property user of harpoonApi.HOAuthAccessToken describes a relationship belongsTo harpoonApi.User
	 * @type {object}
	 */
	"user": {
		"create": function(options){
			Console.log("HOAuthAccessTokenMap - user - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthAccessTokenModel.user in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthAccessTokenMap - user - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("user");
			}
			return options.target;
		}
	},
};
