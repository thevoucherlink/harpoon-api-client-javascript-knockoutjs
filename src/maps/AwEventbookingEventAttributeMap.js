/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwEventbookingEventAttribute Map
 * @type {object}
 */
var AwEventbookingEventAttributeMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwEventbookingEventAttribute.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwEventbookingEventAttributeMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventAttributeModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventAttributeMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventAttribute.eventId
	 * @type {Number}
	 */
	"eventId": {
		"create": function(options){
			Console.log("AwEventbookingEventAttributeMap - eventId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventAttributeModel.eventId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventAttributeMap - eventId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("eventId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventAttribute.storeId
	 * @type {Number}
	 */
	"storeId": {
		"create": function(options){
			Console.log("AwEventbookingEventAttributeMap - storeId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventAttributeModel.storeId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventAttributeMap - storeId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("storeId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventAttribute.attributeCode
	 * @type {String}
	 */
	"attributeCode": {
		"create": function(options){
			Console.log("AwEventbookingEventAttributeMap - attributeCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventAttributeModel.attributeCode in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventAttributeMap - attributeCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attributeCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventAttribute.value
	 * @type {String}
	 */
	"value": {
		"create": function(options){
			Console.log("AwEventbookingEventAttributeMap - value - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventAttributeModel.value in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventAttributeMap - value - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("value");
			}
			return options.target;
		}
	},
};
