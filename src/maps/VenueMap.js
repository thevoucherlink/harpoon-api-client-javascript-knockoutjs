/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Venue Map
 * @type {object}
 */
var VenueMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Venue.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("VenueMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create VenueModel.id in during mapping");
		},
		"update": function(options){
			Console.log("VenueMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("VenueMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create VenueModel.name in during mapping");
		},
		"update": function(options){
			Console.log("VenueMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.address
	 * @type {string}
	 */
	"address": {
		"create": function(options){
			Console.log("VenueMap - address - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create VenueModel.address in during mapping");
		},
		"update": function(options){
			Console.log("VenueMap - address - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("address");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.city
	 * @type {string}
	 */
	"city": {
		"create": function(options){
			Console.log("VenueMap - city - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create VenueModel.city in during mapping");
		},
		"update": function(options){
			Console.log("VenueMap - city - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("city");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.country
	 * @type {string}
	 */
	"country": {
		"create": function(options){
			Console.log("VenueMap - country - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create VenueModel.country in during mapping");
		},
		"update": function(options){
			Console.log("VenueMap - country - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("country");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.coordinates
	 * @type {GeoLocation}
	 */
	"coordinates": {
		"create": function(options){
			Console.log("VenueMap - coordinates - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("VenueMap - coordinates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("coordinates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.phone
	 * @type {string}
	 */
	"phone": {
		"create": function(options){
			Console.log("VenueMap - phone - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create VenueModel.phone in during mapping");
		},
		"update": function(options){
			Console.log("VenueMap - phone - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("phone");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.email
	 * @type {string}
	 */
	"email": {
		"create": function(options){
			Console.log("VenueMap - email - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create VenueModel.email in during mapping");
		},
		"update": function(options){
			Console.log("VenueMap - email - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("email");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Venue.brand
	 * @type {Brand}
	 */
	"brand": {
		"create": function(options){
			Console.log("VenueMap - brand - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("VenueMap - brand - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brand");
			}
			return options.target;
		}
	},
};
