/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Address Map
 * @type {object}
 */
var AddressMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Address.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("AddressMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Main street line for the address (first line)
	 * @type {string}
	 */
	"streetAddress": {
		"create": function(options){
			Console.log("AddressMap - streetAddress - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.streetAddress in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - streetAddress - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("streetAddress");
			}
			return options.target;
		}
	},
	/**
	 * Line to complete the address (second line)
	 * @type {string}
	 */
	"streetAddressComp": {
		"create": function(options){
			Console.log("AddressMap - streetAddressComp - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.streetAddressComp in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - streetAddressComp - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("streetAddressComp");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Address.city
	 * @type {string}
	 */
	"city": {
		"create": function(options){
			Console.log("AddressMap - city - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.city in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - city - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("city");
			}
			return options.target;
		}
	},
	/**
	 * Region, County, Province or State
	 * @type {string}
	 */
	"region": {
		"create": function(options){
			Console.log("AddressMap - region - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.region in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - region - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("region");
			}
			return options.target;
		}
	},
	/**
	 * Country ISO code (must be max of 2 capital letter)
	 * @type {string}
	 */
	"countryCode": {
		"create": function(options){
			Console.log("AddressMap - countryCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.countryCode in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - countryCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("countryCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Address.postcode
	 * @type {string}
	 */
	"postcode": {
		"create": function(options){
			Console.log("AddressMap - postcode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.postcode in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - postcode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("postcode");
			}
			return options.target;
		}
	},
	/**
	 * Postal Attn {person first name}
	 * @type {string}
	 */
	"attnFirstName": {
		"create": function(options){
			Console.log("AddressMap - attnFirstName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.attnFirstName in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - attnFirstName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attnFirstName");
			}
			return options.target;
		}
	},
	/**
	 * Postal Attn {person last name}
	 * @type {string}
	 */
	"attnLastName": {
		"create": function(options){
			Console.log("AddressMap - attnLastName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AddressModel.attnLastName in during mapping");
		},
		"update": function(options){
			Console.log("AddressMap - attnLastName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attnLastName");
			}
			return options.target;
		}
	},
};
