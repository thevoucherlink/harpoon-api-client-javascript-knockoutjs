/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailType Map
 * @type {object}
 */
var EmailTypeMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailType.template1
	 * @type {string}
	 */
	"template1": {
		"create": function(options){
			Console.log("EmailTypeMap - template1 - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailTypeModel.template1 in during mapping");
		},
		"update": function(options){
			Console.log("EmailTypeMap - template1 - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("template1");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailType.template2
	 * @type {string}
	 */
	"template2": {
		"create": function(options){
			Console.log("EmailTypeMap - template2 - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailTypeModel.template2 in during mapping");
		},
		"update": function(options){
			Console.log("EmailTypeMap - template2 - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("template2");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailType.template3
	 * @type {string}
	 */
	"template3": {
		"create": function(options){
			Console.log("EmailTypeMap - template3 - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailTypeModel.template3 in during mapping");
		},
		"update": function(options){
			Console.log("EmailTypeMap - template3 - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("template3");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailType.template4
	 * @type {string}
	 */
	"template4": {
		"create": function(options){
			Console.log("EmailTypeMap - template4 - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailTypeModel.template4 in during mapping");
		},
		"update": function(options){
			Console.log("EmailTypeMap - template4 - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("template4");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailType.template5
	 * @type {string}
	 */
	"template5": {
		"create": function(options){
			Console.log("EmailTypeMap - template5 - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailTypeModel.template5 in during mapping");
		},
		"update": function(options){
			Console.log("EmailTypeMap - template5 - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("template5");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailType.template6
	 * @type {string}
	 */
	"template6": {
		"create": function(options){
			Console.log("EmailTypeMap - template6 - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailTypeModel.template6 in during mapping");
		},
		"update": function(options){
			Console.log("EmailTypeMap - template6 - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("template6");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailType.template7
	 * @type {string}
	 */
	"template7": {
		"create": function(options){
			Console.log("EmailTypeMap - template7 - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailTypeModel.template7 in during mapping");
		},
		"update": function(options){
			Console.log("EmailTypeMap - template7 - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("template7");
			}
			return options.target;
		}
	},
};
