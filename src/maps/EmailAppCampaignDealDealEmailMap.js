/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignDealDealEmail Map
 * @type {object}
 */
var EmailAppCampaignDealDealEmailMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.brandLogo
	 * @type {string}
	 */
	"brandLogo": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.brandLogo in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignBarcode
	 * @type {string}
	 */
	"campaignBarcode": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignBarcode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignBarcode in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignBarcode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignBarcode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignCode
	 * @type {number}
	 */
	"campaignCode": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignCode in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignDescription
	 * @type {string}
	 */
	"campaignDescription": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignDescription in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignDescription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignEndTime
	 * @type {string}
	 */
	"campaignEndTime": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignEndTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignEndTime in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignEndTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignEndTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignLocation
	 * @type {string}
	 */
	"campaignLocation": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignLocation - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignLocation in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignLocation - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignLocation");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignTermsAndConditions
	 * @type {string}
	 */
	"campaignTermsAndConditions": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignTermsAndConditions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignTermsAndConditions in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignTermsAndConditions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignTermsAndConditions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.campaignUsage
	 * @type {string}
	 */
	"campaignUsage": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignUsage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.campaignUsage in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - campaignUsage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignUsage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.orderId
	 * @type {number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.priceText
	 * @type {string}
	 */
	"priceText": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - priceText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.priceText in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - priceText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("priceText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignDealDealEmail.organiserName
	 * @type {string}
	 */
	"organiserName": {
		"create": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - organiserName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignDealDealEmailModel.organiserName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignDealDealEmailMap - organiserName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("organiserName");
			}
			return options.target;
		}
	},
};
