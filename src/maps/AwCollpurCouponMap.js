/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwCollpurCoupon Map
 * @type {object}
 */
var AwCollpurCouponMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwCollpurCoupon.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.dealId
	 * @type {Number}
	 */
	"dealId": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - dealId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.dealId in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - dealId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.purchaseId
	 * @type {Number}
	 */
	"purchaseId": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - purchaseId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.purchaseId in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - purchaseId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("purchaseId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.couponCode
	 * @type {String}
	 */
	"couponCode": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - couponCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.couponCode in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - couponCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.status
	 * @type {String}
	 */
	"status": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.status in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.couponDeliveryDatetime
	 * @type {Date}
	 */
	"couponDeliveryDatetime": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - couponDeliveryDatetime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.couponDeliveryDatetime in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - couponDeliveryDatetime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponDeliveryDatetime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.couponDateUpdated
	 * @type {Date}
	 */
	"couponDateUpdated": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - couponDateUpdated - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.couponDateUpdated in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - couponDateUpdated - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponDateUpdated");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.redeemedAt
	 * @type {Date}
	 */
	"redeemedAt": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - redeemedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.redeemedAt in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - redeemedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurCoupon.redeemedByTerminal
	 * @type {String}
	 */
	"redeemedByTerminal": {
		"create": function(options){
			Console.log("AwCollpurCouponMap - redeemedByTerminal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurCouponModel.redeemedByTerminal in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurCouponMap - redeemedByTerminal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemedByTerminal");
			}
			return options.target;
		}
	},
};
