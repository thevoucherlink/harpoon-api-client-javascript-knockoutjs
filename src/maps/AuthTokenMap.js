/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AuthToken Map
 * @type {object}
 */
var AuthTokenMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AuthToken.accessToken
	 * @type {string}
	 */
	"accessToken": {
		"create": function(options){
			Console.log("AuthTokenMap - accessToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.accessToken in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - accessToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("accessToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AuthToken.expiresIn
	 * @type {number}
	 */
	"expiresIn": {
		"create": function(options){
			Console.log("AuthTokenMap - expiresIn - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.expiresIn in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - expiresIn - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiresIn");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AuthToken.scope
	 * @type {string}
	 */
	"scope": {
		"create": function(options){
			Console.log("AuthTokenMap - scope - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.scope in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - scope - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scope");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AuthToken.refreshToken
	 * @type {string}
	 */
	"refreshToken": {
		"create": function(options){
			Console.log("AuthTokenMap - refreshToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.refreshToken in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - refreshToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("refreshToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AuthToken.tokenType
	 * @type {string}
	 */
	"tokenType": {
		"create": function(options){
			Console.log("AuthTokenMap - tokenType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.tokenType in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - tokenType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tokenType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AuthToken.kid
	 * @type {string}
	 */
	"kid": {
		"create": function(options){
			Console.log("AuthTokenMap - kid - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.kid in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - kid - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("kid");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AuthToken.macAlgorithm
	 * @type {string}
	 */
	"macAlgorithm": {
		"create": function(options){
			Console.log("AuthTokenMap - macAlgorithm - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.macAlgorithm in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - macAlgorithm - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("macAlgorithm");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AuthToken.macKey
	 * @type {string}
	 */
	"macKey": {
		"create": function(options){
			Console.log("AuthTokenMap - macKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AuthTokenModel.macKey in during mapping");
		},
		"update": function(options){
			Console.log("AuthTokenMap - macKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("macKey");
			}
			return options.target;
		}
	},
};
