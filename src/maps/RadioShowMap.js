/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RadioShow Map
 * @type {object}
 */
var RadioShowMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RadioShow.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("RadioShowMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.id in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioShow.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("RadioShowMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.name in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioShow.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("RadioShowMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.description in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioShow.content
	 * @type {string}
	 */
	"content": {
		"create": function(options){
			Console.log("RadioShowMap - content - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.content in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - content - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("content");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioShow.contentType
	 * @type {string}
	 */
	"contentType": {
		"create": function(options){
			Console.log("RadioShowMap - contentType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.contentType in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - contentType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("contentType");
			}
			return options.target;
		}
	},
	/**
	 * Contacts for this show
	 * @type {Contact}
	 */
	"contact": {
		"create": function(options){
			Console.log("RadioShowMap - contact - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("RadioShowMap - contact - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("contact");
			}
			return options.target;
		}
	},
	/**
	 * When the Show starts of being public
	 * @type {date}
	 */
	"starts": {
		"create": function(options){
			Console.log("RadioShowMap - starts - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.starts in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - starts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("starts");
			}
			return options.target;
		}
	},
	/**
	 * When the Show ceases of being public
	 * @type {date}
	 */
	"ends": {
		"create": function(options){
			Console.log("RadioShowMap - ends - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.ends in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - ends - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ends");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioShow.type
	 * @type {string}
	 */
	"type": {
		"create": function(options){
			Console.log("RadioShowMap - type - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.type in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioShow.radioShowTimeCurrent
	 * @type {RadioShowTime}
	 */
	"radioShowTimeCurrent": {
		"create": function(options){
			Console.log("RadioShowMap - radioShowTimeCurrent - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("RadioShowMap - radioShowTimeCurrent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioShowTimeCurrent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RadioShow.imgShow
	 * @type {string}
	 */
	"imgShow": {
		"create": function(options){
			Console.log("RadioShowMap - imgShow - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.imgShow in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - imgShow - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgShow");
			}
			return options.target;
		}
	},
	/**
	 * Url of the sponsor MP3 to be played
	 * @type {string}
	 */
	"sponsorTrack": {
		"create": function(options){
			Console.log("RadioShowMap - sponsorTrack - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.sponsorTrack in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - sponsorTrack - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sponsorTrack");
			}
			return options.target;
		}
	},
	/**
	 * Property radioPresenters of harpoonApi.RadioShow describes a relationship hasMany harpoonApi.RadioPresenter
	 * @type {RadioPresenter}
	 */
	"radioPresenters": {
		"create": function(options){
			Console.log("RadioShowMap - radioPresenters - create");
			Console.debug(JSON.stringify(options));
			return new RadioPresenterModel();
		},
		"update": function(options){
			Console.log("RadioShowMap - radioPresenters - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioPresenters");
			}
			return options.target;
		}
	},
	/**
	 * Property radioStream of harpoonApi.RadioShow describes a relationship belongsTo harpoonApi.RadioStream
	 * @type {object}
	 */
	"radioStream": {
		"create": function(options){
			Console.log("RadioShowMap - radioStream - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.radioStream in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - radioStream - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioStream");
			}
			return options.target;
		}
	},
	/**
	 * Property radioShowTimes of harpoonApi.RadioShow describes a relationship hasMany harpoonApi.RadioShowTime
	 * @type {RadioShowTime}
	 */
	"radioShowTimes": {
		"create": function(options){
			Console.log("RadioShowMap - radioShowTimes - create");
			Console.debug(JSON.stringify(options));
			return new RadioShowTimeModel();
		},
		"update": function(options){
			Console.log("RadioShowMap - radioShowTimes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioShowTimes");
			}
			return options.target;
		}
	},
	/**
	 * Property playlistItem of harpoonApi.RadioShow describes a relationship belongsTo harpoonApi.PlaylistItem
	 * @type {object}
	 */
	"playlistItem": {
		"create": function(options){
			Console.log("RadioShowMap - playlistItem - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RadioShowModel.playlistItem in during mapping");
		},
		"update": function(options){
			Console.log("RadioShowMap - playlistItem - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playlistItem");
			}
			return options.target;
		}
	},
};
