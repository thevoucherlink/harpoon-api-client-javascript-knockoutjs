/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * NotificationFrom Map
 * @type {object}
 */
var NotificationFromMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.NotificationFrom.customer
	 * @type {Customer}
	 */
	"customer": {
		"create": function(options){
			Console.log("NotificationFromMap - customer - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("NotificationFromMap - customer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customer");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationFrom.brand
	 * @type {Brand}
	 */
	"brand": {
		"create": function(options){
			Console.log("NotificationFromMap - brand - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("NotificationFromMap - brand - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brand");
			}
			return options.target;
		}
	},
};
