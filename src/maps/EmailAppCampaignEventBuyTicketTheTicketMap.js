/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignEventBuyTicketTheTicket Map
 * @type {object}
 */
var EmailAppCampaignEventBuyTicketTheTicketMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignBarcode
	 * @type {string}
	 */
	"campaignBarcode": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignBarcode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.campaignBarcode in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignBarcode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignBarcode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignEndTime
	 * @type {string}
	 */
	"campaignEndTime": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignEndTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.campaignEndTime in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignEndTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignEndTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignOrderDate
	 * @type {string}
	 */
	"campaignOrderDate": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignOrderDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.campaignOrderDate in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignOrderDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignOrderDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.campaignStart
	 * @type {string}
	 */
	"campaignStart": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignStart - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.campaignStart in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - campaignStart - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignStart");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.ticketType
	 * @type {string}
	 */
	"ticketType": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - ticketType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.ticketType in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - ticketType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ticketType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.userName
	 * @type {string}
	 */
	"userName": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - userName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.userName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - userName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignEventBuyTicketTheTicket.orderId
	 * @type {number}
	 */
	"orderId": {
		"create": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - orderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignEventBuyTicketTheTicketModel.orderId in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignEventBuyTicketTheTicketMap - orderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("orderId");
			}
			return options.target;
		}
	},
};
