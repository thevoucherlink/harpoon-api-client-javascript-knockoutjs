/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailDashCampaignInvitePartnerNew Map
 * @type {object}
 */
var EmailDashCampaignInvitePartnerNewMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailDashCampaignInvitePartnerNew.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignInvitePartnerNewModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignInvitePartnerNew.brandLogo
	 * @type {string}
	 */
	"brandLogo": {
		"create": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignInvitePartnerNewModel.brandLogo in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignInvitePartnerNew.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignInvitePartnerNewModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignInvitePartnerNew.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignInvitePartnerNewModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignInvitePartnerNew.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignInvitePartnerNewModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignInvitePartnerNew.campaignType
	 * @type {string}
	 */
	"campaignType": {
		"create": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - campaignType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignInvitePartnerNewModel.campaignType in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - campaignType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignInvitePartnerNew.verificationLink
	 * @type {string}
	 */
	"verificationLink": {
		"create": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - verificationLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignInvitePartnerNewModel.verificationLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignInvitePartnerNewMap - verificationLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("verificationLink");
			}
			return options.target;
		}
	},
};
