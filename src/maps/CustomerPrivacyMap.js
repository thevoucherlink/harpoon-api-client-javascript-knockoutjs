/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CustomerPrivacy Map
 * @type {object}
 */
var CustomerPrivacyMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CustomerPrivacy.activity
	 * @type {string}
	 */
	"activity": {
		"create": function(options){
			Console.log("CustomerPrivacyMap - activity - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerPrivacyModel.activity in during mapping");
		},
		"update": function(options){
			Console.log("CustomerPrivacyMap - activity - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("activity");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerPrivacy.notificationPush
	 * @type {boolean}
	 */
	"notificationPush": {
		"create": function(options){
			Console.log("CustomerPrivacyMap - notificationPush - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerPrivacyModel.notificationPush in during mapping");
		},
		"update": function(options){
			Console.log("CustomerPrivacyMap - notificationPush - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationPush");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerPrivacy.notificationBeacon
	 * @type {boolean}
	 */
	"notificationBeacon": {
		"create": function(options){
			Console.log("CustomerPrivacyMap - notificationBeacon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerPrivacyModel.notificationBeacon in during mapping");
		},
		"update": function(options){
			Console.log("CustomerPrivacyMap - notificationBeacon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationBeacon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CustomerPrivacy.notificationLocation
	 * @type {boolean}
	 */
	"notificationLocation": {
		"create": function(options){
			Console.log("CustomerPrivacyMap - notificationLocation - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CustomerPrivacyModel.notificationLocation in during mapping");
		},
		"update": function(options){
			Console.log("CustomerPrivacyMap - notificationLocation - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notificationLocation");
			}
			return options.target;
		}
	},
};
