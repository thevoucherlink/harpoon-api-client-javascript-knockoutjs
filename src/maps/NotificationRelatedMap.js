/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * NotificationRelated Map
 * @type {object}
 */
var NotificationRelatedMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.NotificationRelated.customerId
	 * @type {number}
	 */
	"customerId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - customerId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.customerId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - customerId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customerId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationRelated.brandId
	 * @type {number}
	 */
	"brandId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - brandId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.brandId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - brandId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationRelated.venueId
	 * @type {number}
	 */
	"venueId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - venueId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.venueId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - venueId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("venueId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationRelated.competitionId
	 * @type {number}
	 */
	"competitionId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - competitionId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.competitionId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - competitionId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationRelated.couponId
	 * @type {number}
	 */
	"couponId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - couponId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.couponId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - couponId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationRelated.eventId
	 * @type {number}
	 */
	"eventId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - eventId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.eventId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - eventId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("eventId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationRelated.dealPaidId
	 * @type {number}
	 */
	"dealPaidId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - dealPaidId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.dealPaidId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - dealPaidId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealPaidId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.NotificationRelated.dealGroupId
	 * @type {number}
	 */
	"dealGroupId": {
		"create": function(options){
			Console.log("NotificationRelatedMap - dealGroupId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create NotificationRelatedModel.dealGroupId in during mapping");
		},
		"update": function(options){
			Console.log("NotificationRelatedMap - dealGroupId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealGroupId");
			}
			return options.target;
		}
	},
};
