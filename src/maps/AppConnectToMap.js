/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AppConnectTo Map
 * @type {object}
 */
var AppConnectToMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AppConnectTo.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("AppConnectToMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConnectToModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AppConnectToMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConnectTo.token
	 * @type {string}
	 */
	"token": {
		"create": function(options){
			Console.log("AppConnectToMap - token - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConnectToModel.token in during mapping");
		},
		"update": function(options){
			Console.log("AppConnectToMap - token - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("token");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConnectTo.heading
	 * @type {string}
	 */
	"heading": {
		"create": function(options){
			Console.log("AppConnectToMap - heading - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConnectToModel.heading in during mapping");
		},
		"update": function(options){
			Console.log("AppConnectToMap - heading - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("heading");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConnectTo.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("AppConnectToMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConnectToModel.description in during mapping");
		},
		"update": function(options){
			Console.log("AppConnectToMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConnectTo.link
	 * @type {string}
	 */
	"link": {
		"create": function(options){
			Console.log("AppConnectToMap - link - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConnectToModel.link in during mapping");
		},
		"update": function(options){
			Console.log("AppConnectToMap - link - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("link");
			}
			return options.target;
		}
	},
};
