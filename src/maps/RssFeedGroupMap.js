/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RssFeedGroup Map
 * @type {object}
 */
var RssFeedGroupMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RssFeedGroup.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("RssFeedGroupMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedGroupModel.id in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedGroupMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeedGroup.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("RssFeedGroupMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedGroupModel.name in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedGroupMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeedGroup.sortOrder
	 * @type {number}
	 */
	"sortOrder": {
		"create": function(options){
			Console.log("RssFeedGroupMap - sortOrder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedGroupModel.sortOrder in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedGroupMap - sortOrder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sortOrder");
			}
			return options.target;
		}
	},
	/**
	 * Property rssFeeds of harpoonApi.RssFeedGroup describes a relationship hasMany harpoonApi.RssFeed
	 * @type {RssFeed}
	 */
	"rssFeeds": {
		"create": function(options){
			Console.log("RssFeedGroupMap - rssFeeds - create");
			Console.debug(JSON.stringify(options));
			return new RssFeedModel();
		},
		"update": function(options){
			Console.log("RssFeedGroupMap - rssFeeds - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("rssFeeds");
			}
			return options.target;
		}
	},
};
