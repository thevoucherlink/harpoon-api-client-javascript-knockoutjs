/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * FormRow Map
 * @type {object}
 */
var FormRowMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.FormRow.metadataKey
	 * @type {string}
	 */
	"metadataKey": {
		"create": function(options){
			Console.log("FormRowMap - metadataKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.metadataKey in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - metadataKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metadataKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.type
	 * @type {string}
	 */
	"type": {
		"create": function(options){
			Console.log("FormRowMap - type - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.type in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.title
	 * @type {string}
	 */
	"title": {
		"create": function(options){
			Console.log("FormRowMap - title - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.title in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - title - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("title");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.defaultValue
	 * @type {string}
	 */
	"defaultValue": {
		"create": function(options){
			Console.log("FormRowMap - defaultValue - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.defaultValue in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - defaultValue - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultValue");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.placeholder
	 * @type {string}
	 */
	"placeholder": {
		"create": function(options){
			Console.log("FormRowMap - placeholder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.placeholder in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - placeholder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("placeholder");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.hidden
	 * @type {boolean}
	 */
	"hidden": {
		"create": function(options){
			Console.log("FormRowMap - hidden - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.hidden in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - hidden - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hidden");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.required
	 * @type {boolean}
	 */
	"required": {
		"create": function(options){
			Console.log("FormRowMap - required - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.required in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - required - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("required");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.validationRegExp
	 * @type {string}
	 */
	"validationRegExp": {
		"create": function(options){
			Console.log("FormRowMap - validationRegExp - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.validationRegExp in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - validationRegExp - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("validationRegExp");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.validationMessage
	 * @type {string}
	 */
	"validationMessage": {
		"create": function(options){
			Console.log("FormRowMap - validationMessage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FormRowModel.validationMessage in during mapping");
		},
		"update": function(options){
			Console.log("FormRowMap - validationMessage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("validationMessage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FormRow.selectorOptions
	 * @type {FormSelectorOption}
	 */
	"selectorOptions": {
		"create": function(options){
			Console.log("FormRowMap - selectorOptions - create");
			Console.debug(JSON.stringify(options));
			return new FormSelectorOptionModel();
		},
		"update": function(options){
			Console.log("FormRowMap - selectorOptions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("selectorOptions");
			}
			return options.target;
		}
	},
};
