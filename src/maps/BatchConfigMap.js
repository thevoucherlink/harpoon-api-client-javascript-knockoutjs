/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * BatchConfig Map
 * @type {object}
 */
var BatchConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.BatchConfig.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("BatchConfigMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BatchConfigModel.id in during mapping");
		},
		"update": function(options){
			Console.log("BatchConfigMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.BatchConfig.apiKey
	 * @type {string}
	 */
	"apiKey": {
		"create": function(options){
			Console.log("BatchConfigMap - apiKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BatchConfigModel.apiKey in during mapping");
		},
		"update": function(options){
			Console.log("BatchConfigMap - apiKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("apiKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.BatchConfig.iOSApiKey
	 * @type {string}
	 */
	"iOSApiKey": {
		"create": function(options){
			Console.log("BatchConfigMap - iOSApiKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BatchConfigModel.iOSApiKey in during mapping");
		},
		"update": function(options){
			Console.log("BatchConfigMap - iOSApiKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSApiKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.BatchConfig.androidApiKey
	 * @type {string}
	 */
	"androidApiKey": {
		"create": function(options){
			Console.log("BatchConfigMap - androidApiKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BatchConfigModel.androidApiKey in during mapping");
		},
		"update": function(options){
			Console.log("BatchConfigMap - androidApiKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidApiKey");
			}
			return options.target;
		}
	},
};
