/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwCollpurDeal Map
 * @type {object}
 */
var AwCollpurDealMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwCollpurDeal.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwCollpurDealMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.productId
	 * @type {Number}
	 */
	"productId": {
		"create": function(options){
			Console.log("AwCollpurDealMap - productId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.productId in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - productId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.productName
	 * @type {String}
	 */
	"productName": {
		"create": function(options){
			Console.log("AwCollpurDealMap - productName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.productName in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - productName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.storeIds
	 * @type {String}
	 */
	"storeIds": {
		"create": function(options){
			Console.log("AwCollpurDealMap - storeIds - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.storeIds in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - storeIds - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("storeIds");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.isActive
	 * @type {Number}
	 */
	"isActive": {
		"create": function(options){
			Console.log("AwCollpurDealMap - isActive - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.isActive in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - isActive - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isActive");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.isSuccess
	 * @type {Number}
	 */
	"isSuccess": {
		"create": function(options){
			Console.log("AwCollpurDealMap - isSuccess - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.isSuccess in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - isSuccess - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isSuccess");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.closeState
	 * @type {Number}
	 */
	"closeState": {
		"create": function(options){
			Console.log("AwCollpurDealMap - closeState - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.closeState in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - closeState - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("closeState");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.qtyToReachDeal
	 * @type {Number}
	 */
	"qtyToReachDeal": {
		"create": function(options){
			Console.log("AwCollpurDealMap - qtyToReachDeal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.qtyToReachDeal in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - qtyToReachDeal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyToReachDeal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.purchasesLeft
	 * @type {Number}
	 */
	"purchasesLeft": {
		"create": function(options){
			Console.log("AwCollpurDealMap - purchasesLeft - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.purchasesLeft in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - purchasesLeft - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("purchasesLeft");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.maximumAllowedPurchases
	 * @type {Number}
	 */
	"maximumAllowedPurchases": {
		"create": function(options){
			Console.log("AwCollpurDealMap - maximumAllowedPurchases - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.maximumAllowedPurchases in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - maximumAllowedPurchases - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("maximumAllowedPurchases");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.availableFrom
	 * @type {Date}
	 */
	"availableFrom": {
		"create": function(options){
			Console.log("AwCollpurDealMap - availableFrom - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.availableFrom in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - availableFrom - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("availableFrom");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.availableTo
	 * @type {Date}
	 */
	"availableTo": {
		"create": function(options){
			Console.log("AwCollpurDealMap - availableTo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.availableTo in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - availableTo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("availableTo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.price
	 * @type {String}
	 */
	"price": {
		"create": function(options){
			Console.log("AwCollpurDealMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.price in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.autoClose
	 * @type {Number}
	 */
	"autoClose": {
		"create": function(options){
			Console.log("AwCollpurDealMap - autoClose - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.autoClose in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - autoClose - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("autoClose");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.name
	 * @type {String}
	 */
	"name": {
		"create": function(options){
			Console.log("AwCollpurDealMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.name in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.description
	 * @type {String}
	 */
	"description": {
		"create": function(options){
			Console.log("AwCollpurDealMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.description in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.fullDescription
	 * @type {String}
	 */
	"fullDescription": {
		"create": function(options){
			Console.log("AwCollpurDealMap - fullDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.fullDescription in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - fullDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("fullDescription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.dealImage
	 * @type {String}
	 */
	"dealImage": {
		"create": function(options){
			Console.log("AwCollpurDealMap - dealImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.dealImage in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - dealImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dealImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.isFeatured
	 * @type {Number}
	 */
	"isFeatured": {
		"create": function(options){
			Console.log("AwCollpurDealMap - isFeatured - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.isFeatured in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - isFeatured - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isFeatured");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.enableCoupons
	 * @type {Number}
	 */
	"enableCoupons": {
		"create": function(options){
			Console.log("AwCollpurDealMap - enableCoupons - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.enableCoupons in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - enableCoupons - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("enableCoupons");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.couponPrefix
	 * @type {String}
	 */
	"couponPrefix": {
		"create": function(options){
			Console.log("AwCollpurDealMap - couponPrefix - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.couponPrefix in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - couponPrefix - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponPrefix");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.couponExpireAfterDays
	 * @type {Number}
	 */
	"couponExpireAfterDays": {
		"create": function(options){
			Console.log("AwCollpurDealMap - couponExpireAfterDays - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.couponExpireAfterDays in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - couponExpireAfterDays - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("couponExpireAfterDays");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.expiredFlag
	 * @type {Number}
	 */
	"expiredFlag": {
		"create": function(options){
			Console.log("AwCollpurDealMap - expiredFlag - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.expiredFlag in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - expiredFlag - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiredFlag");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.sentBeforeFlag
	 * @type {Number}
	 */
	"sentBeforeFlag": {
		"create": function(options){
			Console.log("AwCollpurDealMap - sentBeforeFlag - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.sentBeforeFlag in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - sentBeforeFlag - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sentBeforeFlag");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwCollpurDeal.isSuccessedFlag
	 * @type {Number}
	 */
	"isSuccessedFlag": {
		"create": function(options){
			Console.log("AwCollpurDealMap - isSuccessedFlag - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwCollpurDealModel.isSuccessedFlag in during mapping");
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - isSuccessedFlag - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isSuccessedFlag");
			}
			return options.target;
		}
	},
	/**
	 * Property awCollpurDealPurchases of harpoonApi.AwCollpurDeal describes a relationship hasMany harpoonApi.AwCollpurDealPurchases
	 * @type {AwCollpurDealPurchases}
	 */
	"awCollpurDealPurchases": {
		"create": function(options){
			Console.log("AwCollpurDealMap - awCollpurDealPurchases - create");
			Console.debug(JSON.stringify(options));
			return new AwCollpurDealPurchasesModel();
		},
		"update": function(options){
			Console.log("AwCollpurDealMap - awCollpurDealPurchases - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("awCollpurDealPurchases");
			}
			return options.target;
		}
	},
};
