/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EventTicket Map
 * @type {object}
 */
var EventTicketMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EventTicket.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("EventTicketMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventTicketModel.id in during mapping");
		},
		"update": function(options){
			Console.log("EventTicketMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventTicket.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("EventTicketMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventTicketModel.name in during mapping");
		},
		"update": function(options){
			Console.log("EventTicketMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventTicket.price
	 * @type {number}
	 */
	"price": {
		"create": function(options){
			Console.log("EventTicketMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventTicketModel.price in during mapping");
		},
		"update": function(options){
			Console.log("EventTicketMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventTicket.qtyBought
	 * @type {number}
	 */
	"qtyBought": {
		"create": function(options){
			Console.log("EventTicketMap - qtyBought - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventTicketModel.qtyBought in during mapping");
		},
		"update": function(options){
			Console.log("EventTicketMap - qtyBought - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyBought");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventTicket.qtyTotal
	 * @type {number}
	 */
	"qtyTotal": {
		"create": function(options){
			Console.log("EventTicketMap - qtyTotal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventTicketModel.qtyTotal in during mapping");
		},
		"update": function(options){
			Console.log("EventTicketMap - qtyTotal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyTotal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EventTicket.qtyLeft
	 * @type {number}
	 */
	"qtyLeft": {
		"create": function(options){
			Console.log("EventTicketMap - qtyLeft - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EventTicketModel.qtyLeft in during mapping");
		},
		"update": function(options){
			Console.log("EventTicketMap - qtyLeft - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyLeft");
			}
			return options.target;
		}
	},
};
