/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * UdropshipVendorProductAssoc Map
 * @type {object}
 */
var UdropshipVendorProductAssocMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.UdropshipVendorProductAssoc.isUdmulti
	 * @type {Number}
	 */
	"isUdmulti": {
		"create": function(options){
			Console.log("UdropshipVendorProductAssocMap - isUdmulti - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductAssocModel.isUdmulti in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductAssocMap - isUdmulti - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isUdmulti");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProductAssoc.id
	 * @type {number}
	 */
	"id": {
		"create": function(options){
			Console.log("UdropshipVendorProductAssocMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductAssocModel.id in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductAssocMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProductAssoc.isAttribute
	 * @type {Number}
	 */
	"isAttribute": {
		"create": function(options){
			Console.log("UdropshipVendorProductAssocMap - isAttribute - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductAssocModel.isAttribute in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductAssocMap - isAttribute - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isAttribute");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProductAssoc.productId
	 * @type {Number}
	 */
	"productId": {
		"create": function(options){
			Console.log("UdropshipVendorProductAssocMap - productId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductAssocModel.productId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductAssocMap - productId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendorProductAssoc.vendorId
	 * @type {Number}
	 */
	"vendorId": {
		"create": function(options){
			Console.log("UdropshipVendorProductAssocMap - vendorId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductAssocModel.vendorId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductAssocMap - vendorId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorId");
			}
			return options.target;
		}
	},
	/**
	 * Property udropshipVendor of harpoonApi.UdropshipVendorProductAssoc describes a relationship belongsTo harpoonApi.UdropshipVendor
	 * @type {object}
	 */
	"udropshipVendor": {
		"create": function(options){
			Console.log("UdropshipVendorProductAssocMap - udropshipVendor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductAssocModel.udropshipVendor in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductAssocMap - udropshipVendor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendor");
			}
			return options.target;
		}
	},
	/**
	 * Property catalogProduct of harpoonApi.UdropshipVendorProductAssoc describes a relationship belongsTo harpoonApi.CatalogProduct
	 * @type {object}
	 */
	"catalogProduct": {
		"create": function(options){
			Console.log("UdropshipVendorProductAssocMap - catalogProduct - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorProductAssocModel.catalogProduct in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorProductAssocMap - catalogProduct - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("catalogProduct");
			}
			return options.target;
		}
	},
};
