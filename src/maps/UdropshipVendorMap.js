/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * UdropshipVendor Map
 * @type {object}
 */
var UdropshipVendorMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.UdropshipVendor.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("UdropshipVendorMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.id in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.vendorName
	 * @type {String}
	 */
	"vendorName": {
		"create": function(options){
			Console.log("UdropshipVendorMap - vendorName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.vendorName in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - vendorName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.vendorAttn
	 * @type {String}
	 */
	"vendorAttn": {
		"create": function(options){
			Console.log("UdropshipVendorMap - vendorAttn - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.vendorAttn in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - vendorAttn - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorAttn");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.email
	 * @type {String}
	 */
	"email": {
		"create": function(options){
			Console.log("UdropshipVendorMap - email - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.email in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - email - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("email");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.street
	 * @type {String}
	 */
	"street": {
		"create": function(options){
			Console.log("UdropshipVendorMap - street - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.street in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - street - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("street");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.city
	 * @type {String}
	 */
	"city": {
		"create": function(options){
			Console.log("UdropshipVendorMap - city - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.city in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - city - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("city");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.zip
	 * @type {String}
	 */
	"zip": {
		"create": function(options){
			Console.log("UdropshipVendorMap - zip - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.zip in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - zip - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("zip");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.countryId
	 * @type {String}
	 */
	"countryId": {
		"create": function(options){
			Console.log("UdropshipVendorMap - countryId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.countryId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - countryId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("countryId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.regionId
	 * @type {Number}
	 */
	"regionId": {
		"create": function(options){
			Console.log("UdropshipVendorMap - regionId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.regionId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - regionId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("regionId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.region
	 * @type {String}
	 */
	"region": {
		"create": function(options){
			Console.log("UdropshipVendorMap - region - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.region in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - region - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("region");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.telephone
	 * @type {String}
	 */
	"telephone": {
		"create": function(options){
			Console.log("UdropshipVendorMap - telephone - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.telephone in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - telephone - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("telephone");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.fax
	 * @type {String}
	 */
	"fax": {
		"create": function(options){
			Console.log("UdropshipVendorMap - fax - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.fax in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - fax - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("fax");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.status
	 * @type {Boolean}
	 */
	"status": {
		"create": function(options){
			Console.log("UdropshipVendorMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.status in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.password
	 * @type {String}
	 */
	"password": {
		"create": function(options){
			Console.log("UdropshipVendorMap - password - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.password in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - password - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("password");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.passwordHash
	 * @type {String}
	 */
	"passwordHash": {
		"create": function(options){
			Console.log("UdropshipVendorMap - passwordHash - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.passwordHash in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - passwordHash - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("passwordHash");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.passwordEnc
	 * @type {String}
	 */
	"passwordEnc": {
		"create": function(options){
			Console.log("UdropshipVendorMap - passwordEnc - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.passwordEnc in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - passwordEnc - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("passwordEnc");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.carrierCode
	 * @type {String}
	 */
	"carrierCode": {
		"create": function(options){
			Console.log("UdropshipVendorMap - carrierCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.carrierCode in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - carrierCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("carrierCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.notifyNewOrder
	 * @type {Number}
	 */
	"notifyNewOrder": {
		"create": function(options){
			Console.log("UdropshipVendorMap - notifyNewOrder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.notifyNewOrder in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - notifyNewOrder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notifyNewOrder");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.labelType
	 * @type {String}
	 */
	"labelType": {
		"create": function(options){
			Console.log("UdropshipVendorMap - labelType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.labelType in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - labelType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("labelType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.testMode
	 * @type {Number}
	 */
	"testMode": {
		"create": function(options){
			Console.log("UdropshipVendorMap - testMode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.testMode in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - testMode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("testMode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.handlingFee
	 * @type {String}
	 */
	"handlingFee": {
		"create": function(options){
			Console.log("UdropshipVendorMap - handlingFee - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.handlingFee in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - handlingFee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("handlingFee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.upsShipperNumber
	 * @type {String}
	 */
	"upsShipperNumber": {
		"create": function(options){
			Console.log("UdropshipVendorMap - upsShipperNumber - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.upsShipperNumber in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - upsShipperNumber - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("upsShipperNumber");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.customDataCombined
	 * @type {String}
	 */
	"customDataCombined": {
		"create": function(options){
			Console.log("UdropshipVendorMap - customDataCombined - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.customDataCombined in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - customDataCombined - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customDataCombined");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.customVarsCombined
	 * @type {String}
	 */
	"customVarsCombined": {
		"create": function(options){
			Console.log("UdropshipVendorMap - customVarsCombined - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.customVarsCombined in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - customVarsCombined - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customVarsCombined");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.emailTemplate
	 * @type {Number}
	 */
	"emailTemplate": {
		"create": function(options){
			Console.log("UdropshipVendorMap - emailTemplate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.emailTemplate in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - emailTemplate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("emailTemplate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.urlKey
	 * @type {String}
	 */
	"urlKey": {
		"create": function(options){
			Console.log("UdropshipVendorMap - urlKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.urlKey in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - urlKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.randomHash
	 * @type {String}
	 */
	"randomHash": {
		"create": function(options){
			Console.log("UdropshipVendorMap - randomHash - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.randomHash in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - randomHash - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("randomHash");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.createdAt
	 * @type {Date}
	 */
	"createdAt": {
		"create": function(options){
			Console.log("UdropshipVendorMap - createdAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.createdAt in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - createdAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("createdAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.notifyLowstock
	 * @type {Number}
	 */
	"notifyLowstock": {
		"create": function(options){
			Console.log("UdropshipVendorMap - notifyLowstock - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.notifyLowstock in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - notifyLowstock - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notifyLowstock");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.notifyLowstockQty
	 * @type {String}
	 */
	"notifyLowstockQty": {
		"create": function(options){
			Console.log("UdropshipVendorMap - notifyLowstockQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.notifyLowstockQty in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - notifyLowstockQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("notifyLowstockQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.useHandlingFee
	 * @type {Number}
	 */
	"useHandlingFee": {
		"create": function(options){
			Console.log("UdropshipVendorMap - useHandlingFee - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.useHandlingFee in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - useHandlingFee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useHandlingFee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.useRatesFallback
	 * @type {Number}
	 */
	"useRatesFallback": {
		"create": function(options){
			Console.log("UdropshipVendorMap - useRatesFallback - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.useRatesFallback in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - useRatesFallback - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useRatesFallback");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.allowShippingExtraCharge
	 * @type {Number}
	 */
	"allowShippingExtraCharge": {
		"create": function(options){
			Console.log("UdropshipVendorMap - allowShippingExtraCharge - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.allowShippingExtraCharge in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - allowShippingExtraCharge - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("allowShippingExtraCharge");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingExtraChargeSuffix
	 * @type {String}
	 */
	"defaultShippingExtraChargeSuffix": {
		"create": function(options){
			Console.log("UdropshipVendorMap - defaultShippingExtraChargeSuffix - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.defaultShippingExtraChargeSuffix in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - defaultShippingExtraChargeSuffix - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultShippingExtraChargeSuffix");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingExtraChargeType
	 * @type {String}
	 */
	"defaultShippingExtraChargeType": {
		"create": function(options){
			Console.log("UdropshipVendorMap - defaultShippingExtraChargeType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.defaultShippingExtraChargeType in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - defaultShippingExtraChargeType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultShippingExtraChargeType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingExtraCharge
	 * @type {String}
	 */
	"defaultShippingExtraCharge": {
		"create": function(options){
			Console.log("UdropshipVendorMap - defaultShippingExtraCharge - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.defaultShippingExtraCharge in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - defaultShippingExtraCharge - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultShippingExtraCharge");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.isExtraChargeShippingDefault
	 * @type {Number}
	 */
	"isExtraChargeShippingDefault": {
		"create": function(options){
			Console.log("UdropshipVendorMap - isExtraChargeShippingDefault - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.isExtraChargeShippingDefault in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - isExtraChargeShippingDefault - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isExtraChargeShippingDefault");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingId
	 * @type {Number}
	 */
	"defaultShippingId": {
		"create": function(options){
			Console.log("UdropshipVendorMap - defaultShippingId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.defaultShippingId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - defaultShippingId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultShippingId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingUseShipping
	 * @type {Number}
	 */
	"billingUseShipping": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingUseShipping - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingUseShipping in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingUseShipping - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingUseShipping");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingEmail
	 * @type {String}
	 */
	"billingEmail": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingEmail in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingTelephone
	 * @type {String}
	 */
	"billingTelephone": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingTelephone - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingTelephone in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingTelephone - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingTelephone");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingFax
	 * @type {String}
	 */
	"billingFax": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingFax - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingFax in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingFax - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingFax");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingVendorAttn
	 * @type {String}
	 */
	"billingVendorAttn": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingVendorAttn - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingVendorAttn in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingVendorAttn - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingVendorAttn");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingStreet
	 * @type {String}
	 */
	"billingStreet": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingStreet - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingStreet in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingStreet - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingStreet");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingCity
	 * @type {String}
	 */
	"billingCity": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingCity - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingCity in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingCity - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingCity");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingZip
	 * @type {String}
	 */
	"billingZip": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingZip - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingZip in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingZip - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingZip");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingCountryId
	 * @type {String}
	 */
	"billingCountryId": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingCountryId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingCountryId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingCountryId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingCountryId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingRegionId
	 * @type {Number}
	 */
	"billingRegionId": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingRegionId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingRegionId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingRegionId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingRegionId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.billingRegion
	 * @type {String}
	 */
	"billingRegion": {
		"create": function(options){
			Console.log("UdropshipVendorMap - billingRegion - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.billingRegion in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - billingRegion - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("billingRegion");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.subdomainLevel
	 * @type {Number}
	 */
	"subdomainLevel": {
		"create": function(options){
			Console.log("UdropshipVendorMap - subdomainLevel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.subdomainLevel in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - subdomainLevel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("subdomainLevel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.updateStoreBaseUrl
	 * @type {Number}
	 */
	"updateStoreBaseUrl": {
		"create": function(options){
			Console.log("UdropshipVendorMap - updateStoreBaseUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.updateStoreBaseUrl in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - updateStoreBaseUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updateStoreBaseUrl");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.confirmation
	 * @type {String}
	 */
	"confirmation": {
		"create": function(options){
			Console.log("UdropshipVendorMap - confirmation - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.confirmation in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - confirmation - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("confirmation");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.confirmationSent
	 * @type {Number}
	 */
	"confirmationSent": {
		"create": function(options){
			Console.log("UdropshipVendorMap - confirmationSent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.confirmationSent in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - confirmationSent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("confirmationSent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.rejectReason
	 * @type {String}
	 */
	"rejectReason": {
		"create": function(options){
			Console.log("UdropshipVendorMap - rejectReason - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.rejectReason in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - rejectReason - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("rejectReason");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.backorderByAvailability
	 * @type {Number}
	 */
	"backorderByAvailability": {
		"create": function(options){
			Console.log("UdropshipVendorMap - backorderByAvailability - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.backorderByAvailability in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - backorderByAvailability - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("backorderByAvailability");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.useReservedQty
	 * @type {Number}
	 */
	"useReservedQty": {
		"create": function(options){
			Console.log("UdropshipVendorMap - useReservedQty - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.useReservedQty in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - useReservedQty - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("useReservedQty");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.tiercomRates
	 * @type {String}
	 */
	"tiercomRates": {
		"create": function(options){
			Console.log("UdropshipVendorMap - tiercomRates - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.tiercomRates in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - tiercomRates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tiercomRates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.tiercomFixedRule
	 * @type {String}
	 */
	"tiercomFixedRule": {
		"create": function(options){
			Console.log("UdropshipVendorMap - tiercomFixedRule - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.tiercomFixedRule in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - tiercomFixedRule - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tiercomFixedRule");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.tiercomFixedRates
	 * @type {String}
	 */
	"tiercomFixedRates": {
		"create": function(options){
			Console.log("UdropshipVendorMap - tiercomFixedRates - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.tiercomFixedRates in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - tiercomFixedRates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tiercomFixedRates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.tiercomFixedCalcType
	 * @type {String}
	 */
	"tiercomFixedCalcType": {
		"create": function(options){
			Console.log("UdropshipVendorMap - tiercomFixedCalcType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.tiercomFixedCalcType in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - tiercomFixedCalcType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tiercomFixedCalcType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.tiershipRates
	 * @type {String}
	 */
	"tiershipRates": {
		"create": function(options){
			Console.log("UdropshipVendorMap - tiershipRates - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.tiershipRates in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - tiershipRates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tiershipRates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.tiershipSimpleRates
	 * @type {String}
	 */
	"tiershipSimpleRates": {
		"create": function(options){
			Console.log("UdropshipVendorMap - tiershipSimpleRates - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.tiershipSimpleRates in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - tiershipSimpleRates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tiershipSimpleRates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.tiershipUseV2Rates
	 * @type {Number}
	 */
	"tiershipUseV2Rates": {
		"create": function(options){
			Console.log("UdropshipVendorMap - tiershipUseV2Rates - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.tiershipUseV2Rates in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - tiershipUseV2Rates - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tiershipUseV2Rates");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.vacationMode
	 * @type {Number}
	 */
	"vacationMode": {
		"create": function(options){
			Console.log("UdropshipVendorMap - vacationMode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.vacationMode in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - vacationMode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vacationMode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.vacationEnd
	 * @type {Date}
	 */
	"vacationEnd": {
		"create": function(options){
			Console.log("UdropshipVendorMap - vacationEnd - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.vacationEnd in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - vacationEnd - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vacationEnd");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.vacationMessage
	 * @type {String}
	 */
	"vacationMessage": {
		"create": function(options){
			Console.log("UdropshipVendorMap - vacationMessage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.vacationMessage in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - vacationMessage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vacationMessage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberLimitProducts
	 * @type {Number}
	 */
	"udmemberLimitProducts": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberLimitProducts - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberLimitProducts in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberLimitProducts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberLimitProducts");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberProfileId
	 * @type {Number}
	 */
	"udmemberProfileId": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberProfileId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberProfileId in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberProfileId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberProfileId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberProfileRefid
	 * @type {String}
	 */
	"udmemberProfileRefid": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberProfileRefid - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberProfileRefid in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberProfileRefid - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberProfileRefid");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberMembershipCode
	 * @type {String}
	 */
	"udmemberMembershipCode": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberMembershipCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberMembershipCode in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberMembershipCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberMembershipCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberMembershipTitle
	 * @type {String}
	 */
	"udmemberMembershipTitle": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberMembershipTitle - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberMembershipTitle in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberMembershipTitle - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberMembershipTitle");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberBillingType
	 * @type {String}
	 */
	"udmemberBillingType": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberBillingType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberBillingType in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberBillingType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberBillingType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberHistory
	 * @type {String}
	 */
	"udmemberHistory": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberHistory - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberHistory in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberHistory - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberHistory");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberProfileSyncOff
	 * @type {Number}
	 */
	"udmemberProfileSyncOff": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberProfileSyncOff - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberProfileSyncOff in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberProfileSyncOff - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberProfileSyncOff");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udmemberAllowMicrosite
	 * @type {Number}
	 */
	"udmemberAllowMicrosite": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udmemberAllowMicrosite - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udmemberAllowMicrosite in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udmemberAllowMicrosite - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udmemberAllowMicrosite");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.udprodTemplateSku
	 * @type {String}
	 */
	"udprodTemplateSku": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udprodTemplateSku - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.udprodTemplateSku in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udprodTemplateSku - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udprodTemplateSku");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.UdropshipVendor.vendorTaxClass
	 * @type {String}
	 */
	"vendorTaxClass": {
		"create": function(options){
			Console.log("UdropshipVendorMap - vendorTaxClass - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create UdropshipVendorModel.vendorTaxClass in during mapping");
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - vendorTaxClass - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorTaxClass");
			}
			return options.target;
		}
	},
	/**
	 * Property catalogProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.CatalogProduct
	 * @type {CatalogProduct}
	 */
	"catalogProducts": {
		"create": function(options){
			Console.log("UdropshipVendorMap - catalogProducts - create");
			Console.debug(JSON.stringify(options));
			return new CatalogProductModel();
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - catalogProducts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("catalogProducts");
			}
			return options.target;
		}
	},
	/**
	 * Property vendorLocations of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicBrandvenue
	 * @type {HarpoonHpublicBrandvenue}
	 */
	"vendorLocations": {
		"create": function(options){
			Console.log("UdropshipVendorMap - vendorLocations - create");
			Console.debug(JSON.stringify(options));
			return new HarpoonHpublicBrandvenueModel();
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - vendorLocations - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorLocations");
			}
			return options.target;
		}
	},
	/**
	 * Property config of harpoonApi.UdropshipVendor describes a relationship hasOne harpoonApi.HarpoonHpublicVendorconfig
	 * @type {HarpoonHpublicVendorconfig}
	 */
	"config": {
		"create": function(options){
			Console.log("UdropshipVendorMap - config - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - config - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("config");
			}
			return options.target;
		}
	},
	/**
	 * Property partners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendor
	 * @type {UdropshipVendor}
	 */
	"partners": {
		"create": function(options){
			Console.log("UdropshipVendorMap - partners - create");
			Console.debug(JSON.stringify(options));
			return new UdropshipVendorModel();
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - partners - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("partners");
			}
			return options.target;
		}
	},
	/**
	 * Property udropshipVendorProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendorProduct
	 * @type {UdropshipVendorProduct}
	 */
	"udropshipVendorProducts": {
		"create": function(options){
			Console.log("UdropshipVendorMap - udropshipVendorProducts - create");
			Console.debug(JSON.stringify(options));
			return new UdropshipVendorProductModel();
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - udropshipVendorProducts - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("udropshipVendorProducts");
			}
			return options.target;
		}
	},
	/**
	 * Property harpoonHpublicApplicationpartners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicApplicationpartner
	 * @type {HarpoonHpublicApplicationpartner}
	 */
	"harpoonHpublicApplicationpartners": {
		"create": function(options){
			Console.log("UdropshipVendorMap - harpoonHpublicApplicationpartners - create");
			Console.debug(JSON.stringify(options));
			return new HarpoonHpublicApplicationpartnerModel();
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - harpoonHpublicApplicationpartners - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("harpoonHpublicApplicationpartners");
			}
			return options.target;
		}
	},
	/**
	 * Property harpoonHpublicv12VendorAppCategories of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicv12VendorAppCategory
	 * @type {HarpoonHpublicv12VendorAppCategory}
	 */
	"harpoonHpublicv12VendorAppCategories": {
		"create": function(options){
			Console.log("UdropshipVendorMap - harpoonHpublicv12VendorAppCategories - create");
			Console.debug(JSON.stringify(options));
			return new HarpoonHpublicv12VendorAppCategoryModel();
		},
		"update": function(options){
			Console.log("UdropshipVendorMap - harpoonHpublicv12VendorAppCategories - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("harpoonHpublicv12VendorAppCategories");
			}
			return options.target;
		}
	},
};
