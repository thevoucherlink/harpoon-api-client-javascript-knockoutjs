/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * StripeInvoice Map
 * @type {object}
 */
var StripeInvoiceMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.StripeInvoice.object
	 * @type {string}
	 */
	"object": {
		"create": function(options){
			Console.log("StripeInvoiceMap - object - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.object in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - object - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("object");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.amountDue
	 * @type {number}
	 */
	"amountDue": {
		"create": function(options){
			Console.log("StripeInvoiceMap - amountDue - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.amountDue in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - amountDue - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("amountDue");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.applicationFee
	 * @type {number}
	 */
	"applicationFee": {
		"create": function(options){
			Console.log("StripeInvoiceMap - applicationFee - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.applicationFee in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - applicationFee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("applicationFee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.attemptCount
	 * @type {number}
	 */
	"attemptCount": {
		"create": function(options){
			Console.log("StripeInvoiceMap - attemptCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.attemptCount in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - attemptCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attemptCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.attempted
	 * @type {boolean}
	 */
	"attempted": {
		"create": function(options){
			Console.log("StripeInvoiceMap - attempted - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.attempted in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - attempted - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attempted");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.charge
	 * @type {string}
	 */
	"charge": {
		"create": function(options){
			Console.log("StripeInvoiceMap - charge - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.charge in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - charge - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("charge");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.closed
	 * @type {boolean}
	 */
	"closed": {
		"create": function(options){
			Console.log("StripeInvoiceMap - closed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.closed in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - closed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("closed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.currency
	 * @type {string}
	 */
	"currency": {
		"create": function(options){
			Console.log("StripeInvoiceMap - currency - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.currency in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - currency - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("currency");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.date
	 * @type {number}
	 */
	"date": {
		"create": function(options){
			Console.log("StripeInvoiceMap - date - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.date in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - date - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("date");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("StripeInvoiceMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.description in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.endingBalance
	 * @type {number}
	 */
	"endingBalance": {
		"create": function(options){
			Console.log("StripeInvoiceMap - endingBalance - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.endingBalance in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - endingBalance - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("endingBalance");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.forgiven
	 * @type {boolean}
	 */
	"forgiven": {
		"create": function(options){
			Console.log("StripeInvoiceMap - forgiven - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.forgiven in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - forgiven - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("forgiven");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.livemode
	 * @type {boolean}
	 */
	"livemode": {
		"create": function(options){
			Console.log("StripeInvoiceMap - livemode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.livemode in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - livemode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("livemode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.metadata
	 * @type {string}
	 */
	"metadata": {
		"create": function(options){
			Console.log("StripeInvoiceMap - metadata - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.metadata in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - metadata - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metadata");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.nextPaymentAttempt
	 * @type {number}
	 */
	"nextPaymentAttempt": {
		"create": function(options){
			Console.log("StripeInvoiceMap - nextPaymentAttempt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.nextPaymentAttempt in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - nextPaymentAttempt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("nextPaymentAttempt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.paid
	 * @type {boolean}
	 */
	"paid": {
		"create": function(options){
			Console.log("StripeInvoiceMap - paid - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.paid in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - paid - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("paid");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.periodEnd
	 * @type {number}
	 */
	"periodEnd": {
		"create": function(options){
			Console.log("StripeInvoiceMap - periodEnd - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.periodEnd in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - periodEnd - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("periodEnd");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.periodStart
	 * @type {number}
	 */
	"periodStart": {
		"create": function(options){
			Console.log("StripeInvoiceMap - periodStart - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.periodStart in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - periodStart - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("periodStart");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.receiptNumber
	 * @type {string}
	 */
	"receiptNumber": {
		"create": function(options){
			Console.log("StripeInvoiceMap - receiptNumber - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.receiptNumber in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - receiptNumber - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("receiptNumber");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.subscriptionProrationDate
	 * @type {number}
	 */
	"subscriptionProrationDate": {
		"create": function(options){
			Console.log("StripeInvoiceMap - subscriptionProrationDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.subscriptionProrationDate in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - subscriptionProrationDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("subscriptionProrationDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.subtotal
	 * @type {number}
	 */
	"subtotal": {
		"create": function(options){
			Console.log("StripeInvoiceMap - subtotal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.subtotal in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - subtotal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("subtotal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.tax
	 * @type {number}
	 */
	"tax": {
		"create": function(options){
			Console.log("StripeInvoiceMap - tax - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.tax in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - tax - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tax");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.taxPercent
	 * @type {number}
	 */
	"taxPercent": {
		"create": function(options){
			Console.log("StripeInvoiceMap - taxPercent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.taxPercent in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - taxPercent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("taxPercent");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.total
	 * @type {number}
	 */
	"total": {
		"create": function(options){
			Console.log("StripeInvoiceMap - total - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.total in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - total - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("total");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.webhooksDeliveredAt
	 * @type {number}
	 */
	"webhooksDeliveredAt": {
		"create": function(options){
			Console.log("StripeInvoiceMap - webhooksDeliveredAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.webhooksDeliveredAt in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - webhooksDeliveredAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("webhooksDeliveredAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.customer
	 * @type {string}
	 */
	"customer": {
		"create": function(options){
			Console.log("StripeInvoiceMap - customer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.customer in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - customer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customer");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.discount
	 * @type {StripeDiscount}
	 */
	"discount": {
		"create": function(options){
			Console.log("StripeInvoiceMap - discount - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - discount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("discount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.statmentDescriptor
	 * @type {string}
	 */
	"statmentDescriptor": {
		"create": function(options){
			Console.log("StripeInvoiceMap - statmentDescriptor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripeInvoiceModel.statmentDescriptor in during mapping");
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - statmentDescriptor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("statmentDescriptor");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.subscription
	 * @type {StripeSubscription}
	 */
	"subscription": {
		"create": function(options){
			Console.log("StripeInvoiceMap - subscription - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - subscription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("subscription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripeInvoice.lines
	 * @type {StripeInvoiceItem}
	 */
	"lines": {
		"create": function(options){
			Console.log("StripeInvoiceMap - lines - create");
			Console.debug(JSON.stringify(options));
			return new StripeInvoiceItemModel();
		},
		"update": function(options){
			Console.log("StripeInvoiceMap - lines - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("lines");
			}
			return options.target;
		}
	},
};
