/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignCompetitionWinCompetition Map
 * @type {object}
 */
var EmailAppCampaignCompetitionWinCompetitionMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandLogo
	 * @type {string}
	 */
	"brandLogo": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.brandLogo in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.campaignLink
	 * @type {string}
	 */
	"campaignLink": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - campaignLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.campaignLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - campaignLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCompetitionWinCompetition.prizeCollection
	 * @type {string}
	 */
	"prizeCollection": {
		"create": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - prizeCollection - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCompetitionWinCompetitionModel.prizeCollection in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCompetitionWinCompetitionMap - prizeCollection - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("prizeCollection");
			}
			return options.target;
		}
	},
};
