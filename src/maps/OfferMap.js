/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Offer Map
 * @type {object}
 */
var OfferMap = {
	// Ignore
	"ignore": ["coverUpload"],
	// Properties
	/**
	 * harpoonApi.Offer.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("OfferMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.id in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("OfferMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.name in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("OfferMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.description in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.cover
	 * @type {string}
	 */
	"cover": {
		"create": function(options){
			Console.log("OfferMap - cover - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.cover in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - cover - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("cover");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.coverUpload
	 * @type {MagentoImageUpload}
	 */
	"coverUpload": {
		"create": function(options){
			Console.log("OfferMap - coverUpload - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("OfferMap - coverUpload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("coverUpload");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.campaignType
	 * @type {Category}
	 */
	"campaignType": {
		"create": function(options){
			Console.log("OfferMap - campaignType - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("OfferMap - campaignType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.category
	 * @type {Category}
	 */
	"category": {
		"create": function(options){
			Console.log("OfferMap - category - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("OfferMap - category - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("category");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.topic
	 * @type {Category}
	 */
	"topic": {
		"create": function(options){
			Console.log("OfferMap - topic - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("OfferMap - topic - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("topic");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.alias
	 * @type {string}
	 */
	"alias": {
		"create": function(options){
			Console.log("OfferMap - alias - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.alias in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - alias - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("alias");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.from
	 * @type {date}
	 */
	"from": {
		"create": function(options){
			Console.log("OfferMap - from - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.from in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - from - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("from");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.to
	 * @type {date}
	 */
	"to": {
		"create": function(options){
			Console.log("OfferMap - to - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.to in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - to - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("to");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.baseCurrency
	 * @type {string}
	 */
	"baseCurrency": {
		"create": function(options){
			Console.log("OfferMap - baseCurrency - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.baseCurrency in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - baseCurrency - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("baseCurrency");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.priceText
	 * @type {string}
	 */
	"priceText": {
		"create": function(options){
			Console.log("OfferMap - priceText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.priceText in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - priceText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("priceText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.bannerText
	 * @type {string}
	 */
	"bannerText": {
		"create": function(options){
			Console.log("OfferMap - bannerText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.bannerText in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - bannerText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("bannerText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.checkoutLink
	 * @type {string}
	 */
	"checkoutLink": {
		"create": function(options){
			Console.log("OfferMap - checkoutLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.checkoutLink in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - checkoutLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("checkoutLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.nearestVenue
	 * @type {Venue}
	 */
	"nearestVenue": {
		"create": function(options){
			Console.log("OfferMap - nearestVenue - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("OfferMap - nearestVenue - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("nearestVenue");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.actionText
	 * @type {string}
	 */
	"actionText": {
		"create": function(options){
			Console.log("OfferMap - actionText - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.actionText in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - actionText - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("actionText");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.status
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("OfferMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.status in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.collectionNotes
	 * @type {string}
	 */
	"collectionNotes": {
		"create": function(options){
			Console.log("OfferMap - collectionNotes - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.collectionNotes in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - collectionNotes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("collectionNotes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.termsConditions
	 * @type {string}
	 */
	"termsConditions": {
		"create": function(options){
			Console.log("OfferMap - termsConditions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.termsConditions in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - termsConditions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("termsConditions");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.locationLink
	 * @type {string}
	 */
	"locationLink": {
		"create": function(options){
			Console.log("OfferMap - locationLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.locationLink in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - locationLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("locationLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.altLink
	 * @type {string}
	 */
	"altLink": {
		"create": function(options){
			Console.log("OfferMap - altLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.altLink in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - altLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("altLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.redemptionType
	 * @type {string}
	 */
	"redemptionType": {
		"create": function(options){
			Console.log("OfferMap - redemptionType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.redemptionType in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - redemptionType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redemptionType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.brand
	 * @type {Brand}
	 */
	"brand": {
		"create": function(options){
			Console.log("OfferMap - brand - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("OfferMap - brand - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brand");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.closestPurchase
	 * @type {OfferClosestPurchase}
	 */
	"closestPurchase": {
		"create": function(options){
			Console.log("OfferMap - closestPurchase - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("OfferMap - closestPurchase - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("closestPurchase");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.isFeatured
	 * @type {boolean}
	 */
	"isFeatured": {
		"create": function(options){
			Console.log("OfferMap - isFeatured - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.isFeatured in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - isFeatured - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isFeatured");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.qtyPerOrder
	 * @type {number}
	 */
	"qtyPerOrder": {
		"create": function(options){
			Console.log("OfferMap - qtyPerOrder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.qtyPerOrder in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - qtyPerOrder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyPerOrder");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Offer.shareLink
	 * @type {string}
	 */
	"shareLink": {
		"create": function(options){
			Console.log("OfferMap - shareLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create OfferModel.shareLink in during mapping");
		},
		"update": function(options){
			Console.log("OfferMap - shareLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("shareLink");
			}
			return options.target;
		}
	},
};
