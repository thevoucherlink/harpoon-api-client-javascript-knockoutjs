/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwEventbookingEvent Map
 * @type {object}
 */
var AwEventbookingEventMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwEventbookingEvent.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.productId
	 * @type {Number}
	 */
	"productId": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - productId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.productId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - productId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.isEnabled
	 * @type {Number}
	 */
	"isEnabled": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - isEnabled - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.isEnabled in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - isEnabled - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isEnabled");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.eventStartDate
	 * @type {Date}
	 */
	"eventStartDate": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - eventStartDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.eventStartDate in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - eventStartDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("eventStartDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.eventEndDate
	 * @type {Date}
	 */
	"eventEndDate": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - eventEndDate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.eventEndDate in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - eventEndDate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("eventEndDate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.dayCountBeforeSendReminderLetter
	 * @type {Number}
	 */
	"dayCountBeforeSendReminderLetter": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - dayCountBeforeSendReminderLetter - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.dayCountBeforeSendReminderLetter in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - dayCountBeforeSendReminderLetter - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("dayCountBeforeSendReminderLetter");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.isReminderSend
	 * @type {Number}
	 */
	"isReminderSend": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - isReminderSend - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.isReminderSend in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - isReminderSend - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isReminderSend");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.isTermsEnabled
	 * @type {Number}
	 */
	"isTermsEnabled": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - isTermsEnabled - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.isTermsEnabled in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - isTermsEnabled - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isTermsEnabled");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.generatePdfTickets
	 * @type {Number}
	 */
	"generatePdfTickets": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - generatePdfTickets - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.generatePdfTickets in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - generatePdfTickets - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("generatePdfTickets");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.redeemRoles
	 * @type {String}
	 */
	"redeemRoles": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - redeemRoles - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.redeemRoles in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - redeemRoles - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("redeemRoles");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEvent.location
	 * @type {String}
	 */
	"location": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - location - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventModel.location in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - location - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("location");
			}
			return options.target;
		}
	},
	/**
	 * Property tickets of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingEventTicket
	 * @type {AwEventbookingEventTicket}
	 */
	"tickets": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - tickets - create");
			Console.debug(JSON.stringify(options));
			return new AwEventbookingEventTicketModel();
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - tickets - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tickets");
			}
			return options.target;
		}
	},
	/**
	 * Property attributes of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingEventAttribute
	 * @type {AwEventbookingEventAttribute}
	 */
	"attributes": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - attributes - create");
			Console.debug(JSON.stringify(options));
			return new AwEventbookingEventAttributeModel();
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - attributes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attributes");
			}
			return options.target;
		}
	},
	/**
	 * Property purchasedTickets of harpoonApi.AwEventbookingEvent describes a relationship hasMany harpoonApi.AwEventbookingTicket
	 * @type {AwEventbookingTicket}
	 */
	"purchasedTickets": {
		"create": function(options){
			Console.log("AwEventbookingEventMap - purchasedTickets - create");
			Console.debug(JSON.stringify(options));
			return new AwEventbookingTicketModel();
		},
		"update": function(options){
			Console.log("AwEventbookingEventMap - purchasedTickets - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("purchasedTickets");
			}
			return options.target;
		}
	},
};
