/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * RssFeed Map
 * @type {object}
 */
var RssFeedMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.RssFeed.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("RssFeedMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.id in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.category
	 * @type {string}
	 */
	"category": {
		"create": function(options){
			Console.log("RssFeedMap - category - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.category in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - category - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("category");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.subCategory
	 * @type {string}
	 */
	"subCategory": {
		"create": function(options){
			Console.log("RssFeedMap - subCategory - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.subCategory in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - subCategory - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("subCategory");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.url
	 * @type {string}
	 */
	"url": {
		"create": function(options){
			Console.log("RssFeedMap - url - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.url in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - url - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("url");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("RssFeedMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.name in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.sortOrder
	 * @type {number}
	 */
	"sortOrder": {
		"create": function(options){
			Console.log("RssFeedMap - sortOrder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.sortOrder in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - sortOrder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sortOrder");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.styleCss
	 * @type {string}
	 */
	"styleCss": {
		"create": function(options){
			Console.log("RssFeedMap - styleCss - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.styleCss in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - styleCss - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("styleCss");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.noAds
	 * @type {boolean}
	 */
	"noAds": {
		"create": function(options){
			Console.log("RssFeedMap - noAds - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.noAds in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - noAds - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("noAds");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.adMRectVisible
	 * @type {boolean}
	 */
	"adMRectVisible": {
		"create": function(options){
			Console.log("RssFeedMap - adMRectVisible - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.adMRectVisible in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - adMRectVisible - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("adMRectVisible");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.RssFeed.adMRectPosition
	 * @type {number}
	 */
	"adMRectPosition": {
		"create": function(options){
			Console.log("RssFeedMap - adMRectPosition - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.adMRectPosition in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - adMRectPosition - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("adMRectPosition");
			}
			return options.target;
		}
	},
	/**
	 * Property rssFeedGroup of harpoonApi.RssFeed describes a relationship belongsTo harpoonApi.RssFeedGroup
	 * @type {object}
	 */
	"rssFeedGroup": {
		"create": function(options){
			Console.log("RssFeedMap - rssFeedGroup - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create RssFeedModel.rssFeedGroup in during mapping");
		},
		"update": function(options){
			Console.log("RssFeedMap - rssFeedGroup - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("rssFeedGroup");
			}
			return options.target;
		}
	},
};
