/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Deal Map
 * @type {object}
 */
var DealMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.Deal.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("DealMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create DealModel.id in during mapping");
		},
		"update": function(options){
			Console.log("DealMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Deal.type
	 * @type {Category}
	 */
	"type": {
		"create": function(options){
			Console.log("DealMap - type - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("DealMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Deal.price
	 * @type {number}
	 */
	"price": {
		"create": function(options){
			Console.log("DealMap - price - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create DealModel.price in during mapping");
		},
		"update": function(options){
			Console.log("DealMap - price - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("price");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Deal.hasClaimed
	 * @type {boolean}
	 */
	"hasClaimed": {
		"create": function(options){
			Console.log("DealMap - hasClaimed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create DealModel.hasClaimed in during mapping");
		},
		"update": function(options){
			Console.log("DealMap - hasClaimed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hasClaimed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Deal.qtyLeft
	 * @type {number}
	 */
	"qtyLeft": {
		"create": function(options){
			Console.log("DealMap - qtyLeft - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create DealModel.qtyLeft in during mapping");
		},
		"update": function(options){
			Console.log("DealMap - qtyLeft - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyLeft");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Deal.qtyTotal
	 * @type {number}
	 */
	"qtyTotal": {
		"create": function(options){
			Console.log("DealMap - qtyTotal - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create DealModel.qtyTotal in during mapping");
		},
		"update": function(options){
			Console.log("DealMap - qtyTotal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyTotal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Deal.qtyClaimed
	 * @type {number}
	 */
	"qtyClaimed": {
		"create": function(options){
			Console.log("DealMap - qtyClaimed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create DealModel.qtyClaimed in during mapping");
		},
		"update": function(options){
			Console.log("DealMap - qtyClaimed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("qtyClaimed");
			}
			return options.target;
		}
	},
};
