/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * Competition Map
 * @type {object}
 */
var CompetitionMap = {
	// Ignore
	"ignore": ["attendee","chance"],
	// Properties
	/**
	 * harpoonApi.Competition.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("CompetitionMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.id in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.basePrice
	 * @type {number}
	 */
	"basePrice": {
		"create": function(options){
			Console.log("CompetitionMap - basePrice - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.basePrice in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - basePrice - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("basePrice");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.attendee
	 * @type {CompetitionAttendee}
	 */
	"attendee": {
		"create": function(options){
			Console.log("CompetitionMap - attendee - create");
			Console.debug(JSON.stringify(options));
			return new CompetitionAttendeeModel();
		},
		"update": function(options){
			Console.log("CompetitionMap - attendee - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendee");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.attendees
	 * @type {CompetitionAttendee}
	 */
	"attendees": {
		"create": function(options){
			Console.log("CompetitionMap - attendees - create");
			Console.debug(JSON.stringify(options));
			return new CompetitionAttendeeModel();
		},
		"update": function(options){
			Console.log("CompetitionMap - attendees - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendees");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.attendeeCount
	 * @type {number}
	 */
	"attendeeCount": {
		"create": function(options){
			Console.log("CompetitionMap - attendeeCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.attendeeCount in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - attendeeCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendeeCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.hasJoined
	 * @type {boolean}
	 */
	"hasJoined": {
		"create": function(options){
			Console.log("CompetitionMap - hasJoined - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.hasJoined in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - hasJoined - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hasJoined");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.chance
	 * @type {CompetitionChance}
	 */
	"chance": {
		"create": function(options){
			Console.log("CompetitionMap - chance - create");
			Console.debug(JSON.stringify(options));
			return new CompetitionChanceModel();
		},
		"update": function(options){
			Console.log("CompetitionMap - chance - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("chance");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.chanceCount
	 * @type {number}
	 */
	"chanceCount": {
		"create": function(options){
			Console.log("CompetitionMap - chanceCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.chanceCount in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - chanceCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("chanceCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.isWinner
	 * @type {boolean}
	 */
	"isWinner": {
		"create": function(options){
			Console.log("CompetitionMap - isWinner - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.isWinner in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - isWinner - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isWinner");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.facebook
	 * @type {FacebookEvent}
	 */
	"facebook": {
		"create": function(options){
			Console.log("CompetitionMap - facebook - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CompetitionMap - facebook - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebook");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.chances
	 * @type {CompetitionChance}
	 */
	"chances": {
		"create": function(options){
			Console.log("CompetitionMap - chances - create");
			Console.debug(JSON.stringify(options));
			return new CompetitionChanceModel();
		},
		"update": function(options){
			Console.log("CompetitionMap - chances - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("chances");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.earnMoreChancesURL
	 * @type {string}
	 */
	"earnMoreChancesURL": {
		"create": function(options){
			Console.log("CompetitionMap - earnMoreChancesURL - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.earnMoreChancesURL in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - earnMoreChancesURL - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("earnMoreChancesURL");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.type
	 * @type {Category}
	 */
	"type": {
		"create": function(options){
			Console.log("CompetitionMap - type - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("CompetitionMap - type - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("type");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.earnMoreChances
	 * @type {boolean}
	 */
	"earnMoreChances": {
		"create": function(options){
			Console.log("CompetitionMap - earnMoreChances - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.earnMoreChances in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - earnMoreChances - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("earnMoreChances");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.competitionQuestion
	 * @type {string}
	 */
	"competitionQuestion": {
		"create": function(options){
			Console.log("CompetitionMap - competitionQuestion - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.competitionQuestion in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - competitionQuestion - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionQuestion");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.Competition.competitionAnswer
	 * @type {string}
	 */
	"competitionAnswer": {
		"create": function(options){
			Console.log("CompetitionMap - competitionAnswer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CompetitionModel.competitionAnswer in during mapping");
		},
		"update": function(options){
			Console.log("CompetitionMap - competitionAnswer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("competitionAnswer");
			}
			return options.target;
		}
	},
};
