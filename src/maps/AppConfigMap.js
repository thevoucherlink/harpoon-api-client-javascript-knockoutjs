/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AppConfig Map
 * @type {object}
 */
var AppConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AppConfig.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("AppConfigMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * App logo
	 * @type {string}
	 */
	"imgAppLogo": {
		"create": function(options){
			Console.log("AppConfigMap - imgAppLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgAppLogo in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgAppLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgAppLogo");
			}
			return options.target;
		}
	},
	/**
	 * App splash-screen
	 * @type {string}
	 */
	"imgSplashScreen": {
		"create": function(options){
			Console.log("AppConfigMap - imgSplashScreen - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgSplashScreen in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgSplashScreen - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgSplashScreen");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.imgBrandLogo
	 * @type {string}
	 */
	"imgBrandLogo": {
		"create": function(options){
			Console.log("AppConfigMap - imgBrandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgBrandLogo in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgBrandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgBrandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.imgNavBarLogo
	 * @type {string}
	 */
	"imgNavBarLogo": {
		"create": function(options){
			Console.log("AppConfigMap - imgNavBarLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgNavBarLogo in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgNavBarLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgNavBarLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.imgBrandPlaceholder
	 * @type {string}
	 */
	"imgBrandPlaceholder": {
		"create": function(options){
			Console.log("AppConfigMap - imgBrandPlaceholder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgBrandPlaceholder in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgBrandPlaceholder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgBrandPlaceholder");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.imgMenuBackground
	 * @type {string}
	 */
	"imgMenuBackground": {
		"create": function(options){
			Console.log("AppConfigMap - imgMenuBackground - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgMenuBackground in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgMenuBackground - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgMenuBackground");
			}
			return options.target;
		}
	},
	/**
	 * Enable or disable the Radio feature within the app
	 * @type {boolean}
	 */
	"featureRadio": {
		"create": function(options){
			Console.log("AppConfigMap - featureRadio - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureRadio in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureRadio - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureRadio");
			}
			return options.target;
		}
	},
	/**
	 * Enable or disable the Radio autoplay feature within the app
	 * @type {boolean}
	 */
	"featureRadioAutoPlay": {
		"create": function(options){
			Console.log("AppConfigMap - featureRadioAutoPlay - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureRadioAutoPlay in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureRadioAutoPlay - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureRadioAutoPlay");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureNews
	 * @type {boolean}
	 */
	"featureNews": {
		"create": function(options){
			Console.log("AppConfigMap - featureNews - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureNews in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureNews - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureNews");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureEvents
	 * @type {boolean}
	 */
	"featureEvents": {
		"create": function(options){
			Console.log("AppConfigMap - featureEvents - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureEvents in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureEvents - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureEvents");
			}
			return options.target;
		}
	},
	/**
	 * If true the Events list is categorised
	 * @type {boolean}
	 */
	"featureEventsCategorized": {
		"create": function(options){
			Console.log("AppConfigMap - featureEventsCategorized - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureEventsCategorized in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureEventsCategorized - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureEventsCategorized");
			}
			return options.target;
		}
	},
	/**
	 * If true the user will have the possibility to see Events' attendees
	 * @type {boolean}
	 */
	"featureEventsAttendeeList": {
		"create": function(options){
			Console.log("AppConfigMap - featureEventsAttendeeList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureEventsAttendeeList in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureEventsAttendeeList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureEventsAttendeeList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureCommunity
	 * @type {boolean}
	 */
	"featureCommunity": {
		"create": function(options){
			Console.log("AppConfigMap - featureCommunity - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureCommunity in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureCommunity - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureCommunity");
			}
			return options.target;
		}
	},
	/**
	 * If true the Brand list is categories
	 * @type {boolean}
	 */
	"featureCommunityCategorized": {
		"create": function(options){
			Console.log("AppConfigMap - featureCommunityCategorized - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureCommunityCategorized in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureCommunityCategorized - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureCommunityCategorized");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureCompetitions
	 * @type {boolean}
	 */
	"featureCompetitions": {
		"create": function(options){
			Console.log("AppConfigMap - featureCompetitions - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureCompetitions in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureCompetitions - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureCompetitions");
			}
			return options.target;
		}
	},
	/**
	 * If true the user will have the possibility to see Competitions' attendees
	 * @type {boolean}
	 */
	"featureCompetitionsAttendeeList": {
		"create": function(options){
			Console.log("AppConfigMap - featureCompetitionsAttendeeList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureCompetitionsAttendeeList in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureCompetitionsAttendeeList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureCompetitionsAttendeeList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureOffers
	 * @type {boolean}
	 */
	"featureOffers": {
		"create": function(options){
			Console.log("AppConfigMap - featureOffers - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureOffers in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureOffers - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureOffers");
			}
			return options.target;
		}
	},
	/**
	 * If true the Offer list is categories
	 * @type {boolean}
	 */
	"featureOffersCategorized": {
		"create": function(options){
			Console.log("AppConfigMap - featureOffersCategorized - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureOffersCategorized in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureOffersCategorized - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureOffersCategorized");
			}
			return options.target;
		}
	},
	/**
	 * If true the user will have the possibility to follow or unfollow Brands
	 * @type {boolean}
	 */
	"featureBrandFollow": {
		"create": function(options){
			Console.log("AppConfigMap - featureBrandFollow - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureBrandFollow in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureBrandFollow - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureBrandFollow");
			}
			return options.target;
		}
	},
	/**
	 * If true the user will have the possibility to see Brands' followers
	 * @type {boolean}
	 */
	"featureBrandFollowerList": {
		"create": function(options){
			Console.log("AppConfigMap - featureBrandFollowerList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureBrandFollowerList in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureBrandFollowerList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureBrandFollowerList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureWordpressConnect
	 * @type {boolean}
	 */
	"featureWordpressConnect": {
		"create": function(options){
			Console.log("AppConfigMap - featureWordpressConnect - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureWordpressConnect in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureWordpressConnect - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureWordpressConnect");
			}
			return options.target;
		}
	},
	/**
	 * If true the user will be asked to login before getting access to the app
	 * @type {boolean}
	 */
	"featureLogin": {
		"create": function(options){
			Console.log("AppConfigMap - featureLogin - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureLogin in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureLogin - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureLogin");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureHarpoonLogin
	 * @type {boolean}
	 */
	"featureHarpoonLogin": {
		"create": function(options){
			Console.log("AppConfigMap - featureHarpoonLogin - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureHarpoonLogin in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureHarpoonLogin - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureHarpoonLogin");
			}
			return options.target;
		}
	},
	/**
	 * If true the user will have the possibility to login with Facebook
	 * @type {boolean}
	 */
	"featureFacebookLogin": {
		"create": function(options){
			Console.log("AppConfigMap - featureFacebookLogin - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureFacebookLogin in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureFacebookLogin - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureFacebookLogin");
			}
			return options.target;
		}
	},
	/**
	 * If true the user will need to verify its account using a phone number
	 * @type {boolean}
	 */
	"featurePhoneVerification": {
		"create": function(options){
			Console.log("AppConfigMap - featurePhoneVerification - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featurePhoneVerification in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featurePhoneVerification - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featurePhoneVerification");
			}
			return options.target;
		}
	},
	/**
	 * Show / Hide media player
	 * @type {boolean}
	 */
	"featurePlayer": {
		"create": function(options){
			Console.log("AppConfigMap - featurePlayer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featurePlayer in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featurePlayer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featurePlayer");
			}
			return options.target;
		}
	},
	/**
	 * Show / Hide main button
	 * @type {boolean}
	 */
	"featurePlayerMainButton": {
		"create": function(options){
			Console.log("AppConfigMap - featurePlayerMainButton - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featurePlayerMainButton in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featurePlayerMainButton - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featurePlayerMainButton");
			}
			return options.target;
		}
	},
	/**
	 * If true the user actions will be tracked while using the app
	 * @type {boolean}
	 */
	"featureGoogleAnalytics": {
		"create": function(options){
			Console.log("AppConfigMap - featureGoogleAnalytics - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureGoogleAnalytics in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureGoogleAnalytics - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureGoogleAnalytics");
			}
			return options.target;
		}
	},
	/**
	 * Media Player Main Button title
	 * @type {string}
	 */
	"playerMainButtonTitle": {
		"create": function(options){
			Console.log("AppConfigMap - playerMainButtonTitle - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.playerMainButtonTitle in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - playerMainButtonTitle - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playerMainButtonTitle");
			}
			return options.target;
		}
	},
	/**
	 * Media Player config
	 * @type {PlayerConfig}
	 */
	"playerConfig": {
		"create": function(options){
			Console.log("AppConfigMap - playerConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - playerConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playerConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.menu
	 * @type {MenuItem}
	 */
	"menu": {
		"create": function(options){
			Console.log("AppConfigMap - menu - create");
			Console.debug(JSON.stringify(options));
			return new MenuItemModel();
		},
		"update": function(options){
			Console.log("AppConfigMap - menu - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("menu");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.color
	 * @type {string}
	 */
	"color": {
		"create": function(options){
			Console.log("AppConfigMap - color - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.color in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - color - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("color");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.colors
	 * @type {AppColor}
	 */
	"colors": {
		"create": function(options){
			Console.log("AppConfigMap - colors - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - colors - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("colors");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.googleAppId
	 * @type {string}
	 */
	"googleAppId": {
		"create": function(options){
			Console.log("AppConfigMap - googleAppId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.googleAppId in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - googleAppId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("googleAppId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSAppId
	 * @type {string}
	 */
	"iOSAppId": {
		"create": function(options){
			Console.log("AppConfigMap - iOSAppId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSAppId in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSAppId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSAppId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.googleAnalyticsTracking
	 * @type {GoogleAnalyticsTracking}
	 */
	"googleAnalyticsTracking": {
		"create": function(options){
			Console.log("AppConfigMap - googleAnalyticsTracking - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - googleAnalyticsTracking - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("googleAnalyticsTracking");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.loginExternal
	 * @type {LoginExternal}
	 */
	"loginExternal": {
		"create": function(options){
			Console.log("AppConfigMap - loginExternal - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - loginExternal - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("loginExternal");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.rssWebViewCss
	 * @type {string}
	 */
	"rssWebViewCss": {
		"create": function(options){
			Console.log("AppConfigMap - rssWebViewCss - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.rssWebViewCss in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - rssWebViewCss - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("rssWebViewCss");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.ads
	 * @type {AppAd}
	 */
	"ads": {
		"create": function(options){
			Console.log("AppConfigMap - ads - create");
			Console.debug(JSON.stringify(options));
			return new AppAdModel();
		},
		"update": function(options){
			Console.log("AppConfigMap - ads - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ads");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSAds
	 * @type {AppAd}
	 */
	"iOSAds": {
		"create": function(options){
			Console.log("AppConfigMap - iOSAds - create");
			Console.debug(JSON.stringify(options));
			return new AppAdModel();
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSAds - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSAds");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidAds
	 * @type {AppAd}
	 */
	"androidAds": {
		"create": function(options){
			Console.log("AppConfigMap - androidAds - create");
			Console.debug(JSON.stringify(options));
			return new AppAdModel();
		},
		"update": function(options){
			Console.log("AppConfigMap - androidAds - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidAds");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.pushNotificationProvider
	 * @type {string}
	 */
	"pushNotificationProvider": {
		"create": function(options){
			Console.log("AppConfigMap - pushNotificationProvider - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.pushNotificationProvider in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - pushNotificationProvider - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pushNotificationProvider");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.batchConfig
	 * @type {BatchConfig}
	 */
	"batchConfig": {
		"create": function(options){
			Console.log("AppConfigMap - batchConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - batchConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("batchConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.urbanAirshipConfig
	 * @type {UrbanAirshipConfig}
	 */
	"urbanAirshipConfig": {
		"create": function(options){
			Console.log("AppConfigMap - urbanAirshipConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - urbanAirshipConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urbanAirshipConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.pushWooshConfig
	 * @type {PushWooshConfig}
	 */
	"pushWooshConfig": {
		"create": function(options){
			Console.log("AppConfigMap - pushWooshConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - pushWooshConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pushWooshConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.facebookConfig
	 * @type {FacebookConfig}
	 */
	"facebookConfig": {
		"create": function(options){
			Console.log("AppConfigMap - facebookConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - facebookConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("facebookConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.twitterConfig
	 * @type {TwitterConfig}
	 */
	"twitterConfig": {
		"create": function(options){
			Console.log("AppConfigMap - twitterConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - twitterConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("twitterConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.rssTags
	 * @type {RssTags}
	 */
	"rssTags": {
		"create": function(options){
			Console.log("AppConfigMap - rssTags - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - rssTags - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("rssTags");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.connectTo
	 * @type {AppConnectTo}
	 */
	"connectTo": {
		"create": function(options){
			Console.log("AppConfigMap - connectTo - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - connectTo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("connectTo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSVersion
	 * @type {string}
	 */
	"iOSVersion": {
		"create": function(options){
			Console.log("AppConfigMap - iOSVersion - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSVersion in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSVersion - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSVersion");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSBuild
	 * @type {string}
	 */
	"iOSBuild": {
		"create": function(options){
			Console.log("AppConfigMap - iOSBuild - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSBuild in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSBuild - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSBuild");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSGoogleServices
	 * @type {string}
	 */
	"iOSGoogleServices": {
		"create": function(options){
			Console.log("AppConfigMap - iOSGoogleServices - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSGoogleServices in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSGoogleServices - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSGoogleServices");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSTeamId
	 * @type {string}
	 */
	"iOSTeamId": {
		"create": function(options){
			Console.log("AppConfigMap - iOSTeamId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSTeamId in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSTeamId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSTeamId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSAppleId
	 * @type {string}
	 */
	"iOSAppleId": {
		"create": function(options){
			Console.log("AppConfigMap - iOSAppleId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSAppleId in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSAppleId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSAppleId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationPrivateKey
	 * @type {string}
	 */
	"iOSPushNotificationPrivateKey": {
		"create": function(options){
			Console.log("AppConfigMap - iOSPushNotificationPrivateKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSPushNotificationPrivateKey in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSPushNotificationPrivateKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSPushNotificationPrivateKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationCertificate
	 * @type {string}
	 */
	"iOSPushNotificationCertificate": {
		"create": function(options){
			Console.log("AppConfigMap - iOSPushNotificationCertificate - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSPushNotificationCertificate in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSPushNotificationCertificate - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSPushNotificationCertificate");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationPem
	 * @type {string}
	 */
	"iOSPushNotificationPem": {
		"create": function(options){
			Console.log("AppConfigMap - iOSPushNotificationPem - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSPushNotificationPem in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSPushNotificationPem - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSPushNotificationPem");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationPassword
	 * @type {string}
	 */
	"iOSPushNotificationPassword": {
		"create": function(options){
			Console.log("AppConfigMap - iOSPushNotificationPassword - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSPushNotificationPassword in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSPushNotificationPassword - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSPushNotificationPassword");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iOSReleaseNotes
	 * @type {string}
	 */
	"iOSReleaseNotes": {
		"create": function(options){
			Console.log("AppConfigMap - iOSReleaseNotes - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iOSReleaseNotes in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iOSReleaseNotes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iOSReleaseNotes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidBuild
	 * @type {string}
	 */
	"androidBuild": {
		"create": function(options){
			Console.log("AppConfigMap - androidBuild - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidBuild in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidBuild - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidBuild");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidVersion
	 * @type {string}
	 */
	"androidVersion": {
		"create": function(options){
			Console.log("AppConfigMap - androidVersion - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidVersion in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidVersion - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidVersion");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidGoogleServices
	 * @type {string}
	 */
	"androidGoogleServices": {
		"create": function(options){
			Console.log("AppConfigMap - androidGoogleServices - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidGoogleServices in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidGoogleServices - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidGoogleServices");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidGoogleApiKey
	 * @type {string}
	 */
	"androidGoogleApiKey": {
		"create": function(options){
			Console.log("AppConfigMap - androidGoogleApiKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidGoogleApiKey in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidGoogleApiKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidGoogleApiKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidGoogleCloudMessagingSenderId
	 * @type {string}
	 */
	"androidGoogleCloudMessagingSenderId": {
		"create": function(options){
			Console.log("AppConfigMap - androidGoogleCloudMessagingSenderId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidGoogleCloudMessagingSenderId in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidGoogleCloudMessagingSenderId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidGoogleCloudMessagingSenderId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidKey
	 * @type {string}
	 */
	"androidKey": {
		"create": function(options){
			Console.log("AppConfigMap - androidKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidKey in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidKeystore
	 * @type {string}
	 */
	"androidKeystore": {
		"create": function(options){
			Console.log("AppConfigMap - androidKeystore - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidKeystore in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidKeystore - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidKeystore");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidKeystoreConfig
	 * @type {AndroidKeystoreConfig}
	 */
	"androidKeystoreConfig": {
		"create": function(options){
			Console.log("AppConfigMap - androidKeystoreConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - androidKeystoreConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidKeystoreConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidApk
	 * @type {string}
	 */
	"androidApk": {
		"create": function(options){
			Console.log("AppConfigMap - androidApk - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidApk in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidApk - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidApk");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.customerDetailsForm
	 * @type {FormSection}
	 */
	"customerDetailsForm": {
		"create": function(options){
			Console.log("AppConfigMap - customerDetailsForm - create");
			Console.debug(JSON.stringify(options));
			return new FormSectionModel();
		},
		"update": function(options){
			Console.log("AppConfigMap - customerDetailsForm - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customerDetailsForm");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.contact
	 * @type {Contact}
	 */
	"contact": {
		"create": function(options){
			Console.log("AppConfigMap - contact - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - contact - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("contact");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.imgSponsorMenu
	 * @type {string}
	 */
	"imgSponsorMenu": {
		"create": function(options){
			Console.log("AppConfigMap - imgSponsorMenu - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgSponsorMenu in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgSponsorMenu - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgSponsorMenu");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.imgSponsorSplash
	 * @type {string}
	 */
	"imgSponsorSplash": {
		"create": function(options){
			Console.log("AppConfigMap - imgSponsorSplash - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.imgSponsorSplash in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - imgSponsorSplash - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("imgSponsorSplash");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.audioSponsorPreRollAd
	 * @type {string}
	 */
	"audioSponsorPreRollAd": {
		"create": function(options){
			Console.log("AppConfigMap - audioSponsorPreRollAd - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.audioSponsorPreRollAd in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - audioSponsorPreRollAd - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("audioSponsorPreRollAd");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("AppConfigMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.name in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.isChildConfig
	 * @type {boolean}
	 */
	"isChildConfig": {
		"create": function(options){
			Console.log("AppConfigMap - isChildConfig - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.isChildConfig in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - isChildConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("isChildConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.typeNewsFocusSource
	 * @type {string}
	 */
	"typeNewsFocusSource": {
		"create": function(options){
			Console.log("AppConfigMap - typeNewsFocusSource - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.typeNewsFocusSource in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - typeNewsFocusSource - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("typeNewsFocusSource");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.sponsorSplashImgDisplayTime
	 * @type {number}
	 */
	"sponsorSplashImgDisplayTime": {
		"create": function(options){
			Console.log("AppConfigMap - sponsorSplashImgDisplayTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.sponsorSplashImgDisplayTime in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - sponsorSplashImgDisplayTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sponsorSplashImgDisplayTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.radioSessionInterval
	 * @type {number}
	 */
	"radioSessionInterval": {
		"create": function(options){
			Console.log("AppConfigMap - radioSessionInterval - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.radioSessionInterval in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - radioSessionInterval - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioSessionInterval");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.featureRadioSession
	 * @type {boolean}
	 */
	"featureRadioSession": {
		"create": function(options){
			Console.log("AppConfigMap - featureRadioSession - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureRadioSession in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureRadioSession - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureRadioSession");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.wordpressShareUrlStub
	 * @type {string}
	 */
	"wordpressShareUrlStub": {
		"create": function(options){
			Console.log("AppConfigMap - wordpressShareUrlStub - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.wordpressShareUrlStub in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - wordpressShareUrlStub - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("wordpressShareUrlStub");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.iosUriScheme
	 * @type {string}
	 */
	"iosUriScheme": {
		"create": function(options){
			Console.log("AppConfigMap - iosUriScheme - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.iosUriScheme in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - iosUriScheme - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("iosUriScheme");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.androidUriScheme
	 * @type {string}
	 */
	"androidUriScheme": {
		"create": function(options){
			Console.log("AppConfigMap - androidUriScheme - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.androidUriScheme in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - androidUriScheme - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("androidUriScheme");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.branchConfig
	 * @type {BranchConfig}
	 */
	"branchConfig": {
		"create": function(options){
			Console.log("AppConfigMap - branchConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppConfigMap - branchConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("branchConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AppConfig.defaultRadioStreamId
	 * @type {number}
	 */
	"defaultRadioStreamId": {
		"create": function(options){
			Console.log("AppConfigMap - defaultRadioStreamId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.defaultRadioStreamId in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - defaultRadioStreamId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("defaultRadioStreamId");
			}
			return options.target;
		}
	},
	/**
	 * Enable or disable the Triton Player in App
	 * @type {boolean}
	 */
	"featureTritonPlayer": {
		"create": function(options){
			Console.log("AppConfigMap - featureTritonPlayer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.featureTritonPlayer in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - featureTritonPlayer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureTritonPlayer");
			}
			return options.target;
		}
	},
	/**
	 * Property app of harpoonApi.AppConfig describes a relationship belongsTo harpoonApi.App
	 * @type {object}
	 */
	"app": {
		"create": function(options){
			Console.log("AppConfigMap - app - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppConfigModel.app in during mapping");
		},
		"update": function(options){
			Console.log("AppConfigMap - app - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("app");
			}
			return options.target;
		}
	},
};
