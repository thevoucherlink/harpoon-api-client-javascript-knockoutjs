/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CatalogCategoryProduct Map
 * @type {object}
 */
var CatalogCategoryProductMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.CatalogCategoryProduct.categoryId
	 * @type {Number}
	 */
	"categoryId": {
		"create": function(options){
			Console.log("CatalogCategoryProductMap - categoryId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryProductModel.categoryId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryProductMap - categoryId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("categoryId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategoryProduct.productId
	 * @type {Number}
	 */
	"productId": {
		"create": function(options){
			Console.log("CatalogCategoryProductMap - productId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryProductModel.productId in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryProductMap - productId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("productId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.CatalogCategoryProduct.position
	 * @type {Number}
	 */
	"position": {
		"create": function(options){
			Console.log("CatalogCategoryProductMap - position - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryProductModel.position in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryProductMap - position - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("position");
			}
			return options.target;
		}
	},
	/**
	 * Property catalogProduct of harpoonApi.CatalogCategoryProduct describes a relationship belongsTo harpoonApi.CatalogProduct
	 * @type {object}
	 */
	"catalogProduct": {
		"create": function(options){
			Console.log("CatalogCategoryProductMap - catalogProduct - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryProductModel.catalogProduct in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryProductMap - catalogProduct - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("catalogProduct");
			}
			return options.target;
		}
	},
	/**
	 * Property catalogCategory of harpoonApi.CatalogCategoryProduct describes a relationship belongsTo harpoonApi.CatalogCategory
	 * @type {object}
	 */
	"catalogCategory": {
		"create": function(options){
			Console.log("CatalogCategoryProductMap - catalogCategory - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create CatalogCategoryProductModel.catalogCategory in during mapping");
		},
		"update": function(options){
			Console.log("CatalogCategoryProductMap - catalogCategory - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("catalogCategory");
			}
			return options.target;
		}
	},
};
