/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailAppCampaignCouponsTheCoupon Map
 * @type {object}
 */
var EmailAppCampaignCouponsTheCouponMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.appName
	 * @type {string}
	 */
	"appName": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - appName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.appName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - appName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.brandLogo
	 * @type {string}
	 */
	"brandLogo": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandLogo - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.brandLogo in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandLogo - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandLogo");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignBarcode
	 * @type {string}
	 */
	"campaignBarcode": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignBarcode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.campaignBarcode in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignBarcode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignBarcode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignCode
	 * @type {number}
	 */
	"campaignCode": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.campaignCode in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignDescription
	 * @type {string}
	 */
	"campaignDescription": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.campaignDescription in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignDescription");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignEndTime
	 * @type {string}
	 */
	"campaignEndTime": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignEndTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.campaignEndTime in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignEndTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignEndTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignLocation
	 * @type {string}
	 */
	"campaignLocation": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignLocation - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.campaignLocation in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignLocation - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignLocation");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailAppCampaignCouponsTheCoupon.organizerName
	 * @type {string}
	 */
	"organizerName": {
		"create": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - organizerName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailAppCampaignCouponsTheCouponModel.organizerName in during mapping");
		},
		"update": function(options){
			Console.log("EmailAppCampaignCouponsTheCouponMap - organizerName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("organizerName");
			}
			return options.target;
		}
	},
};
