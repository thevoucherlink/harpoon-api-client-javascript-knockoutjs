/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * HOAuthPermission Map
 * @type {object}
 */
var HOAuthPermissionMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.HOAuthPermission.appId
	 * @type {string}
	 */
	"appId": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - appId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthPermissionModel.appId in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - appId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthPermission.userId
	 * @type {string}
	 */
	"userId": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - userId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthPermissionModel.userId in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - userId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("userId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthPermission.issuedAt
	 * @type {date}
	 */
	"issuedAt": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - issuedAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthPermissionModel.issuedAt in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - issuedAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("issuedAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthPermission.expiresIn
	 * @type {number}
	 */
	"expiresIn": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - expiresIn - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthPermissionModel.expiresIn in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - expiresIn - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiresIn");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthPermission.expiredAt
	 * @type {date}
	 */
	"expiredAt": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - expiredAt - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthPermissionModel.expiredAt in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - expiredAt - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("expiredAt");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.HOAuthPermission.scopes
	 * @type {string}
	 */
	"scopes": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - scopes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - scopes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scopes");
			}
			return options.target;
		}
	},
	/**
	 * Property application of harpoonApi.HOAuthPermission describes a relationship belongsTo harpoonApi.OAuthClientApplication
	 * @type {object}
	 */
	"application": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - application - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthPermissionModel.application in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - application - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("application");
			}
			return options.target;
		}
	},
	/**
	 * Property user of harpoonApi.HOAuthPermission describes a relationship belongsTo harpoonApi.User
	 * @type {object}
	 */
	"user": {
		"create": function(options){
			Console.log("HOAuthPermissionMap - user - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create HOAuthPermissionModel.user in during mapping");
		},
		"update": function(options){
			Console.log("HOAuthPermissionMap - user - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("user");
			}
			return options.target;
		}
	},
};
