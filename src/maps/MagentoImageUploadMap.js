/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * MagentoImageUpload Map
 * @type {object}
 */
var MagentoImageUploadMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.MagentoImageUpload.file
	 * @type {string}
	 */
	"file": {
		"create": function(options){
			Console.log("MagentoImageUploadMap - file - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MagentoImageUploadModel.file in during mapping");
		},
		"update": function(options){
			Console.log("MagentoImageUploadMap - file - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("file");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MagentoImageUpload.contentType
	 * @type {string}
	 */
	"contentType": {
		"create": function(options){
			Console.log("MagentoImageUploadMap - contentType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MagentoImageUploadModel.contentType in during mapping");
		},
		"update": function(options){
			Console.log("MagentoImageUploadMap - contentType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("contentType");
			}
			return options.target;
		}
	},
};
