/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * PlayerConfig Map
 * @type {object}
 */
var PlayerConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.PlayerConfig.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("PlayerConfigMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.id in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS, iOS
	 * @type {boolean}
	 */
	"mute": {
		"create": function(options){
			Console.log("PlayerConfigMap - mute - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.mute in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - mute - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("mute");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS, iOS, Android
	 * @type {boolean}
	 */
	"autostart": {
		"create": function(options){
			Console.log("PlayerConfigMap - autostart - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.autostart in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - autostart - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("autostart");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS, iOS, Android
	 * @type {boolean}
	 */
	"repeat": {
		"create": function(options){
			Console.log("PlayerConfigMap - repeat - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.repeat in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - repeat - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("repeat");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS, iOS, Android
	 * @type {boolean}
	 */
	"controls": {
		"create": function(options){
			Console.log("PlayerConfigMap - controls - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.controls in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - controls - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("controls");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS (as visualplaylist)
	 * @type {boolean}
	 */
	"visualPlaylist": {
		"create": function(options){
			Console.log("PlayerConfigMap - visualPlaylist - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.visualPlaylist in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - visualPlaylist - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("visualPlaylist");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS (as displaytitle)
	 * @type {boolean}
	 */
	"displayTitle": {
		"create": function(options){
			Console.log("PlayerConfigMap - displayTitle - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.displayTitle in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - displayTitle - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("displayTitle");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS (as displaydescription)
	 * @type {boolean}
	 */
	"displayDescription": {
		"create": function(options){
			Console.log("PlayerConfigMap - displayDescription - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.displayDescription in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - displayDescription - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("displayDescription");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS, iOS, Android (as stretch)
	 * @type {string}
	 */
	"stretching": {
		"create": function(options){
			Console.log("PlayerConfigMap - stretching - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.stretching in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - stretching - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("stretching");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS
	 * @type {boolean}
	 */
	"hlshtml": {
		"create": function(options){
			Console.log("PlayerConfigMap - hlshtml - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.hlshtml in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - hlshtml - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("hlshtml");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS
	 * @type {string}
	 */
	"primary": {
		"create": function(options){
			Console.log("PlayerConfigMap - primary - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.primary in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - primary - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("primary");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS (as flashplayer)
	 * @type {string}
	 */
	"flashPlayer": {
		"create": function(options){
			Console.log("PlayerConfigMap - flashPlayer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.flashPlayer in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - flashPlayer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("flashPlayer");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS (as base)
	 * @type {string}
	 */
	"baseSkinPath": {
		"create": function(options){
			Console.log("PlayerConfigMap - baseSkinPath - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.baseSkinPath in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - baseSkinPath - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("baseSkinPath");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS
	 * @type {string}
	 */
	"preload": {
		"create": function(options){
			Console.log("PlayerConfigMap - preload - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.preload in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - preload - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("preload");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS, iOS, Android
	 * @type {object}
	 */
	"playerAdConfig": {
		"create": function(options){
			Console.log("PlayerConfigMap - playerAdConfig - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.playerAdConfig in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - playerAdConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playerAdConfig");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS, iOS, Android
	 * @type {object}
	 */
	"playerCaptionConfig": {
		"create": function(options){
			Console.log("PlayerConfigMap - playerCaptionConfig - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.playerCaptionConfig in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - playerCaptionConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playerCaptionConfig");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin)
	 * @type {string}
	 */
	"skinName": {
		"create": function(options){
			Console.log("PlayerConfigMap - skinName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.skinName in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - skinName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("skinName");
			}
			return options.target;
		}
	},
	/**
	 * Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin)
	 * @type {string}
	 */
	"skinCss": {
		"create": function(options){
			Console.log("PlayerConfigMap - skinCss - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.skinCss in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - skinCss - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("skinCss");
			}
			return options.target;
		}
	},
	/**
	 * Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS
	 * @type {string}
	 */
	"skinActive": {
		"create": function(options){
			Console.log("PlayerConfigMap - skinActive - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.skinActive in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - skinActive - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("skinActive");
			}
			return options.target;
		}
	},
	/**
	 * Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS
	 * @type {string}
	 */
	"skinInactive": {
		"create": function(options){
			Console.log("PlayerConfigMap - skinInactive - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.skinInactive in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - skinInactive - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("skinInactive");
			}
			return options.target;
		}
	},
	/**
	 * Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS
	 * @type {string}
	 */
	"skinBackground": {
		"create": function(options){
			Console.log("PlayerConfigMap - skinBackground - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PlayerConfigModel.skinBackground in during mapping");
		},
		"update": function(options){
			Console.log("PlayerConfigMap - skinBackground - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("skinBackground");
			}
			return options.target;
		}
	},
};
