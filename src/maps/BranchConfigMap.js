/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * BranchConfig Map
 * @type {object}
 */
var BranchConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.BranchConfig.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("BranchConfigMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BranchConfigModel.id in during mapping");
		},
		"update": function(options){
			Console.log("BranchConfigMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.BranchConfig.branchLiveKey
	 * @type {string}
	 */
	"branchLiveKey": {
		"create": function(options){
			Console.log("BranchConfigMap - branchLiveKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BranchConfigModel.branchLiveKey in during mapping");
		},
		"update": function(options){
			Console.log("BranchConfigMap - branchLiveKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("branchLiveKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.BranchConfig.branchTestKey
	 * @type {string}
	 */
	"branchTestKey": {
		"create": function(options){
			Console.log("BranchConfigMap - branchTestKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BranchConfigModel.branchTestKey in during mapping");
		},
		"update": function(options){
			Console.log("BranchConfigMap - branchTestKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("branchTestKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.BranchConfig.branchLiveUrl
	 * @type {string}
	 */
	"branchLiveUrl": {
		"create": function(options){
			Console.log("BranchConfigMap - branchLiveUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BranchConfigModel.branchLiveUrl in during mapping");
		},
		"update": function(options){
			Console.log("BranchConfigMap - branchLiveUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("branchLiveUrl");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.BranchConfig.branchTestUrl
	 * @type {string}
	 */
	"branchTestUrl": {
		"create": function(options){
			Console.log("BranchConfigMap - branchTestUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create BranchConfigModel.branchTestUrl in during mapping");
		},
		"update": function(options){
			Console.log("BranchConfigMap - branchTestUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("branchTestUrl");
			}
			return options.target;
		}
	},
};
