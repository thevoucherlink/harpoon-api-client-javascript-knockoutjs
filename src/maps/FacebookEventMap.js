/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * FacebookEvent Map
 * @type {object}
 */
var FacebookEventMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.FacebookEvent.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("FacebookEventMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.id in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FacebookEvent.node
	 * @type {string}
	 */
	"node": {
		"create": function(options){
			Console.log("FacebookEventMap - node - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.node in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - node - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("node");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FacebookEvent.category
	 * @type {string}
	 */
	"category": {
		"create": function(options){
			Console.log("FacebookEventMap - category - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.category in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - category - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("category");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FacebookEvent.updatedTime
	 * @type {string}
	 */
	"updatedTime": {
		"create": function(options){
			Console.log("FacebookEventMap - updatedTime - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.updatedTime in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - updatedTime - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("updatedTime");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FacebookEvent.maybeCount
	 * @type {number}
	 */
	"maybeCount": {
		"create": function(options){
			Console.log("FacebookEventMap - maybeCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.maybeCount in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - maybeCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("maybeCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FacebookEvent.noreplyCount
	 * @type {number}
	 */
	"noreplyCount": {
		"create": function(options){
			Console.log("FacebookEventMap - noreplyCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.noreplyCount in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - noreplyCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("noreplyCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FacebookEvent.attendingCount
	 * @type {number}
	 */
	"attendingCount": {
		"create": function(options){
			Console.log("FacebookEventMap - attendingCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.attendingCount in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - attendingCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attendingCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.FacebookEvent.place
	 * @type {object}
	 */
	"place": {
		"create": function(options){
			Console.log("FacebookEventMap - place - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create FacebookEventModel.place in during mapping");
		},
		"update": function(options){
			Console.log("FacebookEventMap - place - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("place");
			}
			return options.target;
		}
	},
};
