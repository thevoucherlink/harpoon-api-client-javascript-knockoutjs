/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * MenuItem Map
 * @type {object}
 */
var MenuItemMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.MenuItem.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("MenuItemMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.id in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("MenuItemMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.name in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.url
	 * @type {string}
	 */
	"url": {
		"create": function(options){
			Console.log("MenuItemMap - url - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.url in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - url - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("url");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.image
	 * @type {string}
	 */
	"image": {
		"create": function(options){
			Console.log("MenuItemMap - image - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.image in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - image - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("image");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.visibility
	 * @type {boolean}
	 */
	"visibility": {
		"create": function(options){
			Console.log("MenuItemMap - visibility - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.visibility in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - visibility - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("visibility");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.sortOrder
	 * @type {number}
	 */
	"sortOrder": {
		"create": function(options){
			Console.log("MenuItemMap - sortOrder - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.sortOrder in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - sortOrder - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("sortOrder");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.pageAction
	 * @type {boolean}
	 */
	"pageAction": {
		"create": function(options){
			Console.log("MenuItemMap - pageAction - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.pageAction in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - pageAction - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pageAction");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.pageActionIcon
	 * @type {string}
	 */
	"pageActionIcon": {
		"create": function(options){
			Console.log("MenuItemMap - pageActionIcon - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.pageActionIcon in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - pageActionIcon - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pageActionIcon");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.pageActionLink
	 * @type {string}
	 */
	"pageActionLink": {
		"create": function(options){
			Console.log("MenuItemMap - pageActionLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.pageActionLink in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - pageActionLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("pageActionLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.styleCss
	 * @type {string}
	 */
	"styleCss": {
		"create": function(options){
			Console.log("MenuItemMap - styleCss - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.styleCss in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - styleCss - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("styleCss");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.MenuItem.featureRadioDontShowOnCamera
	 * @type {boolean}
	 */
	"featureRadioDontShowOnCamera": {
		"create": function(options){
			Console.log("MenuItemMap - featureRadioDontShowOnCamera - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create MenuItemModel.featureRadioDontShowOnCamera in during mapping");
		},
		"update": function(options){
			Console.log("MenuItemMap - featureRadioDontShowOnCamera - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("featureRadioDontShowOnCamera");
			}
			return options.target;
		}
	},
};
