/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * App Map
 * @type {object}
 */
var AppMap = {
	// Ignore
	"ignore": ["icon","clientKey","javaScriptKey","restApiKey","windowsKey","masterKey","authenticationEnabled","anonymousAllowed","tokenType","owner","collaborators","email","emailVerified","url","callbackUrls","permissions","pushSettings","authenticationSchemes"],
	// Properties
	/**
	 * harpoonApi.App.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("AppMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * Id used internally in Harpoon
	 * @type {number}
	 */
	"internalId": {
		"create": function(options){
			Console.log("AppMap - internalId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.internalId in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - internalId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("internalId");
			}
			return options.target;
		}
	},
	/**
	 * Vendor App owner
	 * @type {number}
	 */
	"vendorId": {
		"create": function(options){
			Console.log("AppMap - vendorId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.vendorId in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - vendorId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("vendorId");
			}
			return options.target;
		}
	},
	/**
	 * Name of the application
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("AppMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.name in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.category
	 * @type {string}
	 */
	"category": {
		"create": function(options){
			Console.log("AppMap - category - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.category in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - category - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("category");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.description
	 * @type {string}
	 */
	"description": {
		"create": function(options){
			Console.log("AppMap - description - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.description in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - description - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("description");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.keywords
	 * @type {string}
	 */
	"keywords": {
		"create": function(options){
			Console.log("AppMap - keywords - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.keywords in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - keywords - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("keywords");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.copyright
	 * @type {string}
	 */
	"copyright": {
		"create": function(options){
			Console.log("AppMap - copyright - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.copyright in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - copyright - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("copyright");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.realm
	 * @type {string}
	 */
	"realm": {
		"create": function(options){
			Console.log("AppMap - realm - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.realm in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - realm - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("realm");
			}
			return options.target;
		}
	},
	/**
	 * Bundle Identifier of the application, com.{company name}.{project name}
	 * @type {string}
	 */
	"bundle": {
		"create": function(options){
			Console.log("AppMap - bundle - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.bundle in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - bundle - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("bundle");
			}
			return options.target;
		}
	},
	/**
	 * URL Scheme used for deeplinking
	 * @type {string}
	 */
	"urlScheme": {
		"create": function(options){
			Console.log("AppMap - urlScheme - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.urlScheme in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - urlScheme - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("urlScheme");
			}
			return options.target;
		}
	},
	/**
	 * Customer need to follow the Brand to display the Offers outside of the Brand profile page
	 * @type {boolean}
	 */
	"brandFollowOffer": {
		"create": function(options){
			Console.log("AppMap - brandFollowOffer - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.brandFollowOffer in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - brandFollowOffer - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandFollowOffer");
			}
			return options.target;
		}
	},
	/**
	 * URL linking to the Privacy page for the app
	 * @type {string}
	 */
	"privacyUrl": {
		"create": function(options){
			Console.log("AppMap - privacyUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.privacyUrl in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - privacyUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("privacyUrl");
			}
			return options.target;
		}
	},
	/**
	 * URL linking to the Terms of Service page for the app
	 * @type {string}
	 */
	"termsOfServiceUrl": {
		"create": function(options){
			Console.log("AppMap - termsOfServiceUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.termsOfServiceUrl in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - termsOfServiceUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("termsOfServiceUrl");
			}
			return options.target;
		}
	},
	/**
	 * Restrict the use of the app to just the customer with at least the value specified, 0 = no limit
	 * @type {number}
	 */
	"restrictionAge": {
		"create": function(options){
			Console.log("AppMap - restrictionAge - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.restrictionAge in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - restrictionAge - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("restrictionAge");
			}
			return options.target;
		}
	},
	/**
	 * URL linking to the Google Play page for the app
	 * @type {string}
	 */
	"storeAndroidUrl": {
		"create": function(options){
			Console.log("AppMap - storeAndroidUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.storeAndroidUrl in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - storeAndroidUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("storeAndroidUrl");
			}
			return options.target;
		}
	},
	/**
	 * URL linking to the iTunes App Store page for the app
	 * @type {string}
	 */
	"storeIosUrl": {
		"create": function(options){
			Console.log("AppMap - storeIosUrl - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.storeIosUrl in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - storeIosUrl - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("storeIosUrl");
			}
			return options.target;
		}
	},
	/**
	 * News Feed source, nativeFeed/rss
	 * @type {string}
	 */
	"typeNewsFeed": {
		"create": function(options){
			Console.log("AppMap - typeNewsFeed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.typeNewsFeed in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - typeNewsFeed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("typeNewsFeed");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.clientSecret
	 * @type {string}
	 */
	"clientSecret": {
		"create": function(options){
			Console.log("AppMap - clientSecret - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.clientSecret in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - clientSecret - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("clientSecret");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.grantTypes
	 * @type {string}
	 */
	"grantTypes": {
		"create": function(options){
			Console.log("AppMap - grantTypes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppMap - grantTypes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("grantTypes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.scopes
	 * @type {string}
	 */
	"scopes": {
		"create": function(options){
			Console.log("AppMap - scopes - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppMap - scopes - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scopes");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.tokenType
	 * @type {string}
	 */
	"tokenType": {
		"create": function(options){
			Console.log("AppMap - tokenType - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.tokenType in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - tokenType - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("tokenType");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.anonymousAllowed
	 * @type {boolean}
	 */
	"anonymousAllowed": {
		"create": function(options){
			Console.log("AppMap - anonymousAllowed - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.anonymousAllowed in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - anonymousAllowed - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("anonymousAllowed");
			}
			return options.target;
		}
	},
	/**
	 * Status of the application, production/sandbox/disabled
	 * @type {string}
	 */
	"status": {
		"create": function(options){
			Console.log("AppMap - status - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.status in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - status - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("status");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.created
	 * @type {date}
	 */
	"created": {
		"create": function(options){
			Console.log("AppMap - created - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.created in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - created - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("created");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.modified
	 * @type {date}
	 */
	"modified": {
		"create": function(options){
			Console.log("AppMap - modified - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.modified in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - modified - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("modified");
			}
			return options.target;
		}
	},
	/**
	 * Size of the Offer List Item, big/small
	 * @type {string}
	 */
	"styleOfferList": {
		"create": function(options){
			Console.log("AppMap - styleOfferList - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.styleOfferList in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - styleOfferList - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("styleOfferList");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.appConfig
	 * @type {AppConfig}
	 */
	"appConfig": {
		"create": function(options){
			Console.log("AppMap - appConfig - create");
			Console.debug(JSON.stringify(options));
			return options.data;
		},
		"update": function(options){
			Console.log("AppMap - appConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appConfig");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.clientToken
	 * @type {string}
	 */
	"clientToken": {
		"create": function(options){
			Console.log("AppMap - clientToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.clientToken in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - clientToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("clientToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.multiConfigTitle
	 * @type {string}
	 */
	"multiConfigTitle": {
		"create": function(options){
			Console.log("AppMap - multiConfigTitle - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.multiConfigTitle in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - multiConfigTitle - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("multiConfigTitle");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.App.multiConfig
	 * @type {AppConfig}
	 */
	"multiConfig": {
		"create": function(options){
			Console.log("AppMap - multiConfig - create");
			Console.debug(JSON.stringify(options));
			return new AppConfigModel();
		},
		"update": function(options){
			Console.log("AppMap - multiConfig - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("multiConfig");
			}
			return options.target;
		}
	},
	/**
	 * Property rssFeeds of harpoonApi.App describes a relationship hasMany harpoonApi.RssFeed
	 * @type {RssFeed}
	 */
	"rssFeeds": {
		"create": function(options){
			Console.log("AppMap - rssFeeds - create");
			Console.debug(JSON.stringify(options));
			return new RssFeedModel();
		},
		"update": function(options){
			Console.log("AppMap - rssFeeds - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("rssFeeds");
			}
			return options.target;
		}
	},
	/**
	 * Property rssFeedGroups of harpoonApi.App describes a relationship hasMany harpoonApi.RssFeedGroup
	 * @type {RssFeedGroup}
	 */
	"rssFeedGroups": {
		"create": function(options){
			Console.log("AppMap - rssFeedGroups - create");
			Console.debug(JSON.stringify(options));
			return new RssFeedGroupModel();
		},
		"update": function(options){
			Console.log("AppMap - rssFeedGroups - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("rssFeedGroups");
			}
			return options.target;
		}
	},
	/**
	 * Property radioStreams of harpoonApi.App describes a relationship hasMany harpoonApi.RadioStream
	 * @type {RadioStream}
	 */
	"radioStreams": {
		"create": function(options){
			Console.log("AppMap - radioStreams - create");
			Console.debug(JSON.stringify(options));
			return new RadioStreamModel();
		},
		"update": function(options){
			Console.log("AppMap - radioStreams - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("radioStreams");
			}
			return options.target;
		}
	},
	/**
	 * Property customers of harpoonApi.App describes a relationship hasMany harpoonApi.Customer
	 * @type {Customer}
	 */
	"customers": {
		"create": function(options){
			Console.log("AppMap - customers - create");
			Console.debug(JSON.stringify(options));
			return new CustomerModel();
		},
		"update": function(options){
			Console.log("AppMap - customers - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customers");
			}
			return options.target;
		}
	},
	/**
	 * Property appConfigs of harpoonApi.App describes a relationship hasMany harpoonApi.AppConfig
	 * @type {AppConfig}
	 */
	"appConfigs": {
		"create": function(options){
			Console.log("AppMap - appConfigs - create");
			Console.debug(JSON.stringify(options));
			return new AppConfigModel();
		},
		"update": function(options){
			Console.log("AppMap - appConfigs - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appConfigs");
			}
			return options.target;
		}
	},
	/**
	 * Property playlists of harpoonApi.App describes a relationship hasMany harpoonApi.Playlist
	 * @type {Playlist}
	 */
	"playlists": {
		"create": function(options){
			Console.log("AppMap - playlists - create");
			Console.debug(JSON.stringify(options));
			return new PlaylistModel();
		},
		"update": function(options){
			Console.log("AppMap - playlists - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("playlists");
			}
			return options.target;
		}
	},
	/**
	 * Property apps of harpoonApi.App describes a relationship hasMany harpoonApi.App
	 * @type {App}
	 */
	"apps": {
		"create": function(options){
			Console.log("AppMap - apps - create");
			Console.debug(JSON.stringify(options));
			return new AppModel();
		},
		"update": function(options){
			Console.log("AppMap - apps - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				options.target.update(options.data);
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				options.target.update(options.data);
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				options.target.update(options.data);
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				options.target.update(options.data);
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("apps");
			}
			return options.target;
		}
	},
	/**
	 * Property parent of harpoonApi.App describes a relationship belongsTo harpoonApi.App
	 * @type {object}
	 */
	"parent": {
		"create": function(options){
			Console.log("AppMap - parent - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AppModel.parent in during mapping");
		},
		"update": function(options){
			Console.log("AppMap - parent - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("parent");
			}
			return options.target;
		}
	},
};
