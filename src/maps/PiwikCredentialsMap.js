/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * PiwikCredentials Map
 * @type {object}
 */
var PiwikCredentialsMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.PiwikCredentials.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.id in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.PiwikCredentials.apiToken
	 * @type {string}
	 */
	"apiToken": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - apiToken - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.apiToken in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - apiToken - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("apiToken");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.PiwikCredentials.appId
	 * @type {string}
	 */
	"appId": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - appId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.appId in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - appId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("appId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.PiwikCredentials.password
	 * @type {string}
	 */
	"password": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - password - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.password in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - password - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("password");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.PiwikCredentials.username
	 * @type {string}
	 */
	"username": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - username - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.username in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - username - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("username");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.PiwikCredentials.scope
	 * @type {string}
	 */
	"scope": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - scope - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.scope in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - scope - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("scope");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.PiwikCredentials.created
	 * @type {date}
	 */
	"created": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - created - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.created in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - created - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("created");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.PiwikCredentials.modified
	 * @type {date}
	 */
	"modified": {
		"create": function(options){
			Console.log("PiwikCredentialsMap - modified - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create PiwikCredentialsModel.modified in during mapping");
		},
		"update": function(options){
			Console.log("PiwikCredentialsMap - modified - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("modified");
			}
			return options.target;
		}
	},
};
