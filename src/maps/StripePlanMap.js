/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * StripePlan Map
 * @type {object}
 */
var StripePlanMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.StripePlan.amount
	 * @type {string}
	 */
	"amount": {
		"create": function(options){
			Console.log("StripePlanMap - amount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.amount in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - amount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("amount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripePlan.currency
	 * @type {string}
	 */
	"currency": {
		"create": function(options){
			Console.log("StripePlanMap - currency - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.currency in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - currency - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("currency");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripePlan.interval
	 * @type {string}
	 */
	"interval": {
		"create": function(options){
			Console.log("StripePlanMap - interval - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.interval in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - interval - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("interval");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripePlan.intervalCount
	 * @type {number}
	 */
	"intervalCount": {
		"create": function(options){
			Console.log("StripePlanMap - intervalCount - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.intervalCount in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - intervalCount - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("intervalCount");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripePlan.metadata
	 * @type {string}
	 */
	"metadata": {
		"create": function(options){
			Console.log("StripePlanMap - metadata - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.metadata in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - metadata - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("metadata");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripePlan.name
	 * @type {string}
	 */
	"name": {
		"create": function(options){
			Console.log("StripePlanMap - name - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.name in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - name - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("name");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripePlan.statement_descriptor
	 * @type {string}
	 */
	"statement_descriptor": {
		"create": function(options){
			Console.log("StripePlanMap - statement_descriptor - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.statement_descriptor in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - statement_descriptor - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("statement_descriptor");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.StripePlan.trial_period_days
	 * @type {number}
	 */
	"trial_period_days": {
		"create": function(options){
			Console.log("StripePlanMap - trial_period_days - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create StripePlanModel.trial_period_days in during mapping");
		},
		"update": function(options){
			Console.log("StripePlanMap - trial_period_days - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("trial_period_days");
			}
			return options.target;
		}
	},
};
