/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailDashCampaignFlowReviewChanges Map
 * @type {object}
 */
var EmailDashCampaignFlowReviewChangesMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailDashCampaignFlowReviewChanges.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowReviewChangesModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowReviewChanges.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowReviewChangesModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowReviewChanges.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowReviewChangesModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowReviewChanges.reDirectLink
	 * @type {string}
	 */
	"reDirectLink": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - reDirectLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowReviewChangesModel.reDirectLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - reDirectLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("reDirectLink");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowReviewChanges.senderFirstName
	 * @type {string}
	 */
	"senderFirstName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - senderFirstName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowReviewChangesModel.senderFirstName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - senderFirstName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("senderFirstName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowReviewChanges.customMessage
	 * @type {string}
	 */
	"customMessage": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - customMessage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowReviewChangesModel.customMessage in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowReviewChangesMap - customMessage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("customMessage");
			}
			return options.target;
		}
	},
};
