/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AwEventbookingEventTicketAttribute Map
 * @type {object}
 */
var AwEventbookingEventTicketAttributeMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.AwEventbookingEventTicketAttribute.id
	 * @type {Number}
	 */
	"id": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketAttributeModel.id in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicketAttribute.ticketId
	 * @type {Number}
	 */
	"ticketId": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - ticketId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketAttributeModel.ticketId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - ticketId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("ticketId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicketAttribute.storeId
	 * @type {Number}
	 */
	"storeId": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - storeId - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketAttributeModel.storeId in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - storeId - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("storeId");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicketAttribute.attributeCode
	 * @type {String}
	 */
	"attributeCode": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - attributeCode - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketAttributeModel.attributeCode in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - attributeCode - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("attributeCode");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.AwEventbookingEventTicketAttribute.value
	 * @type {String}
	 */
	"value": {
		"create": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - value - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create AwEventbookingEventTicketAttributeModel.value in during mapping");
		},
		"update": function(options){
			Console.log("AwEventbookingEventTicketAttributeMap - value - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("value");
			}
			return options.target;
		}
	},
};
