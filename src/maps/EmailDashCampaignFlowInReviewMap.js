/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * EmailDashCampaignFlowInReview Map
 * @type {object}
 */
var EmailDashCampaignFlowInReviewMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.EmailDashCampaignFlowInReview.brandEmail
	 * @type {string}
	 */
	"brandEmail": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - brandEmail - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowInReviewModel.brandEmail in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - brandEmail - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandEmail");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowInReview.brandName
	 * @type {string}
	 */
	"brandName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - brandName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowInReviewModel.brandName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - brandName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowInReview.brandTel
	 * @type {number}
	 */
	"brandTel": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - brandTel - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowInReviewModel.brandTel in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - brandTel - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("brandTel");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowInReview.campaignImage
	 * @type {string}
	 */
	"campaignImage": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - campaignImage - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowInReviewModel.campaignImage in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - campaignImage - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignImage");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowInReview.campaignName
	 * @type {string}
	 */
	"campaignName": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - campaignName - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowInReviewModel.campaignName in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - campaignName - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("campaignName");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.EmailDashCampaignFlowInReview.reDirectLink
	 * @type {string}
	 */
	"reDirectLink": {
		"create": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - reDirectLink - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create EmailDashCampaignFlowInReviewModel.reDirectLink in during mapping");
		},
		"update": function(options){
			Console.log("EmailDashCampaignFlowInReviewMap - reDirectLink - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("reDirectLink");
			}
			return options.target;
		}
	},
};
