/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * TwitterConfig Map
 * @type {object}
 */
var TwitterConfigMap = {
	// Ignore
	"ignore": [],
	// Properties
	/**
	 * harpoonApi.TwitterConfig.id
	 * @type {string}
	 */
	"id": {
		"create": function(options){
			Console.log("TwitterConfigMap - id - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create TwitterConfigModel.id in during mapping");
		},
		"update": function(options){
			Console.log("TwitterConfigMap - id - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("id");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.TwitterConfig.apiKey
	 * @type {string}
	 */
	"apiKey": {
		"create": function(options){
			Console.log("TwitterConfigMap - apiKey - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create TwitterConfigModel.apiKey in during mapping");
		},
		"update": function(options){
			Console.log("TwitterConfigMap - apiKey - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("apiKey");
			}
			return options.target;
		}
	},
	/**
	 * harpoonApi.TwitterConfig.apiSecret
	 * @type {string}
	 */
	"apiSecret": {
		"create": function(options){
			Console.log("TwitterConfigMap - apiSecret - create");
			Console.debug(JSON.stringify(options));
			throw new Error("Trying to create TwitterConfigModel.apiSecret in during mapping");
		},
		"update": function(options){
			Console.log("TwitterConfigMap - apiSecret - update");
			Console.debug(JSON.stringify(options));
			if(options.data)
			{
				return options.data;
			}
			// String is empty
			else if(typeof options.data === "string" && options.data === "")
			{
				return options.data;
			}
			// Number is 0
			else if(typeof options.data === "number" && options.data === 0)
			{
				return options.data;
			}
			// Boolean is false
			else if(typeof options.data === "boolean" && options.data === false)
			{
				return options.data;
			}
			else if(options.data === null)
			{
				options.parent.setPropertyToDefault("apiSecret");
			}
			return options.target;
		}
	},
};
