/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * UdropshipVendor Model
 * @created 2016-09-16T16:35:56-0300
 * @return  {UdropshipVendorModel}                 Model instance
 */
var UdropshipVendorModel = function(){
	this.constructor;
	/**
	 * Model instance
	 * @type {UdropshipVendorModel}
	 */
	var self = this;
	// Properties
	// Properties - Constant
	/**
	 * UdropshipVendorModel map for Knockout mapping
	 * @type {object}
	 */
	self.__map__ = UdropshipVendorMap;
	/**
	 * Default values for model properties name
	 * @type {object}
	 */
	self.__properties_default_value__ = {"id":null,"vendorName":null,"vendorAttn":null,"email":null,"street":null,"city":null,"zip":null,"countryId":null,"regionId":null,"region":null,"telephone":null,"fax":null,"status":null,"password":null,"passwordHash":null,"passwordEnc":null,"carrierCode":null,"notifyNewOrder":null,"labelType":null,"testMode":null,"handlingFee":null,"upsShipperNumber":null,"customDataCombined":null,"customVarsCombined":null,"emailTemplate":null,"urlKey":null,"randomHash":null,"createdAt":null,"notifyLowstock":null,"notifyLowstockQty":null,"useHandlingFee":null,"useRatesFallback":null,"allowShippingExtraCharge":null,"defaultShippingExtraChargeSuffix":null,"defaultShippingExtraChargeType":null,"defaultShippingExtraCharge":null,"isExtraChargeShippingDefault":null,"defaultShippingId":null,"billingUseShipping":null,"billingEmail":null,"billingTelephone":null,"billingFax":null,"billingVendorAttn":null,"billingStreet":null,"billingCity":null,"billingZip":null,"billingCountryId":null,"billingRegionId":null,"billingRegion":null,"subdomainLevel":null,"updateStoreBaseUrl":null,"confirmation":null,"confirmationSent":null,"rejectReason":null,"backorderByAvailability":null,"useReservedQty":null,"tiercomRates":null,"tiercomFixedRule":null,"tiercomFixedRates":null,"tiercomFixedCalcType":null,"tiershipRates":null,"tiershipSimpleRates":null,"tiershipUseV2Rates":null,"vacationMode":null,"vacationEnd":null,"vacationMessage":null,"udmemberLimitProducts":null,"udmemberProfileId":null,"udmemberProfileRefid":null,"udmemberMembershipCode":null,"udmemberMembershipTitle":null,"udmemberBillingType":null,"udmemberHistory":null,"udmemberProfileSyncOff":null,"udmemberAllowMicrosite":null,"udprodTemplateSku":null,"vendorTaxClass":null,"catalogProducts":[],"vendorLocations":[],"config":null,"partners":[],"udropshipVendorProducts":[],"harpoonHpublicApplicationpartners":[],"harpoonHpublicv12VendorAppCategories":[]};
	/**
	 * List of model properties name used for relationships with other models
	 * @type {array}
	 */
	self.__relational_properties__ = ["catalogProducts","vendorLocations","config","partners","udropshipVendorProducts","harpoonHpublicApplicationpartners","harpoonHpublicv12VendorAppCategories"];
	/**
	 * List of model properties name to be ignored during the export
	 * @type {array}
	 */
	self.__ignored_properties__ = [];
	// Properties - Observable
	/**
	 * harpoonApi.UdropshipVendor.id
	 * @type {Number}
	 */
	self.id = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.vendorName
	 * @type {String}
	 */
	self.vendorName = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.vendorAttn
	 * @type {String}
	 */
	self.vendorAttn = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.email
	 * @type {String}
	 */
	self.email = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.street
	 * @type {String}
	 */
	self.street = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.city
	 * @type {String}
	 */
	self.city = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.zip
	 * @type {String}
	 */
	self.zip = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.countryId
	 * @type {String}
	 */
	self.countryId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.regionId
	 * @type {Number}
	 */
	self.regionId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.region
	 * @type {String}
	 */
	self.region = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.telephone
	 * @type {String}
	 */
	self.telephone = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.fax
	 * @type {String}
	 */
	self.fax = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.status
	 * @type {Boolean}
	 */
	self.status = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.password
	 * @type {String}
	 */
	self.password = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.passwordHash
	 * @type {String}
	 */
	self.passwordHash = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.passwordEnc
	 * @type {String}
	 */
	self.passwordEnc = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.carrierCode
	 * @type {String}
	 */
	self.carrierCode = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.notifyNewOrder
	 * @type {Number}
	 */
	self.notifyNewOrder = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.labelType
	 * @type {String}
	 */
	self.labelType = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.testMode
	 * @type {Number}
	 */
	self.testMode = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.handlingFee
	 * @type {String}
	 */
	self.handlingFee = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.upsShipperNumber
	 * @type {String}
	 */
	self.upsShipperNumber = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.customDataCombined
	 * @type {String}
	 */
	self.customDataCombined = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.customVarsCombined
	 * @type {String}
	 */
	self.customVarsCombined = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.emailTemplate
	 * @type {Number}
	 */
	self.emailTemplate = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.urlKey
	 * @type {String}
	 */
	self.urlKey = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.randomHash
	 * @type {String}
	 */
	self.randomHash = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.createdAt
	 * @type {Date}
	 */
	self.createdAt = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.notifyLowstock
	 * @type {Number}
	 */
	self.notifyLowstock = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.notifyLowstockQty
	 * @type {String}
	 */
	self.notifyLowstockQty = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.useHandlingFee
	 * @type {Number}
	 */
	self.useHandlingFee = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.useRatesFallback
	 * @type {Number}
	 */
	self.useRatesFallback = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.allowShippingExtraCharge
	 * @type {Number}
	 */
	self.allowShippingExtraCharge = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingExtraChargeSuffix
	 * @type {String}
	 */
	self.defaultShippingExtraChargeSuffix = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingExtraChargeType
	 * @type {String}
	 */
	self.defaultShippingExtraChargeType = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingExtraCharge
	 * @type {String}
	 */
	self.defaultShippingExtraCharge = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.isExtraChargeShippingDefault
	 * @type {Number}
	 */
	self.isExtraChargeShippingDefault = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.defaultShippingId
	 * @type {Number}
	 */
	self.defaultShippingId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingUseShipping
	 * @type {Number}
	 */
	self.billingUseShipping = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingEmail
	 * @type {String}
	 */
	self.billingEmail = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingTelephone
	 * @type {String}
	 */
	self.billingTelephone = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingFax
	 * @type {String}
	 */
	self.billingFax = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingVendorAttn
	 * @type {String}
	 */
	self.billingVendorAttn = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingStreet
	 * @type {String}
	 */
	self.billingStreet = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingCity
	 * @type {String}
	 */
	self.billingCity = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingZip
	 * @type {String}
	 */
	self.billingZip = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingCountryId
	 * @type {String}
	 */
	self.billingCountryId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingRegionId
	 * @type {Number}
	 */
	self.billingRegionId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.billingRegion
	 * @type {String}
	 */
	self.billingRegion = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.subdomainLevel
	 * @type {Number}
	 */
	self.subdomainLevel = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.updateStoreBaseUrl
	 * @type {Number}
	 */
	self.updateStoreBaseUrl = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.confirmation
	 * @type {String}
	 */
	self.confirmation = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.confirmationSent
	 * @type {Number}
	 */
	self.confirmationSent = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.rejectReason
	 * @type {String}
	 */
	self.rejectReason = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.backorderByAvailability
	 * @type {Number}
	 */
	self.backorderByAvailability = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.useReservedQty
	 * @type {Number}
	 */
	self.useReservedQty = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.tiercomRates
	 * @type {String}
	 */
	self.tiercomRates = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.tiercomFixedRule
	 * @type {String}
	 */
	self.tiercomFixedRule = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.tiercomFixedRates
	 * @type {String}
	 */
	self.tiercomFixedRates = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.tiercomFixedCalcType
	 * @type {String}
	 */
	self.tiercomFixedCalcType = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.tiershipRates
	 * @type {String}
	 */
	self.tiershipRates = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.tiershipSimpleRates
	 * @type {String}
	 */
	self.tiershipSimpleRates = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.tiershipUseV2Rates
	 * @type {Number}
	 */
	self.tiershipUseV2Rates = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.vacationMode
	 * @type {Number}
	 */
	self.vacationMode = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.vacationEnd
	 * @type {Date}
	 */
	self.vacationEnd = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.vacationMessage
	 * @type {String}
	 */
	self.vacationMessage = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberLimitProducts
	 * @type {Number}
	 */
	self.udmemberLimitProducts = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberProfileId
	 * @type {Number}
	 */
	self.udmemberProfileId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberProfileRefid
	 * @type {String}
	 */
	self.udmemberProfileRefid = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberMembershipCode
	 * @type {String}
	 */
	self.udmemberMembershipCode = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberMembershipTitle
	 * @type {String}
	 */
	self.udmemberMembershipTitle = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberBillingType
	 * @type {String}
	 */
	self.udmemberBillingType = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberHistory
	 * @type {String}
	 */
	self.udmemberHistory = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberProfileSyncOff
	 * @type {Number}
	 */
	self.udmemberProfileSyncOff = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udmemberAllowMicrosite
	 * @type {Number}
	 */
	self.udmemberAllowMicrosite = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.udprodTemplateSku
	 * @type {String}
	 */
	self.udprodTemplateSku = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendor.vendorTaxClass
	 * @type {String}
	 */
	self.vendorTaxClass = ko.observable(null);
	/**
	 * Property catalogProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.CatalogProduct
	 * @type {CatalogProduct}
	 */
	self.catalogProducts = ko.observableArray([]);
	/**
	 * Property vendorLocations of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicBrandvenue
	 * @type {HarpoonHpublicBrandvenue}
	 */
	self.vendorLocations = ko.observableArray([]);
	/**
	 * Property config of harpoonApi.UdropshipVendor describes a relationship hasOne harpoonApi.HarpoonHpublicVendorconfig
	 * @type {HarpoonHpublicVendorconfig}
	 */
	self.config = ko.observable(null);
	/**
	 * Property partners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendor
	 * @type {UdropshipVendor}
	 */
	self.partners = ko.observableArray([]);
	/**
	 * Property udropshipVendorProducts of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.UdropshipVendorProduct
	 * @type {UdropshipVendorProduct}
	 */
	self.udropshipVendorProducts = ko.observableArray([]);
	/**
	 * Property harpoonHpublicApplicationpartners of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicApplicationpartner
	 * @type {HarpoonHpublicApplicationpartner}
	 */
	self.harpoonHpublicApplicationpartners = ko.observableArray([]);
	/**
	 * Property harpoonHpublicv12VendorAppCategories of harpoonApi.UdropshipVendor describes a relationship hasMany harpoonApi.HarpoonHpublicv12VendorAppCategory
	 * @type {HarpoonHpublicv12VendorAppCategory}
	 */
	self.harpoonHpublicv12VendorAppCategories = ko.observableArray([]);
	/**
	 * Update model instance
	 * @created 2016-09-16T16:35:56-0300
	 * @param   {object}                 data The model data object
	 * @return  {undefined}                      This method retuns `undefined`
	 */
	self.update = function(data){
		// Data must be an non-null object
		if(typeof data !== "object" || !data)
		{
			Console.error("Skipped updated because `data` is "+(typeof data));
			return;
		}
		// Update model with map
		ko.mapping.fromJS(data, self.__map__, self);
	};
	/**
	 * Turn model instance into JS object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {object}                 JS object
	 */
	self.getJSObject = function(){
		// Map to JS object using Knockout Mapping plugin
		return JSON.parse(self.getJSONString());
	};
	/**
	 * Turn model instance into JSON object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {string}                 JSON string
	 */
	self.getJSONString = function(){
		return ko.toJSON(self);
	};
	/**
	 * Method called by Knockout on ko.mapping.toJS
	 * @created 2016-09-16T19:44:35-0300
	 * @return  {string}                 JSON string
	 */
	self.toJSON = function(){
		// Get JS object for model instance
		var selfJS = ko.toJS(self);
		// Get list of relational properties for this model instance
		var relationalProperties = self.getRelationalProperties();
		// Get list of properties to ingore
		var ignoredProperties = self.getIgnoredProperties();
		// Check all the properties
		for(var propertyName in selfJS)
		{
			var isRelationalProperty = false;
			var isIgnoredProperty = false;
			// Get property
			var property = selfJS[propertyName];
			// Remove methods
			if(property === null)
			{
				delete selfJS[propertyName];
				Console.warn("Property "+propertyName+" has been removed. Value was null");
				continue;
			}
			// Remove methods
			if(typeof property === "function")
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a function");
				continue;
			}
			// Remove constant properties
			if(propertyName.match(/(__.*__)/ig))
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a constant");
				continue;
			}
			// Check if property is a relational property (base it on property name)
			for(var relationalPropertyIndex in relationalProperties)
			{
				// Get relational property name
				var relationalPropertyName = relationalProperties[relationalPropertyIndex];
				// Remove the relational property if found
				if(propertyName === relationalPropertyName)
				{
					delete selfJS[propertyName];
					isRelationalProperty = true;
					break;
				}
			}
			// Continue in case relational property has been found
			if(isRelationalProperty)
			{
				continue;
			}
			// Check if property is a ignored property (base it on property name)
			for(var ignoredPropertyIndex in ignoredProperties)
			{
				// Get ignored property name
				var ignoredPropertyName = ignoredProperties[ignoredPropertyIndex];
				// Remove the ignored property if found
				if(propertyName === ignoredPropertyName)
				{
					delete selfJS[propertyName];
					isIgnoredProperty = true;
					break;
				}
			}
			// Continue in case ignored property has been found
			if(isIgnoredProperty)
			{
				continue;
			}
		}
		return selfJS;
	};
	/**
	 * Get model properties name used for relationships with other models
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getRelationalProperties = function(){
		var relationalProperties = self.__relational_properties__;
		return relationalProperties;
	};
	/**
	 * Get model properties name to be ignored during the export
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getIgnoredProperties = function(){
		var ignored = self.__ignored_properties__;
		return ignored;
	};
	/**
	 * Get default value for model property name
	 * @created 2016-09-16T16:38:38-0300
	 * @param   {string}                 propertyName Model property name
	 * @return  {mixed}                              The default value for a specific property name
	 */
	self.getPropertyDefaultValue = function(propertyName){
		var defaults = self.__properties_default_value__;
		// Select and return the default value for a specific property name
		return defaults[propertyName];
	};
	/**
	 * Reset property default value
	 * @created 2016-09-16T16:47:46-0300
	 * @param   {string}                 propertyName The model property name
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertyToDefault = function(propertyName){
		// Property must exists
		if(self[propertyName] === undefined)
		{
			Console.error("`propertyName` ("+propertyName+") isn't referring to any property");
			return;
		}
		// Property cannot be a constant property
		if(propertyName.match(/(__.*__)/ig))
		{
			Console.debug("`propertyName` ("+propertyName+") is referring to a constant property");
			return;
		}
		// Set default value handling property an observable property
		if(ko.isObservable(self[propertyName]))
		{
			self[propertyName](self.getPropertyDefaultValue(propertyName));
		}
		// Set default value handling property as a constant property
		else
		{
			// Set default value handling property as a particular model instance
			if(typeof self[propertyName] === "object" && typeof self[propertyName].setPropertiesToDefault === "function")
			{
				self[propertyName].setPropertiesToDefault();
			}
			// Ignore functions
			else if(typeof self[propertyName] !== "function")
			{
				var defaultValue = self.getPropertyDefaultValue(propertyName);
				// Apply just if it's not undefined
				if(defaultValue !== undefined)
				{
					self[propertyName] = defaultValue;
				}
			}
		}
	};
	/**
	 * Reset all properties to default valye
	 * @created 2016-09-16T16:49:59-0300
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertiesToDefault = function(){
		var selfJS = ko.toJS(self);
		for(var propertyName in selfJS)
		{
			self.setPropertyToDefault(propertyName);
		}
	};
	// Initialization
	self.setPropertiesToDefault();
};
