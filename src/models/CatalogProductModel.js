/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CatalogProduct Model
 * @created 2016-09-16T16:35:56-0300
 * @return  {CatalogProductModel}                 Model instance
 */
var CatalogProductModel = function(){
	this.constructor;
	/**
	 * Model instance
	 * @type {CatalogProductModel}
	 */
	var self = this;
	// Properties
	// Properties - Constant
	/**
	 * CatalogProductModel map for Knockout mapping
	 * @type {object}
	 */
	self.__map__ = CatalogProductMap;
	/**
	 * Default values for model properties name
	 * @type {object}
	 */
	self.__properties_default_value__ = {"id":null,"attributeSetId":null,"typeId":null,"actionText":null,"agreementId":null,"alias":null,"altLink":null,"campaignId":null,"campaignLiveFrom":null,"campaignLiveTo":null,"campaignShared":null,"campaignStatus":null,"campaignType":null,"checkoutLink":null,"collectionNotes":null,"cost":null,"couponCodeText":null,"couponCodeType":null,"couponType":null,"createdAt":null,"creatorId":null,"description":null,"externalId":null,"facebookAttendingCount":null,"facebookCategory":null,"facebookId":null,"facebookMaybeCount":null,"facebookNode":null,"facebookNoreplyCount":null,"facebookPlace":null,"facebookUpdatedTime":null,"giftMessageAvailable":null,"hasOptions":null,"image":null,"imageLabel":null,"isRecurring":null,"linksExist":null,"linksPurchasedSeparately":null,"linksTitle":null,"locationLink":null,"msrp":null,"msrpDisplayActualPriceType":null,"msrpEnabled":null,"name":null,"newsFromDate":null,"newsToDate":null,"notificationBadge":null,"notificationInvisible":null,"notificationSilent":null,"partnerId":null,"passedValidation":null,"price":null,"priceText":null,"priceType":null,"priceView":null,"pushwooshIds":null,"pushwooshTokens":null,"recurringProfile":null,"redemptionType":null,"requiredOptions":null,"reviewHtml":null,"shipmentType":null,"shortDescription":null,"sku":null,"skuType":null,"smallImage":null,"smallImageLabel":null,"specialFromDate":null,"specialPrice":null,"specialToDate":null,"taxClassId":null,"thumbnail":null,"thumbnailLabel":null,"udropshipCalculateRates":null,"udropshipVendor":null,"udropshipVendorValue":null,"udtiershipRates":null,"udtiershipUseCustom":null,"unlockTime":null,"updatedAt":null,"urlKey":null,"urlPath":null,"validationReport":null,"venueList":null,"visibility":null,"visibleFrom":null,"visibleTo":null,"weight":null,"weightType":null,"termsConditions":null,"competitionQuestion":null,"competitionAnswer":null,"competitionWinnerType":null,"competitionMultijoinStatus":null,"competitionMultijoinReset":null,"competitionMultijoinCooldown":null,"competitionWinnerEmail":null,"competitionQuestionValidate":null,"connectFacebookId":null,"udropshipVendors":[],"productEventCompetition":null,"inventory":null,"catalogCategories":[],"checkoutAgreement":null,"awCollpurDeal":null,"creator":null,"udropshipVendorProducts":[]};
	/**
	 * List of model properties name used for relationships with other models
	 * @type {array}
	 */
	self.__relational_properties__ = ["udropshipVendors","productEventCompetition","inventory","catalogCategories","checkoutAgreement","awCollpurDeal","creator","udropshipVendorProducts"];
	/**
	 * List of model properties name to be ignored during the export
	 * @type {array}
	 */
	self.__ignored_properties__ = [];
	// Properties - Observable
	/**
	 * harpoonApi.CatalogProduct.id
	 * @type {Number}
	 */
	self.id = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.attributeSetId
	 * @type {Number}
	 */
	self.attributeSetId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.typeId
	 * @type {String}
	 */
	self.typeId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.actionText
	 * @type {String}
	 */
	self.actionText = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.agreementId
	 * @type {Number}
	 */
	self.agreementId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.alias
	 * @type {String}
	 */
	self.alias = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.altLink
	 * @type {String}
	 */
	self.altLink = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.campaignId
	 * @type {String}
	 */
	self.campaignId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.campaignLiveFrom
	 * @type {Date}
	 */
	self.campaignLiveFrom = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.campaignLiveTo
	 * @type {Date}
	 */
	self.campaignLiveTo = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.campaignShared
	 * @type {String}
	 */
	self.campaignShared = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.campaignStatus
	 * @type {String}
	 */
	self.campaignStatus = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.campaignType
	 * @type {String}
	 */
	self.campaignType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.checkoutLink
	 * @type {String}
	 */
	self.checkoutLink = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.collectionNotes
	 * @type {String}
	 */
	self.collectionNotes = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.cost
	 * @type {String}
	 */
	self.cost = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.couponCodeText
	 * @type {String}
	 */
	self.couponCodeText = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.couponCodeType
	 * @type {String}
	 */
	self.couponCodeType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.couponType
	 * @type {String}
	 */
	self.couponType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.createdAt
	 * @type {Date}
	 */
	self.createdAt = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.creatorId
	 * @type {String}
	 */
	self.creatorId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.description
	 * @type {String}
	 */
	self.description = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.externalId
	 * @type {String}
	 */
	self.externalId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookAttendingCount
	 * @type {String}
	 */
	self.facebookAttendingCount = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookCategory
	 * @type {String}
	 */
	self.facebookCategory = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookId
	 * @type {String}
	 */
	self.facebookId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookMaybeCount
	 * @type {String}
	 */
	self.facebookMaybeCount = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookNode
	 * @type {String}
	 */
	self.facebookNode = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookNoreplyCount
	 * @type {String}
	 */
	self.facebookNoreplyCount = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookPlace
	 * @type {String}
	 */
	self.facebookPlace = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.facebookUpdatedTime
	 * @type {Date}
	 */
	self.facebookUpdatedTime = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.giftMessageAvailable
	 * @type {Number}
	 */
	self.giftMessageAvailable = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.hasOptions
	 * @type {Number}
	 */
	self.hasOptions = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.image
	 * @type {String}
	 */
	self.image = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.imageLabel
	 * @type {String}
	 */
	self.imageLabel = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.isRecurring
	 * @type {Number}
	 */
	self.isRecurring = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.linksExist
	 * @type {Number}
	 */
	self.linksExist = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.linksPurchasedSeparately
	 * @type {Number}
	 */
	self.linksPurchasedSeparately = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.linksTitle
	 * @type {String}
	 */
	self.linksTitle = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.locationLink
	 * @type {String}
	 */
	self.locationLink = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.msrp
	 * @type {String}
	 */
	self.msrp = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.msrpDisplayActualPriceType
	 * @type {String}
	 */
	self.msrpDisplayActualPriceType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.msrpEnabled
	 * @type {Number}
	 */
	self.msrpEnabled = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.name
	 * @type {String}
	 */
	self.name = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.newsFromDate
	 * @type {Date}
	 */
	self.newsFromDate = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.newsToDate
	 * @type {Date}
	 */
	self.newsToDate = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.notificationBadge
	 * @type {String}
	 */
	self.notificationBadge = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.notificationInvisible
	 * @type {Number}
	 */
	self.notificationInvisible = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.notificationSilent
	 * @type {Number}
	 */
	self.notificationSilent = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.partnerId
	 * @type {String}
	 */
	self.partnerId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.passedValidation
	 * @type {Number}
	 */
	self.passedValidation = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.price
	 * @type {String}
	 */
	self.price = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.priceText
	 * @type {String}
	 */
	self.priceText = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.priceType
	 * @type {Number}
	 */
	self.priceType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.priceView
	 * @type {Number}
	 */
	self.priceView = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.pushwooshIds
	 * @type {String}
	 */
	self.pushwooshIds = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.pushwooshTokens
	 * @type {String}
	 */
	self.pushwooshTokens = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.recurringProfile
	 * @type {String}
	 */
	self.recurringProfile = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.redemptionType
	 * @type {String}
	 */
	self.redemptionType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.requiredOptions
	 * @type {Number}
	 */
	self.requiredOptions = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.reviewHtml
	 * @type {String}
	 */
	self.reviewHtml = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.shipmentType
	 * @type {Number}
	 */
	self.shipmentType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.shortDescription
	 * @type {String}
	 */
	self.shortDescription = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.sku
	 * @type {String}
	 */
	self.sku = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.skuType
	 * @type {Number}
	 */
	self.skuType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.smallImage
	 * @type {String}
	 */
	self.smallImage = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.smallImageLabel
	 * @type {String}
	 */
	self.smallImageLabel = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.specialFromDate
	 * @type {Date}
	 */
	self.specialFromDate = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.specialPrice
	 * @type {String}
	 */
	self.specialPrice = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.specialToDate
	 * @type {Date}
	 */
	self.specialToDate = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.taxClassId
	 * @type {Number}
	 */
	self.taxClassId = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.thumbnail
	 * @type {String}
	 */
	self.thumbnail = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.thumbnailLabel
	 * @type {String}
	 */
	self.thumbnailLabel = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.udropshipCalculateRates
	 * @type {Number}
	 */
	self.udropshipCalculateRates = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.udropshipVendor
	 * @type {Number}
	 */
	self.udropshipVendor = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.udropshipVendorValue
	 * @type {String}
	 */
	self.udropshipVendorValue = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.udtiershipRates
	 * @type {String}
	 */
	self.udtiershipRates = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.udtiershipUseCustom
	 * @type {Number}
	 */
	self.udtiershipUseCustom = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.unlockTime
	 * @type {Number}
	 */
	self.unlockTime = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.updatedAt
	 * @type {Date}
	 */
	self.updatedAt = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.urlKey
	 * @type {String}
	 */
	self.urlKey = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.urlPath
	 * @type {String}
	 */
	self.urlPath = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.validationReport
	 * @type {String}
	 */
	self.validationReport = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.venueList
	 * @type {String}
	 */
	self.venueList = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.visibility
	 * @type {Number}
	 */
	self.visibility = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.visibleFrom
	 * @type {Date}
	 */
	self.visibleFrom = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.visibleTo
	 * @type {Date}
	 */
	self.visibleTo = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.weight
	 * @type {String}
	 */
	self.weight = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.weightType
	 * @type {Number}
	 */
	self.weightType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.termsConditions
	 * @type {String}
	 */
	self.termsConditions = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionQuestion
	 * @type {String}
	 */
	self.competitionQuestion = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionAnswer
	 * @type {String}
	 */
	self.competitionAnswer = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionWinnerType
	 * @type {String}
	 */
	self.competitionWinnerType = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionMultijoinStatus
	 * @type {Boolean}
	 */
	self.competitionMultijoinStatus = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionMultijoinReset
	 * @type {Date}
	 */
	self.competitionMultijoinReset = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionMultijoinCooldown
	 * @type {String}
	 */
	self.competitionMultijoinCooldown = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionWinnerEmail
	 * @type {Boolean}
	 */
	self.competitionWinnerEmail = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.competitionQuestionValidate
	 * @type {Boolean}
	 */
	self.competitionQuestionValidate = ko.observable(null);
	/**
	 * harpoonApi.CatalogProduct.connectFacebookId
	 * @type {String}
	 */
	self.connectFacebookId = ko.observable(null);
	/**
	 * Property udropshipVendors of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.UdropshipVendor
	 * @type {UdropshipVendor}
	 */
	self.udropshipVendors = ko.observableArray([]);
	/**
	 * Property productEventCompetition of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.AwEventbookingEvent
	 * @type {AwEventbookingEvent}
	 */
	self.productEventCompetition = ko.observable(null);
	/**
	 * Property inventory of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.CatalogInventoryStockItem
	 * @type {CatalogInventoryStockItem}
	 */
	self.inventory = ko.observable(null);
	/**
	 * Property catalogCategories of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.CatalogCategory
	 * @type {CatalogCategory}
	 */
	self.catalogCategories = ko.observableArray([]);
	/**
	 * Property checkoutAgreement of harpoonApi.CatalogProduct describes a relationship belongsTo harpoonApi.CheckoutAgreement
	 * @type {object}
	 */
	self.checkoutAgreement = ko.observable(null);
	/**
	 * Property awCollpurDeal of harpoonApi.CatalogProduct describes a relationship hasOne harpoonApi.AwCollpurDeal
	 * @type {AwCollpurDeal}
	 */
	self.awCollpurDeal = ko.observable(null);
	/**
	 * Property creator of harpoonApi.CatalogProduct describes a relationship belongsTo harpoonApi.UdropshipVendor
	 * @type {object}
	 */
	self.creator = ko.observable(null);
	/**
	 * Property udropshipVendorProducts of harpoonApi.CatalogProduct describes a relationship hasMany harpoonApi.UdropshipVendorProduct
	 * @type {UdropshipVendorProduct}
	 */
	self.udropshipVendorProducts = ko.observableArray([]);
	/**
	 * Update model instance
	 * @created 2016-09-16T16:35:56-0300
	 * @param   {object}                 data The model data object
	 * @return  {undefined}                      This method retuns `undefined`
	 */
	self.update = function(data){
		// Data must be an non-null object
		if(typeof data !== "object" || !data)
		{
			Console.error("Skipped updated because `data` is "+(typeof data));
			return;
		}
		// Update model with map
		ko.mapping.fromJS(data, self.__map__, self);
	};
	/**
	 * Turn model instance into JS object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {object}                 JS object
	 */
	self.getJSObject = function(){
		// Map to JS object using Knockout Mapping plugin
		return JSON.parse(self.getJSONString());
	};
	/**
	 * Turn model instance into JSON object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {string}                 JSON string
	 */
	self.getJSONString = function(){
		return ko.toJSON(self);
	};
	/**
	 * Method called by Knockout on ko.mapping.toJS
	 * @created 2016-09-16T19:44:35-0300
	 * @return  {string}                 JSON string
	 */
	self.toJSON = function(){
		// Get JS object for model instance
		var selfJS = ko.toJS(self);
		// Get list of relational properties for this model instance
		var relationalProperties = self.getRelationalProperties();
		// Get list of properties to ingore
		var ignoredProperties = self.getIgnoredProperties();
		// Check all the properties
		for(var propertyName in selfJS)
		{
			var isRelationalProperty = false;
			var isIgnoredProperty = false;
			// Get property
			var property = selfJS[propertyName];
			// Remove methods
			if(property === null)
			{
				delete selfJS[propertyName];
				Console.warn("Property "+propertyName+" has been removed. Value was null");
				continue;
			}
			// Remove methods
			if(typeof property === "function")
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a function");
				continue;
			}
			// Remove constant properties
			if(propertyName.match(/(__.*__)/ig))
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a constant");
				continue;
			}
			// Check if property is a relational property (base it on property name)
			for(var relationalPropertyIndex in relationalProperties)
			{
				// Get relational property name
				var relationalPropertyName = relationalProperties[relationalPropertyIndex];
				// Remove the relational property if found
				if(propertyName === relationalPropertyName)
				{
					delete selfJS[propertyName];
					isRelationalProperty = true;
					break;
				}
			}
			// Continue in case relational property has been found
			if(isRelationalProperty)
			{
				continue;
			}
			// Check if property is a ignored property (base it on property name)
			for(var ignoredPropertyIndex in ignoredProperties)
			{
				// Get ignored property name
				var ignoredPropertyName = ignoredProperties[ignoredPropertyIndex];
				// Remove the ignored property if found
				if(propertyName === ignoredPropertyName)
				{
					delete selfJS[propertyName];
					isIgnoredProperty = true;
					break;
				}
			}
			// Continue in case ignored property has been found
			if(isIgnoredProperty)
			{
				continue;
			}
		}
		return selfJS;
	};
	/**
	 * Get model properties name used for relationships with other models
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getRelationalProperties = function(){
		var relationalProperties = self.__relational_properties__;
		return relationalProperties;
	};
	/**
	 * Get model properties name to be ignored during the export
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getIgnoredProperties = function(){
		var ignored = self.__ignored_properties__;
		return ignored;
	};
	/**
	 * Get default value for model property name
	 * @created 2016-09-16T16:38:38-0300
	 * @param   {string}                 propertyName Model property name
	 * @return  {mixed}                              The default value for a specific property name
	 */
	self.getPropertyDefaultValue = function(propertyName){
		var defaults = self.__properties_default_value__;
		// Select and return the default value for a specific property name
		return defaults[propertyName];
	};
	/**
	 * Reset property default value
	 * @created 2016-09-16T16:47:46-0300
	 * @param   {string}                 propertyName The model property name
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertyToDefault = function(propertyName){
		// Property must exists
		if(self[propertyName] === undefined)
		{
			Console.error("`propertyName` ("+propertyName+") isn't referring to any property");
			return;
		}
		// Property cannot be a constant property
		if(propertyName.match(/(__.*__)/ig))
		{
			Console.debug("`propertyName` ("+propertyName+") is referring to a constant property");
			return;
		}
		// Set default value handling property an observable property
		if(ko.isObservable(self[propertyName]))
		{
			self[propertyName](self.getPropertyDefaultValue(propertyName));
		}
		// Set default value handling property as a constant property
		else
		{
			// Set default value handling property as a particular model instance
			if(typeof self[propertyName] === "object" && typeof self[propertyName].setPropertiesToDefault === "function")
			{
				self[propertyName].setPropertiesToDefault();
			}
			// Ignore functions
			else if(typeof self[propertyName] !== "function")
			{
				var defaultValue = self.getPropertyDefaultValue(propertyName);
				// Apply just if it's not undefined
				if(defaultValue !== undefined)
				{
					self[propertyName] = defaultValue;
				}
			}
		}
	};
	/**
	 * Reset all properties to default valye
	 * @created 2016-09-16T16:49:59-0300
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertiesToDefault = function(){
		var selfJS = ko.toJS(self);
		for(var propertyName in selfJS)
		{
			self.setPropertyToDefault(propertyName);
		}
	};
	// Initialization
	self.setPropertiesToDefault();
};
