/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * CatalogCategory Model
 * @created 2016-09-16T16:35:56-0300
 * @return  {CatalogCategoryModel}                 Model instance
 */
var CatalogCategoryModel = function(){
	this.constructor;
	/**
	 * Model instance
	 * @type {CatalogCategoryModel}
	 */
	var self = this;
	// Properties
	// Properties - Constant
	/**
	 * CatalogCategoryModel map for Knockout mapping
	 * @type {object}
	 */
	self.__map__ = CatalogCategoryMap;
	/**
	 * Default values for model properties name
	 * @type {object}
	 */
	self.__properties_default_value__ = {"id":null,"parentId":null,"createdAt":null,"updatedAt":null,"path":null,"position":null,"level":null,"childrenCount":null,"storeId":null,"allChildren":null,"availableSortBy":null,"children":null,"customApplyToProducts":null,"customDesign":null,"customDesignFrom":null,"customDesignTo":null,"customLayoutUpdate":null,"customUseParentSettings":null,"defaultSortBy":null,"description":null,"displayMode":null,"filterPriceRange":null,"image":null,"includeInMenu":null,"isActive":null,"isAnchor":null,"landingPage":null,"metaDescription":null,"metaKeywords":null,"metaTitle":null,"name":null,"pageLayout":null,"pathInStore":null,"thumbnail":null,"ummCatLabel":null,"ummCatTarget":null,"ummDdBlocks":null,"ummDdColumns":null,"ummDdProportions":null,"ummDdType":null,"ummDdWidth":null,"urlKey":null,"urlPath":null,"catalogProducts":[]};
	/**
	 * List of model properties name used for relationships with other models
	 * @type {array}
	 */
	self.__relational_properties__ = ["catalogProducts"];
	/**
	 * List of model properties name to be ignored during the export
	 * @type {array}
	 */
	self.__ignored_properties__ = [];
	// Properties - Observable
	/**
	 * harpoonApi.CatalogCategory.id
	 * @type {Number}
	 */
	self.id = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.parentId
	 * @type {Number}
	 */
	self.parentId = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.createdAt
	 * @type {Date}
	 */
	self.createdAt = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.updatedAt
	 * @type {Date}
	 */
	self.updatedAt = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.path
	 * @type {String}
	 */
	self.path = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.position
	 * @type {Number}
	 */
	self.position = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.level
	 * @type {Number}
	 */
	self.level = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.childrenCount
	 * @type {Number}
	 */
	self.childrenCount = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.storeId
	 * @type {Number}
	 */
	self.storeId = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.allChildren
	 * @type {String}
	 */
	self.allChildren = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.availableSortBy
	 * @type {String}
	 */
	self.availableSortBy = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.children
	 * @type {String}
	 */
	self.children = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.customApplyToProducts
	 * @type {Number}
	 */
	self.customApplyToProducts = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.customDesign
	 * @type {String}
	 */
	self.customDesign = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.customDesignFrom
	 * @type {Date}
	 */
	self.customDesignFrom = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.customDesignTo
	 * @type {Date}
	 */
	self.customDesignTo = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.customLayoutUpdate
	 * @type {String}
	 */
	self.customLayoutUpdate = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.customUseParentSettings
	 * @type {Number}
	 */
	self.customUseParentSettings = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.defaultSortBy
	 * @type {String}
	 */
	self.defaultSortBy = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.description
	 * @type {String}
	 */
	self.description = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.displayMode
	 * @type {String}
	 */
	self.displayMode = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.filterPriceRange
	 * @type {String}
	 */
	self.filterPriceRange = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.image
	 * @type {String}
	 */
	self.image = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.includeInMenu
	 * @type {Number}
	 */
	self.includeInMenu = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.isActive
	 * @type {Number}
	 */
	self.isActive = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.isAnchor
	 * @type {Number}
	 */
	self.isAnchor = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.landingPage
	 * @type {Number}
	 */
	self.landingPage = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.metaDescription
	 * @type {String}
	 */
	self.metaDescription = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.metaKeywords
	 * @type {String}
	 */
	self.metaKeywords = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.metaTitle
	 * @type {String}
	 */
	self.metaTitle = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.name
	 * @type {String}
	 */
	self.name = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.pageLayout
	 * @type {String}
	 */
	self.pageLayout = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.pathInStore
	 * @type {String}
	 */
	self.pathInStore = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.thumbnail
	 * @type {String}
	 */
	self.thumbnail = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.ummCatLabel
	 * @type {String}
	 */
	self.ummCatLabel = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.ummCatTarget
	 * @type {String}
	 */
	self.ummCatTarget = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.ummDdBlocks
	 * @type {String}
	 */
	self.ummDdBlocks = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.ummDdColumns
	 * @type {Number}
	 */
	self.ummDdColumns = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.ummDdProportions
	 * @type {String}
	 */
	self.ummDdProportions = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.ummDdType
	 * @type {String}
	 */
	self.ummDdType = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.ummDdWidth
	 * @type {String}
	 */
	self.ummDdWidth = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.urlKey
	 * @type {String}
	 */
	self.urlKey = ko.observable(null);
	/**
	 * harpoonApi.CatalogCategory.urlPath
	 * @type {String}
	 */
	self.urlPath = ko.observable(null);
	/**
	 * Property catalogProducts of harpoonApi.CatalogCategory describes a relationship hasMany harpoonApi.CatalogProduct
	 * @type {CatalogProduct}
	 */
	self.catalogProducts = ko.observableArray([]);
	/**
	 * Update model instance
	 * @created 2016-09-16T16:35:56-0300
	 * @param   {object}                 data The model data object
	 * @return  {undefined}                      This method retuns `undefined`
	 */
	self.update = function(data){
		// Data must be an non-null object
		if(typeof data !== "object" || !data)
		{
			Console.error("Skipped updated because `data` is "+(typeof data));
			return;
		}
		// Update model with map
		ko.mapping.fromJS(data, self.__map__, self);
	};
	/**
	 * Turn model instance into JS object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {object}                 JS object
	 */
	self.getJSObject = function(){
		// Map to JS object using Knockout Mapping plugin
		return JSON.parse(self.getJSONString());
	};
	/**
	 * Turn model instance into JSON object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {string}                 JSON string
	 */
	self.getJSONString = function(){
		return ko.toJSON(self);
	};
	/**
	 * Method called by Knockout on ko.mapping.toJS
	 * @created 2016-09-16T19:44:35-0300
	 * @return  {string}                 JSON string
	 */
	self.toJSON = function(){
		// Get JS object for model instance
		var selfJS = ko.toJS(self);
		// Get list of relational properties for this model instance
		var relationalProperties = self.getRelationalProperties();
		// Get list of properties to ingore
		var ignoredProperties = self.getIgnoredProperties();
		// Check all the properties
		for(var propertyName in selfJS)
		{
			var isRelationalProperty = false;
			var isIgnoredProperty = false;
			// Get property
			var property = selfJS[propertyName];
			// Remove methods
			if(property === null)
			{
				delete selfJS[propertyName];
				Console.warn("Property "+propertyName+" has been removed. Value was null");
				continue;
			}
			// Remove methods
			if(typeof property === "function")
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a function");
				continue;
			}
			// Remove constant properties
			if(propertyName.match(/(__.*__)/ig))
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a constant");
				continue;
			}
			// Check if property is a relational property (base it on property name)
			for(var relationalPropertyIndex in relationalProperties)
			{
				// Get relational property name
				var relationalPropertyName = relationalProperties[relationalPropertyIndex];
				// Remove the relational property if found
				if(propertyName === relationalPropertyName)
				{
					delete selfJS[propertyName];
					isRelationalProperty = true;
					break;
				}
			}
			// Continue in case relational property has been found
			if(isRelationalProperty)
			{
				continue;
			}
			// Check if property is a ignored property (base it on property name)
			for(var ignoredPropertyIndex in ignoredProperties)
			{
				// Get ignored property name
				var ignoredPropertyName = ignoredProperties[ignoredPropertyIndex];
				// Remove the ignored property if found
				if(propertyName === ignoredPropertyName)
				{
					delete selfJS[propertyName];
					isIgnoredProperty = true;
					break;
				}
			}
			// Continue in case ignored property has been found
			if(isIgnoredProperty)
			{
				continue;
			}
		}
		return selfJS;
	};
	/**
	 * Get model properties name used for relationships with other models
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getRelationalProperties = function(){
		var relationalProperties = self.__relational_properties__;
		return relationalProperties;
	};
	/**
	 * Get model properties name to be ignored during the export
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getIgnoredProperties = function(){
		var ignored = self.__ignored_properties__;
		return ignored;
	};
	/**
	 * Get default value for model property name
	 * @created 2016-09-16T16:38:38-0300
	 * @param   {string}                 propertyName Model property name
	 * @return  {mixed}                              The default value for a specific property name
	 */
	self.getPropertyDefaultValue = function(propertyName){
		var defaults = self.__properties_default_value__;
		// Select and return the default value for a specific property name
		return defaults[propertyName];
	};
	/**
	 * Reset property default value
	 * @created 2016-09-16T16:47:46-0300
	 * @param   {string}                 propertyName The model property name
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertyToDefault = function(propertyName){
		// Property must exists
		if(self[propertyName] === undefined)
		{
			Console.error("`propertyName` ("+propertyName+") isn't referring to any property");
			return;
		}
		// Property cannot be a constant property
		if(propertyName.match(/(__.*__)/ig))
		{
			Console.debug("`propertyName` ("+propertyName+") is referring to a constant property");
			return;
		}
		// Set default value handling property an observable property
		if(ko.isObservable(self[propertyName]))
		{
			self[propertyName](self.getPropertyDefaultValue(propertyName));
		}
		// Set default value handling property as a constant property
		else
		{
			// Set default value handling property as a particular model instance
			if(typeof self[propertyName] === "object" && typeof self[propertyName].setPropertiesToDefault === "function")
			{
				self[propertyName].setPropertiesToDefault();
			}
			// Ignore functions
			else if(typeof self[propertyName] !== "function")
			{
				var defaultValue = self.getPropertyDefaultValue(propertyName);
				// Apply just if it's not undefined
				if(defaultValue !== undefined)
				{
					self[propertyName] = defaultValue;
				}
			}
		}
	};
	/**
	 * Reset all properties to default valye
	 * @created 2016-09-16T16:49:59-0300
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertiesToDefault = function(){
		var selfJS = ko.toJS(self);
		for(var propertyName in selfJS)
		{
			self.setPropertyToDefault(propertyName);
		}
	};
	// Initialization
	self.setPropertiesToDefault();
};
