/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * AppConfig Model
 * @created 2016-09-16T16:35:56-0300
 * @return  {AppConfigModel}                 Model instance
 */
var AppConfigModel = function(){
	this.constructor;
	/**
	 * Model instance
	 * @type {AppConfigModel}
	 */
	var self = this;
	// Properties
	// Properties - Constant
	/**
	 * AppConfigModel map for Knockout mapping
	 * @type {object}
	 */
	self.__map__ = AppConfigMap;
	/**
	 * Default values for model properties name
	 * @type {object}
	 */
	self.__properties_default_value__ = {"imgAppLogo":"","imgSplashScreen":"","imgBrandLogo":"","imgNavBarLogo":"","imgBrandPlaceholder":"","imgMenuBackground":"","featureRadio":false,"featureRadioAutoPlay":false,"featureNews":true,"featureEvents":true,"featureEventsCategorized":true,"featureEventsAttendeeList":true,"featureCommunity":true,"featureCommunityCategorized":true,"featureCompetitions":true,"featureCompetitionsAttendeeList":true,"featureOffers":true,"featureOffersCategorized":true,"featureBrandFollow":true,"featureBrandFollowerList":true,"featureWordpressConnect":false,"featureLogin":true,"featureHarpoonLogin":false,"featureFacebookLogin":true,"featurePhoneVerification":true,"featurePlayer":false,"featurePlayerMainButton":false,"featureGoogleAnalytics":false,"playerMainButtonTitle":"Main","playerConfig":null,"menu":[],"color":"black","colors":{"appPrimary":"#027fd3","appSecondary":"#26ff00","textPrimary":"#049975","radioNavBar":"#87878c","radioPlayer":"#af2768","radioProgressBar":"#ff00c9","feedSegmentIndicator":"#ff0bc6","textSecondary":"#4e5eea","appGradientPrimary":"#000000","radioPauseButton":"#000000","radioPlayButton":"#000000","menuItem":"#000000","radioPlayerItem":"#000000","radioPauseButtonBackground":"#000000","radioPlayButtonBackground":"#000000","radioTextPrimary":"#000000","radioTextSecondary":"#000000","menuBackground":"#ffffff"},"googleAppId":"","iOSAppId":"","googleAnalyticsTracking":null,"loginExternal":null,"rssWebViewCss":"","ads":[],"iOSAds":[],"androidAds":[],"pushNotificationProvider":"none","batchConfig":null,"urbanAirshipConfig":null,"pushWooshConfig":null,"facebookConfig":null,"twitterConfig":null,"rssTags":null,"connectTo":null,"iOSVersion":"1.0.0","iOSBuild":"1","iOSGoogleServices":"","iOSTeamId":"8LXTLH6AD9","iOSAppleId":"dev@harpoonconnect.com","iOSPushNotificationPrivateKey":"","iOSPushNotificationCertificate":"","iOSPushNotificationPem":"","iOSPushNotificationPassword":"","iOSReleaseNotes":"","androidBuild":"1","androidVersion":"1.0.0","androidGoogleServices":"","androidGoogleApiKey":"","androidGoogleCloudMessagingSenderId":"","androidKey":"","androidKeystore":"","androidKeystoreConfig":null,"androidApk":"","customerDetailsForm":[],"contact":null,"imgSponsorMenu":"","imgSponsorSplash":"","audioSponsorPreRollAd":"","name":"","isChildConfig":false,"typeNewsFocusSource":"rssContent","sponsorSplashImgDisplayTime":0,"radioSessionInterval":0,"featureRadioSession":false,"wordpressShareUrlStub":"","iosUriScheme":"","androidUriScheme":"","branchConfig":null,"defaultRadioStreamId":0,"featureTritonPlayer":false,"app":null};
	/**
	 * List of model properties name used for relationships with other models
	 * @type {array}
	 */
	self.__relational_properties__ = ["app"];
	/**
	 * List of model properties name to be ignored during the export
	 * @type {array}
	 */
	self.__ignored_properties__ = [];
	// Properties - Observable
	/**
	 * harpoonApi.AppConfig.id
	 * @type {string}
	 */
	self.id = ko.observable(null);
	/**
	 * App logo
	 * @type {string}
	 */
	self.imgAppLogo = ko.observable(null);
	/**
	 * App splash-screen
	 * @type {string}
	 */
	self.imgSplashScreen = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.imgBrandLogo
	 * @type {string}
	 */
	self.imgBrandLogo = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.imgNavBarLogo
	 * @type {string}
	 */
	self.imgNavBarLogo = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.imgBrandPlaceholder
	 * @type {string}
	 */
	self.imgBrandPlaceholder = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.imgMenuBackground
	 * @type {string}
	 */
	self.imgMenuBackground = ko.observable(null);
	/**
	 * Enable or disable the Radio feature within the app
	 * @type {boolean}
	 */
	self.featureRadio = ko.observable(null);
	/**
	 * Enable or disable the Radio autoplay feature within the app
	 * @type {boolean}
	 */
	self.featureRadioAutoPlay = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureNews
	 * @type {boolean}
	 */
	self.featureNews = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureEvents
	 * @type {boolean}
	 */
	self.featureEvents = ko.observable(null);
	/**
	 * If true the Events list is categorised
	 * @type {boolean}
	 */
	self.featureEventsCategorized = ko.observable(null);
	/**
	 * If true the user will have the possibility to see Events' attendees
	 * @type {boolean}
	 */
	self.featureEventsAttendeeList = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureCommunity
	 * @type {boolean}
	 */
	self.featureCommunity = ko.observable(null);
	/**
	 * If true the Brand list is categories
	 * @type {boolean}
	 */
	self.featureCommunityCategorized = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureCompetitions
	 * @type {boolean}
	 */
	self.featureCompetitions = ko.observable(null);
	/**
	 * If true the user will have the possibility to see Competitions' attendees
	 * @type {boolean}
	 */
	self.featureCompetitionsAttendeeList = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureOffers
	 * @type {boolean}
	 */
	self.featureOffers = ko.observable(null);
	/**
	 * If true the Offer list is categories
	 * @type {boolean}
	 */
	self.featureOffersCategorized = ko.observable(null);
	/**
	 * If true the user will have the possibility to follow or unfollow Brands
	 * @type {boolean}
	 */
	self.featureBrandFollow = ko.observable(null);
	/**
	 * If true the user will have the possibility to see Brands' followers
	 * @type {boolean}
	 */
	self.featureBrandFollowerList = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureWordpressConnect
	 * @type {boolean}
	 */
	self.featureWordpressConnect = ko.observable(null);
	/**
	 * If true the user will be asked to login before getting access to the app
	 * @type {boolean}
	 */
	self.featureLogin = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureHarpoonLogin
	 * @type {boolean}
	 */
	self.featureHarpoonLogin = ko.observable(null);
	/**
	 * If true the user will have the possibility to login with Facebook
	 * @type {boolean}
	 */
	self.featureFacebookLogin = ko.observable(null);
	/**
	 * If true the user will need to verify its account using a phone number
	 * @type {boolean}
	 */
	self.featurePhoneVerification = ko.observable(null);
	/**
	 * Show / Hide media player
	 * @type {boolean}
	 */
	self.featurePlayer = ko.observable(null);
	/**
	 * Show / Hide main button
	 * @type {boolean}
	 */
	self.featurePlayerMainButton = ko.observable(null);
	/**
	 * If true the user actions will be tracked while using the app
	 * @type {boolean}
	 */
	self.featureGoogleAnalytics = ko.observable(null);
	/**
	 * Media Player Main Button title
	 * @type {string}
	 */
	self.playerMainButtonTitle = ko.observable(null);
	/**
	 * Media Player config
	 * @type {PlayerConfig}
	 */
	self.playerConfig = new PlayerConfigModel();
	/**
	 * harpoonApi.AppConfig.menu
	 * @type {MenuItem}
	 */
	self.menu = ko.observableArray([]);
	/**
	 * harpoonApi.AppConfig.color
	 * @type {string}
	 */
	self.color = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.colors
	 * @type {AppColor}
	 */
	self.colors = new AppColorModel();
	/**
	 * harpoonApi.AppConfig.googleAppId
	 * @type {string}
	 */
	self.googleAppId = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSAppId
	 * @type {string}
	 */
	self.iOSAppId = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.googleAnalyticsTracking
	 * @type {GoogleAnalyticsTracking}
	 */
	self.googleAnalyticsTracking = new GoogleAnalyticsTrackingModel();
	/**
	 * harpoonApi.AppConfig.loginExternal
	 * @type {LoginExternal}
	 */
	self.loginExternal = new LoginExternalModel();
	/**
	 * harpoonApi.AppConfig.rssWebViewCss
	 * @type {string}
	 */
	self.rssWebViewCss = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.ads
	 * @type {AppAd}
	 */
	self.ads = ko.observableArray([]);
	/**
	 * harpoonApi.AppConfig.iOSAds
	 * @type {AppAd}
	 */
	self.iOSAds = ko.observableArray([]);
	/**
	 * harpoonApi.AppConfig.androidAds
	 * @type {AppAd}
	 */
	self.androidAds = ko.observableArray([]);
	/**
	 * harpoonApi.AppConfig.pushNotificationProvider
	 * @type {string}
	 */
	self.pushNotificationProvider = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.batchConfig
	 * @type {BatchConfig}
	 */
	self.batchConfig = new BatchConfigModel();
	/**
	 * harpoonApi.AppConfig.urbanAirshipConfig
	 * @type {UrbanAirshipConfig}
	 */
	self.urbanAirshipConfig = new UrbanAirshipConfigModel();
	/**
	 * harpoonApi.AppConfig.pushWooshConfig
	 * @type {PushWooshConfig}
	 */
	self.pushWooshConfig = new PushWooshConfigModel();
	/**
	 * harpoonApi.AppConfig.facebookConfig
	 * @type {FacebookConfig}
	 */
	self.facebookConfig = new FacebookConfigModel();
	/**
	 * harpoonApi.AppConfig.twitterConfig
	 * @type {TwitterConfig}
	 */
	self.twitterConfig = new TwitterConfigModel();
	/**
	 * harpoonApi.AppConfig.rssTags
	 * @type {RssTags}
	 */
	self.rssTags = new RssTagsModel();
	/**
	 * harpoonApi.AppConfig.connectTo
	 * @type {AppConnectTo}
	 */
	self.connectTo = new AppConnectToModel();
	/**
	 * harpoonApi.AppConfig.iOSVersion
	 * @type {string}
	 */
	self.iOSVersion = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSBuild
	 * @type {string}
	 */
	self.iOSBuild = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSGoogleServices
	 * @type {string}
	 */
	self.iOSGoogleServices = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSTeamId
	 * @type {string}
	 */
	self.iOSTeamId = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSAppleId
	 * @type {string}
	 */
	self.iOSAppleId = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationPrivateKey
	 * @type {string}
	 */
	self.iOSPushNotificationPrivateKey = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationCertificate
	 * @type {string}
	 */
	self.iOSPushNotificationCertificate = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationPem
	 * @type {string}
	 */
	self.iOSPushNotificationPem = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSPushNotificationPassword
	 * @type {string}
	 */
	self.iOSPushNotificationPassword = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iOSReleaseNotes
	 * @type {string}
	 */
	self.iOSReleaseNotes = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidBuild
	 * @type {string}
	 */
	self.androidBuild = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidVersion
	 * @type {string}
	 */
	self.androidVersion = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidGoogleServices
	 * @type {string}
	 */
	self.androidGoogleServices = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidGoogleApiKey
	 * @type {string}
	 */
	self.androidGoogleApiKey = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidGoogleCloudMessagingSenderId
	 * @type {string}
	 */
	self.androidGoogleCloudMessagingSenderId = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidKey
	 * @type {string}
	 */
	self.androidKey = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidKeystore
	 * @type {string}
	 */
	self.androidKeystore = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidKeystoreConfig
	 * @type {AndroidKeystoreConfig}
	 */
	self.androidKeystoreConfig = new AndroidKeystoreConfigModel();
	/**
	 * harpoonApi.AppConfig.androidApk
	 * @type {string}
	 */
	self.androidApk = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.customerDetailsForm
	 * @type {FormSection}
	 */
	self.customerDetailsForm = ko.observableArray([]);
	/**
	 * harpoonApi.AppConfig.contact
	 * @type {Contact}
	 */
	self.contact = new ContactModel();
	/**
	 * harpoonApi.AppConfig.imgSponsorMenu
	 * @type {string}
	 */
	self.imgSponsorMenu = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.imgSponsorSplash
	 * @type {string}
	 */
	self.imgSponsorSplash = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.audioSponsorPreRollAd
	 * @type {string}
	 */
	self.audioSponsorPreRollAd = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.name
	 * @type {string}
	 */
	self.name = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.isChildConfig
	 * @type {boolean}
	 */
	self.isChildConfig = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.typeNewsFocusSource
	 * @type {string}
	 */
	self.typeNewsFocusSource = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.sponsorSplashImgDisplayTime
	 * @type {number}
	 */
	self.sponsorSplashImgDisplayTime = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.radioSessionInterval
	 * @type {number}
	 */
	self.radioSessionInterval = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.featureRadioSession
	 * @type {boolean}
	 */
	self.featureRadioSession = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.wordpressShareUrlStub
	 * @type {string}
	 */
	self.wordpressShareUrlStub = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.iosUriScheme
	 * @type {string}
	 */
	self.iosUriScheme = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.androidUriScheme
	 * @type {string}
	 */
	self.androidUriScheme = ko.observable(null);
	/**
	 * harpoonApi.AppConfig.branchConfig
	 * @type {BranchConfig}
	 */
	self.branchConfig = new BranchConfigModel();
	/**
	 * harpoonApi.AppConfig.defaultRadioStreamId
	 * @type {number}
	 */
	self.defaultRadioStreamId = ko.observable(null);
	/**
	 * Enable or disable the Triton Player in App
	 * @type {boolean}
	 */
	self.featureTritonPlayer = ko.observable(null);
	/**
	 * Property app of harpoonApi.AppConfig describes a relationship belongsTo harpoonApi.App
	 * @type {object}
	 */
	self.app = ko.observable(null);
	/**
	 * Update model instance
	 * @created 2016-09-16T16:35:56-0300
	 * @param   {object}                 data The model data object
	 * @return  {undefined}                      This method retuns `undefined`
	 */
	self.update = function(data){
		// Data must be an non-null object
		if(typeof data !== "object" || !data)
		{
			Console.error("Skipped updated because `data` is "+(typeof data));
			return;
		}
		// Update model with map
		ko.mapping.fromJS(data, self.__map__, self);
	};
	/**
	 * Turn model instance into JS object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {object}                 JS object
	 */
	self.getJSObject = function(){
		// Map to JS object using Knockout Mapping plugin
		return JSON.parse(self.getJSONString());
	};
	/**
	 * Turn model instance into JSON object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {string}                 JSON string
	 */
	self.getJSONString = function(){
		return ko.toJSON(self);
	};
	/**
	 * Method called by Knockout on ko.mapping.toJS
	 * @created 2016-09-16T19:44:35-0300
	 * @return  {string}                 JSON string
	 */
	self.toJSON = function(){
		// Get JS object for model instance
		var selfJS = ko.toJS(self);
		// Get list of relational properties for this model instance
		var relationalProperties = self.getRelationalProperties();
		// Get list of properties to ingore
		var ignoredProperties = self.getIgnoredProperties();
		// Check all the properties
		for(var propertyName in selfJS)
		{
			var isRelationalProperty = false;
			var isIgnoredProperty = false;
			// Get property
			var property = selfJS[propertyName];
			// Remove methods
			if(property === null)
			{
				delete selfJS[propertyName];
				Console.warn("Property "+propertyName+" has been removed. Value was null");
				continue;
			}
			// Remove methods
			if(typeof property === "function")
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a function");
				continue;
			}
			// Remove constant properties
			if(propertyName.match(/(__.*__)/ig))
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a constant");
				continue;
			}
			// Check if property is a relational property (base it on property name)
			for(var relationalPropertyIndex in relationalProperties)
			{
				// Get relational property name
				var relationalPropertyName = relationalProperties[relationalPropertyIndex];
				// Remove the relational property if found
				if(propertyName === relationalPropertyName)
				{
					delete selfJS[propertyName];
					isRelationalProperty = true;
					break;
				}
			}
			// Continue in case relational property has been found
			if(isRelationalProperty)
			{
				continue;
			}
			// Check if property is a ignored property (base it on property name)
			for(var ignoredPropertyIndex in ignoredProperties)
			{
				// Get ignored property name
				var ignoredPropertyName = ignoredProperties[ignoredPropertyIndex];
				// Remove the ignored property if found
				if(propertyName === ignoredPropertyName)
				{
					delete selfJS[propertyName];
					isIgnoredProperty = true;
					break;
				}
			}
			// Continue in case ignored property has been found
			if(isIgnoredProperty)
			{
				continue;
			}
		}
		return selfJS;
	};
	/**
	 * Get model properties name used for relationships with other models
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getRelationalProperties = function(){
		var relationalProperties = self.__relational_properties__;
		return relationalProperties;
	};
	/**
	 * Get model properties name to be ignored during the export
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getIgnoredProperties = function(){
		var ignored = self.__ignored_properties__;
		return ignored;
	};
	/**
	 * Get default value for model property name
	 * @created 2016-09-16T16:38:38-0300
	 * @param   {string}                 propertyName Model property name
	 * @return  {mixed}                              The default value for a specific property name
	 */
	self.getPropertyDefaultValue = function(propertyName){
		var defaults = self.__properties_default_value__;
		// Select and return the default value for a specific property name
		return defaults[propertyName];
	};
	/**
	 * Reset property default value
	 * @created 2016-09-16T16:47:46-0300
	 * @param   {string}                 propertyName The model property name
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertyToDefault = function(propertyName){
		// Property must exists
		if(self[propertyName] === undefined)
		{
			Console.error("`propertyName` ("+propertyName+") isn't referring to any property");
			return;
		}
		// Property cannot be a constant property
		if(propertyName.match(/(__.*__)/ig))
		{
			Console.debug("`propertyName` ("+propertyName+") is referring to a constant property");
			return;
		}
		// Set default value handling property an observable property
		if(ko.isObservable(self[propertyName]))
		{
			self[propertyName](self.getPropertyDefaultValue(propertyName));
		}
		// Set default value handling property as a constant property
		else
		{
			// Set default value handling property as a particular model instance
			if(typeof self[propertyName] === "object" && typeof self[propertyName].setPropertiesToDefault === "function")
			{
				self[propertyName].setPropertiesToDefault();
			}
			// Ignore functions
			else if(typeof self[propertyName] !== "function")
			{
				var defaultValue = self.getPropertyDefaultValue(propertyName);
				// Apply just if it's not undefined
				if(defaultValue !== undefined)
				{
					self[propertyName] = defaultValue;
				}
			}
		}
	};
	/**
	 * Reset all properties to default valye
	 * @created 2016-09-16T16:49:59-0300
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertiesToDefault = function(){
		var selfJS = ko.toJS(self);
		for(var propertyName in selfJS)
		{
			self.setPropertyToDefault(propertyName);
		}
	};
	// Initialization
	self.setPropertiesToDefault();
};
