/*
* @Author: Matteo Zambon
* @Date:   2016-09-16 20:21:11
* @Last Modified by:   Matteo Zambon
* @Last Modified time: 2017-09-04T15:15:04+0100
*/
/* globals ko */
"use strict";
function ConsoleModel(env)
{
	this.constructor;
	var self = this;
	self.env = env || "warn|error";
	self.log = function(data){
		if(self.env.match(/log/))
		{
			console.log(data);
		}
	};
	self.warn = function(data){
		if(self.env.match(/warn/))
		{
			console.warn(data);
		}
	};
	self.error = function(data){
		if(self.env.match(/error/))
		{
			console.error(data);
		}
	};
	self.debug = function(data){
		if(self.env.match(/debug/))
		{
			console.debug(data);
		}
	};
	self.clear = function(){
		if(typeof console.clear === "function")
		{
			console.clear();
		}
	};
}
var Console = new ConsoleModel(window.ConsoleEnv);
/**
 * UdropshipVendorProduct Model
 * @created 2016-09-16T16:35:56-0300
 * @return  {UdropshipVendorProductModel}                 Model instance
 */
var UdropshipVendorProductModel = function(){
	this.constructor;
	/**
	 * Model instance
	 * @type {UdropshipVendorProductModel}
	 */
	var self = this;
	// Properties
	// Properties - Constant
	/**
	 * UdropshipVendorProductModel map for Knockout mapping
	 * @type {object}
	 */
	self.__map__ = UdropshipVendorProductMap;
	/**
	 * Default values for model properties name
	 * @type {object}
	 */
	self.__properties_default_value__ = {"id":null,"vendorId":null,"productId":null,"priority":null,"carrierCode":null,"vendorSku":null,"vendorCost":null,"stockQty":null,"backorders":null,"shippingPrice":null,"status":null,"reservedQty":null,"availState":null,"availDate":null,"relationshipStatus":null,"udropshipVendor":null,"catalogProduct":null};
	/**
	 * List of model properties name used for relationships with other models
	 * @type {array}
	 */
	self.__relational_properties__ = ["udropshipVendor","catalogProduct"];
	/**
	 * List of model properties name to be ignored during the export
	 * @type {array}
	 */
	self.__ignored_properties__ = [];
	// Properties - Observable
	/**
	 * harpoonApi.UdropshipVendorProduct.id
	 * @type {Number}
	 */
	self.id = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.vendorId
	 * @type {Number}
	 */
	self.vendorId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.productId
	 * @type {Number}
	 */
	self.productId = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.priority
	 * @type {Number}
	 */
	self.priority = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.carrierCode
	 * @type {String}
	 */
	self.carrierCode = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.vendorSku
	 * @type {String}
	 */
	self.vendorSku = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.vendorCost
	 * @type {String}
	 */
	self.vendorCost = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.stockQty
	 * @type {String}
	 */
	self.stockQty = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.backorders
	 * @type {Number}
	 */
	self.backorders = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.shippingPrice
	 * @type {String}
	 */
	self.shippingPrice = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.status
	 * @type {Number}
	 */
	self.status = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.reservedQty
	 * @type {String}
	 */
	self.reservedQty = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.availState
	 * @type {String}
	 */
	self.availState = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.availDate
	 * @type {Date}
	 */
	self.availDate = ko.observable(null);
	/**
	 * harpoonApi.UdropshipVendorProduct.relationshipStatus
	 * @type {String}
	 */
	self.relationshipStatus = ko.observable(null);
	/**
	 * Property udropshipVendor of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.UdropshipVendor
	 * @type {object}
	 */
	self.udropshipVendor = ko.observable(null);
	/**
	 * Property catalogProduct of harpoonApi.UdropshipVendorProduct describes a relationship belongsTo harpoonApi.CatalogProduct
	 * @type {object}
	 */
	self.catalogProduct = ko.observable(null);
	/**
	 * Update model instance
	 * @created 2016-09-16T16:35:56-0300
	 * @param   {object}                 data The model data object
	 * @return  {undefined}                      This method retuns `undefined`
	 */
	self.update = function(data){
		// Data must be an non-null object
		if(typeof data !== "object" || !data)
		{
			Console.error("Skipped updated because `data` is "+(typeof data));
			return;
		}
		// Update model with map
		ko.mapping.fromJS(data, self.__map__, self);
	};
	/**
	 * Turn model instance into JS object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {object}                 JS object
	 */
	self.getJSObject = function(){
		// Map to JS object using Knockout Mapping plugin
		return JSON.parse(self.getJSONString());
	};
	/**
	 * Turn model instance into JSON object
	 * @created 2016-09-16T16:32:49-0300
	 * @return  {string}                 JSON string
	 */
	self.getJSONString = function(){
		return ko.toJSON(self);
	};
	/**
	 * Method called by Knockout on ko.mapping.toJS
	 * @created 2016-09-16T19:44:35-0300
	 * @return  {string}                 JSON string
	 */
	self.toJSON = function(){
		// Get JS object for model instance
		var selfJS = ko.toJS(self);
		// Get list of relational properties for this model instance
		var relationalProperties = self.getRelationalProperties();
		// Get list of properties to ingore
		var ignoredProperties = self.getIgnoredProperties();
		// Check all the properties
		for(var propertyName in selfJS)
		{
			var isRelationalProperty = false;
			var isIgnoredProperty = false;
			// Get property
			var property = selfJS[propertyName];
			// Remove methods
			if(property === null)
			{
				delete selfJS[propertyName];
				Console.warn("Property "+propertyName+" has been removed. Value was null");
				continue;
			}
			// Remove methods
			if(typeof property === "function")
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a function");
				continue;
			}
			// Remove constant properties
			if(propertyName.match(/(__.*__)/ig))
			{
				delete selfJS[propertyName];
				Console.debug("Property "+propertyName+" has been removed. Value was a constant");
				continue;
			}
			// Check if property is a relational property (base it on property name)
			for(var relationalPropertyIndex in relationalProperties)
			{
				// Get relational property name
				var relationalPropertyName = relationalProperties[relationalPropertyIndex];
				// Remove the relational property if found
				if(propertyName === relationalPropertyName)
				{
					delete selfJS[propertyName];
					isRelationalProperty = true;
					break;
				}
			}
			// Continue in case relational property has been found
			if(isRelationalProperty)
			{
				continue;
			}
			// Check if property is a ignored property (base it on property name)
			for(var ignoredPropertyIndex in ignoredProperties)
			{
				// Get ignored property name
				var ignoredPropertyName = ignoredProperties[ignoredPropertyIndex];
				// Remove the ignored property if found
				if(propertyName === ignoredPropertyName)
				{
					delete selfJS[propertyName];
					isIgnoredProperty = true;
					break;
				}
			}
			// Continue in case ignored property has been found
			if(isIgnoredProperty)
			{
				continue;
			}
		}
		return selfJS;
	};
	/**
	 * Get model properties name used for relationships with other models
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getRelationalProperties = function(){
		var relationalProperties = self.__relational_properties__;
		return relationalProperties;
	};
	/**
	 * Get model properties name to be ignored during the export
	 * @created 2016-09-16T16:36:49-0300
	 * @return  {[string]}                 List of model properties name
	 */
	self.getIgnoredProperties = function(){
		var ignored = self.__ignored_properties__;
		return ignored;
	};
	/**
	 * Get default value for model property name
	 * @created 2016-09-16T16:38:38-0300
	 * @param   {string}                 propertyName Model property name
	 * @return  {mixed}                              The default value for a specific property name
	 */
	self.getPropertyDefaultValue = function(propertyName){
		var defaults = self.__properties_default_value__;
		// Select and return the default value for a specific property name
		return defaults[propertyName];
	};
	/**
	 * Reset property default value
	 * @created 2016-09-16T16:47:46-0300
	 * @param   {string}                 propertyName The model property name
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertyToDefault = function(propertyName){
		// Property must exists
		if(self[propertyName] === undefined)
		{
			Console.error("`propertyName` ("+propertyName+") isn't referring to any property");
			return;
		}
		// Property cannot be a constant property
		if(propertyName.match(/(__.*__)/ig))
		{
			Console.debug("`propertyName` ("+propertyName+") is referring to a constant property");
			return;
		}
		// Set default value handling property an observable property
		if(ko.isObservable(self[propertyName]))
		{
			self[propertyName](self.getPropertyDefaultValue(propertyName));
		}
		// Set default value handling property as a constant property
		else
		{
			// Set default value handling property as a particular model instance
			if(typeof self[propertyName] === "object" && typeof self[propertyName].setPropertiesToDefault === "function")
			{
				self[propertyName].setPropertiesToDefault();
			}
			// Ignore functions
			else if(typeof self[propertyName] !== "function")
			{
				var defaultValue = self.getPropertyDefaultValue(propertyName);
				// Apply just if it's not undefined
				if(defaultValue !== undefined)
				{
					self[propertyName] = defaultValue;
				}
			}
		}
	};
	/**
	 * Reset all properties to default valye
	 * @created 2016-09-16T16:49:59-0300
	 * @return  {undefined}                              This method retuns `undefined`
	 */
	self.setPropertiesToDefault = function(){
		var selfJS = ko.toJS(self);
		for(var propertyName in selfJS)
		{
			self.setPropertyToDefault(propertyName);
		}
	};
	// Initialization
	self.setPropertiesToDefault();
};
